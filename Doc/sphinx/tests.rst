.. highlight:: rest

.. _sec-tests:

Tests
=====

Here we describe tests for the primary physics modules in the ORION2 code, with explanations for the various compile-time and run-time parameters (given in `orion2.ini`) that govern the test setup.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   tests/mhd
   tests/gravity
   tests/isothermal
   tests/sink
   tests/turbulencedriving
   tests/hydro
   tests/starangularmom
   tests/starangularmommerger
   tests/starisowind
   tests/starjet
