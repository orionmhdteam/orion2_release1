.. highlight:: rest

.. _ssec-tests-gravity:

Gravity solver test
===================


The uniform density cloud test here is for testing the gravity solver in ORION2.  The test is located in ``Test_Prob/MHD/Isothermal/UniformCloud``.

* :ref:`sssec-tests-gravity`

.. _sssec-tests-gravity:

Gravitational collapse of a uniform density cloud
-------------------------------------------------

Test description
^^^^^^^^^^^^^^^^

The test is based on the unifrom density gas cloud test created for testing ORION1 gravity solver as shown in Truelove et al. (1998) <https://ui.adsabs.harvard.edu/abs/1998ApJ...495..821T/abstract>`_.  The details and discussion of the test are described there and will not be repeated here.

In this test, the size of the entire domain is 2x2x2 region using a 64x64x64 grid.  Maximum four levels of refinement on density are allowed in the test.  The gas cloud has a radius of 0.25 and a uniform density of 2, embedded inside a density of 0.02 low density background initially.  Isothermal EOS is used and no magnetic field.  The refinement is defined by the Jeans conditions, with a Jeans number of 1/4, to prevent numerical fragmentation during gravitation collapse of the cloud.  Outflow boundaries are used.  Users can try out the peridoic boundaries conditions and the results will not be exactly the same, as expected.

How to run the test
^^^^^^^^^^^^^^^^^^^

The test setup is in the ``Test_Prob/MHD/Isothermal/UniformCloud`` subdirectory. Compile the test as described in the :ref:`sec-quickstart` instructions. This will build the ``orion2`` executable, which you can run as described under :ref:`sec-running`. Users can impose refinement grid in this test (e.g. 4 levels of refinement are used in this test.  See orion2.ini) to see how AMR computation can improve the the resolution locally.

What does success look like?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The following figures show the density slices of the gas cloud through the center udring the collapse using visualization software VisIt (<https://wci.llnl.gov/simulation/computer-codes/visit>).  In the bottom middle panel, the small gas cloud is magnified.  The bottom right panel is the same as the bottom middle panel but with the high level refinemnt grids plotted.  At the time of the last panel, there are only 3 level of refinement.  The test in fact can continue further.  Users also can easily put in a uniform magnetic field and the cloud will collapse into a disk.

.. image:: results/uniformcloud.png
	   

