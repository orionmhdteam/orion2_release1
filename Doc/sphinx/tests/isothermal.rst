.. highlight:: rest
  
.. _ssec-isothermal:

Isothermal Collapse
=======================
The test in this section applies to the module for gravity and sink particles. This test is located in the ``Test_Prob/Sink/IsothermalSphere`` directory. The available test is:

* :ref:`sssec-tests-sink-isothermalsphere`

.. _sssec-tests-sink-isothermalsphere:

Isothermal Sphere
-----------------

Test description
^^^^^^^^^^^^^^^^

This test is described in `Shu (1977, ApJ, 214, 488) <https://ui.adsabs.harvard.edu/abs/1977ApJ...214..488S/abstract>`_. The test problem follows the similarity solution described by

.. math:: x = r/(c_s t)\\
          v(x) = u(r,t)/c_s\\
          m(x) = x^2 \alpha(x-v)\\
          M(r,t) = m(x) c_s^3 t/G \\
          \rho(r,t) = \alpha(x)/(4\pi G t^2)\\
          \rho(r,0) = (c_s^2 A)/(4\pi G) r^{-2}

The initial setup consists of an isothermal T=10 K (:math:`c_s=0.18` km/s is the sound speed) sphere of gas with density profile :math:`rho(r,t=1.e29e12 s)` with basegrid :math:`32^3` and 2 levels of refinement. Please note that the initialization reads in the file tabular.h to set up the initial conditions, based on the parameters set in orion2.ini, and therefore this file must be included in the test problem compilation directory. Note that this test problem also includes the init.sink file, which places a sink particle with mass m at the center of the computational domain at initialization.

How to run the test
^^^^^^^^^^^^^^^^^^^

The test setup may be found in the ``Test_Prob/Sink/IsothermalSphere`` subdirectory. Compile the test as described in the :ref:`sec-quickstart` instructions. Specifically, ``cd`` into the test problem directory and do::

  python $ORION2_DIR/setup.py --with-orion2 --no-gui
  make -j8

This will build the ``orion2`` executable, which you can run as described under :ref:`sec-running`::

  mpirun -np X ./orion2

When the code finished running, it will have produced a series of output files, ``data.0000.3d.hdf5`` through ``data.0020.3d.hdf5``, containing the solution at times from 0 to 0.1 Myr.

What does success look like?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The density and velocity profile for the last snapshot (``data.0020.3d.hdf5``) should follow the tabulated solution included in ``tabular.py``.

A script that uses `yt <https://yt-project.org/>` to produce diagnostic plots of the output is included in the directory as ``IsothermalSphere_Plot.py``; if you have ``yt`` installed, you can run the script via::

  python IsoThermalSphere_Plot.py

and it will produce two files ``IsothermalSphere_density.png`` and ``IsothermalSphere_v.png`` containing diagnostic plots. A sample successful test, showing the density and velocity profiles along the x-axis compared to the analytical Shu solution in ``tabular.py``, is shown below. Please note that the velocity and density profiles reach a plateau and/or minumum around x=0 because of the sink particle. However, outside this region the simulation and analytical solution are in excellent agreement.

.. image:: results/IsothermalSphere_density.png
   :width: 800
   
.. image:: results/IsothermalSphere_v.png
   :width: 800

