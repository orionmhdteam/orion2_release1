.. highlight:: rest

.. _ssec-tests-hydro:

Hydrodynamics Tests
===================

The two tests in this section apply to the hydrodynamics module are located in ``Test_Prob/HD`` directory.  There are inherited from PLUTO 3.0 and modified for testing ORION2 pure hydrodynamics module.

* :ref:`sssec-tests-hd-Sedov`
* :ref:`sssec-tests-hd-Sod`

.. _sssec-tests-hd-Sedov:

Sedov-Taylor blast wave
-----------------------

Test description
^^^^^^^^^^^^^^^^

It is a typical test for hydro dynamical codes.  The test is to create a blast wave from an initially high energy but small region located at a corner in the computational grid, similar to the explosion of a bomb (Neumann 1941, Sedov 1946, Taylor 1950).  The latest discussion of this blastwave model can be found in Ro et al. (2019) <https://ui.adsabs.harvard.edu/abs/2019ApJ...878..150R/abstract>`_.  Reflective boundary conditions are employed at three of the six boundaries and the others adopt outflow boundaries.  Since the blastwave is spherically symmetric, this choice of boundaries can use a smaller computational grid.

How to run the test
^^^^^^^^^^^^^^^^^^^

The test setup is in the ``Test_Prob/HD/Sedov`` subdirectory. Compile the test as described in the :ref:`sec-quickstart` instructions. This will build the ``orion2`` executable, which you can run as described under :ref:`sec-running`. Users can impose refinement grids in this test (e.g. 2 levels of refinement are used in this test, see orion2.ini) to see how AMR computation can improve the resolution locally.

What does success look like?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A density slice after 0.05 time unit is shown in the left panel of the following figure using the visualization software VisIt (<https://wci.llnl.gov/simulation/computer-codes/visit>).  A cross section from the center of the blast wave is shown at the right panel.  Users will see the propagation of the waves in time from all the data files dumped out in the test.  Users can verifiy the results with analytical solution.

.. image:: results/Sedov.png
   
.. _sssec-tests-hd-Sod:

Sod shock test
--------------

Test description
^^^^^^^^^^^^^^^^

The Sod shock test (Sod 1978) <https://ui.adsabs.harvard.edu/abs/1978JCoPh..27....1S/abstract>`_ is commonly used to test hydrodynamical codes.  The details and discussion of the Sod shock tube test can be found in the original paper by Sod (1978) <https://ui.adsabs.harvard.edu/abs/1978JCoPh..27....1S/abstract>`_ or in Toro (1999).

A contact discontiuity is set at the middle of the tube with different densities and pressures on the two sides.  The entire tube is static initially.

How to run the test
^^^^^^^^^^^^^^^^^^^
The test setup is in the ``Test_Prob/HD/Sod`` subdirectory. The compilation and running of the test are the same as in the above Sedov-Taylor blast wave test.

What does success look like?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The density profiles at the beginning and after 0.2 time units are shown in the following figures. Users can verifiy the results with analytical solution.

.. image:: results/Sod_HD1.png
.. image:: results/Sod_HD2.png
	   
