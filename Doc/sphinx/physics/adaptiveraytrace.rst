.. highlight:: rest

.. _ssec-art:

Adaptive Ray Trace
==========================================

The adaptive ray trace is a backend for radiative transfer modules such as the Direct Radiation module. This module describes the propagation of light rays 
from sources incorporating a long characterisitics based ray tracing described in Rosen et. al. (2017) <https://ui.adsabs.harvard.edu/abs/2017JCoPh.330..924R/>`_. 
These rays originate from sources (e.g., star particles), propagate radially outward, and adaptively split when the sampling becomes too coarse; 
thereby conserving the solid angle of the rays with respect to the source to reduce computational cost. The splitting criteria is based on the HEALPix pixel sampling 
(Hierarchical Equal Area isoLatitude Pixelation) developed by Gorski et. al. 2005. The adaptive ray trace routines employed in Orion 2 are used to create and propagate 
the rays from sources.


Equations solved
^^^^^^^^^^^^^^^^
The adaptive ray trace module performs adaptive long characteristics based ray tracing for point sources and is the driver for propagating rays. It must be coupled to a method that 

solves the radiative transfer equation taking into account absorption by the fluid (i.e., we neglect scattering):

math::\bf{n}I(\nu,\bf{n} = -\kappa(\bf{n},\nu)I(\bf{n},\nu)

where :math:I(\bf{n},\nu) and :math:\kappa(\bf{n},\nu) is the direction (:math:\bf{n}) and frequency (:math:\nu) dependent intensity and absorption coefficient, respectively.

The :math:I(\bf{n},\nu) is represented by a series of rays that originate from the source and adaptively split to conserve solid angle so that at least :math:\Phi_C rays must intersect the cell. That is, rays will be split when

math::\Omega_{\rm cell}{\Omega_{\rm ray}} = \frac{N_{\rm pix}(j)}{4 \pi} \left(\frac{\Delta x}{r}\right)^2 < \Phi_C

where :math:\Omega_{\rm cell}= and :math:{\Omega_{\rm ray}= are the solid angle of the cell and ray, respecitvely,  :math:N_{\rm pix} is the Healpix pixel, :math:\Delta x is the cell width, and :math:r is the radial distance the ray has traveled from its source.

Compile-time options
^^^^^^^^^^^^^^^^^^^^
The adaptive ray tracing module acts as a back end for specific radiative transfer methods (e.g., direct radiation) and can not be used alone. Therefore, the complie-time options
will be specific to the radiative transfer module in use. Please refer to that modules documentation.

Run-time Options
^^^^^^^^^^^^^^^^
(note ALR: this will be updated as the Direct Radiation module and adaptive ray trace are cleaned up/updated for release)
The initialization file orion2.ini has several run-time options for problems that use direct radiation. These include inputs for the rays, which originate from the source, and are traced along normal directions of the centres of HEALPix pixels. 

* ``raytrace.minrays``: minimum number of rays on the HEALPix l=0 level. (default = 12)

* ``raytrace.minRayLevel``: Minimum Healpix ray level for rays to be initiated for each source. The number of pixels/rays at initialization is :math:N_{\rm pix}=N_{\rm min} \times 4^{l_{min}} where math:N_{\rm min}= ``raytrace.minrays`` and math:l_{\rm min}= ``raytrace.minRayLevel`` (default = 2)

* ``raytrace.maxRayLevel``: "Maximum" ray level required for the asynchronous ray parallel algorithim, please see Rosen et. al. (2017) <https://ui.adsabs.harvard.edu/abs/2017JCoPh.330..924R/>`_ for a detailed description of this input. Note that ``raytrace.maxRayLevel``>>``raytrace.minRayLevel``. (default = 20, it is recommended to keep this to the default value).


	





