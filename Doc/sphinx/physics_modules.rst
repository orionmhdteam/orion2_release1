.. highlight:: rest

.. _sec-physics_modules:

Physics Modules
=================

Here we describe the primary physics modules in the orion2 code, with explanations for the various compile-time and run-time (given in `orion2.ini`) parameters that affect their behavior. The modules described in this page are:

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   physics/mhd
   physics/gravity
   physics/sinks
   physics/driving
   physics/adaptiveraytrace
   physics/subgridmodels
