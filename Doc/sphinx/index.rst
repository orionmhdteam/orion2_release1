.. ORION2 documentation master file, created by
   sphinx-quickstart on Fri Apr 26 09:41:18 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ORION2's documentation!
==================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
	     
   license
   intro
   getting_started
   compiling
   quickstart
   running
   infrastructure
   physics_modules
   tools
   tests
   issues
   contributing	     

License & Conduct Agreement
============================

Below is a summary of the rules governing the use of the ORION2 MHD code as well as being a positive active member in the ORION2 collaboration. Users are responsible for following all of the applicable rules described below.

* Public Release. [Rules about public release use...]
* The code is currently housed at UC Berkeley. Repo administration is handled by PS Li. Changes to administration must be discussed among the voting members. Any changes must be publicly stated to the entire collaboration.
* As an active member, you agree to:
   * Refrain from conduct detracting from your ability or that of your colleagues to work effectively.
   * Respect the contributions of others whether personal or public.
   * Accept appropriate responsibility for your behavior.
   * Have an obligation to be familiar with this basic code of conduct.  Lack of awareness or misunderstanding of the code of conduct is not itself a defense to a charge of unethical conduct.
   * Work toward maintaining the collaborative nature of the group. 


Acknowledgments in Publications
================================
All publications resulting from the use of the ORION2 code must acknowledge the appropriate methodology papers. The user should see the bibliography found in :ref:`sec-intro` to find relevant papers to cite in their work. 

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
