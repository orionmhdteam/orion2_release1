;+
function mirror, a, left=left, right=right,$
                 top=top, bottom=bottom

;
;  Build a symmetric image out of a bidimensional array a.
;
;  left, right, bottom or top can be either:
;
;  1 = reflect the image w/ respect to the axis
;     [useful w/ reflective boundary conditions]
;
;  2 = reflect the image w/ respect to the axis midpoint
;      [useful w/ point-reflective boundary conditions]
;
;  3 = do not reflect, simply copy the array
;      [useful w/ periodic boundary conditions]
;
;
;
;
;-


ss = size(a)

ndim = ss[0]
nx   = ss[1]
ny   = ss[2]

IF (ndim EQ 1) THEN ny = 1
tmp  = a

; -----------------------------
;        bottom 
; -----------------------------

if (KEYWORD_SET(bottom)) then begin
  ng = bottom/abs(bottom)
  bottom = abs(bottom)

  scrh = fltarr(nx,2*ny)

  if (bottom eq 1) then begin

    scrh(*,ny:*)   =  tmp(*,*)
    scrh(*,0:ny-1) = ng*reverse(tmp(*,*),2)
    
  endif
  if (bottom eq 2) then begin

    scrh(*,ny:*)   =  tmp(*,*)
    scrh(*,0:ny-1) = ng*rotate(tmp(*,*),2)
  endif
  if (bottom eq 3) then begin
     scrh(*,ny:*)   =  tmp(*,*)
     scrh(*,0:ny-1) =  ng*tmp(*,*)
  endif


  nx  = nx
  ny  = 2*ny
  tmp = scrh
endif

;
; Symmetrize top
;

if (KEYWORD_SET(top)) then begin
  ng = top/abs(top)
  top = abs(top)
  scrh = fltarr(nx,2*ny)
  if (top eq 1) then begin
    scrh(*,ny:*)   = ng*reverse(tmp(*,*),2)
    scrh(*,0:ny-1) = tmp(*,*)
  endif
  if (top eq 2) then begin
    scrh(*,ny:*)   = ng*rotate(tmp(*,*),2)
    scrh(*,0:ny-1) = tmp(*,*)
  endif
  if (top eq 3) then begin
    scrh(*,ny:*)   = ng*tmp(*,*)
    scrh(*,0:ny-1) = tmp(*,*)
  endif

  nx  = nx
  ny  = 2*ny
  tmp = scrh
endif

;
; Symmetrize left
;

if (KEYWORD_SET(left)) then begin
  ng = left/abs(left)
  left = abs(left)
  scrh = fltarr(2*nx,ny)
  if (left eq 1) then begin
    scrh(nx:*,*)   = tmp(*,*)
    scrh(0:nx-1,*) = ng*reverse(tmp(*,*),1)
  endif
  if (left eq 2) then begin
    scrh(nx:*,*)   = tmp(*,*)
    scrh(0:nx-1,*) = ng*rotate(tmp(*,*),2)
  endif
  if (left eq 3) then begin
    scrh(nx:*,*)   = tmp(*,*)
    scrh(0:nx-1,*) = ng*tmp(*,*)
  endif

  nx = 2*nx
  ny = ny
  tmp = scrh
endif

;
; Symmetrize right
;

if (KEYWORD_SET(right)) then begin
  ng = right/abs(right)
  right = abs(right)
  scrh = fltarr(2*nx,ny)
  if (right eq 1) then begin
    scrh(nx:*,*)   = ng*reverse(tmp(*,*),1)
    scrh(0:nx-1,*) = tmp(*,*)
  endif
  if (right eq 2) then begin
    scrh(nx:*,*)   = ng*rotate(tmp(*,*),2)
    scrh(0:nx-1,*) = tmp(*,*)
  endif
  if (right eq 3) then begin
    scrh(nx:*,*)   = ng*tmp(*,*)
    scrh(0:nx-1,*) = tmp(*,*)
  endif

endif

return,scrh
end
