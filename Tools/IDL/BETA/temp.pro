
;
;  Set the variable k to store data for
;  different cases;
;

NCASES = 4
dir_name=['TVDLF','HLLE','ROE','Godunov','Local']

gmm    = 5./3.
gmm1   = gmm - 1.0

A_in     = 1.0^2/2.0
dV       = (x1*dx1) # dx2

tot_vol  = total(dV)

tot_rho_in = fltarr(NCASES, nlast+1)
tot_en_in  = fltarr(NCASES, nlast+1)
tot_rho    = fltarr(NCASES, nlast+1)
tot_eps    = fltarr(NCASES, nlast+1)
tot_kin    = fltarr(NCASES, nlast+1)
tot_vort   = fltarr(NCASES, nlast+1)

L_cocoon   = fltarr(NCASES, nlast+1)
R_cocoon   = fltarr(NCASES, nlast+1)

dq         = fltarr(n2)

mach    = 10.0
nu      = 3.0
dt      = 0.3

rho_flux = mach*A_in/tot_vol
en_flux  = (1.0/gmm1 + 0.5*mach*mach)*mach*A_in/tot_vol

;
; provide a first guess to cocoon size
;

i_cocoon  = 5
for n = 0, nlast   do begin


  print," Processing file ",n

  pload,out=n
  nf = 0
  ;nf = n

;
; find cocoon lenght by velocity gradient
; in the z-direction
;

;goto,SKIP

t0 = systime(1)

  j = (WHERE(v2(0,*,nf) LT 1.e-4))[0]
  L_cocoon(k,n) = x2(j)

print," #1 took ",systime(1)-t0
t0 = systime(1)

;
; find cocoon width
;

  jc    = 0
  i     = i_cocoon
  WHILE (NOT (jc EQ -1)) DO BEGIN
    jc   = (WHERE( pr(i,*,nf) GT 1.0001/gmm))[0]
    i    = i + 1
    ;print,i,jc
  ENDWHILE
  i_cocoon = i+1
  R_cocoon(k,n) = x1(i)
print," #2 took ",systime(1)-t0
t0 = systime(1)


;
;  Compute integral quantities
;

SKIP:

  ;w = curl(v1(nf),v2(nf),x1,x2,dx1,dx2)
  ; Compute curl*2.0*dr
  ; make it fast by using uniform grid in r and z

  w = (shift(v1(nf) ,0,-1) - shift(v1(nf),0,1)) -$
      (shift(v2(nf),-1, 0) - shift(v2(nf),1,0))*(dx1(1)/dx2(1))

  w(*,0)    = 0.0
  w(*,n2-1) = 0.0
  w(0,*)    = 0.0
  w(n1-1,*) = 0.0

  tot_vort(k,n)   = total(w*dV)/tot_vol
  tot_rho(k,n)    = total(rho(nf)*dV)/tot_vol
  tot_rho_in(k,n) = rho_flux*time(n)

  tot_eps(k,n) = total(dV*pr(nf))/gmm1/tot_vol
  tot_kin(k,n) = 0.5*total(dV*rho(nf)*(v1(nf)^2. + v2(nf)^2))/tot_vol
  tot_en_in(k,n) = en_flux*time(n)

print," # 3  took: ",systime(1)-t0

endfor


;tot_en = tot_eps + tot_kin


; set file names


save_dir = "/scratch/inato010/Andrea/pluto/Jet_Comparison/"
IF (k EQ 0) THEN BEGIN
  openw,50,save_dir+"tvdlf1.dat"
  openw,51,save_dir+"tvdlf2.dat"
ENDIF

IF (k EQ 1) THEN BEGIN
  openw,50,save_dir+"hlle1.dat"
  openw,51,save_dir+"hlle2.dat"
ENDIF

IF (k EQ 2) THEN BEGIN
  openw,50,save_dir+"roe1.dat"
  openw,51,save_dir+"roe2.dat"
ENDIF

IF (k EQ 3) THEN BEGIN
  openw,50,save_dir+"godunov1.dat"
  openw,51,save_dir+"godunov2.dat"
ENDIF

printf,50,"# CASE: ",dir_name[k]
printf,50,"#"
printf,50,"# time, rho, eps, kin, vort, en_flux"
printf,50,"# "

printf,51,"# CASE: ",dir_name[k]
printf,51,"#"
printf,51,"# time, L, R, Vhead"
printf,51,"# "

q     = deriv(time(0:nlast),L_cocoon(k,*))
for n = 0,nlast do begin

  printf,50,time(n),tot_rho(k,n),tot_eps(k,n),tot_kin(k,n),tot_vort(k,n),$
            tot_en_in(k,n)
  printf,51,time(n), L_cocoon(k,n),R_cocoon(k,n),q(n)

endfor
close,50
close,51

end