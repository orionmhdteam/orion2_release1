
PRO make_movie, data, begf=begf, endf=endf, finc=finc,$
                filename=filename,quality=quality

; Open an MPEG sequence:


sdata = size(data)
nframes = sdata(1)
nx      = sdata(2)
ny      = sdata(3)

print," > Image has size ",nx,ny

IF (NOT KEYWORD_SET(begf)) THEN begf = 0
IF (NOT KEYWORD_SET(endf)) THEN endf = nframes-1
IF (NOT KEYWORD_SET(finc)) THEN finc = 1
IF (NOT KEYWORD_SET(filename)) THEN filename = "movie.mpg"
IF (NOT KEYWORD_SET(quality)) THEN quality = 100

IF (endf GT nframes) THEN endf = nframes

mpegID = MPEG_OPEN([nx,ny],quality=quality)

q = bytarr(3,nx,ny)
tvlct,r,g,b,/get
ncount = 0
FOR n = begf, endf, finc DO BEGIN
  print," > putting frame ",n
  q[0,*,*] = r(data(n,*,*))
  q[1,*,*] = g(data(n,*,*))
  q[2,*,*] = b(data(n,*,*))
  MPEG_PUT, mpegID, IMAGE=q, FRAME=ncount,/color
  ncount = ncount + 1
ENDFOR


; Save the MPEG sequence in the file myMovie.mpg:
MPEG_SAVE, mpegID, FILENAME=filename

; Close the MPEG sequence:

MPEG_CLOSE, mpegID
END


nlast = 634
frame = bytarr(nlast+1, n1,n2)
for n = 0,nlast do begin
  pload,out=n,/swap_endian
  frame(n,*,*) = bytscl(b3(0))
endfor
make_movie,frame,quality=100
end
