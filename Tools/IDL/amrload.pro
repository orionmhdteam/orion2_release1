COMMON shared, rho,v1,v2,v3,b1,b2,b3,pr,tr1,fneut,fHI,fHeI,fHeII,fCI,fCII,fCIII,fCIV,fCV,fNI,fNII,fNIII,fNIV,fNV,fOI,fOII,fOIII,fOIV,fOV,fNeI,fNeII,fNeIII,fNeIV,fNeV,fSI,fSII,fSIII,fSIV,fSV,level_num,x,dx,npts, time

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;  amrload
;
;
;
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
PRO amrload, nout=nout,silent=silent

COMMON shared

; ---------------------------------------------------------
;    set display 
; ---------------------------------------------------------

IF (!D.NAME EQ 'X') THEN BEGIN
  device, retain=2, decomposed=0, true_color = 24
ENDIF

IF (NOT KEYWORD_SET(nout)) THEN nout = 0

MAX_NVAR = 64
MAX_OUT  = 8000
str      = ' '
nvar = 0
var_name = strarr(MAX_NVAR)

fdata = 'amr1d_data.out'
fpar  = 'amr1d.out'

close,/all

openr,1,fdata,_EXTRA=extra,ERROR=err
IF (NOT (err EQ 0)) THEN BEGIN
  print," ! Error: file "+fdata+" was not found "
  return
ENDIF

openr,2,fpar,ERROR=err
IF (NOT (err EQ 0)) THEN BEGIN
  print," ! Error: file amr1d.out was not found "
  return
ENDIF

; -----------------------
;   Read ascii file
; -----------------------

readf,2, format='(A10,I2)',str,levels
readf,2, format='(A10,F)' ,str,dx0
readf,2, format='(A10,I4)',str,nvar
levels = fix(levels)
nvar   = fix(nvar)
n = 0

WHILE (n LT nvar) DO BEGIN
  readf,2,format='(A)',str
  var_name(n) = strcompress(str,/remove_all)
  n = n + 1
ENDWHILE

npoint = intarr(MAX_OUT)
time   = fltarr(MAX_OUT)
dt     = fltarr(MAX_OUT)
WHILE (NOT EOF(2)) DO BEGIN
  readf, 2, n, ntime, a, b, c
  npoint(n) = fix(a)
  time(n)   = b
  dt(n)     = c
ENDWHILE

IF (nout gt n) THEN BEGIN
  print," ! File # ",strcompress(string(nout),/remove_all)," not found"
  return
ENDIF

; ----------------------------------
;  determine first and last index
;  of the data chunk to be read
; ----------------------------------

for n = 0, nout - 1 do begin
for i = 1, nvar + 2 do begin
  offset = ulong64(ulong64(npoint(n))*4)
  SKIP_LUN,1, offset
endfor
endfor

npts = npoint(nout)
data = fltarr(npoint(nout), nvar + 2)
readu,1,data

IF (NOT KEYWORD_SET (silent)) THEN BEGIN
 print," - Amr file =  ",strcompress(string(nout),/remove_all),"; t = ",$ 
           strcompress(string(time(nout),format=('(e12.2)')),/remove_all)
 print,"   nvars    =  ",strcompress(string(nvar),/remove_all)," ::",$
          "{"+var_name(0:nvar-1)+"}"
 print,"   npts     =  ",strcompress(string(npoint(nout)),/remove_all)
 print,"   Levels   =  ",strcompress(string(levels),/remove_all)
ENDIF

; ---------------------------
;        Load grid
; ---------------------------

dx = data(*,0)
x  = data(*,1)

scrh = (x(npts-1)+0.5*dx(npts-1) - (x(0)-0.5*dx(0)))/dx0
IF (NOT KEYWORD_SET (silent)) THEN BEGIN
 print,"   Base res = ",strcompress(fix(scrh))
 print,"   Equiv rs = ",strcompress(long(scrh*2^levels))
ENDIF

; ---------------------------------------
;         Load data values
; ---------------------------------------

FOR i = 0, nvar - 1 DO BEGIN
  npos = i + 2
  IF (var_name(i) EQ 'rho') THEN BEGIN
     rho = data(*,npos)
  ENDIF
  IF (var_name(i) EQ 'pr') THEN BEGIN
     pr = data(*,npos)
  ENDIF
  IF (var_name(i) EQ 'v1') THEN BEGIN
     v1 = data(*,npos)
  ENDIF
  IF (var_name(i) EQ 'v2') THEN BEGIN
     v2 = data(*,npos)
  ENDIF
  IF (var_name(i) EQ 'v3') THEN BEGIN
     v3 = data(*,npos)
  ENDIF
  IF (var_name(i) EQ 'b1') THEN BEGIN
     b1 = data(*,npos)
  ENDIF
  IF (var_name(i) EQ 'b2') THEN BEGIN
     b2 = data(*,npos)
  ENDIF
  IF (var_name(i) EQ 'b3') THEN BEGIN
     b3 = data(*,npos)
  ENDIF
  IF (var_name(i) EQ 'tr1') THEN BEGIN
     tr1 = data(*,npos)
  ENDIF
  IF (var_name(i) EQ 'fneut') THEN BEGIN
     fneut = data(*,npos)
  ENDIF
  IF (var_name(i) EQ 'fHI') THEN BEGIN
     fHI = data(*,npos)
  ENDIF
  IF (var_name(i) EQ 'fHeI') THEN BEGIN
     fHeI = data(*,npos)
  ENDIF
  IF (var_name(i) EQ 'fHeII') THEN BEGIN
     fHeII = data(*,npos)
  ENDIF
  IF (var_name(i) EQ 'fCI') THEN BEGIN
     fCI = data(*,npos)
  ENDIF
  IF (var_name(i) EQ 'fCII') THEN BEGIN
     fCII = data(*,npos)
  ENDIF
  IF (var_name(i) EQ 'fCIII') THEN BEGIN
     fCIII = data(*,npos)
  ENDIF
  IF (var_name(i) EQ 'fCIV') THEN BEGIN
     fCIV = data(*,npos)
  ENDIF
  IF (var_name(i) EQ 'fCV') THEN BEGIN
     fCV = data(*,npos)
  ENDIF
  IF (var_name(i) EQ 'fNI') THEN BEGIN
     fNI = data(*,npos)
  ENDIF
  IF (var_name(i) EQ 'fNII') THEN BEGIN
     fNII = data(*,npos)
  ENDIF
  IF (var_name(i) EQ 'fNIII') THEN BEGIN
     fNIII = data(*,npos)
  ENDIF
  IF (var_name(i) EQ 'fNIV') THEN BEGIN
     fNIV = data(*,npos)
  ENDIF
  IF (var_name(i) EQ 'fNV') THEN BEGIN
     fNV = data(*,npos)
  ENDIF
  IF (var_name(i) EQ 'fOI') THEN BEGIN
     fOI = data(*,npos)
  ENDIF
  IF (var_name(i) EQ 'fOII') THEN BEGIN
     fOII = data(*,npos)
  ENDIF
  IF (var_name(i) EQ 'fOIII') THEN BEGIN
     fOIII = data(*,npos)
  ENDIF
  IF (var_name(i) EQ 'fOIV') THEN BEGIN
     fOIV = data(*,npos)
  ENDIF
  IF (var_name(i) EQ 'fOV') THEN BEGIN
     fOV = data(*,npos)
  ENDIF
  IF (var_name(i) EQ 'fNeI') THEN BEGIN
     fNeI = data(*,npos)
  ENDIF
  IF (var_name(i) EQ 'fNeII') THEN BEGIN
     fNeII = data(*,npos)
  ENDIF
  IF (var_name(i) EQ 'fNeIII') THEN BEGIN
     fNeIII = data(*,npos)
  ENDIF
  IF (var_name(i) EQ 'fNeIV') THEN BEGIN
     fNeIV = data(*,npos)
  ENDIF
  IF (var_name(i) EQ 'fNeV') THEN BEGIN
     fNeV = data(*,npos)
  ENDIF
  IF (var_name(i) EQ 'fSI') THEN BEGIN
     fSI = data(*,npos)
  ENDIF
  IF (var_name(i) EQ 'fSII') THEN BEGIN
     fSII = data(*,npos)
  ENDIF
  IF (var_name(i) EQ 'fSIII') THEN BEGIN
     fSIII = data(*,npos)
  ENDIF
  IF (var_name(i) EQ 'fSIV') THEN BEGIN
     fSIV = data(*,npos)
  ENDIF
  IF (var_name(i) EQ 'fSV') THEN BEGIN
     fSV = data(*,npos)
  ENDIF

ENDFOR

; ---------------------------------------
;   DETERMINE LEVEL STRUCTURE BY LOOKING
;   AT THE CHANGES IN DX
; ---------------------------------------

r0  = -1
npatch = intarr(levels + 1)
xbeg   = fltarr(levels + 1, 64)
xend   = fltarr(levels + 1, 64)

FOR l = 0, levels DO npatch(l) = 0
FOR i = 0, npoint(nout) - 1 DO BEGIN
  r = alog(dx0*1.00001/dx(i))/alog(2)

  ; ----------------------------
  ;       beg patch
  ; ----------------------------

  r = round(r)
  IF (r GT r0) THEN BEGIN
    FOR l = r0 + 1, r DO BEGIN
      npatch(l) = npatch(l) + 1
      xbeg(l,npatch(l)) = x(i) - 0.5*dx(i)
      ;print, " BEG Patch,",npatch(l), " Lev: ",l
    ENDFOR
  ENDIF

  ; ----------------------------
  ;       end patch
  ; ----------------------------

  IF (r LT r0) THEN BEGIN
    FOR l = r + 1, r0 DO BEGIN
      xend(l,npatch(l)) = x(i) - 0.5*dx(i)
      ;print, " END Patch,",npatch(l), "; Lev: ",l
    ENDFOR
  ENDIF

  r0 = r
ENDFOR

i = npoint(nout) - 1
FOR l = r0, 0, -1 DO BEGIN
  xend(l,npatch(l)) = x(i) + 0.5*dx(i)
  ;print, " END Patch,",npatch(l), "; Lev: ",l
ENDFOR

; ---- print a brief summary ----

FOR l = 0, levels DO BEGIN
  ;print, "Level "+strcompress(string(l),/remove_all),$
  ;";  npatches: ",+strcompress(string(npatch(l)),/remove_all)
  FOR np = 1, npatch(l) DO BEGIN
    ;print, xbeg(l,np),xend(l,np)
  ENDFOR
ENDFOR

;
; PLOT grid structure
;

level_num = alog(dx0/dx)/alog(2.0)
;plot,x,level_num,xtitle="x",ytitle="AMR Level",charsize=2.,$
 ;    thick = 2.0

end
