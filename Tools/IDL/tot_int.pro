;+
;
; NAME:       tot_int
;
; AUTHOR:    Andrea Mignone (mignone@to.astro.it)
;
; PURPOSE:     to compute the volume integral of a two
;              dimensional array
;
; SYNTAX:       result = tot_int(q,x1,x2,dx1,dx2,/geo)
;
; DESCRIPTION: tot_int returns a scalar. Inputs are
;
;               u  = 2-D array
;               x1 = 1-D array of n1 points containing the x1 zone centers
;               x2 = 1-D array of n2 points containing the x2 zone centers
;               dx1 = 1-D array of n1 points containing mesh widths
;               dx2 = 1-D array of n2 points containing mesh widths
;
;               The geometry MUST be specified using one of
;
;                 /cart, /pol, /cyl, /sphe
;
; LAST MODIFIED: 4 / 08 / 2005
;
;-

function tot_int, q, x1, x2, dx1, dx2, $
                  cart = cart,$
                  pol  = pol,$
                  cyl  = cyl,$
                  sphe = sphe

sz = size(q)

ndim = sz[0]
n1   = sz[1]
n2   = sz[2]

dV = fltarr(n1,n2)

; ------------------------------------------------
;  define area and volume elements for the
;  different coordinate systems
; ------------------------------------------------

geometry_is_set = 0

IF (KEYWORD_SET(cart)) THEN BEGIN
  dV = dx1 # dx2
  geometry_is_set = 1
ENDIF

IF (KEYWORD_SET(pol)) THEN BEGIN
  dV = (x1*dx1) # dx2
  geometry_is_set = 1
ENDIF

IF (KEYWORD_SET(cyl)) THEN BEGIN
  dV = (x1*dx1) # dx2
  geometry_is_set = 1
ENDIF

IF (KEYWORD_SET(sphe)) THEN BEGIN
  dV = (x1*x1*dx1) # (sin(x2)*dx2)
  geometry_is_set = 1
ENDIF

IF (NOT geometry_is_set) THEN BEGIN
  print," ! Please define a geometry !"
  print,"  "
  print,"   /cart for cartesian   (x,y)"
  print,"   /pol  for polar       (r,phi)"
  print,"   /cyl  for cylindrical (r,z)"
  print,"   /cart for spherical   (r,theta)"
  return, 0
ENDIF

; ------------------------------------------------
;              Make divergence
; ------------------------------------------------

a = total(q*dV, /double)

return, (a)
end
