; ----------------------------------------------------
;
; ----------------------------------------------------

function wavenumber,n
  k=[findgen(n/2+1),-reverse(1+findgen(n/2-1))]
  return,k
end

; -----------------------------------------------------
;
;
;
; -----------------------------------------------------

function curl,b1,b2

 s  = size(b1)
 n1 = s(1)
 n2 = s(2)


 kk = complex(replicate(0.0,n1),wavenumber(n1))
 kx = kk#replicate(1.0,n2)

 kk = complex(replicate(0.0,n2),wavenumber(n2))
 ky = replicate(1.0,n1)#kk

 j = ky*b1 - kx*b2

 return,j
end


; -----------------------------------------------------
;
;   get_field:
;
;  return the potential component of a
;  a solenoidal field in 2-D on a periodic
;  domain.
;
; -----------------------------------------------------

function get_vec_pot, b1, b2, x1, x2

 s  = size(b1)
 n1 = s(1)
 n2 = s(2)

 xpos = x1#replicate(1.0,n2)
 ypos = replicate(1.0,n1)#x2

 bxtot = 0.*total(b1)/(1.d0*n1*n2)
 bytot = 0.*total(b2)/(1.d0*n1*n2)

 b1 = b1 - bxtot
 b2 = b2 - bytot


 fb1 = fft(b1,-1)
 fb2 = fft(b2,-1)

 fj = curl(fb1,fb2)


;-----------------


 kx = wavenumber(n1)#replicate(1.0,n2)

 ky = replicate(1.0,n1)#wavenumber(n2)

 k2 = kx^2 + ky^2

;-----------------

 k2(0,0) = 1.0  ; to avoid dividing by zero

 fa = fj/k2
 fa(0,0) = complex(0.0,0.0)

 a = fft(fa,1)

 return,float(a + bytot*xpos - bxtot*ypos)

end

