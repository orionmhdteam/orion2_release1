
function jetpos,a,x1,x2,nbeg,nend

  szx1 = size(x1)
  szx2 = size(x2)
  n1   = szx1(1)
  n2   = szx2(1)

  nout = nend - nbeg + 1
  pos  = fltarr(nend+1)

  for n    = nbeg, nend do begin
    w      = a(0,*,n)
;    wp     = where( abs(w/w(n2-1) - 1.) GE 0.1) ; if u use pressure
    wp     = where( abs(w) GE 1.e-2)  ; if u use tracer
    pos(n) = x2(max([max(wp),0.0]))
  endfor

  return,pos
end


