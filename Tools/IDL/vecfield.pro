;+
;
;  NAME:      vecfield
;
;  AUTHOR:    Andrea Mignone (mignone@to.astro.it)
;
;  PURPOSE:   produces a two-dimensional velocity field plot. 
;
;  SYNTAX:    vecfield, u, v, x, y[,spacing=spacing][,extra]
;
;  KEYWORDS:
;
;
;       u = the X component of the two-dimensional field. 
;           u must be a two-dimensional array.
;
;       v = the Y component of the two-dimensional field. 
;           v must be a two-dimensional array.
;
;       x = the x-coordinate
;
;       y = the y-coordinate
;
;       spacing = the spacing between one arrow and the next
;                 Higher values will produce a more "rarefied" plot
;
;       extra = all the keyword accepted by velovect, such as
;
;               /overplot, length, etc...
;
;
;  LAST MODIFIED:    Nov 30, 2006
;
;-

PRO vecfield, u, v, x, y, _EXTRA=extra, spacing=spacing

su = size(u)

nx = su(1)
ny = su(2)

IF (NOT KEYWORD_SET(spacing)) THEN spacing = 10

nxr = nx/spacing
nyr = ny/spacing

ur = congrid(u, nxr, nyr)
vr = congrid(v, nxr, nyr)
xr = congrid(x, nxr)
yr = congrid(y, nyr)

velovect, ur, vr, xr, yr, _extra=EXTRA

END

