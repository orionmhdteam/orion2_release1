cmake_host_system_information(RESULT HOSTNAME QUERY HOSTNAME)
set(CTEST_SITE "${HOSTNAME}")
set(CTEST_BUILD_NAME "${CMAKE_HOST_SYSTEM_NAME}-${CMAKE_HOST_SYSTEM_PROCESSOR}")
set(CTEST_SOURCE_DIRECTORY "./Test_Prob")
set(CTEST_BINARY_DIRECTORY "${CTEST_SOURCE_DIRECTORY}")

set(MODEL continuous)

# dashboard settings (optional)
#set(CTEST_PROJECT_NAME         "ORION2")
#set(CTEST_NIGHTLY_START_TIME   "01:00:00 AEST")
#set(CTEST_DROP_METHOD          "http")
#set(CTEST_DROP_SITE            "myTestNode.myNetwork.com")
#set(CTEST_DROP_LOCATION        "/cdash/submit.php?project=ORION2")
#set(CTEST_DROP_SITE_CDASH      TRUE)

## setttings
set(CTEST_TIMEOUT           "7200") # seconds
set($ENV{LC_MESSAGES}      "en_EN" )

# run tests
ctest_start(${MODEL} TRACK ${MODEL} )
ctest_test(BUILD  "${CTEST_BINARY_DIRECTORY}" RETURN_VALUE res)

# submit test results to CDash dashboard (optional)
#ctest_submit(RETURN_VALUE res)
