
import numpy as np

# save and return power spectrum from simulation data
## vx/y/z = numpy arrays of velocity field, should be COM corrected
## L = NDIM numpy array of the simulation domain size corresponding to 
##     vx, vy,and vz
## fout = filename to save power spectrum
## returns : power spectrum e and wavenumber k
def ek(vx,vy,vz,L,fout='pspec.out'):
    nx = vx.shape[0]
    ny = vy.shape[0] 
    nz = vz.shape[0]
    dims = nx
    print "computing power spectrum right?...", nx, ny, nz
    fvx = np.fft.fftn(vx)/(nx*ny*nz)
    fvy = np.fft.fftn(vy)/(nx*ny*nz)
    fvz = np.fft.fftn(vz)/(nx*ny*nz)
    fvtot = np.abs(fvx)**2 +  np.abs(fvy)**2 +  np.abs(fvz)**2
    print "sum of fvtot = ", np.sum(fvtot)

    kmin = np.min(1.0/L)
    kmax = np.min(0.5*dims/L)
    print "dims, kmin and kmax = ", dims, kmin, kmax

  
    kbins = np.arange(kmin,kmax,kmin)
 
    kx = np.fft.fftfreq(nx)*nx/(L[0])
    ky = np.fft.fftfreq(ny)*ny/(L[1])
    kz = np.fft.fftfreq(nz)*nz/(L[2])

    kx3d, ky3d, kz3d = np.meshgrid(kx, ky, kz, indexing="ij")
    k = np.sqrt(kx3d**2 + ky3d**2 + kz3d**2)

    whichbin = np.digitize(k.flat, kbins)
    num = kbins.size
    espec = np.zeros(num)
    for n in range(0,num-1):
        espec[n] = np.sum(fvtot.flat[whichbin==n])
    print "sum of espec before trim ", np.sum(espec)
    k = 0.5*(kbins[0:num-1] + kbins[1:num])
    espec = espec[1:num]
    print "sum of espec after trim ", np.sum(espec)

    # and write the data to file
    np.savetxt(fout,zip(k,espec))

    return espec[:-1], k[:-1]
