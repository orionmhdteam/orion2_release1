from __future__ import print_function
#from builtins import range
import os
import shutil
import sys
import string
import re
import time
import file
import ut
import menu
import screen1

###############################################################
#
#              B U I L D       P R O B L E M
#
###############################################################
def problem(work_dir, orion2_path, orion2_dir, additional_files,
            additional_flags, additional_cppflags, makefile_template,
            AUTO_UPDATE):

  HD     = 1
  RHD    = 2
  MHD    = 3
  SRMHD  = 4
  RMHD   = 5

  WITH_CHOMBO = 0
  WITH_ORION2 = 0
  WITH_AMR_1D = 0

# **********************************
#    check command line arguments
# **********************************

  for x in sys.argv:
    if (x == "--with-chombo" or x == "--with-chombo:"):
      WITH_CHOMBO = 1
    if (x == "--with-orion2" or x == "--with-orion2:"):
      WITH_ORION2 = 1
    if (x == "--with-amr1d"):
      WITH_AMR_1D = 1

# -- default makefile template --

  if (WITH_AMR_1D):
    makefile_template.append('/Src/Templates/makefile.amr1d')
  elif (WITH_CHOMBO):
    makefile_template.append('/Src/Templates/makefile.chombo')
  elif (WITH_ORION2):
    makefile_template.append('/Src/Templates/makefile.orion2')
  else:
    makefile_template.append('/Src/Templates/makefile')

# Create user-editable menu

  entries = []
  options = []
  default = []
  scrh    = []

  entries.append('PHYSICS')
  options.append(['HD','RHD','MHD','RMHD'])
  default.append('HD')

  entries.append('DIMENSIONS')
  if (WITH_AMR_1D):
    options.append(['1'])
    default.append('1')
  elif (WITH_CHOMBO):
    options.append(['2','3'])
    default.append('2')
  elif (WITH_ORION2):
    options.append(['3'])
    default.append('3')
  else:
    options.append(['1','2','3'])
    default.append('1')


  entries.append('COMPONENTS')
  options.append(['1','2','3'])
  default.append('1')

  entries.append('GEOMETRY')
  if (WITH_AMR_1D or WITH_CHOMBO):
    options.append(['CARTESIAN','CYLINDRICAL'])
    default.append('CARTESIAN')
  elif (WITH_ORION2):
    options.append('CARTESIAN')
    default.append('CARTESIAN')
  else:
    options.append(['CARTESIAN','CYLINDRICAL','POLAR','SPHERICAL'])
    default.append('CARTESIAN')

  entries.append('INCLUDE_BODY_FORCE')
  options.append(['NO','EXPLICIT'])
  default.append('NO')

  entries.append('INCLUDE_COOLING')
  options.append(['NO','POWER_LAW','TABULATED','RAYMOND','NEQ','H2_COOL'])
  default.append('NO')

  entries.append('INCLUDE_PARTICLES')
  options.append(['NO','YES'])
  default.append('NO')

  entries.append('INTERPOLATION')
  if (WITH_AMR_1D):
    options.append(['FLAT','LINEAR'])
  elif (WITH_CHOMBO or WITH_ORION2):
    options.append(['FLAT','LINEAR','LINEAR_MULTID','PARABOLIC'])
  else:
    options.append(['FLAT','LINEAR','CENO3','PARABOLIC','WENO5'])

  default.append('LINEAR')

  entries.append('TIME_STEPPING')
  if (WITH_AMR_1D or WITH_CHOMBO or WITH_ORION2):
    options.append(['HANCOCK','CHARACTERISTIC_TRACING'])
    default.append('HANCOCK')
  else:
    options.append(['EULER','HANCOCK','CHARACTERISTIC_TRACING','RK2','RK3'])
    default.append('RK2')

  entries.append('DIMENSIONAL_SPLITTING')
  if (WITH_AMR_1D):
    options.append(['YES','YES'])
    default.append('YES')
  elif (WITH_CHOMBO or WITH_ORION2):
    options.append(['NO','NO'])
    default.append('NO')
  else:
    options.append(['YES','NO'])
    default.append('YES')

  entries.append('NTRACER')
  for n in range(25): scrh.append(repr(n))
  options.append(scrh)
  default.append('0')

  scrh =[]
  entries.append('USER_DEF_PARAMETERS')
  for n in range(25): scrh.append(repr(n+1))
  options.append(scrh)
  default.append('1')

# Turbulence Driving
  if (WITH_ORION2):
    entries.append('TURBULENCE_DRIVING')
    options.append(['NO','YES','HDF5','INITIAL_ONLY'])
    default.append('NO')

# print to stdout or pout
  if (WITH_ORION2):
    entries.append('PRINT_TO_STDOUT')
    options.append(['NO','YES'])
    default.append('NO')

# flatten to first order on strong rarefactions
  if (WITH_ORION2):
    entries.append('RAREFACTION_FLATTEN')
    options.append(['NO','YES'])
    default.append('NO')

# Sink Particles
  if (WITH_ORION2):
    entries.append('SINK_PARTICLES')
    options.append(['NO','YES'])
    default.append('NO')

# Star Particles
  if (WITH_ORION2):
    entries.append('STAR_PARTICLES')
    options.append(['NO','YES'])
    default.append('NO')

# Star Type
  if (WITH_ORION2):
    entries.append('STAR_TYPE')
    options.append(['PRIMORDIAL','CONTEMPORARY'])
    default.append('CONTEMPORARY')

  if (WITH_ORION2):
    entries.append('DOEINTAVE')
    options.append(['NO','YES'])
    default.append('NO')

  if (WITH_ORION2):
    entries.append('DOEINTREFLUX')
    options.append(['NO','YES'])
    default.append('NO')

# Self Gravity
  if (WITH_ORION2):
    entries.append('SELF_GRAVITY')
    options.append(['NO','YES'])
    default.append('NO')

  if (WITH_ORION2):
    entries.append('NEWTON_COOL')
    options.append(['NO','YES'])
    default.append('NO')


  if (WITH_ORION2):
    entries.append('BARO_COOL')
    options.append(['NO','YES'])
    default.append('NO')

  if (WITH_ORION2):
    entries.append('READHDF5ICS')
    options.append(['NO','YES'])
    default.append('NO')

  if (WITH_ORION2):
    entries.append('EXTRACTICS')
    options.append(['NO','YES'])
    default.append('NO')

# --------------------------------------
#   IF DEFINITIONS.H ALREADY EXISTS,
#   READ DEFAULT FROM IT.
#    - IF IT IS AN OLD VERSION, READ WHAT
#      IS POSSIBLE.
# --------------------------------------

  if (os.path.exists(work_dir+'/definitions.h')):
    for x in entries:
      try:
        scrh = file.string_list(work_dir+'/definitions.h',x)
        tmp  = scrh[0].split()


        try:

	# -- check if the default value is still available --

          i  = entries.index(tmp[1])
          y  = options[i]
          i2 = y.index(tmp[2])
          default[entries.index(x)] = tmp[2]

        except ValueError:
          continue

      except IndexError:
        continue


#  print options[1],tmp
#  sys.exit()

#  ****  ok, call menu  ****

  if AUTO_UPDATE == 0:
    selection = ''
    selection = menu.select(entries,options,default,'Setup problem')

# -------------------------------
# DEFINE PHYSICS MODULE SUB-MENUS
# -------------------------------

  i              = entries.index('DIMENSIONS')
  dimensions     = default[i];
  i              = entries.index('PHYSICS')
  physics_module = default[i]
  i              = entries.index('GEOMETRY')
  geometry       = default[i]
  i              = entries.index('INTERPOLATION')
  interpolation  = default[i]
  i              = entries.index('TIME_STEPPING')
  time_evolution = default[i]
  i              = entries.index('DIMENSIONAL_SPLITTING')
  dimensional_splitting = default[i]

# HD

  if physics_module == 'HD':
    entries_HD = []
    options_HD = []
    default_HD = []

    entries_HD.append('EOS')
    options_HD.append(['IDEAL','ISOTHERMAL'])
    default_HD.append('IDEAL')

    entries_HD.append('THERMAL_CONDUCTION')
    options_HD.append(['NO','EXPLICIT','SUPER_TIME_STEPPING'])
    default_HD.append('NO')

    if (geometry == "POLAR" and physics_module == "HD" and dimensional_splitting == "YES"):
      entries_HD.append('FARGO_SCHEME')
      options_HD.append(['NO','YES'])
      default_HD.append('NO')


#  Read HD pre-existing HD submenu defaults, if they exists

    if (os.path.exists(work_dir+'/definitions.h')):
      for x in entries_HD:
        try:
          scrh = file.string_list(work_dir+'/definitions.h',x)
          tmp  = scrh[0].split()
          default_HD[entries_HD.index(x)] = tmp[2]
        except IndexError:
          continue


    if AUTO_UPDATE == 0:
      selection = ''
      selection = menu.select(entries_HD,options_HD,default_HD,'HD MENU')

# RHD

  if physics_module == 'RHD':
    entries_RHD = []
    options_RHD = []
    default_RHD = []

    entries_RHD.append('EOS')
    options_RHD.append(['IDEAL','TAUB'])
    default_RHD.append('IDEAL')

    entries_RHD.append('USE_FOUR_VELOCITY')
    options_RHD.append(['NO','YES'])
    default_RHD.append('NO')

#  Read RHD pre-existing RHD submenu defaults, if they exists

    if (os.path.exists(work_dir+'/definitions.h')):
      for x in entries_RHD:
        try:
          scrh = file.string_list(work_dir+'/definitions.h',x)
          tmp  = scrh[0].split()
          default_RHD[entries_RHD.index(x)] = tmp[2]
        except IndexError:
          continue


    if AUTO_UPDATE == 0:
      selection = ''
      selection = menu.select(entries_RHD,options_RHD,default_RHD,'RHD MENU')

# MHD

  if (physics_module == 'MHD'):
    entries_MHD = []
    options_MHD = []
    default_MHD = []

    entries_MHD.append('EOS')
    options_MHD.append(['IDEAL','ISOTHERMAL'])
    default_MHD.append('IDEAL')

    entries_MHD.append('ENTROPY_SWITCH')
    options_MHD.append(['NO','YES'])
    default_MHD.append('NO')

    entries_MHD.append('MHD_FORMULATION')
    options_MHD.append(['NONE','EIGHT_WAVES','FLUX_CT'])
    default_MHD.append('FLUX_CT')

#    entries_MHD.append('INCLUDE_DIVB_DIFFUSION')
#    options_MHD.append(['NO','YES'])
#    default_MHD.append('NO')

    entries_MHD.append('INCLUDE_BACKGROUND_FIELD')
    options_MHD.append(['NO','YES'])
    default_MHD.append('NO')

    entries_MHD.append('THERMAL_CONDUCTION')
    if (WITH_CHOMBO == 1 or WITH_ORION2 == 1):
      options_MHD.append(['NO'])
    else:
      options_MHD.append(['NO','EXPLICIT','SUPER_TIME_STEPPING'])

    default_MHD.append('NO')

#  Read MHD pre-existing MHD submenu defaults, if they exists

    if (os.path.exists(work_dir+'/definitions.h')):
      for x in entries_MHD:
        try:
          scrh = file.string_list(work_dir+'/definitions.h',x)
          tmp  = scrh[0].split()
          default_MHD[entries_MHD.index(x)] = tmp[2]
        except IndexError:
          continue


    if AUTO_UPDATE == 0:
      selection = ''
      if (physics_module == 'MHD'):
        selection = menu.select(entries_MHD,options_MHD,default_MHD,'MHD MENU')


# RMHD

  if physics_module == 'RMHD':
    entries_RMHD = []
    options_RMHD = []
    default_RMHD = []

    entries_RMHD.append('EOS')
    options_RMHD.append(['IDEAL','TAUB'])
    default_RMHD.append('IDEAL')

    entries_RMHD.append('MHD_FORMULATION')
    options_RMHD.append(['NONE','EIGHT_WAVES','FLUX_CT'])
    default_RMHD.append('NONE')

#    entries_RMHD.append('INCLUDE_DIVB_DIFFUSION')
#    options_RMHD.append(['NO','YES'])
#    default_RMHD.append('NO')

#  Read RMHD pre-existing RMHD submenu defaults, if they exists

    if (os.path.exists(work_dir+'/definitions.h')):
      for x in entries_RMHD:
        try:
          scrh = file.string_list(work_dir+'/definitions.h',x)
          tmp  = scrh[0].split()
          default_RMHD[entries_RMHD.index(x)] = tmp[2]
        except IndexError:
          continue

    if AUTO_UPDATE == 0:
      selection = ''
      if (physics_module == 'RMHD'):
        selection = menu.select(entries_RMHD,options_RMHD,default_RMHD,'RMHD MENU')

# --------------------------------------------------------------------------------------
#                       NOW INPUT USER_DEF PARAMETERS:
#
#  Read them from definitions.h if the file exists; or assign  'SCRH' otherwise.
# --------------------------------------------------------------------------------------

  i    = entries.index('USER_DEF_PARAMETERS')
  npar = int(default[i])
  u_def_par = []
  if (os.path.exists(work_dir+'/definitions.h')):
    scrh = file.word_find(work_dir+'/definitions.h','parameters')
    k0   = scrh[0] + 2
    scrh = file.read_lines(work_dir+'/definitions.h', k0, k0 + npar)
    for n in range(npar):
      try:
        x = scrh[n].split()
        if (x[1] != "Default,"):
          u_def_par.append(x[1])
        else:
          u_def_par.append('SCRH')
      except IndexError:
        u_def_par.append('SCRH')
#        scrh[n + 1] = '#define SCRH  x'


  else:
    for n in range(npar):
      u_def_par.append('SCRH')


  if AUTO_UPDATE == 0:
    menu.put(int(default[i]),u_def_par,'USER DEF SECTION')

# -----------------------------------------------------
#   Create the list 'tmp' that will be used to write
#   the new definitions.h header file
# -----------------------------------------------------

  tmp = []
  for x in entries:
    i = entries.index(x)
    y = default[i]

    tmp.append('#define    '+x+'   '+y+'\n')


  tmp.append('\n/* -- physics dependent declarations -- */\n\n');

  if (physics_module == 'MHD' or physics_module == 'SRMHD'):
    for x in entries_MHD:
      i = entries_MHD.index(x)
      tmp.append('#define    '+x+'   '+default_MHD[i]+'\n')

  if physics_module == 'HD':
    for x in entries_HD:
      i = entries_HD.index(x)
      tmp.append('#define    '+x+'   '+default_HD[i]+'\n')

  if physics_module == 'RHD':
    for x in entries_RHD:
      i = entries_RHD.index(x)
      tmp.append('#define    '+x+'   '+default_RHD[i]+'\n')

  if physics_module == 'RMHD':
    for x in entries_RMHD:
      i = entries_RMHD.index(x)
      tmp.append('#define    '+x+'   '+default_RMHD[i]+'\n')


  tmp.append('\n/* -- pointers to user-def parameters -- */\n\n');

  for x in u_def_par:
    i = u_def_par.index(x)
    tmp.append('#define  '+x+'   '+repr(i)+'\n')

    # !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    #   In this section, also, udpate the orion2.ini
    #   initialization file by appending the correct
    #   sequence of user defined parameters
    # !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


    orionini = work_dir+"/orion2.ini"

    scrh = file.string_find (orionini, 'Parameters')
    ipos = scrh[0] + 2 + i    # start writing udef par. at this point in orion2.ini
    try:
      scrh = file.word_find (orionini, x)
      if len(scrh) == 0:

        #   user def parameter does not exist in orion2.ini:
        #   insert it

        file.insert (orionini, x+'    0\n', ipos)

      else:

        if (scrh[0] != ipos):

        #   user def parameter exists, but it is not where it
        #   should be; swap lines
          file.swap_lines(orionini, ipos, scrh[0])

    except IndexError:
      print(" Index error occured")

  # delete all other lines in orion2.ini

  file.delete_lines(orionini, ipos + 1, ipos + 256)

# ----------------------------------------------
#  check dependencies for conditional inclusion
#  of additional object files; this is useful
#  for makefile creation
# ----------------------------------------------

# additional files will be compiled
# depending on the user's choice of
# time integration scheme

# the next line remove all elements from additional_files
# and orion2_path so the list can be built from scratch

#  for x in additional_files: additional_files.remove(x)
#  for x in orion2_path      : orion2_path.remove(x)

  additional_files[:] = []
  additional_flags[:] = []
  additional_cppflags[:] = []
  orion2_path[:] = []

  #  *****************************
  #       physics module
  #  *****************************

  sts_flag = 0 # if changed to 1, include the super-time-stepping driver

  if (physics_module == 'HD'):
    orion2_path.append('HD/')
    entries_MOD = entries_HD
    default_MOD = default_HD

  elif (physics_module == 'RHD'):
    orion2_path.append('RHD/')
    entries_MOD = entries_RHD
    default_MOD = default_RHD
  elif (physics_module == 'MHD'):
    orion2_path.append('MHD/')
    entries_MOD = entries_MHD
    default_MOD = default_MHD
    n = entries_MOD.index('MHD_FORMULATION')
    if (default_MOD[n] == 'FLUX_CT'):
      orion2_path.append('MHD/FCT/')

  elif (physics_module == 'RMHD'):
    orion2_path.append('RMHD/')
    entries_MOD = entries_RMHD
    default_MOD = default_RMHD
    n = entries_MOD.index('MHD_FORMULATION')
    if (default_MOD[n] == 'FLUX_CT'):
      orion2_path.append('MHD/FCT/')

  #  *****************************
  #        Interpolation
  #  *****************************

  if   (interpolation == 'LINEAR'):
    additional_files.append('slopes_plm.o')
  if   (interpolation == 'LINEAR_MULTID'):
    additional_files.append('slopes_plm_multid.o')
  elif (interpolation == 'FLAT'):
    additional_files.append('slopes_flat.o')
  elif (interpolation == 'PARABOLIC'):
    additional_files.append('slopes_ppm.o')
  elif (interpolation == 'WENO5'):
    additional_files.append('slopes_flat.o')
    additional_files.append('rhs_weno5.o')
  elif (interpolation == 'CENO3'):
    additional_files.append('slopes_ceno3.o')

  n = entries.index('TURBULENCE_DRIVING')
  if (default[n] == 'YES' or default[n] == 'HDF5' or default[n] == 'INITIAL_ONLY'):
    additional_cppflags.append('-DPUMP')

  n = entries.index('SELF_GRAVITY')
  if (default[n] == 'YES'):
    additional_cppflags.append('-DGRAVITY')

  #  *****************************
  #       split / unsplit
  #  *****************************

  if (dimensional_splitting == 'NO'):

    if (time_evolution == "CHARACTERISTIC_TRACING" or
        time_evolution == "HANCOCK"):
#      if (physics_module == "RMHD"):
#        if (not (WITH_CHOMBO or WITH_ORION2)): additional_files.append('unsplit_ctu_mhd.o')
#      else:
        if (not (WITH_CHOMBO or WITH_ORION2)): additional_files.append('unsplit_ctu.o')

    else:
      additional_files.append('unsplit.o')
  else:
      additional_files.append('sweep.o')

  if (time_evolution == 'HANCOCK'):
    additional_files.append('hancock.o')
  elif (time_evolution == 'CHARACTERISTIC_TRACING'):
    additional_files.append('char_tracing.o')

  #  *****************************
  #        Body Forces
  #  *****************************

  n = entries.index('INCLUDE_BODY_FORCE')
  if (default[n] != 'NO'):
    additional_files.append('body_force.o')
    additional_files.append('add_body_force.o')


  #  *****************************
  #          Cooling
  #  *****************************

  n = entries.index('INCLUDE_COOLING')

  if (default[n] == 'POWER_LAW'):
    orion2_path.append('Cooling/Power_Law/')
  elif (default[n] == 'TABULATED'):
    orion2_path.append('Cooling/Tab/')
    additional_files.append('cooling_source.o')
    additional_files.append('cooling_ode_solver.o')
  elif (default[n] == 'RAYMOND'):
    orion2_path.append('Cooling/Raymond/')
    additional_files.append('cooling_source.o')
    additional_files.append('cooling_ode_solver.o')
  elif (default[n] == 'NEQ'):
    orion2_path.append('Cooling/NEQ/')
    additional_files.append('cooling_source.o')
    additional_files.append('cooling_ode_solver.o')
#    additional_flags.append("-D'orion2_dir=\""+orion2_dir+"\"'\n")
  elif (default[n] == 'H2_COOL'):
    orion2_path.append('Cooling/H2_COOL/')
    additional_files.append('cooling_source.o')
    additional_files.append('cooling_ode_solver.o')


  #  *****************************
  #    Need super time stepping ?
  #  *****************************

  if (sts_flag == 1): additional_files.append('sts.o')

  #  *****************************
  #           Particles
  #  *****************************

  n = entries.index('INCLUDE_PARTICLES')
  if (default[n] == 'YES'):
    orion2_path.append('Particles/')


  #  *****************************
  #           Sink Particles
  #  *****************************

  n = entries.index('SINK_PARTICLES')
  if (default[n] == 'YES'):
    orion2_path.append('Sink_Particles/')
    additional_cppflags.append('-I$(SRC)/')
    additional_cppflags.append('-I$(SRC)/Sink_Particles')
    additional_cppflags.append('-DSINKPARTICLE')
    additional_cppflags.append('-DSINKID')


  #  *****************************
  #           Star Particles
  #  *****************************

  n = entries.index('STAR_PARTICLES')
  if (default[n] == 'YES'):
    # sink particles must be on for star particles to be on.
    if default[entries.index('SINK_PARTICLES')] != 'YES':
      orion2_path.append('Sink_Particles/')
      additional_cppflags.append('-I$(SRC)/')
      additional_cppflags.append('-I$(SRC)/Sink_Particles')
      additional_cppflags.append('-DSINKPARTICLE')
      additional_cppflags.append('-DSINKID')
    orion2_path.append('Star_Particles/')
    additional_cppflags.append('-I$(SRC)/')
    additional_cppflags.append('-I$(SRC)/Star_Particles')
    additional_cppflags.append('-DSTARPARTICLE')

  n = entries.index('STAR_TYPE')
  if(default[n] == 'PRIMORDIAL'):
    additional_cppflags.append('-DPRIMORDIALSTAR')

# ----------------------------------------
#  DEFINE ALL NON-USER FRIENDLY CONSTANTS
# ----------------------------------------

  tmp.append('\n/* -- supplementary constants (user editable) -- */ \n\n')

  no_us_fr = []

#  no_us_fr.append('#define  PRECISION             DOUBLE\n')
  no_us_fr.append('#define  INITIAL_SMOOTHING     NO\n')
#  no_us_fr.append('#define  VOLUME_GRID           NO\n')
  no_us_fr.append('#define  WARNING_MESSAGES      NO\n')
  no_us_fr.append('#define  PRINT_TO_FILE         NO\n')
  no_us_fr.append('#define  SHOCK_FLATTENING      NO\n')
  no_us_fr.append('#define  ARTIFICIAL_VISCOSITY  NO\n')
  if (interpolation != "WENO5"):
    no_us_fr.append('#define  CHAR_LIMITING         NO\n')
    no_us_fr.append('#define  LIMITER               DEFAULT\n')

# add geometry-dependent switches

#  if (geometry == "POLAR" and physics_module == "HD"):
#    no_us_fr.append('#define  FARGO_SCHEME          NO\n')

# add flux ct switches for MHD

  if (physics_module == 'MHD'):
    i        = entries_MHD.index('MHD_FORMULATION')
    mhd_form = default_MHD[i]
    if (mhd_form == "FLUX_CT"):
      no_us_fr.append('#define  CT_EMF_AVERAGE     ARITHMETIC\n')
      no_us_fr.append('#define  CT_EN_CORRECTION   NO\n')
      no_us_fr.append('#define  CT_VEC_POT_INIT    YES\n')
      no_us_fr.append('#define  COMPUTE_DIVB       NO\n')

# add flux ct switches for RMHD

  if (physics_module == 'RMHD'):
    i        = entries_RMHD.index('MHD_FORMULATION')
    mhd_form = default_RMHD[i]
    if (mhd_form == "FLUX_CT"):
      no_us_fr.append('#define  CT_EMF_AVERAGE     ARITHMETIC\n')
      no_us_fr.append('#define  CT_EN_CORRECTION   NO\n')
      no_us_fr.append('#define  CT_VEC_POT_INIT    YES\n')


  if (time_evolution == 'HANCOCK'):
    if (physics_module == 'RMHD'):
      no_us_fr.append('#define  PRIMITIVE_HANCOCK  NO\n')
    else:
      no_us_fr.append('#define  PRIMITIVE_HANCOCK  YES\n')


  if (sts_flag == 1):
    no_us_fr.append('#define  STS_nu        0.7\n')


# ------------------------------------------------
#  Read pre-existing non-user-editable defaults;
#  Use the default value if they exist
# ------------------------------------------------

  if (os.path.exists(work_dir+'/definitions.h')):
    for x in no_us_fr:
      try:
        xl   = x.split()
        scrh = file.string_list(work_dir+'/definitions.h',xl[1])
        no_us_fr[no_us_fr.index(x)] = scrh[0]
      except IndexError:
        continue



# add elements of no_us_fr to tmp

  for x in no_us_fr:
    tmp.append(x)

# -- create definitions.h --

  file.create_file(work_dir+'/definitions.h',tmp)

# -- if chombo is used, modify the number
#    of dimensions in Make.defs.local     --

#  if (WITH_CHOMBO):
#    mk_local = orion2_dir+"/Lib/Chombo/lib/mk/Make.defs.local"
#    scrh = file.word_find(mk_local,'DIM')
#    ipos = scrh[1]
#    file.replace_line (mk_local, 'DIM    = '+dimensions+'\n', ipos)


# check for div.B diffusion operator

#  if (physics_module == 'MHD' or physics_module == 'SRMHD' or physics_module == 'RMHD'):

#    n = entries_MOD.index('INCLUDE_DIVB_DIFFUSION')
#    if (default_MOD[n] == 'YES'):
#      additional_files.append('mhd_diffusion.o')

  return
###############################################################
#
#
#                  BUILD MAKEFILE
#
#
# - if no makefile is present, build one from scratch using
#   the templates in Src/Templates
#
# - if AUTO_UPDATE = 0 build a new one anyway, overwriting the
#   old one (if present)
#
# - if AUTO_UPDATE = 1 attempt to automatically update the
#   existing makefile
#
###############################################################
def makefile(work_dir, orion2_path, orion2_dir, additional_files,
             additional_flags, additional_cppflags, makefile_template,
             AUTO_UPDATE):


 screen1.dumpclear ("Building makefile...\n")
 this_makefile = work_dir+'/makefile'

# not all makefiles need the ARCH variable...

 need_ARCH = 1
 WITH_CHOMBO = 0
 WITH_ORION2 = 0

# if CHOMBO is required, change dir to Lib/Chombo/lib,
# > make vars to generate a list of all the variables
# needed by CHOMBO makefile and copy the list
# to make.vars in local working directory.
# This file will be used later by ORION2 makefile.

 for x in sys.argv:
   if (x == "--with-chombo" or x == "--with-chombo:"):
     WITH_CHOMBO = 1
     need_ARCH = 0
   if (x == "--with-orion2" or x == "--with-orion2:"):
     WITH_ORION2 = 1
     need_ARCH = 0

   # get the number of DIMENSIONS from definitions.h

     scrh = file.string_list (work_dir+"/definitions.h", "DIMENSIONS")
     scrh = scrh[0].split()

  # build chombo configuration string

     chombo_config_string = 'DIM='+scrh[2]
     if (x == '--with-chombo:'):
       i = sys.argv.index(x) + 1
       for y in sys.argv[i:]: chombo_config_string += ' '+y
     if (x == '--with-orion2:'):
       i = sys.argv.index(x) + 1
       for y in sys.argv[i:]: chombo_config_string += ' '+y

     os.chdir(orion2_dir+"/Lib/Chombo3.2/lib")
     os.system("make "+chombo_config_string+" vars > make.vars\n")
     os.system("mv make.vars "+work_dir+"\n")


# create a new makefile if not present or update an existing one

 if (need_ARCH):
   if (os.path.exists(this_makefile)): scrh = file.string_list(this_makefile,'ARCH')
   if (AUTO_UPDATE == 0) or (not os.path.exists(this_makefile)) or (len(scrh) == 0):

# define architecture

     entries = os.listdir(orion2_dir + '/Config')
     entries.sort()
     machine = menu.select0(entries,"Change makefile",'')  # call menu to select config. file
     arch    = 'ARCH         = '+ machine + '\n'
   else:                           # try to update makefile automatically
     arch = scrh[0]


# copy template

 shutil.copy(orion2_dir + makefile_template[0],this_makefile)

# write main ORION2 dir

 scrh = file.word_find(this_makefile,'ORION2_DIR')
 ipos = scrh[0]
 file.replace_line (this_makefile, 'ORION2_DIR    = '+orion2_dir+'\n', ipos)

#  write architecture

 if (need_ARCH):
   scrh = file.word_find(this_makefile,'ARCH')
   ipos = scrh[0]
   file.replace_line (this_makefile, arch, ipos)

#  Write additional objects to makefile

 scrh = file.word_find(this_makefile,'Additional_object_files_here')
 ipos = scrh[0] + 2

 for x in additional_files:
   file.insert(this_makefile, 'OBJ += '+x + '\n', ipos)
   ipos = ipos + 1

# add included makefile; useful for header dependences

 for x in orion2_path:
   file.insert(this_makefile, 'include $(SRC)/' + x + 'makefile' + '\n',ipos)
   ipos = ipos + 1

#  Write additional flags for C++ compiler

 for x in additional_cppflags:
   file.insert(this_makefile, 'CPPFLAGS += '+x+'\n', ipos)
   ipos = ipos + 1

#  Write additional flags for C compiler

 for x in additional_flags:
   file.insert(this_makefile, 'CFLAGS += '+x+'\n', ipos)
   ipos = ipos + 1

# Check for libpng

 if (WITH_CHOMBO == 0 or WITH_ORION2 == 0):
   if (os.path.exists("sysconf.out")):
     scrh = file.string_find ("sysconf.out", "LIBPNG")
     line = file.read_lines("sysconf.out", scrh[0], scrh[0])
     line_list = line[0].split()
     if (line_list[2] == 'YES'):
       file.insert(this_makefile, 'LDFLAGS += -lpng\n', ipos)
       ipos = ipos + 1
       file.insert(this_makefile, 'CFLAGS += -DHAVE_LIBPNG\n', ipos)

 screen1.dumpclear (" ")
 return
