import h5py
import numpy as np
from optparse import OptionParser
import sys

tracerN = 1

def copy_groups(keyin, keyout):
    for attr in fin[keyin].attrs.keys():
        try:
            fout.create_group(keyin.replace(keyin,keyout))
        except:
            pass # the group is already created
    #
    next_keys = fin[keyin].keys()
    for next_key in next_keys:
        next = next_key
        if keyin != '/': next = keyin+'/'+next_key
        # if the key is a group, recursivley burrow into it.
        if type(fin[next]) == h5py.highlevel.Group: copy_groups(next,next.replace(keyin,keyout))

# recursively copy all hdf5 attributes in the file hierarchy
def copy_attrs(keyin, keyout):
    for attr in fin[keyin].attrs.keys():
        dtype = h5py.h5a.open(fin[keyin].id,attr).dtype
        if dtype.isbuiltin: 
            fout[keyin.replace(keyin,keyout)].attrs[attr] = dtype.type(fin[keyin].attrs[attr])
        else:
            fout[keyin.replace(keyin,keyout)].attrs[attr] = np.array([fin[keyin].attrs[attr]], dtype=dtype)
    #
    next_keys = fin[keyin].keys()
    for next_key in next_keys:
        next = next_key
        if keyin != '/': next = keyin+'/'+next_key
        # if the key is a group, recursivley burrow into it.
        if type(fin[next]) == h5py.highlevel.Group: copy_attrs(next,next.replace(keyin,keyout))

# coarsen the boxes, *:datatype=0, and *:offsets=0 datasets 
# on each level and write to fout.
def copy_datasets(levelin, levelout):
    box_idt = np.dtype([('lo_i', np.int32), ('lo_j', np.int32), ('lo_k', np.int32), 
                        ('hi_i', np.int32), ('hi_j', np.int32), ('hi_k', np.int32)])

    fout[levelout].attrs['dx'] = fin[levelin].attrs['dx']
    # coarsen the prob domain
    dom = fin[levelin].attrs['prob_domain']
    ndim = len(dom)/2
    fout[levelout].attrs['prob_domain'] = np.array([tuple(dom)], dtype=box_idt)[0]
    # copy over the processor assignments
    fout[levelout]['Processors'] = fin[levelin]['Processors'].value
    # coarsen the boxes
    boxes = fin[levelin]['boxes'].value
    ncells = 0
    ncellsFlx = 0
    for ibox in range(0,len(boxes)):
        box = np.array(list(boxes[ibox]),dtype=np.int)
        boxes[ibox] = tuple(box)
        # count the number of coarse cells and flux box enteries in this box
        ncells += np.product(box[ndim:2*ndim] - box[0:ndim] + 1)
        for i in range(0,ndim): 
            stagger = np.array([0,0,0],dtype=np.int)
            stagger[i] += 1
            ncellsFlx += np.product(box[ndim:2*ndim] - box[0:ndim] + 1 + stagger)
    fout[levelout]['boxes'] = boxes
    # loop over the various datas on this levelin
    for key in fin[levelin]:
        if not ':datatype=0' in key: continue
        if type(fin[levelin][key]) != h5py.highlevel.Dataset: continue
        dset_name = key.split(':')[0]
        if dset_name == 'face_data': # is there a more general way to distinguish flux boxes from cell-center data?
            # this is a flux box
            # create a dataset and offset big enough for the data
            fout[levelout].create_dataset(dset_name+':datatype=0',[ncellsFlx],dtype=np.float)
            fout[levelout].create_dataset(dset_name+':offsets=0',[len(boxes)+1],dtype=np.int64)
            # write the begining data offset
            offsets = fin[levelin][dset_name+':offsets=0'].value
            coarse_offset = 0
            for ibox in range(0,len(offsets)-1):
                # read and reshape the current box data
                box = np.array(list(boxes[ibox]),dtype=np.int)
                # read the data
                count = 0
                fout[levelout][dset_name+':offsets=0'][ibox] = coarse_offset
                for i in range(0,ndim):
                    # read and reshape the current data box
                    stagger = np.array([0,0,0],dtype=np.int)
                    stagger[i] += 1
                    box = np.array(list(boxes[ibox]),dtype=np.int)
                    shape = box[ndim:2*ndim] - box[0:ndim] + 1 + stagger
                    n = np.product(shape)
                    data = fin[levelin][dset_name+':datatype=0'][offsets[ibox]+count:offsets[ibox]+count+n].reshape(shape,order='F')
                    count += n

                    # flatten and write the data
                    fout[levelout][dset_name+':datatype=0'][coarse_offset:coarse_offset+data.size] = data.T.flatten()[:]
                    coarse_offset += data.size
                # write the end data offset
                fout[levelout][dset_name+':offsets=0'][ibox+1] = coarse_offset
        else:
            # this is a regular dataset
            # create a dataset and offset big enough for the data
            ncomps = fin[levelin+'/'+dset_name+'_attributes'].attrs['comps']
            fout[levelout].create_dataset(dset_name+':datatype=0',[ncells*ncomps],dtype=np.float)
            fout[levelout].create_dataset(dset_name+':offsets=0',[len(boxes)+1],dtype=np.int64)
            # read the offsets into the 1D array
            offsets = fin[levelin][dset_name+':offsets=0'].value
            coarse_offset = 0
            for ibox in range(0,len(offsets)-1):
                # read and reshape the current box data
                box = np.array(list(boxes[ibox]),dtype=np.int)
                shape = box[ndim:2*ndim] - box[0:ndim] + 1
                shape = list(shape) + [ncomps]
                data = fin[levelin][dset_name+':datatype=0'][offsets[ibox]:offsets[ibox+1]].reshape(shape,order='F')
                # flatten and write the data
                fout[levelout][dset_name+':datatype=0'][coarse_offset:coarse_offset+data.size] = data.T.flatten()[:]
                # write the data offsets
                fout[levelout][dset_name+':offsets=0'][ibox] = coarse_offset
                fout[levelout][dset_name+':offsets=0'][ibox+1] = coarse_offset+data.size
                coarse_offset += data.size

###################
# input parameters, read from command line
###################
parser = OptionParser()
parser.add_option('--infile', dest='infile', 
                  help='input file name')
parser.add_option('--outfile', dest='outfile', 
                  help='output file name')
parser.add_option('--inlevel', dest='inlevel', 
                  help='level to be copied from infile.  Default=0', default=0)
parser.add_option('--outlevel', dest='outlevel', 
                  help='level added to outfile.  Default=1', default=1)
(options, args) = parser.parse_args()
error = False
if options.infile == None: 
    error = True
    print "Error, no --infile option specified"
if options.outfile == None: 
    error = True
    print "Error, no --outfile option specified"
if error: sys.exit(-1)
###################
# begin computation
###################
inlevel= int(options.inlevel)
outlevel=int(options.outlevel)
fin=h5py.File(options.infile,'r')
fout=h5py.File(options.outfile,'a')
copy_groups('level_'+str(inlevel),'level_'+str(outlevel))
copy_attrs('level_'+str(inlevel),'level_'+str(outlevel))
# copy the data
copy_datasets('level_'+str(inlevel),'level_'+str(outlevel))
# set a regrid interval for the new level
fout['/'].attrs['regrid_interval_'+str(outlevel-1)] = 2
# set the new max level
fout['/'].attrs['max_level'] = outlevel
fout['/'].attrs['num_levels'] = outlevel+1
# for some reason all levels < max_level have a redundant num_compnents 
# and component names attributes that chombo must have
num_comps = fout['/'].attrs['num_components']
for i in range(0,outlevel):
    fout['level_'+str(i)].attrs['num_components'] = num_comps
    for comp in range(0,num_comps):
        fout['level_'+str(i)].attrs['component_'+str(comp)] = fout['/'].attrs['component_'+str(comp)]
fin.close()
fout.close()
