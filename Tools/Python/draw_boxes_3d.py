''''
Visualize the ORION2 grid-processor map in 3D!
Author: Brandt Gaches
Purpose: Draw the orion2 grid boxes for each of the processors in 3D. The code provides for 6 different colors to distinguish between levels. Boxes on higher levels will be drawn with finer lines. It is not recommended to use this for many processors - although the code will certainly do so (The image may just be huge and cluttered!). It has been tested with all the different refinement criteria for grids up to 256^3 and 24 processors. The plot is partially interactable in 3D - you can rotate and zoom easily. However, the more boxes/processors the slower this will be. You can rotate around the boxes for each processor separately.
Use: Example call - python draw_boxes --fname=data.00001.3d.hdf5
This will draw all the boxes in the hdf5 dataset. Note: ORION2 boxes can have negative indices. I.e. a 64^3 box will have indices that go from -32 to 31.
'''

import h5py
import matplotlib.pyplot as plt
import matplotlib
from matplotlib.patches import Rectangle
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import sys
from optparse import OptionParser

def draw_box(axes, rr, level, bc, color):
	xl = bc[0]; yl = bc[1]; zl = bc[2]; xh = bc[3]; yh = bc[4]; zh = bc[5]
	xl /= rr**level; yl /= rr**level; zl /= rr**level; xh /= rr**level; yh /= rr**level; zh /= rr**level
	axes.plot((xl, xh), (yl, yl), (zl,zl), '-', color=color, linewidth=4./2**level)
	axes.plot((xl, xl), (yl, yh), (zl,zl), '-', color=color, linewidth=4./2**level)
	axes.plot((xl, xl), (yl, yl), (zl,zh), '-', color=color, linewidth=4./2**level)
	axes.plot((xl, xh), (yl, yl), (zh,zh), '-', color=color, linewidth=4./2**level)
	axes.plot((xh, xh), (yl, yl), (zl,zh), '-', color=color, linewidth=4./2**level)
	axes.plot((xh, xh), (yl, yh), (zl,zl), '-', color=color, linewidth=4./2**level)
	axes.plot((xh, xh), (yl, yh), (zh,zh), '-', color=color, linewidth=4./2**level)
	axes.plot((xl, xl), (yl, yh), (zh,zh), '-', color=color, linewidth=4./2**level)
	axes.plot((xl, xh), (yh, yh), (zl,zl), '-', color=color, linewidth=4./2**level)
	axes.plot((xl, xh), (yh, yh), (zh,zh), '-', color=color, linewidth=4./2**level)
	axes.plot((xl, xl), (yh, yh), (zl,zh), '-', color=color, linewidth=4./2**level)
	axes.plot((xh, xh), (yh, yh), (zl,zh), '-', color=color, linewidth=4./2**level)

parser = OptionParser()
parser.add_option('--fname', dest='fname', help="Input file name - assumed to be a data.___.hdf5 file")

(options, args) = parser.parse_args()

if options.fname == None:
	print("ERROR: No file name provided!")
	sys.exit(1)

print("Drawing 3D Boxes for: %s"%(options.fname))

f = h5py.File(options.fname, 'r')
NLEV = len(f.keys()) - 2

#Run through the levels quickly to find the number of processors
NPROC = 0
for i in range(NLEV+1):
	proc_str = 'level_%d/Processors'%i
	procs = f[proc_str].value
	NPROC = max(NPROC, max(procs))
NPROC += 1

print("Number of AMR levels: %d"%(NLEV))
print("Number of processors: %d"%(NPROC))

NROWS = np.sqrt(NPROC)
if (NROWS - int(NROWS)) > 0.5:
	NROWS += 1
NROWS = int(NROWS)
NCOL = float(NPROC)/float(NROWS)
if (NCOL - int(NCOL)) > 0.5:
	NCOL += 1
NCOL = int(NCOL)

fig = plt.figure(1, figsize = (10 + NROWS, 10 + NCOL), facecolor='white')

colors = ['#a6cee3', '#1f78b4', '#b2df8a', '#33a02c', '#fb9a99', '#e31a1c']
PROB_DOMAIN = f['level_0'].attrs['prob_domain']
XDOM = np.fabs(PROB_DOMAIN[0])
YDOM = np.fabs(PROB_DOMAIN[1])
ZDOM = np.fabs(PROB_DOMAIN[2])


axs = []
for i in range(NPROC):
	axi = fig.add_subplot(NROWS, NCOL, i+1, projection='3d')
	axi.set_xlim(-XDOM, XDOM)
	axi.set_ylim(-YDOM, YDOM)
	axi.set_zlim(-ZDOM, ZDOM)
	axi.set_title("Proc %d"%(i+1))
	axi.set_xlabel("x")
	axi.set_ylabel("y")
	axi.set_zlabel("z")
	axs.append(axi)

for i in range(NLEV+1):
	proc_str = 'level_%d/Processors'%i
	box_str = 'level_%d/boxes'%i
	lev_str = 'level_%d'%i

	ref_ratio = f[lev_str].attrs['ref_ratio']

	boxes = f[box_str].value
	procs = f[proc_str].value

	NBOXES = len(boxes)
	print("Level: %d\tNumber of boxes: %d\n"%(i, NBOXES))
	for j in range(NBOXES):
		draw_box(axs[procs[j]], ref_ratio, i, boxes[j], colors[i%5])

fig.savefig(options.fname+"_3dBOXES.png")
plt.show()

