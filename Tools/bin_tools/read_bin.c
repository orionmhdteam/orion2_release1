#include "data.h"
#include "SwapEndian.h"

/* *************************************************************** */
void Read_Binary (Data *Data, int nrec, int is_float, int swap_endian)
/*
 *
 * PURPOSE
 *
 *   Read record nrec in a binary file and store its content
 *   in the array Data->data.
 *   Here row is the fastest index while col is the slowest one.
 *    
 *   nrec:         the record number
 *   is_float:     must = 1 if data is in single precision
 *   swap_endian:  must = 1 if byte order is different from
 *                 the machine you're using it.
 *
 *
 ***************************************************************** */
{
  int  ii, jj, kk;
  int  ir, ic;
  int  nx, ny, nz;
  int  offset, col_offset, row_offset;
  int  data_size;
  char filename[128];
  double dbl_var;
  float  flt_var;
  double **uu;
  FILE *fin;

  nx = Data->nx;
  ny = Data->ny;
  nz = Data->nz;

  if (is_float){
    data_size = sizeof(float);
  }else{
    data_size = sizeof(double);
  }
    
  sprintf (filename, Data->file);
  fin = fopen (filename, "r");
  if (fin == NULL){
    printf ("! File %s does not exist\n",filename);
    exit(1);
  }

   /*  ----  skip first nrec file ----  */

  if (nrec < 0){
    offset = (nrec*nx*ny*nz)*data_size;
    fseek (fin, offset, SEEK_END);
  }else{
    offset = nrec*nx*ny*nz*data_size;
    fseek (fin, offset, SEEK_SET);
  }

/* --------------------------------------------
       set offsets for slice reading
   -------------------------------------------- */

  if (Data->slice_plane == XY_PLANE){

    kk     = Data->slice_coord;
    offset = kk*nx*ny*data_size; 
    fseek (fin, offset, SEEK_CUR);
    col_offset = 0;
    row_offset = 0;

  } else if (Data->slice_plane == XZ_PLANE){

    jj     = Data->slice_coord;
    offset = jj*nx*data_size;
    fseek (fin, offset, SEEK_CUR);

    col_offset = 0;
    row_offset = nx*(ny - 1)*data_size; 

  } else if (Data->slice_plane == YZ_PLANE){
    
    ii     = Data->slice_coord;
    offset = ii*data_size;
    fseek (fin, offset, SEEK_CUR);
      
    col_offset = (nx - 1)*data_size; 
    row_offset = 0;

   }

/* -----------------------------------------
              read data 
   ----------------------------------------- */

  uu = Data->data;

  if (Data->is_float){ 
    for (ir = 0; ir < Data->nrow; ir++) {
      for (ic = 0; ic < Data->ncol; ic++) {     
        fread (&flt_var, data_size, 1, fin);
        if (feof(fin)){
          printf (" ! end of file reached\n");
          exit(1);
        }
        if (Data->swap_endian) SWAP_FLOAT(flt_var);
        uu[ir][ic] = (double) flt_var;
        fseek(fin, col_offset, SEEK_CUR);
      }
      fseek(fin, row_offset, SEEK_CUR);
    }
  }else{
    for (ir = 0; ir < Data->nrow; ir++) {
      for (ic = 0; ic < Data->ncol; ic++) {     
        fread (uu[ir] + ic, data_size, 1, fin);
        if (feof(fin)){
          printf (" ! end of file reached\n");
          exit(1);
        }
        fseek(fin, col_offset, SEEK_CUR);
      }
      fseek(fin, row_offset, SEEK_CUR);
    }
  }

/* ---------------------------------------------
      need to swap endianity (only for double) ? 
   --------------------------------------------- */

  if (Data->swap_endian && !Data->is_float){
    for (jj = 0; jj < Data->nrow; jj++){
    for (ii = 0; ii < Data->ncol; ii++){
      SWAP_DOUBLE (uu[jj][ii]);
    }}
  }    

  fclose (fin);
}
