#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include"SwapEndian.h"

#define NO 0
#define YES 1

void Print_Usage ();

/* ************************************************ */
int main(int argc, char **argv)
/* 
 *
 * PURPOSE
 *
 *   Convert a binary data file 
 *   from double precision to single
 *   precision.
 *
 *
 *
 *  SYNOPSIS :
 *
 *   dbl2flt file [options] 
 *
 *
 * LAST MODIFIED: 
 *
 *   3 June 2007 by A. Mignone, e-mail: mignone@to.astro.it 
 *
 *************************************************** */
{
  int   arg_is_file[65536], i, swap_flt, swap_dbl;
  int   single_output_file = NO;
  char  *fname_in, fname_out[256];
  double dbl_x;
  float  flt_x;
  FILE  *fin, *fout;

  printf ("dbl2flt - version 1.0\n");
  printf ("Last modified Wed May 30 2007\n");
  printf ("Copyright(C) 2006,2007 by A. Mignone (mignone@to.astro.it)\n\n");

  if (argc < 2) Print_Usage();

/* -----------------------------------------
           default values 
   ----------------------------------------- */

  swap_flt = swap_dbl = NO;

/* -------------------------------------------
      parse command line options
   ------------------------------------------- */

  for (i = 1; i < argc; i++){

    arg_is_file[i] = NO;
    if (!strcmp(argv[i], "--help") ||
        !strcmp(argv[i], "-help")){

      Print_Usage();

    }else if (!strcmp(argv[i],"-swap-flt")){

      swap_flt = YES;

    }else if (!strcmp(argv[i],"-swap-dbl")){

      swap_dbl = YES;

    }else if (!strcmp(argv[i],"-o")){
                                                                                                                               
     /* -- open single output file -- */

      sprintf (fname_out,"%s",argv[++i]);
      single_output_file = YES;
      fout = fopen(fname_out,"wb");

    }else{
      arg_is_file[i] = YES;
    }
  }

/* -------------------------------------------------
       open data file for reading and writing 
   ------------------------------------------------- */

  for (i = 1; i < argc; i++){

  /* -- loop only on arguments 
        representing a valid file name -- */ 

    if (!arg_is_file[i]) continue;
    if (!strcmp(argv[i],fname_out)) continue;
    
    fname_in = argv[i];
    if ((fin = fopen (fname_in, "r")) == NULL){
      printf ("! File %s does not exist\n",fname_in);
      break;
    }

  /* -- output name -- */

    if (single_output_file == NO){
      sprintf (fname_out,"%s.flt",fname_in);
      fout = fopen(fname_out,"wb");
    }

    printf("Converting (double) file %s to (float) %s ...",
            fname_in,fname_out);
    fflush (stdout);

    for (;;){ 
      fread(&dbl_x, sizeof(double), 1, fin);
      if (feof(fin)) break; /* check end of file before writing */

      if (swap_dbl) SWAP_DOUBLE (dbl_x);
      flt_x = (float)dbl_x;
      if (swap_flt) SWAP_FLOAT  (flt_x);
      fwrite (&flt_x, sizeof(float), 1, fout);
    }

    printf("done.\n");
    fclose (fin);
    if (single_output_file == NO) fclose (fout);

  }

  if (single_output_file) fclose (fout);
  return(0);
}

/* ****************************************************************************  */
void Print_Usage ()
/*
 *
 *
 *
 *
 *
 ****************************************************************************** */
{
  printf ("Purpose: convert binary files from double to single precision.\n\n");
  printf ("Usage: dbl2flt file [options]\n\n");
  printf (" file             the name of a valid binary data file(s) in\n");
  printf ("                  double precision;\n");
  printf ("[options] are:\n\n");
  printf (" -o <name>        specify the name of the single precision output\n");
  printf ("                  file. The default is file.flt;\n");
  printf (" -swap-dbl        swap endianity when reading double precision data.\n"); 
  printf ("                  Use this switch if the byte order of the data and\n");
  printf ("                  the processor differ.\n");
  printf (" -swap-flt        swap endianity when writing single precision data.\n");
  printf ("                  Use this switch if data and processor have the same \n");
  printf ("                  byte order, but you wish to read it on a machine with\n");
  printf ("                  different endianity.\n\n");

  printf ("Example:  Given the files (in double precision) rho.0001, rho.0002, ...,\n");
  printf ("-------   the command\n\n");
  printf ("               dbl2flt rho.*\n\n");
  printf ("          will convert all files beginning with 'rho.' into single precision\n");
  printf ("          files rho.0001.flt rho.0002.flt, ...\n\n");

  printf ("Example:  Given the files (in double precision) q.0001, q.0002, ...,\n");
  printf ("-------   the command\n\n");
  printf ("               dbl2flt q.* -o qall.bin\n\n");
  printf ("          will convert and append every file beginning with 'q.' into\n");
  printf ("          to the single file 'qall.bin'.\n\n");

  printf ("Example:       dbl2flt q.* -swap-flt -o qall.bin\n");
  printf ("-------   \n");
  printf ("          same as before, but swap the endianity when writing single\n");
  printf ("          precision data. This is useful, for example, when data has been\n");
  printf ("          written on a machine with byte order different from the processor\n");
  printf ("          where you intend to read the data.\n");

  exit(0);
}
