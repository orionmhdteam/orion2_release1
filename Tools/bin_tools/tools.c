#include<stdio.h>
#include<stdlib.h>

/* ############################################################# */
double **matrix (int nx, int ny)
/*
 #
 #  Allocate memory for a 2-D matrix with (nx,ny) points
 # in double precision
 #
 ############################################################### */
{
/*  Allocate space for a matrix */

  int i;
  double **m;

  m = (double **) malloc ((size_t) nx * sizeof (double *));

  if (!m) {
    printf ("Allocation Failure in matrix (1) \n");
    exit (1);
  }

  m[0] = (double *) malloc ((size_t) nx * ny * sizeof (double));

  if (!m[0]) {
    printf ("Allocation Failure in matrix (1) \n");
    exit (1);
  }

  for (i = 1; i < nx; i++)
    m[i] = m[i - 1] + ny;


  return m;
}
/* ############################################################### */
double *vector (int npoint)
/*
 #
 #   Allocate memory for a npoint 1-D array in double precision
 #
 ################################################################# */
{
  double *v;

  v = (double *) malloc ((size_t) (npoint) * sizeof (double));
  if (!v) {
    printf ("Allocate failure in vector \n");
    exit (1);
  }

  return v;
}
/* ################################################################## */
void free_vector (double *v)
/*
 #
 #
 #################################################################### */
{
  free ((char *)v);
}

/* ***************************************************************** */
void free_matrix (double **m)
/* 
 *
 * 
 ******************************************************************* */
{
  free ((char *) m[0]);
  free ((char *) m);
}
