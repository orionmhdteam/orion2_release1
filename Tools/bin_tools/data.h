#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>

#define YES  1
#define NO   0

#define dmax(a,b)  ( (a) >= (b) ? (a) : (b) )
#define dmin(a,b)  ( (a) <= (b) ? (a) : (b) )

#define XY_PLANE 0
#define XZ_PLANE 1
#define YZ_PLANE 2

typedef struct Data{
  char   *file;
  int    slice_plane, slice_coord;
  int    beg_rec, end_rec;
  int    nx, ny, nz;   /* -- dimensions of the array -- */
  int    is_float;     /* -- must = 1 when data is in single precision -- */
  int    nrow, ncol;   /* -- dimensions of the slice -- */
  int    swap_endian;  /* -- swap endianity when = 1 -- */
  double max_data, min_data;
  double **data;
  void   (*transform)();
} Data;


double **matrix (int, int);
double *vector (int);
void free_vector (double *);
void free_matrix (double **);
void Read_Binary (Data *, int, int, int);
