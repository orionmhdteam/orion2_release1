#!/bin/bash
############################################################################
#                                                                          #
#  pluto2gp  converts an ascii file as output of plutos bin2ascii tool     #
#+           to a gnuplot like ascii file containing dimensions and        #
#+           prints it to stdout. uses grid.out                            #
#                                                                          #
#  usage:    pluto2gp file                                                 #
#                                                                          #
#  published under GPL v2 (http://www.gnu.org/copyleft/gpl.html)           #
#  Copyright (C) 2006 Steffen Brinkmann, LSW/ZAH, University of Heidelberg #
#  send bug-reports to <s.brinkmann@lsw.uni-heidelberg.de>                 #
#                                                                          #
############################################################################
VERSION='pluto2gp ver. 0.2 (2007-05-15)'

# everybody in the house? --------------------------------------------------
[[ -f grid.out ]] || { echo "ERR: file grid.out not found!" 1>&2 ;exit 1; }
[[ -f $1 ]] || { echo "ERR: file $1 not found!" 1>&2 ;exit 1; }
# --------------------------------------------------------------------------

# select exact location of coordinates: ------------------------------------
#o inner face:
#> loc=2
#o centre of cell:
loc=3
#o outer face:
#> loc=4
# --------------------------------------------------------------------------

# get xgrid and ygrid ------------------------------------------------------
awk -v loc=$loc ' NR==1 {nx=$1} \
      NR>1 && NR<nx+2 {print $loc}' grid.out > xgrid.tmp
awk -v loc=$loc 'NR==1 {nx=$1} \
      NR==nx+2 {ny=$1} \
      NR>nx+2 && NR<nx+ny+3 {print $loc}' grid.out > ygrid.tmp
# --------------------------------------------------------------------------

# multiply xgrid ny times --------------------------------------------------
echo "" >> xgrid.tmp
cp xgrid.tmp xgrid.tmp.1
ny=$(( $(wc -l ygrid.tmp|awk '{print $1}') - 1 ))
for i in $(seq 1 $ny)
do
  cat xgrid.tmp.1 >> xgrid.tmp
done
# --------------------------------------------------------------------------

#multiply ygrid nx times ---------------------------------------------------
nx=$(wc -l xgrid.tmp.1|awk '{print $1-1}')
cp ygrid.tmp ygrid.tmp.1
awk -v nx=$nx '{for (i=0 ; i<nx ; i++) {print $0;}print " "}' ygrid.tmp.1 > ygrid.tmp
# --------------------------------------------------------------------------

#put it together -----------------------------------------------------------
paste xgrid.tmp ygrid.tmp $1 
# --------------------------------------------------------------------------

#some debug info -----------------------------------------------------------
#echo "nx= $nx"
#echo "ny= $ny"
# --------------------------------------------------------------------------

#cleanup -------------------------------------------------------------------
rm -f xgrid.tmp.1
rm -f ygrid.tmp.1
#rm -f xgrid.tmp
#rm -f ygrid.tmp
# --------------------------------------------------------------------------
