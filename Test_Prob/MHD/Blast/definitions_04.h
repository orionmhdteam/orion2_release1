#define    PHYSICS   MHD
#define    DIMENSIONS   3
#define    COMPONENTS   3
#define    GEOMETRY   CARTESIAN
#define    INCLUDE_BODY_FORCE   NO
#define    INCLUDE_COOLING   NO
#define    INCLUDE_PARTICLES   NO
#define    INTERPOLATION   PARABOLIC
#define    TIME_STEPPING   CHARACTERISTIC_TRACING
#define    DIMENSIONAL_SPLITTING   NO
#define    NTRACER   0
#define    USER_DEF_PARAMETERS   7

/* -- physics dependent declarations -- */

#define    EOS   IDEAL
#define    ENTROPY_SWITCH   NO
#define    MHD_FORMULATION   FLUX_CT
#define    INCLUDE_BACKGROUND_FIELD   NO
#define    RESISTIVE_MHD   NO
#define    THERMAL_CONDUCTION   NO
#define    VISCOSITY   NO

/* -- pointers to user-def parameters -- */

#define  P_IN   0
#define  P_OUT   1
#define  BMAG   2
#define  THETA   3
#define  PHI   4
#define  RADIUS   5
#define  GAMMA   6

/* -- supplementary constants (user editable) -- */ 

#define  INITIAL_SMOOTHING     NO
#define  WARNING_MESSAGES      YES
#define  PRINT_TO_FILE         YES
#define  SHOCK_FLATTENING      NO
#define  ARTIFICIAL_VISCOSITY  NO
#define  CHAR_LIMITING         YES
#define  LIMITER               mc_lim
#define  CT_EMF_AVERAGE     UCT_HLL
#define  CT_EN_CORRECTION   YES
#define  CT_VEC_POT_INIT    YES
#define  SAVE_VEC_POT          NO
