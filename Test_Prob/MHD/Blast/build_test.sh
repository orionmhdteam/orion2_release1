#!/bin/bash

## ensure we are in the source directory
cd $ORION2_DIR/Test_Prob/MHD/Blast

## create makefile
python3 $ORION2_DIR/setup.py --with-orion2 --no-gui

## compile
make
