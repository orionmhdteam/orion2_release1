#Script to make Field_Loop Slice Plots
#Run this script in directory with data files from 
#Field Loop test problem

import yt
import glob

if __name__=="__main__":
    filepath = 'data.[0-9][0-9][0-9][0-9].3d.hdf5'
    filenames = glob.glob(filepath)
    filenames.sort()

    ts = yt.DatasetSeries(filenames)
    Bmag_max = 0
    for i,ds in enumerate(ts.piter()):
        print("Making plot for " + str(ds))

        if i == 0:
            Bmag_max = ds.r['magnetic_field_strength'].max()
        slc = yt.SlicePlot(ds, 'z', ('gas', 'magnetic_field_strength'))
        slc.set_zlim(('gas', 'magnetic_field_strength'), Bmag_max/100, Bmag_max)
        slc.save()
    
