#define    PHYSICS   MHD
#define    DIMENSIONS   3
#define    COMPONENTS   3
#define    GEOMETRY   CARTESIAN
#define    INCLUDE_BODY_FORCE   NO
#define    INCLUDE_COOLING   NO
#define    INCLUDE_PARTICLES   NO
#define    INTERPOLATION   LINEAR
#define    TIME_STEPPING   CHARACTERISTIC_TRACING
#define    DIMENSIONAL_SPLITTING   NO
#define    NTRACER   0
#define    USER_DEF_PARAMETERS   9
#define    TURBULENCE_DRIVING   HDF5
#define    PRINT_TO_STDOUT   YES
#define    RAREFACTION_FLATTEN   NO
#define    SINK_PARTICLES   NO
#define    STAR_PARTICLES   NO
#define    STAR_TYPE   CONTEMPORARY
#define    DOEINTAVE   NO
#define    DOEINTREFLUX   NO
#define    SELF_GRAVITY   NO
#define    NEWTON_COOL   NO
#define    BARO_COOL   YES
#define    READHDF5ICS   NO
#define    EXTRACTICS   NO

/* -- physics dependent declarations -- */

#define    EOS   IDEAL
#define    ENTROPY_SWITCH   NO
#define    MHD_FORMULATION   FLUX_CT
#define    INCLUDE_BACKGROUND_FIELD   NO
#define    THERMAL_CONDUCTION   NO

/* -- pointers to user-def parameters -- */

#define  den0   0
#define  beta0   1
#define  cs0   2
#define  vinit   3
#define  ldrv   4
#define  GAMMA   5
#define  T0   6
#define  barochoice   7
#define  rhobaro   8

/* -- supplementary constants (user editable) -- */ 

#define  INITIAL_SMOOTHING     NO
#define  WARNING_MESSAGES      NO
#define  PRINT_TO_FILE         YES
#define  SHOCK_FLATTENING      MULTID
#define  ARTIFICIAL_VISCOSITY  NO
#define  CHAR_LIMITING         YES
#define  LIMITER               mc_lim 
#define  CT_EMF_AVERAGE        UCT_HLL
#define  CT_EN_CORRECTION      YES
#define  CT_VEC_POT_INIT       NO
#define  COMPUTE_DIVB          NO
