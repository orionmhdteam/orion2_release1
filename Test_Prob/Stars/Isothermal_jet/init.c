#include "orion2.h"

/* This is a bit ugly but ...
   include a tabular.h header with tabular data arrays for 
   v[i] = in fall speed at x = (i+1)*DELTA
   alpha[i] = dimensionless density x = (i+1)*DELTA
*/
#include "tabular.h" 

void shuSolution(real r, real t, real *rho, real *u) {
/* 
   interpolate the shu solution for rho(r,t) and infall speed u(r,t)
 */
  /* From Shu (1977)
     x=r/(at)
     v(x) = u(r,t)/a
     M(r,t) =m(x)  a**3*t/G
     m = x**2 alpha*(x-v)
     rho(r,t) = alpha(x)/(4 pi G t**2)
     rho(r,0) = (a**2 A)/(4 pi G) r**(-2)*/

  real a = aux[cs0]; // = isothermal sound speed = 18000. cm/s for the KKN04 setup
  real v_interp, alpha_interp, x, xlo, xhi;
  int i;
  
  x = r/(a*t);
  i = (int)(x/DELTA-1.); // x is between i and i+1 in table
  if(i<0) i = 0;
  if(i>N_TABLE-1) {
    printf("ERROR, init.c::analytic fell off the table, have a nice day!\n");
    fflush(stdout);
    exit(1);
  }
  // do some linear interpolation
  xlo = DELTA*(i+1.);
  xhi = DELTA*(i+2.);
  v_interp = v[i] + (v[i+1] - v[i])*(xhi-xlo)/DELTA;
  alpha_interp = alpha[i] + (alpha[i+1] - alpha[i])*(xhi-xlo)/DELTA;
  // put in dimensional units
  *rho = alpha_interp/(4.*CONST_PI*CONST_G*pow(t,2));
  *u = v_interp*a;
}

/* ************************************************************** */
void INIT (real *us, real x1, real x2, real x3,
           int i, int j, int k, int *prim_var)
/* 
 *
 * 
 * NAME 
 * 
 *   INIT
 *
 *
 * PURPOSE
 *
 *   Set inital condition. 
 *
 *
 * ARGUMENTS 
 *
 *   us (OUT)           a pointer to a array_1D of primitive 
 *                      (when *prim_var = 1) or conservative
 *                      (when *prim_var = 0) variables;
 *
 *   x1, x2, x3 (IN)    the three coordinates; the meaning is different
 *                      depending on the geometry:
 *
 *                       x1      x2      x3
 *                       -------------------                
 *                        x       y       z     in cartesian geometry
 *                        r       z       -     in cylindrical geometry 
 *                        r      phi      z     in polar geometry 
 *                        r     theta    phi    in spherical geometry
 *                  
 *   i , j , k  (IN)    the integer indexes of the cell containing the 
 *                      point x1, x2, x3.
 *                
 *   *prim_var (OUT)    an integer flag. When *prim_var = 1 initial 
 *                      conditions are assigned in terms of 
 *                      primitive values. When *prim_var = 0 initial
 *                      conditions are given in terms of conservative 
 *                      value.
 *
 *
 * Variable names are accessed as us[nv], where
 *
 *   nv = DN is density
 *   nv = PR is pressure
 *  
 *   Vector components are labelled always as VX,VY,VZ, but alternative
 *   labels may be used:
 *
 *   Cartesian       VX     VY      VZ
 *   Cylindrical    iVR    iVZ     iVPHI
 *   Polar          iVR    iVPHI   iVZ
 *   Spherical      iVR    iVTH    iVPHI
 *
 * 
 *
 **************************************************************** */
{

  real r, rho, u;
  *prim_var = 1;

  SMALL_DN = 1.e-24; // erg/cc
  SMALL_PR = 1.e-14; // g/cc
  T_CUT_COOL = 1.;   // K

  r = D_EXPAND(x1*x1, + x2*x2, + x3*x3);
  r = sqrt(r);

  if(r>1.85893603498e+17) {
    shuSolution(1.85893603498e+17, 1.29302897416e+12, &rho, &u);
  } else{
    shuSolution(r, 1.29302897416e+12, &rho, &u);
  }

  us[DN] = rho;
  us[VX] = -u*x1/r;
  us[VY] = -u*x2/r;
  us[VZ] = -u*x3/r;

  #if EOS == IDEAL
   gmm = aux[GAMMA];
   us[PR] = rho*aux[cs0]*aux[cs0]/gmm;
  #else
   C_ISO = aux[cs0];
  #endif

#if PHYSICS == MHD
   us[BX] = aux[bx0];
   us[BY] = aux[by0];
   us[BZ] = aux[bz0]; 
#endif
}
/* **************************************************************** */
void ANALYSIS (const Data *d, Grid *grid)
/* 
 *
 *
 * NAME
 *
 *   ANALYSIS
 *
 *
 * PURPOSE
 *  
 *   Perform some pre-processing data
 *
 * 
 * ARGUMENTS
 *
 *   uu(IN)     solution data.
 *
 *   GG(IN)     pointer to array of GRID structures  
 *
 **************************************************************** */
{

}
#if PHYSICS == MHD
/* ************************************************************** */
void BACKGROUND_FIELD (real x1, real x2, real x3, real *B0)
/* 
 *
 *
 * NAME
 * 
 *   BACKGROUND_FIELD
 *
 *
 * PURPOSE
 *
 *   Define the component of a static, curl-free background 
 *   magnetic field.
 *
 *
 * ARGUMENTS
 *
 *   x1, x2, x3  (IN)    coordinates
 *
 *   B0         (OUT)    array_1D component of the background field.
 *
 *
 **************************************************************** */
{
   B0[0] = 0.0;
   B0[1] = 0.0;
   B0[2] = 0.0;
}
#endif


/* ************************************************************** */
void USERDEF_BOUNDARY (const Data *d, int side, Grid *grid)
/* 
 *
 * 
 **************************************************************** */
{
}


#if ADD_INTERNAL_BOUNDARY == YES
/* ************************************************************** */

void INTERNAL_BOUNDARY(real ***uu[], real ***uu_old[],
                       Grid *grid, 
                       int i0, int i1, int j0, int j1, int k0, int k1)

/* 
 *
 *
 * NAME
 *
 *   INTERNAL_BOUNDARY
 *
 *
 * PURPOSE
 *
 *   Allow the user to control 
 *
 *
 * ARGUMENTS
 *
 *   uu      (IN/OUT)    three-dimensional array_1D containing the solution data;
 *
 *   uu_old  (IN/OUT)    old, kept for backward compatibility;
 *
 *   grid    (IN)        pointer to grid structures;
 * 
 *   i0, j0, k0 (IN)     indexes of the lower-coordinate point inside
 *                       the domain; 
 *
 *   i1, j1, k1, (IN)    indexes of the upper-coordinate point inside
 *                       the domain.
 *   
 *  
 *
 *
 **************************************************************** */
{
  int  i, j, k;
  real x1, x2, x3;
  
  for (k = k0; k <= k1; k++) { x3 = grid[KDIR].x[k];  
  for (j = j0; j <= j1; j++) { x2 = grid[JDIR].x[j];  
  for (i = i0; i <= i1; i++) { x1 = grid[IDIR].x[i];  
         
  }}}
    
}

#endif
