#This script computes the zero age main sequence (ZAMS)
# luminosity, radius, and effective temperature for a star of 
#mass m (either in Msun or gram units)
#These fitting formulae are from Tout et al. 1996

import math

def lzams(m):
    ALPHA    =0.39704170
    BETA     =8.52762600
    GAMMA    =0.00025546
    DELTA    =5.43288900
    EPSILON  =5.56357900
    ZETA     =0.78866060
    ETA      =0.00586685 
    THETA    =1.71535900
    IOTA     =6.59778800
    KAPPA   =10.08855000
    LAMBDA   =1.01249500
    MU       =0.07490166
    NU       =0.01077422
    XI       =3.08223400
    UPSILON =17.84778000
    PI       =0.00022582

    lsun = 3.846e33
    msun = 1.989e33
    if (m > 1e20):
        msol = m/msun
    else:
        msol = m

    return(lsun * (ALPHA*msol**5.5 + BETA*msol**11.) / (GAMMA+msol**3.+
                                                        DELTA*msol**5.+EPSILON*msol**7.+ 
                                                        ZETA*msol**8.+ETA*msol**9.5) )


def rzams(m):
    ALPHAR    =0.39704170
    BETAR     =8.52762600
    GAMMAR    =0.00025546
    DELTAR    =5.43288900
    EPSILONR  =5.56357900
    ZETAR     =0.78866060
    ETAR      =0.00586685 
    THETAR    =1.71535900
    IOTAR     =6.59778800
    KAPPAR    =10.08855000
    LAMBDAR   =1.01249500
    MUR       =0.07490166
    NUR       =0.01077422
    XIR       =3.08223400
    UPSILONR =17.84778000
    PIR       =0.00022582

    rsun = 6.9598e10
    msun = 1.9891e33
    if (m > 1e20):
        msol = m/msun
    else:
        msol = m

    return(rsun * (THETAR*msol**2.5 + IOTAR*msol**6.5 + KAPPAR*msol**11.+ 
                   LAMBDAR*msol**19.0+MUR*msol**19.5 ) / (NUR+XIR*msol**2.+
                                                          UPSILONR*msol**8.5+msol**18.5+ 
                                                          PIR*msol**19.5))

def tzams(m):
    l=lzams(m)
    r=rzams(m)

    SIGMAB =  5.670373e-5 
    return((l/(4*math.pi*r**2.*SIGMAB))**0.25) 
