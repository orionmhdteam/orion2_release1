#include"orion2.h"

static void FLIP_SIGN (int side, int type, int *vsign);

/* ******************************************************************* */
void BOUNDARY (const Data *d, int idim, Grid *grid)
/*!
 *
 *
 *  Set boundary conditions on cell centered and 
 *  face centered data. 
 *
 *
 * \param d (in/out)      solution arrays, containing centered and 
 *                        staggered fields 
 *
 * \param idim (in)       the dimension where the boundary is needed;
 *        - idim = IDIR   first dimension (x)
 *        - idim = JDIR   second dimenson (y)
 *        - idim = KDIR   third dimension (z)
 *        - idim = ALLDIR all dimensions
 *
 * \param grid (in)     grid arrays. 
 *
 *
 *
 *
 * \author A. Mignone (mignone@to.astro.it)
 * 
 * \date   Last modified Feb 19 2008
 *
 *
 ******************************************************************* */
{
  int  is, nv;
  int  side[6] = {X1_BEG, X1_END, X2_BEG, X2_END, X3_BEG, X3_END};
  int  type[6], sbeg, send, vsign[NVAR+2];
  int  par_dim[3] = {0, 0, 0};
  double ***q;

/*
int i,j,k;
for (nv = NVAR; nv--; ){
X1_BEG_LOOP(k,j,i) d->Vc[nv][k][j][i] = 1.e8*i*nv + 5.e7*j + k*j*2.33e8;
X1_END_LOOP(k,j,i) d->Vc[nv][k][j][i] = 1.e8*i + 3.e7*j*nv + k*j*2.33e8;
X2_BEG_LOOP(k,j,i) d->Vc[nv][k][j][i] = 1.e8*i + 5.e7*j + k*j*2.33e8;
X2_END_LOOP(k,j,i) d->Vc[nv][k][j][i] = 1.e8*i - 4.e7*j + k*j*2.33e8*nv;
X3_BEG_LOOP(k,j,i) d->Vc[nv][k][j][i] = 1.e8*i + 5.e7*j + k*j*2.33e8;
X3_END_LOOP(k,j,i) d->Vc[nv][k][j][i] = 1.e8*i + 1.e7*j*nv + k*j*2.33e8;
}
*/
/* ------------------------------------------
    Check how many procs are 
    available in each direction. 
   ------------------------------------------ */

  D_EXPAND(par_dim[0] = grid[IDIR].nproc > 1;  ,
           par_dim[1] = grid[JDIR].nproc > 1;  ,
           par_dim[2] = grid[KDIR].nproc > 1;)

/*  -----------------------------------------
      Check the solution inside the 
      computational domain. 
    -----------------------------------------  */

  USERDEF_BOUNDARY (d, 0, grid);
  
/*  -----------------------------------------
      Exchange data between processors 
    -----------------------------------------  */
   
  #ifdef PARALLEL
   MPI_Barrier (MPI_COMM_WORLD);

/*   for (nv = 0; nv < NVAR; nv++) AL_Exchange (d->Vc[nv][0][0], SZ); */
   for (nv = 0; nv < NVAR; nv++) AL_Exchange_dim (d->Vc[nv][0][0], par_dim, SZ);

   /* --------------------------------------------
       Exchange staggered magnetic fields in the 
       transverse directions
      -------------------------------------------- */
       
   #ifdef STAGGERED_MHD 
    for (nv = 0; nv < DIMENSIONS; nv++) AL_Exchange_dim (d->Vs[nv][0][0], par_dim, SZ);
   #endif

   MPI_Barrier (MPI_COMM_WORLD);
  #endif
 
/*  ------------------------------------------------------------------------------
      When idim == ALLDIR boundaries are imposed on ALL sides, so a loop from 
      sbeg = 0 to send = 2*DIMENSIONS - 1 is performed. 
      When idim = n, boundaries are imposed at the beginning and the
      end of the n-th direction.
    ------------------------------------------------------------------------------ */ 

  if (idim == ALLDIR) {
    sbeg = 0;
    send = 2*DIMENSIONS - 1;
  } else {
    sbeg = 2*idim;
    send = 2*idim + 1;
  }

/* --------------------------------------------------------
                  Loop on directions       
   -------------------------------------------------------- */

  type[0] = grid[IDIR].lbound;
  type[1] = grid[IDIR].rbound;

  type[2] = grid[JDIR].lbound;
  type[3] = grid[JDIR].rbound;

  type[4] = grid[KDIR].lbound;
  type[5] = grid[KDIR].rbound;

  for (is = sbeg; is <= send; is++){

    if (type[is] == 0) continue;  /* no physical boundary: skip */

    if (type[is] == OUTFLOW) {

      for (nv = 0; (q = BOUND_VAR(d,side[is],nv)) != NULL; nv++){
        OUTFLOW_BOUND (q, side[is]);            
      }

    }else if (  (type[is] == REFLECTIVE) 
             || (type[is] == AXISYMMETRIC)
             || (type[is] == EQTSYMMETRIC)){

      FLIP_SIGN (side[is], type[is], vsign);
      for (nv = 0; (q = BOUND_VAR(d,side[is],nv)) != NULL; nv++){
        REFLECTIVE_BOUND (q, side[is], vsign[nv]);
      }

    }else if (type[is] == PERIODIC) {

  /* -------------------------------------
      impose periodic boundary if there's 
      only 1 proc in this direction
     ------------------------------------- */

      if (!par_dim[is/2]) {
        for (nv = 0; (q = BOUND_VAR(d,side[is],nv)) != NULL; nv++){
          PERIODIC_BOUND (q, side[is]);
        } 
      }

    }else if (type[is] == USERDEF) {

      #ifdef PSI_GLM
      {int i,j,k;
       switch(side[is]){
         case X1_BEG: X1_BEG_LOOP(k,j,i) d->Vc[PSI_GLM][k][j][i] = 0.0; break;
         case X1_END: X1_END_LOOP(k,j,i) d->Vc[PSI_GLM][k][j][i] = 0.0; break;
         case X2_BEG: X2_BEG_LOOP(k,j,i) d->Vc[PSI_GLM][k][j][i] = 0.0; break;
         case X2_END: X2_END_LOOP(k,j,i) d->Vc[PSI_GLM][k][j][i] = 0.0; break;
         case X3_BEG: X3_BEG_LOOP(k,j,i) d->Vc[PSI_GLM][k][j][i] = 0.0; break;
         case X3_END: X3_END_LOOP(k,j,i) d->Vc[PSI_GLM][k][j][i] = 0.0; break;
       }
      }
      #endif

      USERDEF_BOUNDARY (d, side[is], grid);
    }

  /* -- assign normal component of staggered B 
        using the div.B = 0 condition           -- */

    #ifdef STAGGERED_MHD
     FILL_MAGNETIC_FIELD (d->Vs, side[is], grid);  
    #endif

  }
   
/* -- entropy boundary values are assigned in Entropy_Switch -- */

  #if ENTROPY_SWITCH == YES
   if (ISTEP == 1) {
     Entropy_Switch (d, grid); 
   }
  #endif

}

/* ************************************************************** */
void OUTFLOW_BOUND (double ***q, int side)
/*
 *
 *
 *
 *
 **************************************************************** */
{
  int  i, j, k;

  if (side == X1_BEG) { 

    X1_BEG_LOOP(k,j,i) q[k][j][i] = q[k][j][IBEG];

  }else if (side == X1_END){

    X1_END_LOOP(k,j,i) q[k][j][i] = q[k][j][IEND];

  }else if (side == X2_BEG){

    X2_BEG_LOOP(k,j,i) q[k][j][i] = q[k][JBEG][i];

  }else if (side == X2_END){

    X2_END_LOOP(k,j,i) q[k][j][i] = q[k][JEND][i];
 
  }else if (side == X3_BEG){

    X3_BEG_LOOP(k,j,i) q[k][j][i] = q[KBEG][j][i];

  }else if (side == X3_END){

    X3_END_LOOP(k,j,i) q[k][j][i] = q[KEND][j][i];

  }
}

/* *********************************************************** */
void FLIP_SIGN (int side, int type, int *vsign)
/*
 *
 * PURPOSE
 *
 *   flip the sign of vector components with respect to 
 *   axis side. 
 *   Depending on type, one needs to symmetrize or 
 *   anti-symmetrize:
 *
 *   REFLECTIVE:    Vn -> -Vn,  Bn -> -Bn
 *                  Vp ->  Vp,  Bp ->  Bp
 *                  Vt ->  Vt,  Bt ->  Bt
 *
 *   AXISYMMETRIC:  Vn -> -Vn,  Bn -> -Bn
 *                  Vp ->  Vp,  Bp ->  Bp
 *                  Vt -> -Vt,  Bt -> -Bt
 *
 *   EQTSYMMETRIC:  Vn -> -Vn,  Bn ->  Bn
 *                  Vp ->  Vp,  Bp -> -Bp
 *                  Vt ->  Vt,  Bt -> -Bt
 *
 *    where (n) is the normal components, (p) and (t)
 *    are the transverse (or poloidal and toroidal for
 *    cylindrical and spherical coordinates) components.
 * 
 ************************************************************** */
{
  int nv;
  int Vn, Vp, Vt;
  int Bn, Bp, Bt;

  /* -------------------------------------
       get normal (n), poloidal (p) and 
       toroidal (t) vector components
       The ordering is the same as in 
       SET_INDEXES()
     ------------------------------------- */

  if (side == X1_BEG || side == X1_END){
    Vn = VX; Vp = VY; Vt = VZ;
    #if PHYSICS == MHD || PHYSICS == RMHD
     Bn = BX; Bp = BY; Bt = BZ;
    #endif
  }else if (side == X2_BEG || side == X2_END){
    Vn = VY; Vp = VX; Vt = VZ;
    #if PHYSICS == MHD || PHYSICS == RMHD
     Bn = BY; Bp = BX; Bt = BZ;
    #endif
  }else if (side == X3_BEG || side == X3_END){
    Vn = VZ; Vp = VX; Vt = VY;
    #if PHYSICS == MHD || PHYSICS == RMHD
     Bn = BZ; Bp = BX; Bt = BY;
    #endif
  }

  /* ---------------------------------------
       determine which variables flip sign
     --------------------------------------- */

  for (nv = 0; nv < NVAR+2; nv++) vsign[nv] = 1.0;

  vsign[Vn] = -1.0;
  #if PHYSICS == MHD || PHYSICS == RMHD
   vsign[Bn] = -1.0;
  #endif

  #if COMPONENTS == 3
   if (type == AXISYMMETRIC){
     vsign[Vt] = -1.0;
     #if PHYSICS == MHD || PHYSICS == RMHD
      vsign[Bt] = -1.0;
     #endif
   }
  #endif

  #if PHYSICS == MHD || PHYSICS == RMHD
   if (type == EQTSYMMETRIC){
     EXPAND(vsign[Bn] =  1.0; ,
            vsign[Bp] = -1.0; ,
            vsign[Bt] = -1.0;)
     #ifdef PSI_GLM 
      vsign[PSI_GLM] = -1.0;
     #endif
   }
  #endif

/* -- set magnetic field staggered components sign -- */

  #ifdef STAGGERED_MHD
   D_EXPAND(                    ,
     vsign[NVAR]   = vsign[Bp]; ,
     vsign[NVAR+1] = vsign[Bt];)   
  #endif
}
/* *********************************************************** */
void REFLECTIVE_BOUND (double ***q, int side, int s)
/*
 *
 * PURPOSE
 *
 *   make symmetric (s = 1) or anti-symmetric (s=-1) 
 *   variable profiles with respect to coordinate axis 
 *   given by side.
 *
 ************************************************************** */
{
  int   i, j, k;

  if (side == X1_BEG) {   

    X1_BEG_LOOP(k,j,i) q[k][j][i] = s*q[k][j][2*IBEG - i - 1];
 
  }else if (side == X1_END){  

    X1_END_LOOP(k,j,i) q[k][j][i] = s*q[k][j][2*IEND - i + 1];

  }else if (side == X2_BEG){  

    X2_BEG_LOOP(k,j,i) q[k][j][i] = s*q[k][2*JBEG - j - 1][i];

  }else if (side == X2_END){  

    X2_END_LOOP(k,j,i) q[k][j][i] = s*q[k][2*JEND - j + 1][i];

  }else if (side == X3_BEG){  

    X3_BEG_LOOP(k,j,i) q[k][j][i] = s*q[2*KBEG - k - 1][j][i];

  }else if (side == X3_END){  

    X3_END_LOOP(k,j,i) q[k][j][i] = s*q[2*KEND - k + 1][j][i];

  }
}

/* *********************************************************** */
void PERIODIC_BOUND (double ***q, int side)
/*
 *
 *
 *
 *
 ************************************************************** */
{
  int  i, j, k;

  if (side == X1_BEG) { 

    X1_BEG_LOOP(k,j,i) q[k][j][i] = q[k][j][i + NX];

  }else if (side == X1_END){

    X1_END_LOOP(k,j,i) q[k][j][i] = q[k][j][i - NX];

  }else if (side == X2_BEG){

    X2_BEG_LOOP(k,j,i) q[k][j][i] = q[k][j + NY][i];

  }else if (side == X2_END){

    X2_END_LOOP(k,j,i) q[k][j][i] = q[k][j - NY][i];
 
  }else if (side == X3_BEG){

    X3_BEG_LOOP(k,j,i) q[k][j][i] = q[k + NZ][j][i];

  }else if (side == X3_END){

    X3_END_LOOP(k,j,i) q[k][j][i] = q[k - NZ][j][i];

  }
}


#ifdef PARALLEL
/* *********************************************************** */
void FORCE_PERIODIC_BOUND (real ***V, int dir, Grid *grid)
/*
 *
 *
 *  Force periodic boundary conditions on direction
 *  dir (=IDIR,JDIR,KDIR) even if it was not originally
 *  set to be periodic.
 *
 ************************************************************* */
{
  int i,j,k,nv;
  int ngh;
  static int dest = -1;
  int stag = 1, rtag = 1;
  MPI_Comm cartcomm;
  MPI_Status istat;
  MPI_Request req;
  static int nprocs[3], periods[3], coords[3];
  static int first_call = 1;
  static MPI_Datatype XVect, YVect;

  ngh = grid[dir].nghost;

  if (first_call){
    MPI_Type_vector (NZ_TOT*NY_TOT, ngh, NX_TOT, MPI_DOUBLE, &XVect);
    MPI_Type_vector (NZ_TOT, NX_TOT*ngh, NX_TOT*NY_TOT, MPI_DOUBLE, &YVect);
    MPI_Type_commit (&XVect);
    MPI_Type_commit (&YVect);
    first_call = 0;
  }

  AL_Get_cart_comm(SZ, &cartcomm);
  MPI_Barrier (MPI_COMM_WORLD);

/* --------------------------------------
     get rank of the processor lying 
     on the opposite side of the domain
   -------------------------------------- */

  if (grid[dir].lbound != 0){
    MPI_Cart_get(cartcomm, DIMENSIONS, nprocs, periods, coords);
    coords[dir] += nprocs[dir] - 1;
    MPI_Cart_rank (cartcomm, coords, &dest);
  }

  if (grid[dir].rbound != 0){
    MPI_Cart_get(cartcomm, DIMENSIONS, nprocs, periods, coords);
    coords[dir] += - nprocs[dir] + 1;
    MPI_Cart_rank (cartcomm, coords, &dest); 
  }

/* -------------------------------------------------
    If there's only one processor on that direction
    call PERIODIC_BOUND
   ------------------------------------------------- */

  if (grid[dir].lbound != 0 && grid[dir].rbound != 0){
    if (dir == IDIR) {

      X1_BEG_LOOP(k,j,i) V[k][j][i] = V[k][j][i + NX];
      X1_END_LOOP(k,j,i) V[k][j][i] = V[k][j][i - NX];

    }else if (dir == JDIR){
      PERIODIC_BOUND (V, X2_BEG);
      PERIODIC_BOUND (V, X2_END);
    }else if (dir == KDIR){
      PERIODIC_BOUND (V, X3_BEG);
      PERIODIC_BOUND (V, X3_END);
    }
    MPI_Barrier (MPI_COMM_WORLD);
    return;
  }       
  
/* ------------------------------------------------
        Exchange data by filling ghost zones 
   ------------------------------------------------ */

  if (grid[dir].lbound != 0){
    if (prank != dest){
      if (dir == IDIR){
        MPI_Sendrecv (V[0][0] + IBEG, 1, XVect, dest, stag,
                      V[0][0] + 0   , 1, XVect, dest, rtag,
                      MPI_COMM_WORLD, &istat);
      }
      if (dir == JDIR){
        MPI_Sendrecv (V[0][JBEG], 1, YVect, dest, stag,
                      V[0][0]   , 1, YVect, dest, rtag,
                      MPI_COMM_WORLD, &istat);
      }

      if (dir == KDIR){
        MPI_Sendrecv (V[KBEG][0], NX_TOT*NY_TOT*ngh, MPI_DOUBLE, dest, stag,
                      V[0][0]   , NX_TOT*NY_TOT*ngh, MPI_DOUBLE, dest, rtag,
                      MPI_COMM_WORLD, &istat);
      }

    }
  }

  if (grid[dir].rbound != 0){
    if (prank != dest){
      if (dir == IDIR){
        MPI_Sendrecv (V[0][0] + IEND - (ngh-1), 1, XVect, dest, stag,
                      V[0][0] + IEND + 1      , 1, XVect, dest, rtag,
                      MPI_COMM_WORLD, &istat);
      }
      if (dir == JDIR){
        MPI_Sendrecv (V[0][JEND-ngh+1], 1, YVect, dest, stag,
                      V[0][JEND+1]    , 1, YVect, dest, rtag,
                      MPI_COMM_WORLD, &istat);
      }

      if (dir == KDIR){
        MPI_Sendrecv (V[KEND-ngh+1][0], NX_TOT*NY_TOT*ngh, MPI_DOUBLE, dest, stag,
                      V[KEND+1][0]    , NX_TOT*NY_TOT*ngh, MPI_DOUBLE, dest, rtag,
                      MPI_COMM_WORLD, &istat);
      }

    }
  }

  MPI_Barrier (MPI_COMM_WORLD);
}
#endif

/* *************************************************** */
real ***BOUND_VAR(const Data *d, int side, int nv)
/*
 *
 * Return the nv-th variable requiring 
 * boundary conditions on side side.
 * These include: all cell-centered variables
 * and the two transverse components of staggered
 * fields.
 *
 **************************************************** */
{
  if (nv < NVAR) return d->Vc[nv];

  #ifdef STAGGERED_MHD
  if (side == X1_BEG || side == X1_END){
    D_EXPAND( 
                                              ,
      if (nv == NVAR)     return d->Vs[BYs];  ,
      if (nv == (NVAR+1)) return d->Vs[BZs];)
    return (NULL);
  }

  if (side == X2_BEG || side == X2_END){
    D_EXPAND( 
      if (nv == NVAR)     return d->Vs[BXs];  ,
                                              ,
      if (nv == (NVAR+1)) return d->Vs[BZs];)
    return (NULL);
  }

  if (side == X3_BEG || side == X3_END){
    if (nv == NVAR)     return d->Vs[BXs]; 
    if (nv == (NVAR+1)) return d->Vs[BYs];  
    return (NULL);
  }
  #endif
  return NULL;
}

