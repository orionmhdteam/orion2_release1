#include "orion2.h"


/* ******************************************************************** */
void PRIMTOCON(real *uprim[], real *ucons[], int ibeg, int iend)
/*
 *
 *
 *    CONVERT PRIMITIVE VECTOR UPRIM TO CONSERVATIVE VECT. UCONS
 *
 ********************************************************************* */
{
  int    ii, nv;
  real *u, *v, rho;
  double temp, eth, pressure;

  #if EOS == IDEAL
   double gmm1;
   gmm1 = gmm - 1.0;
  #endif



  for (ii = ibeg; ii <= iend; ii++) {

    u = ucons[ii];
    v = uprim[ii];

    u[DN]  = rho = v[DN];
    EXPAND(u[MX] = rho*v[VX]; ,
           u[MY] = rho*v[VY]; ,
           u[MZ] = rho*v[VZ];)



#if EOS == IDEAL
      u[EN] = EXPAND(v[VX]*v[VX], + v[VY]*v[VY], + v[VZ]*v[VZ]);
      u[EN] = 0.5*rho*u[EN] + v[PR]/gmm1;
#endif                 

    for (nv = NFLX; nv < (NFLX + NSCL); nv++) u[nv] = v[nv]*rho;
  }
}

/* ******************************************************************** */
int CONTOPRIM (real **ucons, real **uprim, int ibeg, int iend, char *flag)
/*
 *
 * PURPOSE
 *
 *    Convert conservative vector to primitive vector
 *
 ********************************************************************* */
{
  int   i, nv, status=0;
  real  tau, m2, KIN, rhog1, rho;
  real  temp, eth, pressure;
  real  *u, *v;
  real  vmax;
  int j;


  #if EOS == IDEAL
   double gmm1;
   gmm1 = gmm - 1.0;
  #endif


  for (i = ibeg; i <= iend; i++) {

    flag[i] = 0;
    u = ucons[i];
    v = uprim[i];

    m2  = EXPAND(u[MX]*u[MX], + u[MY]*u[MY], + u[MZ]*u[MZ]);

  /* ------------------------------------------------
       check for unphysical densities and energies 
     ------------------------------------------------ */

    if (u[DN] < 0.0) {
      print ("! CONTOPRIM: negative density (%8.2e), ",u[DN]);
      WHERE (i, NULL);
      u[DN]    = SMALL_DN;
      flag[i] |= DN_FAIL;

      /* Note: flooring the density can cause unphysically large
      velocities, so here we check if the new velocity exceeds the
      ceiling. If it does, we set it to the ceiling. */

      vmax = abs(u[MX]/u[DN]);
      if (CH_SPACEDIM > 1)
	vmax = vmax > abs(u[MY]/u[DN]) ? vmax : abs(u[MY]/u[DN]);
      if (CH_SPACEDIM > 2)
	vmax = vmax > abs(u[MZ]/u[DN]) ? vmax : abs(u[MZ]/u[DN]);
      if (vmax > CEIL_VA) {
	u[MX] = CEIL_VA*u[DN]*(u[MX]/u[DN]/vmax);
	if (CH_SPACEDIM > 1) {
	  u[MY] = CEIL_VA*u[DN]*(u[MY]/u[DN]/vmax);
	  if (CH_SPACEDIM > 2) {
	    u[MZ] = CEIL_VA*u[DN]*(u[MZ]/u[DN]/vmax);
	  } 
	}
      }
    }

    v[DN] = rho = u[DN];
    KIN   = 0.5*m2/u[DN];


    #if EOS == IDEAL
     if (u[EN] < 0.0) {
       KIN = 0.5*m2/u[DN];
       WARNING(
         print("! CONTOPRIM: negative energy (%8.2e), ", u[EN]);
         WHERE (i, NULL);
       )
       u[EN]    = SMALL_PR/gmm1 + KIN;
       flag[i] |= EN_FAIL;
     }
    #endif

    #if EOS == IDEAL
    v[PR] = gmm1*(u[EN] - KIN);
    #endif   // IF EOS == IDEAL

    tau   = 1.0/u[DN];

    #if ENTROPY_SWITCH == YES
     rhog1 = pow(v[DN], gmm1);
     if (flag[i]){
       v[PR] = u[ENTR]*rhog1; 
       u[EN] = v[PR]/gmm1 + KIN;     
     } else {
       u[ENTR] = v[PR]/rhog1;
     }
    #endif

/* ------------------------------------------
     check pressure positivity 
   ------------------------------------------ */

    #if EOS == IDEAL
    if (v[PR] < 0.0) {
      WARNING(
        print ("! CONTOPRIM: negative pressure (%8.2e), ",v[PR]);
        WHERE (i, NULL);
      )

      v[PR]    = SMALL_PR;
      u[EN]    = v[PR]/gmm1 + KIN;
      flag[i] |= PR_FAIL;
    }
    #endif

/* ---------------------------------------
          complete conversion 
   --------------------------------------- */
 
    EXPAND(v[VX] = u[MX]*tau; ,
           v[VY] = u[MY]*tau; ,
           v[VZ] = u[MZ]*tau;)

    for (nv = NFLX; nv < NVAR; nv++) v[nv] = u[nv]*tau;

    status |= flag[i];
  }

  return(status);
}

/*void MY_SOURCE (const Data *d, real dt, Time_Step *Dts, Grid *GXYZ)
     //add my mass source term
     {
        int k,j,i;
        real x1,x2,z,w;//coordinate
        real keplervelocity,kepleromega;
        real scaleheight,Temperature,C_ISO;
        DOM_LOOP(k,j,i){
            x1=GXYZ[IDIR].x[i];
            x2=GXYZ[JDIR].x[j];
            z=GXYZ[KDIR].x[k];
            w=sqrt(x1*x1+x2*x2);//radius
            keplervelocity = sqrt(6.67e-8*2.e33/w);
            kepleromega = keplervelocity/w;
            if ((w>1.5e14)&&(w<3.e14))
            {
               if ((z>5.90625e13)&&(z<6.0e13))//&&(x2>2.1e14)&&(x2<2.4e14))
                  {
                    d->Vc[DN][k][j][i] = 1.e-12;//update density
                    d->Vc[VX][k][j][i] = x2*kepleromega;
                    d->Vc[VY][k][j][i] = -x1*kepleromega;//anti-azimuthal
                    d->Vc[VZ][k][j][i] = -keplervelocity/3.;//down v
                    d->Vc[PR][k][j][i] = 4.e-3;   //pressure den*cs*cs 100K
                  }
       
               if ((z>-6.e13)&&(z<-5.90625e13))//&&(x2<-2.1e14)&&(x2>-2.4e14))
                  {
                    //printf("TMD!!!!");
                    d->Vc[DN][k][j][i] = 1.e-12;//update density
                    d->Vc[VX][k][j][i] = x2*kepleromega;
                    d->Vc[VY][k][j][i] = -x1*kepleromega;//anti-azimuthal
                    d->Vc[VZ][k][j][i] = keplervelocity/3.;//up v
                    d->Vc[PR][k][j][i] = 4.e-3;   //pressure den*cs*cs 100K
                  }
            }
        }//end of loop
     }

*/


