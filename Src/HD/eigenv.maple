##############################################################
#                 Maple script for 
#
#          HD CHARACTERISTIC DECOMPOSITION
#
#  find:  primitive left eigenvectors
#         conservative right eigenvectors for the 
#         Euler 3D equations with 2 passive scalars
#
#
#  Variable order:  rho, u, v, w, p, T1, T2
#
#  Date: Setp 13, 2003
###############################################################

restart;
with(linalg);

A := matrix([[ u , rho   , 0 , 0 , 0   , 0 , 0],
             [ 0 , u     , 0 , 0 ,1/rho, 0 , 0], 
             [ 0 , 0     , u , 0 ,  0  , 0 , 0],
             [ 0 , 0     , 0 , u ,  0  , 0 , 0],
             [ 0 ,rho*a^2, 0 , 0 ,  u  , 0 , 0],
             [ 0 , 0     , 0 , 0 ,  0  , u , 0],
             [ 0 , 0     , 0 , 0 ,  0  , 0 , u]]);

scrh := eigenvectors(A);

lambda := Vector([u-a,u,u,u,u+a,u,u]):
LD := Matrix(1..7,1..7,lambda,shape=diagonal);

# primitive right eigenvectors;
#
# It is derived from scrh, but
# with the u-a, u+a eigenvectors divided by  -2 and +2

Rp := matrix([[    1    ,  1, 0    , 0    ,   1  , 0, 0],
              [  -a/rho ,  0, 0    , 0    , a/rho, 0, 0], 
              [   0     ,  0, 0    , 1/rho,  0   , 0, 0],
              [   0     ,  0, 1/rho, 0    ,  0   , 0, 0],
              [   a*a   ,  0, 0    , 0    , a*a  , 0, 0],
              [   0     ,  0, 0    , 0    ,  0   , 1, 0],
              [   0     ,  0, 0    , 0    ,  0   , 0, 1]]);

d  := det(Rp):
Lp := inverse(Rp);

# Verify 

C   := multiply(Rp,multiply(LD,Lp)):
simplify(C);

#
# Jacobian (prim to con)
#

dUdW := matrix([[ 1 , 0 , 0  , 0 , 0 , 0 , 0 ],
                [ u ,rho, 0  , 0 , 0 , 0 , 0 ], 
                [ v , 0 , rho, 0 , 0 , 0 , 0 ],
                [ w , 0 , 0  ,rho, 0 , 0 , 0 ],
                [ (u*u+v*v+w*w)/2, rho*u , rho*v , rho*w  ,  1/(gamma-1), 0, 0],
                [ T1 , 0   , 0   , 0 , 0 , rho, 0],
                [ T2 , 0   , 0   , 0 , 0 ,  0 ,rho]]);
 

# Find conservative eigenvectors:

Rc := multiply(dUdW,Rp);

# ------------------------------------------------------------
#
#  Obtain left conservative eigenvectors from the right ones
#
# ------------------------------------------------------------

restart;
with(linalg);
unprotect(Gamma);
K := (u*u+v*v+w*w)/2;
H := a^2/Gamma + K;

Rc := matrix([[    1  ,  1, 0, 0,   1  ],
              [  u-a  ,  u, 0, 0,  u+a ], 
              [   v   ,  v, 1, 0,   v  ],
              [   w   ,  w, 0, 1,   w  ],
              [  H-u*a,  K, v, w, H+u*a]]);

Lc := simplify(evalm(2*(H-K)*inverse(Rc)));

# -----------------------------------------------------
#   characteristic decomposition for the isentropic
#   equations. Version # 1, uses
#
#   W (primitive) = (rho, v, s) with s = p/rho^gamma
#
# -----------------------------------------------------

restart;
with(linalg):
with(VectorCalculus):

A := matrix([[ u       , rho , 0 , 0 , 0     , 0 , 0],
             [ a^2/rho , u   , 0 , 0 ,p/s/rho, 0 , 0], 
             [ 0       , 0   , u , 0 ,  0    , 0 , 0],
             [ 0       , 0   , 0 , u ,  0    , 0 , 0],
             [ 0       , 0   , 0 , 0 ,  u    , 0 , 0],
             [ 0       , 0   , 0 , 0 ,  0    , u , 0],
             [ 0       , 0   , 0 , 0 ,  0    , 0 , u]]);

scrh := eigenvectors(A);

lambda := Vector([u-a,u,u,u,u+a,u,u]):
LD     := Matrix(1..7,1..7,lambda,shape=diagonal);

# primitive right eigenvectors;
#
# It is derived from scrh, by multiplying 
# the (u-a) and (u+a) eigenvector by a/rho

Rp := matrix([[  1     ,  1       , 0    , 0 , 1    , 0, 0],
              [ -a/rho ,  0       , 0    , 0 , a/rho, 0, 0], 
              [  0     ,  0       , 1    , 0 , 0    , 0, 0],
              [  0     ,  0       , 0    , 1 , 0    , 0, 0],
              [  0     ,  -a^2*s/p, 0    , 0 , 0    , 0, 0],
              [  0     ,  0       , 0    , 0 , 0    , 1, 0],
              [  0     ,  0       , 0    , 0 , 0    , 0, 1]]);

d  := det(Rp):
Lp := inverse(Rp);

# Verify 

C   := multiply(Rp,multiply(LD,Lp)):
simplify(C);

#
# Jacobian (prim to con)
#

dUdW := Jacobian( [rho, rho*u, rho*v, rho*w, rho*s, rho*T1, rho*T2], 
                  [rho,u,v,w,s, T1, T2]);

#dUdW := matrix([[ 1  ,  0 , 0  , 0 ,  0 , 0  ,  0],
#                [ u  , rho, 0  , 0 ,  0 , 0  ,  0], 
#                [ v  ,  0 , rho, 0 ,  0 , 0  ,  0],
#                [ w  ,  0 , 0  ,rho,  0 , 0  ,  0],
#                [ s  ,  0 , 0  , 0 , rho, 0  ,  0],
#                [ T1 ,  0 , 0   , 0 , 0 , rho,  0],
#                [ T2 ,  0 , 0   , 0 , 0 ,  0 , rho]]);
 
# Find conservative eigenvectors:

Rc := multiply(dUdW,Rp);

# -----------------------------------------------------
#   characteristic decomposition for the isentropic
#   equations. Version # 2, uses
#
#   W (primitive) = (rho, v, p) 
#
# -----------------------------------------------------

restart;
with(linalg);
with(VectorCalculus):

A := matrix([[ u , rho   , 0 , 0 , 0   , 0 , 0],
             [ 0 , u     , 0 , 0 ,1/rho, 0 , 0], 
             [ 0 , 0     , u , 0 ,  0  , 0 , 0],
             [ 0 , 0     , 0 , u ,  0  , 0 , 0],
             [ 0 ,rho*a^2, 0 , 0 ,  u  , 0 , 0],
             [ 0 , 0     , 0 , 0 ,  0  , u , 0],
             [ 0 , 0     , 0 , 0 ,  0  , 0 , u]]);

scrh := eigenvectors(A);

lambda := Vector([u-a,u,u,u,u+a,u,u]):
LD := Matrix(1..7,1..7,lambda,shape=diagonal);

# primitive right eigenvectors;
#
# It is derived from scrh, but
# with the u-a, u+a eigenvectors divided by  -2 and +2

Rp := matrix([[    1    ,  1, 0    , 0    ,   1  , 0, 0],
              [  -a/rho ,  0, 0    , 0    , a/rho, 0, 0], 
              [   0     ,  0, 0    , 1/rho,  0   , 0, 0],
              [   0     ,  0, 1/rho, 0    ,  0   , 0, 0],
              [   a*a   ,  0, 0    , 0    , a*a  , 0, 0],
              [   0     ,  0, 0    , 0    ,  0   , 1, 0],
              [   0     ,  0, 0    , 0    ,  0   , 0, 1]]);

d  := det(Rp):
Lp := inverse(Rp);

# Verify 

C   := multiply(Rp,multiply(LD,Lp)):
simplify(C);

#
# Jacobian (prim to con)
#

#s := p/rho^gamma;
#dUdW := Jacobian( [rho, rho*u, rho*v, rho*w, rho*s, rho*T1, rho*T2], 
#                  [rho,u,v,w,p, T1, T2]);

dUdW := matrix([[ 1       , 0 , 0  , 0 , 0 , 0 , 0 ],
                [ u       ,rho, 0  , 0 , 0 , 0 , 0 ], 
                [ v       , 0 , rho, 0 , 0 , 0 , 0 ],
                [ w       , 0 , 0  ,rho, 0 , 0 , 0 ],
                [ -(gamma-1)*s, 0 , 0  , 0 ,s*rho/p, 0 , 0],
                [ T1 , 0   , 0   , 0 , 0 , rho, 0],
                [ T2 , 0   , 0   , 0 , 0 ,  0 ,rho]]);
 
# Find conservative eigenvectors:

Rc := multiply(dUdW,Rp);

########################################################
#   characteristic decomposition for the isothermal
#   Euler equations.
#
#   Date: 23/03/2012
#
########################################################

# -- primitive matrix form
restart;
with(LinearAlgebra);
Ap := Matrix([[ u      , rho   , 0 , 0],
              [ c^2/rho, u     , 0 , 0], 
              [ 0      , 0     , u , 0],
              [ 0      , 0     , 0 , u]]);
(lambda, Rp) := Eigenvectors(Ap);
Lp := MatrixInverse(Rp);

# -- conservative matrix form
restart;
with(VectorCalculus);
with(LinearAlgebra);

dFdU := Jacobian([m[x], m[x]^2/rho + c^2*rho, m[y]*m[x]/rho, m[z]*m[x]/rho],
                 [rho,m[x],m[y],m[z]]); 
m[x] := rho*u;
m[y] := rho*v;
m[z] := rho*w;

(lambda, Rc) := Eigenvectors(dFdU);
Lc := MatrixInverse(Rc);
