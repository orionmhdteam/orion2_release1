#include "orion2.h"

#define MAX_ITER   5
#define small_p       1.e-9
#define small_rho     1.e-9

/* ***************************************************************************** */
void TWO_SHOCK (const State_1D *state, int beg, int end, 
                real *cmax, Grid *grid)
/*
 *
 * NAME
 *
 *   RIEMANN
 *
 *
 * PURPOSE
 *
 *  - Solve riemann problem for the Euler equations 
 *    using the two-shock approximation
 * 
 *     Reference:    "FLASH: an Adaptive Mesh Hydrodynamics Code for Modeling
 *                    Astrophysical Thermonuclear Flashes"
 *                    Fryxell et al,
 *                    The Astrophysical Journal Supplement Series 
 *                    131:273-334, 2000
 *                     --> see page 292-294
 *   
 *  - On input, it takes left and right primitive state
 *    vectors state->vL and state->vR at zone edge i+1/2;
 *    On output, return flux and pressure vectors at the
 *    same interface.
 *
 *  - Also, compute maximum wave propagation speed (cmax) 
 *    for  explicit time step computation
 *  
 *
 * LAST_MODIFIED
 *
 *   April 4th 2006, by Andrea Mignone  (mignone@to.astro.it)
 *
 *
 ******************************************************************************* */
{
#if EOS == IDEAL
  int i, iter, nv;

  real   vxl, taul, cl, *ql;
  real   vxr, taur, cr, *qr;
  real   zs, taus, cs, *qs;
  real   pstar, ustar, rho_star, cstar;
  real   sigma, lambda_s, lambda_star, zeta, dp;
  real   g1_g, scrh1, scrh2, scrh3, scrh4;
  static real  **ws, **us;
  static double **fL, **fR, *pL, *pR;
  double *uL, *uR;

  if (ws == NULL){
    ws = Array_2D(NMAX_POINT, NVAR, double);  
    us = Array_2D(NMAX_POINT, NVAR, double);  

    fL = Array_2D(NMAX_POINT, NFLX, double);  
    fR = Array_2D(NMAX_POINT, NFLX, double);  
    pL = Array_1D(NMAX_POINT, double);  
    pR = Array_1D(NMAX_POINT, double);  
  }

/*  ---------------------------------------------------------------
                       SOLVE RIEMANN PROBLEM
    ---------------------------------------------------------------   */

  g1_g = 0.5*(gmm + 1.0)/gmm;
  for (i = beg; i <= end; i++) {

    uL = state->uL[i];
    uR = state->uR[i];

    #if SHOCK_FLATTENING == MULTID   

    /* ---------------------------------------------
       Switch to HLL in proximity of strong shocks.
      --------------------------------------------- */

     if (CHECK_ZONE(i, FLAG_HLL) || CHECK_ZONE(i+1, FLAG_HLL)){
       HLL_SPEED (state->vL, state->vR, &cl - i, &cr - i, i, i);
       FLUX  (state->uL, state->vL, fL, pL, i, i);
       FLUX  (state->uR, state->vR, fR, pR, i, i);

       cs    = dmax(fabs(cl), fabs(cr));
       cmax[i] = cs;
       cl    = dmin(0.0, cl);
       cr    = dmax(0.0, cr);
       scrh1  = 1.0/(cr - cl);
       for (nv = NFLX; nv--; ){
         state->flux[i][nv]  = cl*cr*(uR[nv] - uL[nv])
                            +  cr*fL[i][nv] - cl*fR[i][nv];
         state->flux[i][nv] *= scrh1;
       }
       state->press[i] = (cr*pL[i] - cl*pR[i])*scrh1;
       continue;
     }
    #endif
  
    qr = state->vR[i];
    ql = state->vL[i];
      
    cl = sqrt(gmm*ql[PR]*ql[DN]);
    cr = sqrt(gmm*qr[PR]*qr[DN]);

    taul = 1.0/ql[DN];
    taur = 1.0/qr[DN];

  /* -- First guess -- */

    pstar = qr[PR] - ql[PR] - cr*(qr[V1] - ql[V1]);
    pstar = ql[PR] + pstar*cl/(cl + cr);
    pstar = dmax(small_p , pstar);

  /* -- Begin to iterate --  */

    for (iter = 1; iter <= MAX_ITER; iter++) {
      vxl = cl*sqrt(1.0 + g1_g*(pstar - ql[PR])/ql[PR]);
      vxr = cr*sqrt(1.0 + g1_g*(pstar - qr[PR])/qr[PR]);

      scrh1 = vxl*vxl;
      scrh1 = 2.0*scrh1*vxl/(scrh1 + cl*cl);
      
      scrh2 = vxr*vxr;
      scrh2 = 2.0*scrh2*vxr/(scrh2 + cr*cr);

      scrh3 = ql[V1] - (pstar - ql[PR])/vxl;
      scrh4 = qr[V1] + (pstar - qr[PR])/vxr;

      dp = scrh1*scrh2/(scrh1 + scrh2)*(scrh4 - scrh3);

      pstar -= dp;
/*
      scrh1 = 4.0*taul*vxl*vxl;
      scrh1 = -scrh1*vxl/(scrh1 - (gmm + 1.0)*(pstar - ql[PR]));
      scrh2 = 4.0*taur*vxr*vxr;
      scrh2 =  scrh2*vxr/(scrh2 - (gmm + 1.0)*(pstar - qr[PR]));

      scrh3 = ql[V1] - (pstar - ql[PR])/vxl;
      scrh4 = qr[V1] + (pstar - qr[PR])/vxr;
      dp    = (scrh4 - scrh3)*(scrh1*scrh2)/(scrh2 - scrh1);
      pstar = pstar  + dp;
*/

      pstar = dmax (small_p, pstar);
      MAX_RIEM = dmax(MAX_RIEM,iter);
      if (fabs(dp/pstar) < 1.e-6) break;
/*
      if (iter == MAX_ITER) {
        print ("Rieman solver not converging  ps %12.6e  dp %12.6e %12.6e  %12.6e %12.6e %12.6e \n",
           pstar,dp, scrh1, scrh2, scrh3, scrh4);
      }
*/
    }

/*            End iterating           */

    scrh3 = ql[V1] - (pstar - ql[PR])/vxl;
    scrh4 = qr[V1] + (pstar - qr[PR])/vxr;
    ustar = 0.5*(scrh3 + scrh4);

    if (ustar > 0.0) {
      sigma = 1.0;
      taus  = taul;
      cs    = cl*taul;
      zs    = vxl;
      qs    = ql;
    }else{
      sigma = -1.0;
      taus  = taur;
      cs    = cr*taur;
      zs    = vxr;
      qs    = qr;
    }
        
    rho_star = taus - (pstar - qs[PR])/(zs*zs);
    rho_star = dmax(small_rho,1.0/rho_star);
    cstar    = sqrt(gmm*pstar/rho_star);
     if (pstar < qs[PR]){
      lambda_s    = cs    - sigma*qs[V1];
      lambda_star = cstar - sigma*ustar;
    }else{
      lambda_s    = lambda_star = zs*taus  - sigma*qs[V1];
    }      

  /* -- Now find solution -- */

    if (lambda_star > 0.0){
      
      ws[i][DN] = rho_star;
      ws[i][V1] = ustar;  
      ws[i][PR] = pstar;
                
    } else if (lambda_s < 0.0){
      
      ws[i][DN] = qs[DN];
      ws[i][V1] = qs[V1];
      ws[i][PR] = qs[PR];

    } else {  /*   linearly interpolate rarefaction fan  */

      scrh1 = dmax(lambda_s - lambda_star, lambda_s + lambda_star);
      zeta  = 0.5*(1.0 + (lambda_s + lambda_star)/scrh1);

      ws[i][DN] = zeta*rho_star + (1.0 - zeta)*qs[DN];
      ws[i][V1] = zeta*ustar    + (1.0 - zeta)*qs[V1];
      ws[i][PR] = zeta*pstar    + (1.0 - zeta)*qs[PR];
    }  
        
  /* -- transverse velocities are advected --  */

    EXPAND(                    , 
           ws[i][V2] = qs[V2]; ,  
           ws[i][V3] = qs[V3];)

  /* -- compute flux -- */

    PRIMTOCON (ws, us, i, i);
    FLUX(us, ws, state->flux, state->press, i, i);

  /* -- compute max speed -- */

    cstar = sqrt(gmm*ws[i][PR]/ws[i][DN]);
    scrh1 = fabs(ws[i][V1])/cstar;
    MAX_MACH_NUMBER = dmax(scrh1, MAX_MACH_NUMBER);
    scrh1 = fabs(ws[i][V1]) + cstar;
    cmax[i] = scrh1;
  }

  #if ARTIFICIAL_VISCOSITY == YES
   VISC_FLUX (state, beg, end, grid);
  #endif
#endif /* EOS == IDEAL */
}

#undef MAX_ITER   
#undef small_p     
#undef small_rho     
