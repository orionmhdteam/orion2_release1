#include "orion2.h"

/* ********************************************************************* */
void LF_SOLVER (const State_1D *state, int beg, int end, 
                real *cmax, Grid *grid)
/*
 *
 * NAME
 *
 *   LF_SOLVER
 *
 *
 * PURPOSE
 *
 *   - Solve Riemann problem using the Lax-Friedrichs 
 *     Rusanov solver:
 *
 *     F(i+1/2) = [FL + FR - c*(UR - UL)]*0.5
 *
 *     where c = max_speed[(UR + UL)*0.5]
 *
 *   - On input, it takes left and right primitive state
 *     vectors state->vL and state->vR at zone edge i+1/2;
 *     On output, return flux and pressure vectors at the
 *     same interface.
 *
 *   - Also, compute maximum wave propagation speed (cmax) 
 *     for  explicit time step computation
 *  
 *
 *
 * LAST_MODIFIED
 *
 *   April 4th 2006, by Andrea Mignone  (mignone@to.astro.it)
 *
 *
 **************************************************************************** */
{
  int    nv, i;
  static real **fL, **fR, **vRL;
  static real *cRL_min, *cRL_max, *pL, *pR;
  double *uR, *uL;
  
  if (fR == NULL){
    fR  = Array_2D(NMAX_POINT, NFLX, double);
    fL  = Array_2D(NMAX_POINT, NFLX, double);
    vRL = Array_2D(NMAX_POINT, NFLX, double);
    cRL_min = Array_1D(NMAX_POINT, double);
    cRL_max = Array_1D(NMAX_POINT, double);
    pR  = Array_1D(NMAX_POINT, double);
    pL  = Array_1D(NMAX_POINT, double);
  }

/* -------------------------------------------------------------------
                     i  -- >   i + 1/2
   ------------------------------------------------------------------- */

  for (i = beg; i <= end; i++)           {
  for (nv = NFLX; nv--;  ) {
    vRL[i][nv] = 0.5*(state->vL[i][nv] + state->vR[i][nv]);
  }}

/* -----------  COMPUTE MAX EIGENVALUE -------------------------  */

  MAX_CH_SPEED (vRL, cRL_min, cRL_max, beg, end);

  for (i = beg; i <= end; i++) {
    cRL_max[i] = dmax(fabs(cRL_max[i]), fabs(cRL_min[i]));
    /*PS: replace using old style cmax
      cmax[i] = cRL_max[i];
    */
    *cmax  = dmax(*cmax, cRL_max[i]);
    #if EOS == IDEAL
     MAX_MACH_NUMBER = dmax(MAX_MACH_NUMBER, fabs(vRL[i][V1])/sqrt(gmm*vRL[i][PR]/vRL[i][DN]));
    #elif EOS == ISOTHERMAL
     MAX_MACH_NUMBER = dmax(MAX_MACH_NUMBER, fabs(vRL[i][V1])/C_ISO);
    #endif
  }

  FLUX (state->uL, state->vL, fL, pL, beg, end);
  FLUX (state->uR, state->vR, fR, pR, beg, end);

  for (i = beg; i <= end; i++) {
    uR = state->uR[i];
    uL = state->uL[i];

    for (nv = NFLX; nv--;  ) {
      state->flux[i][nv] = 0.5*(fL[i][nv] + fR[i][nv] - cRL_max[i]*(uR[nv] - uL[nv]));
    }
    state->press[i] = 0.5*(pL[i] + pR[i]);
  }
}
