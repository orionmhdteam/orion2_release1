#include"orion2.h"

/* ****************************************************************** */
Riemann_Solver *SET_SOLVER (const char *solver)
/*
 *
 * PURPOSE
 *
 *   return a pointer to a riemann solver function
 *
 *
 *
 *
 ********************************************************************* */
{
  
  #ifdef FINITE_DIFFERENCE
   return (&FD_FLUX);
  #endif

/* ------------------------------------------------------
       Set Pointers for SOLVERS 
   ------------------------------------------------------ */

  #if EOS == IDEAL
   if ( !strcmp(solver, "two_shock"))  return (&TWO_SHOCK);
   else if (!strcmp(solver, "tvdlf"))  return (&LF_SOLVER);
   else if (!strcmp(solver, "roe"))    return (&ROE_SOLVER);
   else if (!strcmp(solver, "ausm+"))  return (&AUSMp);
   else if (!strcmp(solver, "hlle") || 
            !strcmp(solver, "hll"))    return (&HLL_SOLVER);
   else if (!strcmp(solver, "hllc"))   return (&HLLC_SOLVER);
  #elif EOS == ISOTHERMAL
   if (!strcmp(solver, "tvdlf"))       return (&LF_SOLVER);
   else if (!strcmp(solver, "roe"))    return (&ROE_SOLVER);
   else if (!strcmp(solver, "hlle") || 
            !strcmp(solver, "hll"))    return (&HLL_SOLVER);
   else if (!strcmp(solver, "hllc"))   return (&HLLC_SOLVER);
  #endif

  print1 ("\n! Solver '%s' is not available.\n", solver);
  QUIT_ORION2(1);
}
