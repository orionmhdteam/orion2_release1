#include "orion2.h"

/* ************************************************************************ */
int GET_NGHOST (Input *ini)
/*! 
 *
 * Compute the number of ghost zones, depending on the interpolation 
 * algorithm.
 *
 *
 ************************************************************************** */
{
  int nv, nghost ;
  Limiter *limiter[NVAR];
  
  #if INTERPOLATION == FLAT

   nghost = 2;

  #elif INTERPOLATION == LINEAR || INTERPOLATION == LIM3

   nghost = 2;
   SET_LIMITER (limiter);

   for (nv = 0; nv < NVAR; nv++){
     if (limiter[nv] == fourth_order_lim) nghost = 3;
   }   

   #if LIMITER == TRIAD_LIM 
    nghost = 3;  
   #endif

  #elif INTERPOLATION == LINEAR_MULTID

   nghost = 2;

  #elif INTERPOLATION == CENO3 

   SET_LIMITER (limiter);

   nghost = 3;
/*  
  for (nv = 0; nv < NVAR; nv++){
     if (limiter[nv] == triad_lim) nghost = 4;
   }   
*/
  #elif INTERPOLATION == PARABOLIC || INTERPOLATION == WENO5 || \
        INTERPOLATION == mpPPM     || INTERPOLATION == MP5

   nghost = 4;

  #elif INTERPOLATION == WENO7

   nghost = 5;

  #elif INTERPOLATION == WENO9

   nghost = 6;

  #endif

  #if SHOCK_FLATTENING == ONED

   nghost = dmax(4, nghost);

  #endif

/* ------------------------------------------------------
    The MULTID shock flattening only need 2 ghost zones.
    However for axisymmetric simulations with CTU 
    3 zones will ensure that flag[][][] will remain 
    symmetric around the axis.
   ------------------------------------------------------ */

  #if SHOCK_FLATTENING == MULTID
   nghost = dmax(4, nghost);
  #endif

/* ----------------------------------------------------
    The following should operate on the static grid 
    version of the code. Add an extra row of boundary
    zones if CTU+CT+userdef boundary are selected.
    At least 3 ghost zones.
   ---------------------------------------------------- */

  #ifndef CH_SPACEDIM    /* -- do not use with chombo -- */
  #ifdef SINGLE_STEP 
  #ifdef STAGGERED_MHD
   #if DIMENSIONAL_SPLITTING == NO
    nghost++;
   #endif
  #endif
  #endif
  #endif

    /*--------------------------------------------------
      ORION2 requires 4 ghost zones for the MHD to 
      function
      --------------------------------------------------*/

    nghost = 4;

  return (nghost);
}

