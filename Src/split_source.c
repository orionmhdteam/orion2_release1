#include "orion2.h"

/* **************************************************************************  */
void SPLIT_SOURCE (const Data *d, double dt, Time_Step *Dts, Grid *grid)
/*
 *
 * PURPOSE
 *
 *   Main driver for handling source terms as a separate
 *   step using operator splitting.
 *
 *   Source terms may be:
 *
 *    - optically thin radiative losses (cooling)
 *    - Diffusion operators: 
 *       * resistivity 
 *       * Thermal conduction
 *
 *
 *
 *
 ***************************************************************************** */
{
  int i, j, k, nv;
  static real **v;
  real t_save;

/*  ---- GLM SOURCE TERMS ----  */

  #ifdef PSI_GLM
   GLM_SOURCE (d->Vc, grid);
  #endif

/*  ----  Optically thin Cooling sources  ----  */

  #if INCLUDE_COOLING != NO
   #if INCLUDE_COOLING == POWER_LAW  /* -- solve exactly -- */
    POWER_LAW_COOLING (d->Vc, dt, Dts, grid);
   #else
    COOLING_SOURCE (d, dt, Dts, grid);
   #endif
  #endif

/* ---- PARABOLIC TERMS ---- */

  #if RESISTIVE_MHD == SUPER_TIME_STEPPING
   ADD_OHM_HEAT(d, delta_t*0.5, grid);
   STS (d, Dts, grid);
   ADD_OHM_HEAT(d, delta_t*0.5, grid);
  #endif
                                                                                                                                                                             
  #if THERMAL_CONDUCTION == SUPER_TIME_STEPPING
   STS (d, Dts, grid);
  #endif

}

