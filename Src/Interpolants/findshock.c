#include"orion2.h"

#ifndef EPS_P
 #define EPS_P 5.0
#endif

/* *************************************************************** */
void FIND_SHOCK (const Data *d, Grid *grid)
/*
 *
 *
 * PURPOSE
 *
 *  Check if strong shocks are present in the computational domain.  
 *  If so, revert to minmod limiter in the interpolation 
 *  routines and to HLL in the Riemann solver.
 *
 *  Example: if "X" marks the shock, then flag zones as follows:
 * 
 *  |---|---|---|---|---|---| 
 *            X
 *        M   M   M
 *          H   H
 *
 *  M = FLAG_MINMOD
 *  H = FLAG_HLL
 *
 *  In more than 1D, the flag strategy is extended to neighbor
 *  zones.
 *  
 ***************************************************************** */
{
  int  i, j, k, nv;
  int  ibeg, jbeg, kbeg;
  int  iend, jend, kend;
  int  ip, jp, kp;
  real dpx, dvx, pxmin;
  real dpy, dvy, pymin;
  real dpz, dvz, pzmin;
  real divv, pmin, gradp, scrh;
  real ***rho, ***vx, ***vy, ***vz, ***pr;
  double *x, *y, *z;
  char ***flag;
  real *dx, *dy, *dz, rp2, rm2;
  static real *r, *s;
  
  rho = pr = d->Vc[DN];
  EXPAND(vx = d->Vc[VX];  ,
         vy = d->Vc[VY];  ,
         vz = d->Vc[VZ];)
  #if EOS != ISOTHERMAL
   pr = d->Vc[PR];
  #endif

  flag = d->flag;

  if (r == NULL) {
    r    = Array_1D(NMAX_POINT, double);
    s    = Array_1D(NMAX_POINT, double);
    ITOT_LOOP(i) r[i] = fabs(grid[IDIR].x[i]);
    JTOT_LOOP(j) s[j] = fabs(sin(grid[JDIR].x[j]));
  }

/* -- get the maximum allowable stencil for a linear interpolants -- */

  jbeg = jend = kbeg = kend = 0;

  D_EXPAND(ibeg = 1; iend = NX_TOT - 2;  ,   
           jbeg = 1; jend = NY_TOT - 2;  ,  
           kbeg = 1; kend = NZ_TOT - 2;)

  dx = grid[IDIR].dx; x = grid[IDIR].x;
  dy = grid[JDIR].dx; y = grid[JDIR].x;
  dz = grid[KDIR].dx; z = grid[KDIR].x;

  for (k = kbeg; k <= kend; k++){ 
  for (j = jbeg; j <= jend; j++){ 
  for (i = ibeg; i <= iend; i++){
/*
flag[k][j][i] |= FLAG_MINMOD;
flag[k][j][i] |= FLAG_HLL;
continue;
*/
    #if GEOMETRY == CARTESIAN

     D_EXPAND(dvx = vx[k][j][i + 1] - vx[k][j][i - 1];  ,
              dvy = vy[k][j + 1][i] - vy[k][j - 1][i];  ,
              dvz = vz[k + 1][j][i] - vz[k - 1][j][i];)

    #elif GEOMETRY == CYLINDRICAL

     D_EXPAND(dvx = (  fabs(r[i + 1])*vx[k][j][i + 1] 
                     - fabs(r[i - 1])*vx[k][j][i - 1])/fabs(r[i]);  ,
              dvy = vy[k][j + 1][i] - vy[k][j - 1][i];  ,
              dvz = vz[k + 1][j][i] - vz[k - 1][j][i];)

    #elif GEOMETRY == POLAR

     D_EXPAND(dvx = (  r[i + 1]*vx[k][j][i + 1] 
                     - r[i - 1]*vx[k][j][i - 1])/r[i];         ,
              dvy = (vy[k][j + 1][i] - vy[k][j - 1][i])/r[i];  ,
              dvz = vz[k + 1][j][i] - vz[k - 1][j][i];)

    #elif GEOMETRY == SPHERICAL

     rp2 = r[i + 1]*r[i + 1];
     rm2 = r[i - 1]*r[i - 1];

     D_EXPAND(dvx = (  rp2*vx[k][j][i + 1] 
                     - rm2*vx[k][j][i - 1])/(r[i]*r[i]);       ,
              dvy = (  s[j + 1]*vy[k][j + 1][i] 
                     - s[j - 1]*vy[k][j - 1][i])/(r[i]*s[j]);  ,
              dvz = (vz[k + 1][j][i] - vz[k - 1][j][i])/(r[i]*s[j]);)

    #endif

    divv = D_EXPAND(dvx, + dvy*dx[i]/dy[j], + dvz*dx[i]/dz[k]);
 
    if (divv < 0.0){
      D_EXPAND(pxmin = dmin(pr[k][j][i + 1], pr[k][j][i - 1]);  ,  
               pymin = dmin(pr[k][j + 1][i], pr[k][j - 1][i]);  , 
               pzmin = dmin(pr[k + 1][j][i], pr[k - 1][j][i]);)   

      D_EXPAND(dpx = fabs(pr[k][j][i + 1] - pr[k][j][i - 1])/pxmin;  ,  
               dpy = fabs(pr[k][j + 1][i] - pr[k][j - 1][i])/pymin;  , 
               dpz = fabs(pr[k + 1][j][i] - pr[k - 1][j][i])/pzmin;)   
                
      D_EXPAND(gradp  = dpx;   ,
               gradp += dpy;   ,
               gradp += dpz;)

      if (gradp > EPS_P) {

        flag[k][j][i]   |= FLAG_MINMOD;

        D_EXPAND(
          flag[k][j][i+1] |= FLAG_MINMOD;
          flag[k][j][i-1] |= FLAG_MINMOD;  ,
          flag[k][j-1][i] |= FLAG_MINMOD;  
          flag[k][j+1][i] |= FLAG_MINMOD;  ,
          flag[k-1][j][i] |= FLAG_MINMOD;
          flag[k+1][j][i] |= FLAG_MINMOD;)

        flag[k][j][i]   |= FLAG_HLL;

      }
    }
      
  }}}

  #ifdef PARALLEL
   AL_Exchange (flag[0][0], SZ_char);
  #endif
/*
{
  double t0, t1, i0, i1;
  FILE *fp;

  t0 = glob_time;
  t1 = t0 + delta_t;
  i0 = (int)(t0/0.08);
  i1 = (int)(t1/0.08);

  if ((i0 != i1 && ISTEP == 1) || NSTEP == 0) {
    if (NSTEP == 0) fp = fopen("flat.out","wb");
    else {fp = fopen("flat.out","r+b");
          fseek(fp, 0, SEEK_END);
         }
    DOM_LOOP(k,j,i){
      dpx = (double) flag[k][j][i];
      fwrite (&dpx, sizeof(double), 1, fp);
    }
    fclose(fp);
  } 
}
*/

}
#undef EPS_P

