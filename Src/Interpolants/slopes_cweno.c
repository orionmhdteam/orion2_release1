#include "orion2.h"

/* ************************************************************* */
void RECONSTRUCT (const State_1D *state, Grid *grid)
/* 
 *
 *
 *  PURPOSE
 *    
 *   Provide third-order central WENO recontruction, based
 *   on a non-oscillatory piecewise parabolic reconstruction.
 *
 *   Reference paper:
 *
 *   "A third-order semidiscrete central scheme for
 *    conservation laws and convection-diffusion 
 *    equations", A. Kurganov, D. Levy,
 *    SIAM J.Sci.Comput 22, 4 (2000), pp 1461
 * 
 *
 *
 *  LAST MODIFIED
 *
 *   Sept 18, 2005 by A. mignone (mignone@to.astro.it).
 *
 **************************************************************** */
{
  int    nv, i, beg, end;
  static real *dv;
  real **vp, **vm, **vv;
  real cl, cr, cc;
  real d2, dc;
  real ISl, ISr, ISc;
  real alphal, alphar, alphac;
  real wl, wc, wr, scrh;
  real eps = 1.e-6;
  real A, B, C;
  static real **up, **um;

  if (up == NULL){
    up = Array_2D(NMAX_POINT, NVAR, double);
    um = Array_2D(NMAX_POINT, NVAR, double);
  }

  cl = cr = 0.25;
  cc = 0.5;

  beg = grid[DIR].lbeg - 1;
  end = grid[DIR].lend + 1;

  PRIMTOCON (state->v, state->u, beg - 2, end + 2);
/*
  vv = state->v;
  vp = state->vp;
  vm = state->vm;
*/
  vv = state->u;
  vp = up;
  vm = um;

  if (dv == NULL) {
    dv = Array_1D(NMAX_POINT, double);
  }

  #if !(TIME_STEPPING == RK2 || TIME_STEPPING == RK3)
   print1 (" ! CWENO interpolants only works with RK integrators\n");
   QUIT_ORION2 (1);
  #endif

/* ----------------------------------------------------------
    Compute left and right interface values;
    use the CWENO reconstruction in 
    "A third-order semidiscrete central scheme for
     conservation laws and convection-diffusion 
     equations", A. Kurganov, D. Levy,
     SIAM J.Sci.Comput 22, 4, (2000), pp 1461

     Eq. 3.3 and coefficients before eq. 4.6 
   ---------------------------------------------------------- */

   for (nv = 0; nv < NVAR; nv++) {
     for (i = beg - 2; i <= end + 1; i++) {
       dv[i] = vv[i + 1][nv] - vv[i][nv];
     }

     for (i = beg - 1; i <= end + 1; i++) {
       d2 = dv[i] - dv[i - 1];   /*  2nd derivative */
       dc = dv[i] + dv[i - 1]; 

       ISl = dv[i - 1]*dv[i - 1] + eps;
       ISr = dv[i]*dv[i] + eps;
       ISc = 13.0*d2*d2/3.0 + 0.25*dc*dc + eps;

       alphal = cl/(ISl*ISl);
       alphac = cc/(ISc*ISc);
       alphar = cr/(ISr*ISr);

       scrh = 1.0/(alphal + alphac + alphar);
       wl = alphal*scrh;
       wc = alphac*scrh;
       wr = alphar*scrh;
     
       A = vv[i][nv] - wc*d2/12.0;
       B = wr*dv[i] + 0.5*wc*dc + wl*dv[i - 1];
       C = 2.0*wc*d2;

       vp[i][nv] = A + 0.5*B + 0.125*C;
       vm[i][nv] = A - 0.5*B + 0.125*C;
     }
   }
   
   CONTOPRIM (vp, state->vp, beg - 1, end + 1);
   CONTOPRIM (vm, state->vm, beg - 1, end + 1);

/* --------------------------------------------------
            Relativistic Limiter
   -------------------------------------------------- */

  #if PHYSICS == RHD || PHYSICS == RMHD
   RELATIVISTIC_LIM (state, beg, end);
  #endif

/*  -------------------------------------------
        Shock flattening 
    -------------------------------------------  */

  #if SHOCK_FLATTENING == YES 
   FLATTEN_1D (state, beg, end);
  #endif

}

