#include "orion2.h"
#define MIN(X, Y)  ((X) < (Y) ? (X) : (Y))

/* ******************************************************** */
void CHECK_PRIM_STATES (real **vM, real **vP, real **v0,  
                        int beg, int end)
/*
 *
 *  PURPOSE
 *
 *   check if primitive states vL and vR are physically
 *   admissible. 
 *   Replace their value with v0 otherwise.
 *
 *
 ********************************************************** */
{
  int    i, nv, switch_to_1st;
  real scrhm, scrhp;
  real *ac, *ap, *am;

  for (i = beg; i <= end; i++){
  
    switch_to_1st = 0;

    ac = v0[i];
    am = vM[i];
    ap = vP[i];

    /*  ----  Prevent unphysical states by revertin to first
            order in time and space,  i.e.  set dw = 0      ----  */

    #if EOS != ISOTHERMAL
     switch_to_1st = (ap[PR] < 0.) || (am[PR] < 0.) ;
    #endif
    switch_to_1st = switch_to_1st || 
                    (ap[DN] < 0.) || (am[DN] < 0.) ;

    #if RAREFACTION_FLATTEN == YES
     /* AJC - Flatten on small densities, pressures rather than just
	negatives. ---- */
     #if EOS != ISOTHERMAL
      switch_to_1st = (ap[PR] < SMALL_PR) || (am[PR] < SMALL_PR) ;
     #endif
     switch_to_1st = switch_to_1st || 
                     (ap[DN] < SMALL_DN) || (am[DN] < SMALL_DN) ;

     /* AJC - If the density becomes very small 
	during the characteristic trace, then the assumption of
	approximatley straight Riemann invariants is probably 
	not even approximatley correct.   Go to 1st order ---- */
    
     switch_to_1st = switch_to_1st || ap[DN] < 0.1*ac[DN];
     switch_to_1st = switch_to_1st || am[DN] < 0.1*ac[DN];
     #if EOS != ISOTHERMAL
       switch_to_1st = switch_to_1st || ap[PR] < 0.1*ac[PR];
       switch_to_1st = switch_to_1st || am[PR] < 0.1*ac[PR];
     #endif

     #if EOS != ISOTHERMAL
       /* AJC - flatten very strong transonic rarefactions */
       switch_to_1st = switch_to_1st || 
        (ap[V1]*am[V1] > 0.0 && ap[V1]-am[V1] > sqrt(v0[i][PR]/v0[i][DN]));
     #endif
     #if EOS == ISOTHERMAL
       switch_to_1st = switch_to_1st ||
        (ap[V1]*am[V1] > 0.0 && ap[V1]-am[V1] > C_ISO);
     #endif

    #endif

    /*  ----  Check for superluminal velocities  ---- */

    #if (PHYSICS == RHD && USE_FOUR_VELOCITY == NO) || PHYSICS == RMHD 
     scrhm = EXPAND(am[VX]*am[VX], + am[VY]*am[VY], + am[VZ]*am[VZ]);
     scrhp = EXPAND(ap[VX]*ap[VX], + ap[VY]*ap[VY], + ap[VZ]*ap[VZ]);
     switch_to_1st = switch_to_1st || (scrhm >= 1.0);
     switch_to_1st = switch_to_1st || (scrhp >= 1.0);
    #endif

    if (switch_to_1st){
/*
      WARNING (
        print (" ! CHECK_PRIM_STATES: Unphysical state, ");
        WHERE (i,NULL);
      )
*/
      #ifdef STAGGERED_MHD
       scrhp = ap[B1];
       scrhm = am[B1];
      #endif

      for (nv = 0; nv < NVAR; nv++){
        am[nv] = ap[nv] = ac[nv];
      }

      #ifdef STAGGERED_MHD
       ap[B1] = scrhp;
       am[B1] = scrhm;
      #endif
      
    }
  }
}


/*
#if GEOMETRY == CYLINDRICAL
if (NSWEEP == 1) {
 for (nv = 0; nv < NVAR; nv++){
   if (nv == VX) scrh = fabs(v1[3][nv] + v1[4][nv]);
   else          scrh = fabs(v1[3][nv] - v1[4][nv]);
   
   #if PHYSICS == RMHD 
    if (nv == BZ) scrh = fabs(v1[3][nv] + v1[4][nv]);
   #endif
   
   if (scrh > 1.e-8){
     printf ("symmetry violated, z : %d,  var: %d\n", *nyp, nv);
     SHOW(rhs,4);
     SHOW(rhs,3);
     printf (" --- centered:\n");
     SHOW(vv,0); SHOW(vv,1); SHOW(vv,2); SHOW(vv,3); 
     SHOW(vv,4); SHOW(vv,5); SHOW(vv,6); SHOW(vv,7);
     printf (" --- edge\n");
     SHOW(vl,3); SHOW(vr,3);
     SHOW(vr,2); SHOW(vl,4);

     printf ("Source: \n");
     SHOW(src,3);SHOW(src,4);
     scrhp =  fl[4][M1]*GG->A[4]/GG->dV[4] - src[4][M1];
     scrhm = -fr[2][M1]*GG->A[2]/GG->dV[3] - src[3][M1];
     printf ("%12.6e  %12.6e\n",scrhp, scrhm);
     exit(1);
   }
 }
}
#endif
*/


/* ******************************************************** */
void CHECK_CONS_STATE (real **uM, real **uP, 
                       real **u0, int beg, int end)
/*
 *
 *  PURPOSE
 *
 *   check if conservative states uR and uL are
 *   physically admissible. 
 *   Replace their left and right interpolated values 
 *   inside the cell with uu0 otherwise.
 *
 *
 ********************************************************** */
{
  int   i, nv;
  real  scrhp = 1.0, scrhm = 1.0;
  real  *q0, *qm, *qp;

  for (i = beg; i <= end; i++){
  
    q0 = u0[i];
    qm = uM[i];
    qp = uP[i];

    #if PHSYSICS == HD && EOS != ISOTHERMAL
     
     scrhp = EXPAND(qp[MX]*qp[MX], + qp[MY]*qp[MY], + qp[MZ]*qp[MZ]);
     scrhp = qp[EN] - 0.5*scrhp/qp[DN];

     scrhm = EXPAND(qm[MX]*qm[MX], + qm[MY]*qm[MY], + qm[MZ]*qm[MZ]);
     scrhm = qm[EN] - 0.5*scrhm/qm[DN];

    #elif PHYSICS == MHD && EOS != ISOTHERMAL

     scrhp  = EXPAND(qp[MX]*qp[MX], + qp[MY]*qp[MY], + qp[MZ]*qp[MZ]);
     scrhp /= qp[DN];
     scrhp += EXPAND(qp[BX]*qp[BX], + qp[BY]*qp[BY], + qp[BZ]*qp[BZ]);
     scrhp = qp[EN] - 0.5*scrhp;

     scrhm  = EXPAND(qm[MX]*qm[MX], + qm[MY]*qm[MY], + qm[MZ]*qm[MZ]);
     scrhm /= qm[DN];
     scrhm += EXPAND(qm[BX]*qm[BX], + qm[BY]*qm[BY], + qm[BZ]*qm[BZ]);
     scrhm  = qm[EN] - 0.5*scrhm;

    #elif PHYSICS == RHD

     scrhp = EXPAND(qp[MX]*qp[MX], + qp[MY]*qp[MY], + qp[MZ]*qp[MZ]);
     scrhp = qp[EN]*qp[EN] - scrhp - qp[DN]*qp[DN]; 

     scrhm = EXPAND(qm[MX]*qm[MX], + qm[MY]*qm[MY], + qm[MZ]*qm[MZ]);
     scrhm = qm[EN]*qm[EN] - scrhm - qm[DN]*qm[DN]; 

    #endif

    if (scrhp  < 0.0 || scrhm  < 0.0 ||
        #if EOS != ISOTHERMAL
         qm[EN] < 0.0 || qp[EN] < 0.0 ||
        #endif
        qm[DN] < 0.0 || qp[DN] < 0.0) {
      WARNING (
        print (" ! CHECK_CONS_STATE: Unphysical state, ");
        WHERE (i,NULL);
      )
      for (nv = 0; nv < NVAR; nv++) {
         qm[nv] = qp[nv] = q0[nv];
      }
    }
  }
}
