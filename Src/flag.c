#include"orion2.h"

static char ***flag;

/* *************************************************************** */
void FLAG_RESET (const Data *d)
/*
 *
 *
 * PURPOSE
 *
 *  
 ***************************************************************** */
{
  int  i, j, k;
  
  if (flag == NULL) flag = d->flag;

  KTOT_LOOP(k){
  JTOT_LOOP(j){
  ITOT_LOOP(i){
    flag[k][j][i] = 0;
  }}}

}

/* ********************************************************* */
char CHECK_ZONE (int z, int bit)
/*
 *
 *
 *
 *********************************************************** */
{

  if (flag == NULL){
    print1 ("! NULL pointer in CHECK_ZONE\n");
    QUIT_ORION2(1);
  }
  if (DIR == IDIR) return (flag[*NZ_PT][*NY_PT][z] & bit);
  if (DIR == JDIR) return (flag[*NZ_PT][z][*NX_PT] & bit);
  if (DIR == KDIR) return (flag[z][*NY_PT][*NX_PT] & bit);
  else{
    print1 ("! Wrong dir in SHOCK_IN\n");
    QUIT_ORION2(1);
  }

}
#undef EPS_P

