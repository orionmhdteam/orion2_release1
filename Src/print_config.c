#include "orion2.h"

static void CHECK_CONFIG ();

/* ******************************************************************** */
void PRINT_CONFIG ()
/*
 *
 *   Write a summary of the selected options
 *
      ____                         __ __
     / __ \ ____ __ ___   __  __  / // /
    / /_/ // __// // _ \ /  \/ / / // /
    \____//_/  /_/ \___//_/\__/ /_//_/    
                                  
 *
 * 
 *                        
 *
 ********************************************************************** */
{
  int  divb_print=0;
  FILE *fconf;
  time_t time_now;
  char  str1[128], str2[128], str3[128];

  CHECK_CONFIG ();

  print1 ("\n");

  print1("      ____                         __ __ \n");
  print1("     / __ \ ____ __ ___   __  __  / // / \n");
  print1("    / /_/ // __// // _ \ /  \/ / / // /  \n");
  print1("    \____//_/  /_/ \___//_/\__/ /_//_/   \n");
  print1("=============================    v. %s  \n", ORION2_VERSION);
  print1 ("\n> System:\n\n");

  if ( (fconf = fopen("sysconf.out","r")) != NULL){

    while (fscanf (fconf, "%s %s %s\n", str1, str2, str3) != EOF) {

      if (!strcmp(str1,"USER")) 
        print1 ("  %s:             %s\n",str1, str3);
      else if (!strcmp(str1,"WORKING_DIR"))
        print1 ("  %s:      %s\n",str1, str3);
      else if (!strcmp(str1,"SYSTEM_NAME"))
        print1 ("  %s:      %s\n",str1, str3);
      else if (!strcmp(str1,"NODE_NAME"))
        print1 ("  %s:        %s\n",str1, str3);
      else if (!strcmp(str1,"ARCH"))
        print1 ("  %s:             %s\n",str1, str3);
      else if (!strcmp(str1,"BYTE_ORDER"))
        print1 ("  %s:       %s\n\n",str1, str3);
    }

  }else{
    print1 ("! sysconf.out file not found \n\n");
  }
 
  time(&time_now);
  print1("> Local time:    %s\n",asctime(localtime(&time_now)));
      
  if (COMPONENTS < DIMENSIONS) {
    print1 ("Sorry, but the number of vector components can\n");
    print1 ("not be less than the dimension number.\n");
    print1 ("Please edit definitions.h and fix this.\n");
    QUIT_ORION2(1);
  }

  print1 ("> Configuration:\n\n");
  if (PHYSICS == HD) 
    print1 ("  PHYSICS:          HD\n");

  if (PHYSICS == RHD)
    print1 ("  PHYSICS:          RHD\n");
  
  if (PHYSICS == MHD) {
    print1 ("  PHYSICS:          MHD [div.B: ");
    divb_print = 1;
  }

  if (PHYSICS == RMHD) {
    print1 ("  PHYSICS:          RMHD [div.B: ");
    divb_print = 1;
  }

  #if PHYSICS == MHD || PHYSICS == RMHD
   if (divb_print){
     if (MHD_FORMULATION == NONE)
       print1 ("None]\n");
     else if (MHD_FORMULATION == EIGHT_WAVES)
       print1 ("Powell's 8wave]\n");
     else if (MHD_FORMULATION == DIV_CLEANING)
       print1 ("Divergence Cleaning]\n");
     else if (MHD_FORMULATION == FLUX_CT){
        print1 ("CT/");
       #if CT_EMF_AVERAGE == ARITHMETIC
        print1 ("Ar. average]\n");
       #elif CT_EMF_AVERAGE == UCT_CONTACT
        print1 ("UCT_CONTACT]\n");
       #elif CT_EMF_AVERAGE == UCT0
        print1 ("UCT0]\n");
       #elif CT_EMF_AVERAGE == UCT_HLL
        print1 ("UCT_HLL]\n");
       #endif
     }
   }
  #endif
  print1 ("  DIMENSIONS:       %d\n", DIMENSIONS);
  print1 ("  COMPONENTS:       %d\n", COMPONENTS);
  print1 ("  TRACERS:          %d\n", NTRACER);
  print1 ("  VARIABLES:        %d\n", NVAR);

  print1 ("  GEOMETRY:         ");
  if (GEOMETRY == CARTESIAN)    print1 ("Cartesian\n");
  if (GEOMETRY == CYLINDRICAL)  print1 ("Cylindrical\n");
  if (GEOMETRY == POLAR)        print1 ("Polar\n");
  if (GEOMETRY == SPHERICAL)    print1 ("Spherical\n");

  print1 ("  BODY_FORCE:       ");
  if (INCLUDE_BODY_FORCE != NO)  print1 ("EXPLICIT\n");
  if (INCLUDE_BODY_FORCE == NO)  print1 ("OFF\n");

  print1 ("  EOS:              ");
  if (EOS == IDEAL)        print1 ("Ideal\n");
  if (EOS == ISOTHERMAL)   print1 ("Isothermal\n");
  if (EOS == TAUB)         print1 ("Taub - TM\n");

  print1 ("  TIME INTEGRATOR:  ");
  if (TIME_STEPPING == EULER)            print1 ("Euler\n");
  if (TIME_STEPPING == CHARACTERISTIC_TRACING)
                                         print1 ("Characteristic Tracing\n");
  if (TIME_STEPPING == HANCOCK)          print1 ("Hancock\n");
  if (TIME_STEPPING == RK2)              print1 ("Runga-Kutta II\n");
  if (TIME_STEPPING == RK3)              print1 ("Runga_Kutta III\n");

  print1 ("  DIM. SPLITTING:   ");
  if (DIMENSIONAL_SPLITTING == YES)  print1 ("Yes\n");
  else                               print1 ("No\n");
  
  #ifdef FARGO_SCHEME
   #if FARGO_SCHEME == YES
    print1 ("            (Fargo scheme enabled)\n");
   #endif
  #endif

  print1 ("  INTERPOLATION:    ");
  if (INTERPOLATION == FLAT)          print1 ("Flat\n");
  if (INTERPOLATION == LINEAR)
    #ifdef CHAR_LIMITING
     if (CHAR_LIMITING == YES)         print1 ("Linear (Characteristic lim)\n");
     else                              print1 ("Linear (Primitive lim)\n");
    #endif
  if (INTERPOLATION == LINEAR_MULTID) print1 ("Linear_Multid\n");
  if (INTERPOLATION == CENO3)         print1 ("CENO 3rd order\n");
  if (INTERPOLATION == PARABOLIC)     print1 ("Parabolic\n");
  if (INTERPOLATION == WENO5)         print1 ("WENO 5th order\n");

  print1 ("\n");
}

/* ********************************************************** */
void CHECK_CONFIG ()
/*
 *
 *
 * Check if the selected configuration is 
 * available.
 *
 *
 ************************************************************ */
{
  #if DIMENSIONS == 3 

   #if GEOMETRY  == CYLINDRICAL 
    print1 ("\n! Cylindrical coordinates are only 2D.\n");
    print1 ("! Use polar instead.\n");
    QUIT_ORION2(1);
   #endif

   #if GEOMETRY == SPHERICAL 
    #ifdef SINGLE_STEP
     print1 ("\n ! Spherical 3D only works with RK integrators\n");
     QUIT_ORION2(1);
    #endif
   #endif

  #endif

  #if DIMENSIONAL_SPLITTING == NO && DIMENSIONS == 1
   print1 ("! Can not integrate a 1-D problem with an unsplit method \n");
   QUIT_ORION2(1);
  #endif
/*
  #if GEOMETRY == SPHERICAL || GEOMETRY == POLAR
   #if TIME_STEPPING != RK2 || TIME_STEPPING != RK3
    print1 (" ! Spherical and Polar geometries work with RK integrators\");
    QUIT_ORION2(1);
   #endif
  #endif
*/


}
