#include "orion2.h"

/* ************************************************************************ */
void SET_INDEXES (Index *indx, const State_1D *state, Grid *grid)
/*
 *
 * PURPOSE:
 *
 *   Set labels, domain integration indexes 
 *   everytime the sweep direction is changed;
 *
 *   The indexes coincide with the usual ones (IBEG, IEND,...)
 *   most of the time, but can be expanded one further zone  
 *   either in the transverse or normal directions or both.
 *   This depends on the integration algorithm.
 *
 *   With cell-centered fields, the following table holds:
 *
 *                       RK        CTU
 *                  +-------------------------------------------
 *   Transverse++   |    NO        YES, at predictor step
 *                  |
 *   Normal++       |    NO        NO
 *
 *
 *   If constrained transport is enabled, then
 *
 *                       RK        CTU
 *                  +-------------------------------------------
 *   Transverse++   |    YES       YES
 *                  |
 *   Normal++       |    NO        YES, at predictor step
 * 
 *
 *
 *   Predictor step (ISTEP = 1) in CTU requires extra transverse loop
 *   to obtain transverse predictor in any case.
 *   Also, with CTU+CT, one needs to expand the grid of one zone 
 *   in the NORMAL direction as well.
 *   This allows to computed fully corner coupled
 *   states in the boundary to get electric field
 *   components during the constrained transport algorithm. 
 *
 ************************************************************************ */                           
{

  IBEG = grid[IDIR].lbeg; IEND = grid[IDIR].lend;
  JBEG = grid[JDIR].lbeg; JEND = grid[JDIR].lend;
  KBEG = grid[KDIR].lbeg; KEND = grid[KDIR].lend;

  if (DIR == IDIR) {   /* -- Order: X-Y-Z  {in,t1,t2 = i,j,k} -- */

    EXPAND(V1 = M1 = VX;  , 
           V2 = M2 = VY;  ,
           V3 = M3 = VZ;)
    #if PHYSICS == MHD || PHYSICS == RMHD
     EXPAND(B1 = BX;  , 
            B2 = BY;  , 
            B3 = BZ;)
    #endif

    indx->ntot   = grid[IDIR].np_tot;
    indx->beg    = IBEG; indx->end    = IEND;
    indx->t1_beg = JBEG; indx->t1_end = JEND;
    indx->t2_beg = KBEG; indx->t2_end = KEND;

  }else if (DIR == JDIR){ /* -- Order: Y-X-Z  {in,t1,t2 = j,i,k} -- */

    EXPAND(V1 = M1 = VY;  , 
           V2 = M2 = VX;  , 
           V3 = M3 = VZ;)
    #if PHYSICS == MHD || PHYSICS == RMHD
     EXPAND(B1 = BY; , 
            B2 = BX; ,  
            B3 = BZ;)
    #endif
    
    indx->ntot   = grid[JDIR].np_tot;
    indx->beg    = JBEG; indx->end    = JEND;
    indx->t1_beg = IBEG; indx->t1_end = IEND;
    indx->t2_beg = KBEG; indx->t2_end = KEND;

  }else if (DIR == KDIR){ /* -- Order: Z-X-Y  {in,t1,t2 = k,i,j} -- */

    V1 = M1 = VZ;
    V2 = M2 = VX;
    V3 = M3 = VY;
    #if PHYSICS == MHD || PHYSICS == RMHD
     B1 = BZ;  
     B2 = BX;  
     B3 = BY;
    #endif

    indx->ntot   = grid[KDIR].np_tot;
    indx->beg    = KBEG; indx->end    = KEND;
    indx->t1_beg = IBEG; indx->t1_end = IEND;
    indx->t2_beg = JBEG; indx->t2_end = JEND;

  }

  #if INTERPOLATION == MP5
   indx->beg--;
   indx->end++;
  #endif

/* ------------------------------------------------------
    Expand grid one further zone to account for 
    proper flux computation. This is necessary to 
    compute the EMF in the boundary zones.
   ------------------------------------------------------ */

  #ifdef STAGGERED_MHD
   D_EXPAND(                                 ,
            indx->t1_beg--; indx->t1_end++;  ,
            indx->t2_beg--; indx->t2_end++;)
   #ifdef SINGLE_STEP
    if (ISTEP == 1){
      indx->beg--;
      indx->end++; 
      D_EXPAND(                                 ,
               indx->t1_beg--; indx->t1_end++;  ,
               indx->t2_beg--; indx->t2_end++;)
    }
   #endif
  #else
   #ifdef SINGLE_STEP 
    #if DIMENSIONAL_SPLITTING == NO
     if (ISTEP == 1){
     D_EXPAND(                                 ,
              indx->t1_beg--; indx->t1_end++;  ,
              indx->t2_beg--; indx->t2_end++;) 
     }
    #endif
   #endif
  #endif


{
  int i, k1,k2;
  for (i = 0; i < NMAX_POINT; i++){
    for (k1 = NVAR; k1--;  ){
    for (k2 = NVAR; k2--;  ){
      state->Lp[i][k1][k2] = state->Rp[i][k1][k2] = 0.0;
    }}
  }  
}

  return;
}

