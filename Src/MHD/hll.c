#include"orion2.h"

/* ***************************************************************************** */
void HLL_SOLVER (const State_1D *state, int beg, int end, 
                 real *cmax, Grid *grid)
/* 
 *
 *
 * NAME
 *
 *   HLL_SOLVER
 *
 *
 * PURPOSE
 *
 *  - Solve riemann problem for the MHD equations using the 
 *    single state HLL Riemann solver
 * 
 *   - On input, it takes left and right primitive state
 *     vectors state->vL and state->vR at zone edge i+1/2;
 *     On output, return flux and pressure vectors at the
 *     same interface.
 *
 *   - Also, compute maximum wave propagation speed (cmax) 
 *     for  explicit time step computation
 *  
 *
 * LAST_MODIFIED
 *
 *   April 4th 2006, by Andrea Mignone  (mignone@to.astro.it)
 *             
 *
 ******************************************************************************** */
{
  int    nv, i;
  real scrh;
  double *uL, *uR, *SR, *SL;
  static double **fL, **fR, **Uhll;
  static double **VL, **VR, **UL, **UR;
  static real *pL, *pR;
  real **bgf;
    
  if (fL == NULL){
    fL = Array_2D(NMAX_POINT, NFLX, double);
    fR = Array_2D(NMAX_POINT, NFLX, double);
    pL = Array_1D(NMAX_POINT, double);
    pR = Array_1D(NMAX_POINT, double);
    Uhll = Array_2D(NMAX_POINT, NFLX, double);
    #ifdef GLM_MHD
     VL = Array_2D(NMAX_POINT, NVAR, double);
     VR = Array_2D(NMAX_POINT, NVAR, double);
     UL = Array_2D(NMAX_POINT, NVAR, double);
     UR = Array_2D(NMAX_POINT, NVAR, double);
    #endif
  }

  #if INCLUDE_BACKGROUND_FIELD == YES
   bgf = GET_BACKGROUND_FIELD (beg, end, FACE_CENTER, grid);
   #ifdef GLM_MHD 
    print1 ("! GLM MHD and BACKGROUND_FIELD incompatible\n");
    QUIT_ORION2(1);
   #endif
  #endif

/* ------------------------------------------------
     Solve 2x2 Riemann problem with GLM Cleaning
   ------------------------------------------------ */

  #ifdef GLM_MHD
   GLM_2X2_SOLVE (state, VL, VR, beg, end, grid);
   PRIMTOCON (VL, UL, beg, end);
   PRIMTOCON (VR, UR, beg, end);
  #else
   VL = state->vL; UL = state->uL;
   VR = state->vR; UR = state->uR;
  #endif

  /* ----------------------------------------
       Compute physical fluxes at zone edge 
     ---------------------------------------- */
     
  FLUX (UL, VL, bgf, fL, pL, beg, end);
  FLUX (UR, VR, bgf, fR, pR, beg, end);

  /* ----------------------------------------
       get max and min signal velocities
     ---------------------------------------- */
             
  SL = state->SL; SR = state->SR;
  HLL_SPEED (VL, VR, bgf, SL, SR, beg, end);

  /* ----------------------------------------
             compute HLL flux
     ---------------------------------------- */	     

  for (i = beg; i <= end; i++) {

    scrh  = dmax(fabs(SL[i]), fabs(SR[i]));
    *cmax = dmax(*cmax, scrh);

    if (SL[i] > 0.0){
    
      for (nv = 0; nv < NFLX; nv++) {
        state->flux[i][nv] = fL[i][nv];
      }
      state->press[i] = pL[i];
      
    }else if (SR[i] < 0.0){
    
      for (nv = 0; nv < NFLX; nv++) {
        state->flux[i][nv] = fR[i][nv];
      }
      state->press[i] = pR[i];
      
    }else{
    
      uL = UL[i]; uR = UR[i];

      scrh = 1.0 / (SR[i] - SL[i]);
    
      for (nv = 0; nv < NFLX; nv++) {
        state->flux[i][nv] = SL[i]*SR[i]*(uR[nv] - uL[nv]) +
                             SR[i]*fL[i][nv] - SL[i]*fR[i][nv];
        state->flux[i][nv] *= scrh;
      }
      state->press[i] = (SR[i]*pL[i] - SL[i]*pR[i])*scrh;
    }

  }


/* -----------------------------------------------------
               initialize source term
   ----------------------------------------------------- */

  #if MHD_FORMULATION == EIGHT_WAVES
/*
   ROE_DIVB_SOURCE (state, beg, end, grid);
*/
/*
   for (i = beg; i <= end; i++) {
     uR = state->uR[i]; uL = state->uL[i];
     scrh = 1.0 / (SR[i] - SL[i]);
     for (nv = 0; nv < NFLX; nv++) {
       Uhll[i][nv] = SR[i]*uR[nv] - SL[i]*uL[nv] +
                     fL[i][nv] - fR[i][nv];
       Uhll[i][nv] *= scrh;
     }
     Uhll[i][M1] += (pL[i] - pR[i])*scrh;
   }
*/
   HLL_DIVB_SOURCE (state, Uhll, beg + 1, end, grid);

  #endif

}

