#include "orion2.h"

/* *********************************************************************  */
void MOM_SOURCE (const State_1D *state, int beg, int end,
                 int a, int b, real *src, Grid *grid)
/* 
 *
 * PURPOSE
 *
 *   Compute tensor components of the momentum flux.
 *   On output returns T(a,b)
 *
 * ARGUMENTS
 *
 *   state   :  a pointer to a state structure
 *   beg, end:  initial and final points of the grid
 *   a,b     :  the two component of the tensor;
 *   src     :  a 1-D vector containing T(a,b) at every i
 * 
 *
 * LAST MODIFIED
 *
 *   Oct 1st, 2006 by A. Mignone.  e-mail: mignone@to.astro.it
 *    
 *
 *********************************************************************** */
{
  int  i;
  real *u, *v;
  real **bgf;

  #if INCLUDE_BACKGROUND_FIELD == YES
   bgf = GET_BACKGROUND_FIELD (beg, end, CELL_CENTER, grid);
  #endif
  
  for (i = beg; i <= end; i++){
    v = state->vh[i];
    u = state->uh[i];
    src[i] = u[MX + a]*v[VX + b] - v[BX + a]*v[BX + b];
    #if INCLUDE_BACKGROUND_FIELD == YES
     src[i] -= bgf[i][BX + a]*v[BX + b] + v[BX + a]*bgf[i][BX + b];
    #endif
  }
}

/* *********************************************************************  */
void OMEGA_SOURCE (const State_1D *state, int beg, int end,
                   int a, int b, real *src, Grid *grid)
/* 
 *
 * PURPOSE
 *
 *   Compute tensor components of the induction "tensor".
 *   On output returns T(a,b)
 *
 * ARGUMENTS
 *
 *   state   :  a pointer to a state structure
 *   beg, end:  initial and final points of the grid
 *   a,b     :  the two component of the tensor;
 *   src     :  a 1-D vector containing T(a,b) at every i
 * 
 *
 * LAST MODIFIED
 *
 *   Oct 1st, 2006 by A. Mignone.  e-mail: mignone@to.astro.it
 *
 *********************************************************************** */
{
  int  i;
  real *u, *v;
  real **bgf;

  #if INCLUDE_BACKGROUND_FIELD == YES
   bgf = GET_BACKGROUND_FIELD (beg, end, CELL_CENTER, grid);
  #endif

  for (i = beg; i <= end; i++){
    v = state->vh[i];
    u = state->uh[i];
    src[i] = v[VX + a]*u[BX + b] - u[BX + a]*v[VX + b];
    #if INCLUDE_BACKGROUND_FIELD == YES
     src[i] += v[VX + a]*bgf[i][BX + b] - bgf[i][BX + a]*v[VX + b];
    #endif
  }
}

#if MHD_FORMULATION == EIGHT_WAVES
/* *************************************************************************** */
void ROE_DIVB_SOURCE (const State_1D *state, int is, int ie, Grid *grid)
/* 
 *
 * PURPOSE
 *
 *   Include Powell div.B source term to momentum, induction
 *   and energy equation for Roe and TVDLF solvers.
 *
 *********************************************************************** */
{
  int    i;
  real btx, bty, btz, bx, by, bz, vx, vy, vz;
  real r, s;
  real *Ar, *Ath;
  real *vm, **bgf;
  real *src, *v;
  static real *divB, *vp;
  Grid   *GG;

  if (divB == NULL){
    divB = Array_1D(NMAX_POINT, double);
    vp   = Array_1D(NMAX_POINT, double);
  }

/* ----------------------- ---------------------
    compute magnetic field normal component 
    interface value by arithmetic averaging
   -------------------------------------------- */

  for (i = is - 1; i <= ie; i++) {
    vp[i] = 0.5*(state->vL[i][B1] + state->vR[i][B1]);
  }
  vm  = vp - 1;
  GG  = grid + DIR;
  
/* --------------------------------------------
    Compute div.B contribution from the normal 
    direction (1) in different geometries 
   -------------------------------------------- */
  
  #if GEOMETRY == CARTESIAN

   for (i = is; i <= ie; i++) {
     divB[i] = (vp[i] - vm[i])/GG->dx[i];
   }

  #elif GEOMETRY == CYLINDRICAL

   if (DIR == IDIR){   /* -- r -- */
     Ar  = grid[IDIR].A;
     for (i = is; i <= ie; i++) {
       divB[i] = (vp[i]*Ar[i] - vm[i]*Ar[i - 1])/GG->dV[i];
     }
   }else if (DIR == JDIR){  /* -- z -- */
     for (i = is; i <= ie; i++) {
       divB[i] = (vp[i] - vm[i])/GG->dx[i];
     }
   }

  #elif GEOMETRY == POLAR

   if (DIR == IDIR){  /* -- r -- */
     Ar  = grid[IDIR].A;
     for (i = is; i <= ie; i++) {
       divB[i] = (vp[i]*Ar[i] - vm[i]*Ar[i - 1])/GG->dV[i];
     }
   }else if (DIR == JDIR){  /* -- phi -- */
     r = grid[IDIR].x[*NX_PT];
     for (i = is; i <= ie; i++) {
       divB[i] = (vp[i] - vm[i])/(r*GG->dx[i]);
     }
   }else if (DIR == KDIR){  /* -- z -- */
     for (i = is; i <= ie; i++) {
       divB[i] = (vp[i] - vm[i])/GG->dx[i];
     }
   }

  #elif GEOMETRY == SPHERICAL

   if (DIR == IDIR){  /* -- r -- */
     Ar  = grid[IDIR].A;
     for (i = is; i <= ie; i++) {
       divB[i] = (vp[i]*Ar[i] - vm[i]*Ar[i - 1])/GG->dV[i];
     }
   }else if (DIR == JDIR){  /* -- theta -- */
     Ath = grid[JDIR].A;
     r   = grid[IDIR].x[*NX_PT];
     for (i = is; i <= ie; i++) {
       divB[i] = (vp[i]*Ath[i] - vm[i]*Ath[i - 1]) /
                 (r*GG->dV[i]);
     }
   }else if (DIR == KDIR){  /* -- phi -- */
     r = grid[IDIR].x[*NX_PT];
     s = sin(grid[JDIR].x[*NY_PT]);
     for (i = is; i <= ie; i++) {
       divB[i] = (vp[i] - vm[i])/(r*s*GG->dx[i]);
     }
   }

  #endif

  #if INCLUDE_BACKGROUND_FIELD == YES
   bgf = GET_BACKGROUND_FIELD (is - 1, ie, CELL_CENTER, grid);
  #endif

/* ---------------------
     Add source terms
   -------------------- */

  for (i = is; i <= ie; i++) {

    v   = state->vh[i];
    src = state->src[i];

    EXPAND (vx = v[VX];  ,
            vy = v[VY];  ,
            vz = v[VZ];)

    EXPAND (bx = btx = v[BX];  ,
            by = bty = v[BY];  ,
            bz = btz = v[BZ];)

    #if INCLUDE_BACKGROUND_FIELD == YES
     btx += bgf[i][BX];
     bty += bgf[i][BY];
     btz += bgf[i][BZ];
    #endif

    src[DN] = 0.0;
    EXPAND (src[MX] = -divB[i]*btx;  ,
            src[MY] = -divB[i]*bty;  ,
            src[MZ] = -divB[i]*btz;)

    #if EOS != ISOTHERMAL
     src[EN] = -divB[i]*(EXPAND(vx*bx, +vy*by, +vz*bz));
    #endif
    EXPAND (src[BX] = -divB[i]*vx;   ,
            src[BY] = -divB[i]*vy;   ,
            src[BZ] = -divB[i]*vz;)
  }
}

/* *********************************************************************  */
void HLL_DIVB_SOURCE (const State_1D *state, real **Uhll, int beg, int end,
                      Grid *grid)
/* 
 *
 * PURPOSE
 *
 *   Include div.B source term to momentum, induction
 *   and energy equation. Used in conjunction with 
 *   an HLL-type Riemann solver. 
 * 
 * LAST_MODIFIED
 *
 *   April 7th 2006, by Andrea Mignone  (mignone@to.astro.it)
 *
 *********************************************************************** */
{
  int i, nv;
  double vc[NVAR], *A, *src, *vm;
  double r, s, vB;
  static double *divB, *vp;
  Grid *GG;

  if (divB == NULL){
    divB = Array_1D(NMAX_POINT, double);
    vp   = Array_1D(NMAX_POINT, double);
  }

  GG = grid + DIR;
  vm = vp - 1;
  A  = grid[DIR].A;

/* --------------------------------------------
    Compute normal component of the field 
   -------------------------------------------- */

  for (i = beg - 1; i <= end; i++) {
    vp[i] = state->flux[i][DN] < 0.0 ? state->vR[i][B1]: state->vL[i][B1];
  }

/* --------------------------------------------
    Compute div.B contribution from the normal 
    direction (1) in different geometries 
   -------------------------------------------- */

  
  #if GEOMETRY == CARTESIAN

   for (i = beg; i <= end; i++) {
     divB[i] = (vp[i] - vm[i])/GG->dx[i];
   }

  #elif GEOMETRY == CYLINDRICAL

   if (DIR == IDIR){   /* -- r -- */
     for (i = beg; i <= end; i++) {
       divB[i] = (vp[i]*A[i] - vm[i]*A[i - 1])/GG->dV[i];
     }
   }else if (DIR == JDIR){  /* -- z -- */
     for (i = beg; i <= end; i++) {
       divB[i] = (vp[i] - vm[i])/GG->dx[i];
     }
   }

  #elif GEOMETRY == POLAR

   if (DIR == IDIR){        /* -- r -- */
     for (i = beg; i <= end; i++) {
       divB[i] = (vp[i]*A[i] - vm[i]*A[i - 1])/GG->dV[i];
     }
   }else if (DIR == JDIR){  /* -- phi -- */
     r = grid[IDIR].x[*NX_PT];
     for (i = beg; i <= end; i++) {
       divB[i] = (vp[i] - vm[i])/(r*GG->dx[i]);
     }
   }else if (DIR == KDIR){  /* -- z -- */
     for (i = beg; i <= end; i++) {
       divB[i] = (vp[i] - vm[i])/GG->dx[i];
     }
   }

  #elif GEOMETRY == SPHERICAL

   if (DIR == IDIR){  /* -- r -- */
     for (i = beg; i <= end; i++) {
       divB[i] = (vp[i]*A[i] - vm[i]*A[i - 1])/GG->dV[i];
     }
   }else if (DIR == JDIR){  /* -- theta -- */
     r   = grid[IDIR].x[*NX_PT];
     for (i = beg; i <= end; i++) {
       divB[i] = (vp[i]*A[i] - vm[i]*A[i - 1])/(r*GG->dV[i]);
     }
   }else if (DIR == KDIR){  /* -- phi -- */
     r = grid[IDIR].x[*NX_PT];
     s = sin(grid[JDIR].x[*NY_PT]);
     for (i = beg; i <= end; i++) {
       divB[i] = (vp[i] - vm[i])/(r*s*GG->dx[i]);
     }
   }

  #endif

  /* -----------------------------------------
          compute total source terms
     ----------------------------------------- */
     
  for (i = beg; i <= end; i++) {

    src = state->src[i];
    for (nv = NFLX; nv--;  ) vc[nv] = state->vh[i][nv];

    vB = EXPAND(vc[VX]*vc[BX], + vc[VY]*vc[BY], + vc[VZ]*vc[BZ]);
    src[DN] = 0.0;
    EXPAND(src[MX] = -vc[BX]*divB[i];  ,
           src[MY] = -vc[BY]*divB[i];  ,
           src[MZ] = -vc[BZ]*divB[i];)

    EXPAND(src[BX] = -vc[VX]*divB[i];  ,
           src[BY] = -vc[VY]*divB[i];  ,
	       src[BZ] = -vc[VZ]*divB[i];)

    #if EOS != ISOTHERMAL
     src[EN] = -vB*divB[i];
    #endif

  }  
}
#endif

