/*
*      ____                         __ __
*     / __ \ ____ __ ___   __  __  / // /
*    / /_/ // __// // _ \ /  \/ / / // /
*    \____//_/  /_/ \___//_/\__/ /_//_/
*    
*    Based on Pluto 3.
*
*    Please refer to COPYING in Orion2's root directory.
*
*   Modification: Orional code based on PLUTO3.0
*   1) PS(03/17/09): Increase local memory allocation for MAR.
*   2) PS(06/05/09): Add emf retrieval for AMR.
*   3) PS(09/23/09): Upgrade to PLUTO 3.0.1.
*   4) PS(11/05/09): Change temfx, temfy, and temfz not to include ghostzones.
*
*/

#include "orion2.h"

/* ********************************************************************** */
void FCT_UPDATE(int step, int m_numGhost, real ***temfx, real ***temfy, real ***temfz, const Data *d, Grid *grid)
/*
 *
 * PURPOSE:
 *
 *   Update staggered magnetic field using Stokes' thoerem.
 *   Reference: "A staggered mesh algorithm using high-order 
 *               Godunov fluxes to ensure solenoidal magnetic
 *               field in MHD simulation"
 *               Balsara, Spicer, JCP 1999, 149, 270
 *
 ************************************************************************ */              
{
  int  i, j, k, nv;
  int  ibeg, jbeg, kbeg;
  int  iend, jend, kend;
  real rhs_x, rhs_y, rhs_z, dt;
  real *dx, *dy, *dz, *A1, *A2, *dV1, *dV2;
  real *r, *rp, *th, r_2;
  real ***ex, ***ey, ***ez;
  EMF *emf;
  static Data_Arr bf0;
  
  if (bf0 == NULL){
    bf0 = Array_4D(DIMENSIONS, NMAX_POINT, NMAX_POINT, NMAX_POINT, double);
  }

/* ---- check div.B ---- */

  #if CHECK_DIVB_CONDITION == YES
   if (step == 1) CHECK_DIVB (d->Vs, grid);
  #endif

  emf = EMF_GET (d, step, grid);

  #if SAVE_VEC_POT == YES
   VEC_POT (d, emf, NULL, grid);
  #endif

  ex = emf->ex;
  ey = emf->ey;
  ez = emf->ez;

/* ---- compute right hand side (1):
        initialize arrays             ---- */

  if (step == 1){
  
    for (nv = 0; nv < DIMENSIONS; nv++){
    KTOT_LOOP(k)
    JTOT_LOOP(j)
    ITOT_LOOP(i) 
      bf0[nv][k][j][i] = d->Vs[nv][k][j][i];
    }

  }else if (step == 2){
  
    for (nv = 0; nv < DIMENSIONS; nv++){
    KTOT_LOOP(k)
    JTOT_LOOP(j)
    ITOT_LOOP(i) 
      #ifdef SINGLE_STEP
       d->Vs[nv][k][j][i] = bf0[nv][k][j][i];
      #elif TIME_STEPPING == RK2
       d->Vs[nv][k][j][i] = 0.5*(bf0[nv][k][j][i] + d->Vs[nv][k][j][i]);
      #elif TIME_STEPPING == RK3
       d->Vs[nv][k][j][i] = 0.75*bf0[nv][k][j][i] + 0.25*d->Vs[nv][k][j][i];
      #endif
    }
    
  }else if (step == 3){

    for (nv = 0; nv < DIMENSIONS; nv++){
    KTOT_LOOP(k)
    JTOT_LOOP(j)
    ITOT_LOOP(i) 
      d->Vs[nv][k][j][i] = (bf0[nv][k][j][i] + 2.0*d->Vs[nv][k][j][i])/3.0;
    }

  }

/* ---- set dt ---- */

  dt = delta_t;

  #ifdef SINGLE_STEP

   if (step == 1) dt *= 0.5;
    
  #elif TIME_STEPPING == RK2

   if (step == 2) dt *= 0.5;

  #elif TIME_STEPPING == RK3

   if (step == 2)      dt *= 0.25;
   else if (step == 3) dt *= 2.0/3.0;
  
  #endif

/* ---------------------------------------------------
     compute right hand side (2): 
   
               add flux contribution 
   --------------------------------------------------- */

  dx = grid[IDIR].dx; 
  dy = grid[JDIR].dx;
  dz = grid[KDIR].dx;

  r   = grid[IDIR].x;
  rp  = grid[IDIR].xr;
  A1  = grid[IDIR].A;
  dV1 = grid[IDIR].dV;

  th  = grid[JDIR].x;
  A2  = grid[JDIR].A;
  dV2 = grid[JDIR].dV;

  /* -------- update bx -------- */

  for (k = emf->kbeg + KOFFSET; k <= emf->kend; k++){
  for (j = emf->jbeg + 1      ; j <= emf->jend; j++){
  for (i = emf->ibeg          ; i <= emf->iend; i++){
     
    #if GEOMETRY == CARTESIAN

     rhs_x = D_EXPAND(  0.0                                       , 
                      - dt/dy[j]*(ez[k][j][i] - ez[k][j - 1][i])  ,
                      + dt/dz[k]*(ey[k][j][i] - ey[k - 1][j][i]) ); 

    #elif GEOMETRY == CYLINDRICAL

     rhs_x = - dt/dy[j]*(ez[k][j][i] - ez[k][j - 1][i]);

    #elif GEOMETRY == POLAR 

     rhs_x = D_EXPAND(   0.0                                               ,
                       - dt/(A1[i]*dy[j])*(ez[k][j][i] - ez[k][j - 1][i])  , 
                       + dt/dz[k]        *(ey[k][j][i] - ey[k - 1][j][i])); 

    #elif GEOMETRY == SPHERICAL 

     rhs_x = D_EXPAND( 0.0                                                                  ,
                      - dt/(rp[i]*dV2[j])*(A2[j]*ez[k][j][i] - A2[j - 1]*ez[k][j - 1][i])   ,
                      + dt*dy[j]/(rp[i]*dV2[j]*dz[k])*(ey[k][j][i] - ey[k - 1][j][i]));

    #endif    
      
    d->Vs[BXs][k][j][i] += rhs_x;

  }}}

  /* -------- update by -------- */

  for (k = emf->kbeg + KOFFSET; k <= emf->kend; k++){
  for (j = emf->jbeg          ; j <= emf->jend; j++){
  for (i = emf->ibeg + 1      ; i <= emf->iend; i++){
     
    #if GEOMETRY == CARTESIAN

     rhs_y = D_EXPAND(  dt/dx[i]*(ez[k][j][i] - ez[k][j][i - 1])   ,  
                                                                   ,   
                      - dt/dz[k]*(ex[k][j][i] - ex[k - 1][j][i]) ); 

    #elif GEOMETRY == CYLINDRICAL

     rhs_y =   dt/dV1[i]*(A1[i]*ez[k][j][i] - A1[i - 1]*ez[k][j][i - 1]);

    #elif GEOMETRY == POLAR 

     rhs_y =  D_EXPAND(  dt/dx[i]*(ez[k][j][i] - ez[k][j][i - 1])    , 
                                                                     ,
                       - dt/dz[k]*(ex[k][j][i] - ex[k - 1][j][i]));
 
    #elif GEOMETRY == SPHERICAL 

     rhs_y = D_EXPAND( + dt/(r[i]*dx[i])*(rp[i]*ez[k][j][i] - rp[i - 1]*ez[k][j][i - 1])    ,
                                                                                            ,                                           
                       - dt/(r[i]*A2[j]*dz[k])*(ex[k][j][i] - ex[k - 1][j][i]));

    #endif    
      
    d->Vs[BYs][k][j][i] += rhs_y;  

  }}}

  /* -------- update bz -------- */

  #if DIMENSIONS == 3
  for (k = emf->kbeg    ; k <= emf->kend; k++){
  for (j = emf->jbeg + 1; j <= emf->jend; j++){
  for (i = emf->ibeg + 1; i <= emf->iend; i++){
     
     #if GEOMETRY == CARTESIAN

      rhs_z = - dt/dx[i]*(ey[k][j][i] - ey[k][j][i - 1])
              + dt/dy[j]*(ex[k][j][i] - ex[k][j - 1][i]); 

     #elif GEOMETRY == POLAR 

      rhs_z = - dt/dV1[i]*(A1[i]*ey[k][j][i] - A1[i - 1]*ey[k][j][i - 1])
              + dt/(r[i]*dy[j])*(ex[k][j][i] - ex[k][j - 1][i]);

     #elif GEOMETRY == SPHERICAL 

      rhs_z = - dt/(r[i]*dx[i])*(rp[i]*ey[k][j][i] - rp[i - 1]*ey[k][j][i - 1])
              + dt/(r[i]*dy[j])*(ex[k][j][i] - ex[k][j - 1][i]);
     #endif
      
     d->Vs[BZs][k][j][i] += rhs_z;

   }}}
  #endif

  /*PS: retrieve EMFs */
  if (step == 2) {
    
    int ghm1=m_numGhost-1;
    for (k = emf->kbeg; k <= emf->kend; k++){
      for (j = emf->jbeg; j <= emf->jend; j++){
	for (i = emf->ibeg+1; i <= emf->iend; i++){      
       	  temfx[k-ghm1][j-ghm1][i-m_numGhost]=ex[k][j][i];
	  //	  temfx[k-ghm1][j-ghm1][i-m_numGhost]=0;
	}
      }
    }
    for (k = emf->kbeg; k <= emf->kend; k++){
      for (j = emf->jbeg+1; j <= emf->jend; j++){
	for (i = emf->ibeg; i <= emf->iend; i++){      
       	  temfy[k-ghm1][j-m_numGhost][i-ghm1]=ey[k][j][i];
	  //	  temfy[k-ghm1][j-m_numGhost][i-ghm1]=0;
	}
      }
    }
    for (k = emf->kbeg+1; k <= emf->kend; k++){
      for (j = emf->jbeg; j <= emf->jend; j++){
	for (i = emf->ibeg; i <= emf->iend; i++){      
       	  temfz[k-m_numGhost][j-ghm1][i-ghm1]=ez[k][j][i];
	  //	  temfz[k-m_numGhost][j-ghm1][i-ghm1]=0;
	}
      }
    }
    /*
    printf("%17.15e %17.15e %17.15e %17.15e %17.15e\n",temfx[11][48][29],
	   temfx[12][48][29],temfx[13][48][29],temfx[14][48][29],temfx[15][48][29]);
    printf("%17.15e %17.15e %17.15e %17.15e %17.15e\n",temfx[11][0][29],
	   temfx[12][0][29],temfx[13][0][29],temfx[14][0][29],temfx[15][0][29]);
    */
    /*
     for (k = emf->kbeg; k <= emf->kend; k++){
      for (j = emf->jbeg; j <= emf->jend; j++){
	for (i = emf->ibeg+1; i <= emf->iend; i++){      
	  temfx[k+1][j+1][i]=ex[k][j][i];
	}
      }
    }
    for (k = emf->kbeg; k <= emf->kend; k++){
      for (j = emf->jbeg+1; j <= emf->jend; j++){
	for (i = emf->ibeg; i <= emf->iend; i++){      
	  temfy[k+1][j][i+1]=ey[k][j][i];
	}
      }
    }
    for (k = emf->kbeg+1; k <= emf->kend; k++){
      for (j = emf->jbeg; j <= emf->jend; j++){
	for (i = emf->ibeg; i <= emf->iend; i++){      
	  temfz[k][j+1][i+1]=ez[k][j][i];
	}
      }
    }
    printf("%17.15e %17.15e %17.15e %17.15e %17.15e\n",temfx[11][52][29],
	   temfx[12][52][29],temfx[13][52][29],temfx[14][52][29],temfx[15][52][29]);
    printf("%17.15e %17.15e %17.15e %17.15e %17.15e\n",temfx[11][4][29],
	   temfx[12][4][29],temfx[13][4][29],temfx[14][4][29],temfx[15][4][29]);
    */
 }

}

/* ****************************************************** */
void CHECK_DIVB (real ***bf[], Grid *grid)
/*
 *
 *
 *
 *
 ******************************************************** */
{
  int i,j,k;
  real divB;
  real ***bx, ***by, ***bz;
  real *dx, *dy, *dz;
  real *Ar, *s, *dmu, *r;
  real dbmax=0.0;

  D_EXPAND( bx = bf[0];  ,
            by = bf[1];  ,
            bz = bf[2]; )
    
  Ar  = grid[IDIR].A;
  s   = grid[JDIR].A;
  dmu = grid[JDIR].dV;

  r    = grid[IDIR].x;
  dx   = grid[IDIR].dx;
  dy   = grid[JDIR].dx;
  dz   = grid[KDIR].dx;
      
  /* PS: Flux-ct scheme
#ifdef STAGGERED_MHD
    int ibeg = IBEG - 2;
    int jbeg = JBEG - 2;
    int kbeg = KBEG - 2;
    int iend = IEND + 2;
    int jend = JEND + 2;
    int kend = KEND + 2;
#else
    int ibeg = IBEG;
    int jbeg = JBEG;
    int kbeg = KBEG;
    int iend = IEND;
    int jend = JEND;
    int kend = KEND;
#endif
  */

  for (k = KBEG - KOFFSET; k <= KEND + KOFFSET; k++){

    #if DIMENSIONS < 3
     dz[k] = 1.0;
    #endif

    for (j = JBEG - 1; j <= JEND + 1; j++){
    for (i = IBEG - 1; i <= IEND + 1; i++){

    #if GEOMETRY == CARTESIAN

     divB = D_EXPAND(   (bx[k][j][i] - bx[k][j][i-1])*dy[j]*dz[k]  ,
                      + (by[k][j][i] - by[k][j-1][i])*dx[i]*dz[k]  , 
                      + (bz[k][j][i] - bz[k-1][j][i])*dx[i]*dy[j] );

    #elif GEOMETRY == CYLINDRICAL

     divB =        dy[j]*(bx[k][j][i]*Ar[i] - bx[k][j][i-1]*Ar[i-1])
            + fabs(r[i])*dx[i]*(by[k][j][i] - by[k][j-1][i]);

    #elif GEOMETRY == POLAR

     divB = D_EXPAND(
              dz[k]*dy[j]*(bx[k][j][i]*Ar[i] - bx[k][j][i-1]*Ar[i-1]) ,
            + dx[i]*dz[k]*(by[k][j][i] - by[k][j-1][i])               ,
            + r[i]*dx[i]*dy[j]*(bz[k][j][i] - bz[k-1][j][i])
           );

    #elif GEOMETRY == SPHERICAL
         
     divB = D_EXPAND(  
               dmu[j] *dz[k]*(bx[k][j][i]*Ar[i]  - bx[k][j][i-1]*Ar[i-1])   ,
             + r[i]*dx[i]*dz[k]*(by[k][j][i]*s[j] - by[k][j-1][i]*s[j-1])  ,
             + r[i]*dx[i]*dy[j]*(bz[k][j][i]        - bz[k-1][j][i])
            );

    #endif

     dbmax = dmax(dbmax,fabs(divB));
     if (fabs(divB) > 1.e-6) {
       print ("! divB != 0 in FCT_UPDATE (%12.6e) , ijk = %d %d %d \n", divB, i,j,k);
       print ("! Beg, End = [%d, %d]  [%d, %d]  [%d, %d]\n",IBEG, IEND,
               JBEG, JEND, KBEG, KEND);
       D_EXPAND( print ("b1: %12.6e  %12.6e\n",bx[k][j][i],bx[k][j][i-1]); ,
                 print ("b2: %12.6e  %12.6e\n",by[k][j][i],by[k][j-1][i]); ,
                 print ("b3: %12.6e  %12.6e\n",bz[k][j][i],bz[k-1][j][i]); )

       QUIT_ORION2(1);
     }

    }}
  }

}
