#include "orion2.h"

static double MC_LIM (double dp, double dm);

/* ********************************************************** */
void STORE_VEL_SLOPES (EMF *emf, const State_1D *state, 
                       int beg, int end)
/*
 *  Modification: Orional code based on PLUTO3.0
 *  1) PS(03/17/09): Increase local memory allocation for AMR.
 *  2) PS(09/23/09): Upgrade to PLUTO 3.0.1.
 *
 *
 ************************************************************ */
{
  int i, j, k;

/* ----------------------------------------------------
             Allocate static memory areas 
   ---------------------------------------------------- */

  if (emf->dvx_dx == NULL){

    /*PS: Change to PLUTO 3.0.1 that may save some memory. */
    emf->dvx_dx = Array_3D(NMAX_POINT, NMAX_POINT, NMAX_POINT, double);
    emf->dvx_dy = Array_3D(NMAX_POINT, NMAX_POINT, NMAX_POINT, double);

    emf->dvy_dx = Array_3D(NMAX_POINT, NMAX_POINT, NMAX_POINT, double);
    emf->dvy_dy = Array_3D(NMAX_POINT, NMAX_POINT, NMAX_POINT, double);

    #if DIMENSIONS == 3
     emf->dvx_dz = Array_3D(NMAX_POINT, NMAX_POINT, NMAX_POINT, double);
     emf->dvy_dz = Array_3D(NMAX_POINT, NMAX_POINT, NMAX_POINT, double);

     emf->dvz_dx = Array_3D(NMAX_POINT, NMAX_POINT, NMAX_POINT, double);
     emf->dvz_dy = Array_3D(NMAX_POINT, NMAX_POINT, NMAX_POINT, double);
     emf->dvz_dz = Array_3D(NMAX_POINT, NMAX_POINT, NMAX_POINT, double);
    #endif
     
  }

  if (DIR == IDIR){

    for (i = beg; i <= end; i++) { 
      emf->dvx_dx[*NZ_PT][*NY_PT][i] = state->vp[i][VX] - state->vm[i][VX];
      emf->dvy_dx[*NZ_PT][*NY_PT][i] = state->vp[i][VY] - state->vm[i][VY];
      #if DIMENSIONS == 3
       emf->dvz_dx[*NZ_PT][*NY_PT][i] = state->vp[i][VZ] - state->vm[i][VZ];
      #endif
    }

  }else if (DIR == JDIR){

    for (j = beg; j <= end; j++) {
      emf->dvx_dy[*NZ_PT][j][*NX_PT] = state->vp[j][VX] - state->vm[j][VX];
      emf->dvy_dy[*NZ_PT][j][*NX_PT] = state->vp[j][VY] - state->vm[j][VY];
      #if DIMENSIONS == 3
       emf->dvz_dy[*NZ_PT][j][*NX_PT] = state->vp[j][VZ] - state->vm[j][VZ];
      #endif
    }

  }else if (DIR == KDIR){

    for (k = beg; k <= end; k++) {
      emf->dvx_dz[k][*NY_PT][*NX_PT] = state->vp[k][VX] - state->vm[k][VX];
      emf->dvy_dz[k][*NY_PT][*NX_PT] = state->vp[k][VY] - state->vm[k][VY];
      #if DIMENSIONS == 3
       emf->dvz_dz[k][*NY_PT][*NX_PT] = state->vp[k][VZ] - state->vm[k][VZ];
      #endif
    }
  }
}
/* *********************************************************** */
void STAGGERED_SLOPES (const Data_Arr b, EMF *emf, Grid *grid)
/* 
 *
 * PURPOSE
 *
 *  Compute slopes of staggered magnetic fields components
 *  Exclude normal derivatives (i.e. dbx_dx).
 *
 *
 *
 ************************************************************* */
{
  int    i,j,k;
  double ***bx, ***by, ***bz;

  D_EXPAND(
    bx = b[BXs];  , 
    by = b[BYs];  ,
    bz = b[BZs];
  )

  if (emf->dbx_dy == NULL){
    /*PS: Change to PLUTO 3.0.1 that may save some memory. */
    emf->dbx_dy = Array_3D(NMAX_POINT, NMAX_POINT, NMAX_POINT, double);
    emf->dby_dx = Array_3D(NMAX_POINT, NMAX_POINT, NMAX_POINT, double);
    #if DIMENSIONS == 3
     emf->dbx_dz = Array_3D(NMAX_POINT, NMAX_POINT, NMAX_POINT, double);
     emf->dby_dz = Array_3D(NMAX_POINT, NMAX_POINT, NMAX_POINT, double);
     emf->dbz_dx = Array_3D(NMAX_POINT, NMAX_POINT, NMAX_POINT, double);
     emf->dbz_dy = Array_3D(NMAX_POINT, NMAX_POINT, NMAX_POINT, double);
    #endif
   
  }
 
  for (k = KOFFSET; k < NZ_TOT - KOFFSET; k++){
  for (j = JOFFSET; j < NY_TOT - JOFFSET; j++){
  for (i = IOFFSET; i < NX_TOT - IOFFSET; i++){
    emf->dbx_dy[k][j][i] = MC_LIM(bx[k][j+1][i] - bx[k][j][i], 
                                  bx[k][j][i]   - bx[k][j-1][i]);
    emf->dby_dx[k][j][i] = MC_LIM(by[k][j][i+1] - by[k][j][i], 
                                  by[k][j][i]   - by[k][j][i-1]);

    #if DIMENSIONS == 3
     emf->dbx_dz[k][j][i] = MC_LIM(bx[k+1][j][i] - bx[k][j][i], 
                                   bx[k][j][i]   - bx[k-1][j][i]);
     emf->dby_dz[k][j][i] = MC_LIM(by[k+1][j][i] - by[k][j][i], 
                                   by[k][j][i]   - by[k-1][j][i]);
     emf->dbz_dx[k][j][i] = MC_LIM(bz[k][j][i+1] - bz[k][j][i], 
                                   bz[k][j][i]   - bz[k][j][i-1]);
     emf->dbz_dy[k][j][i] = MC_LIM(bz[k][j+1][i] - bz[k][j][i], 
                                   bz[k][j][i]   - bz[k][j-1][i]);
    #endif
  }}}

}

/* ********************************************************* */
double MC_LIM (double dp, double dm)
/*
 *
 *
 *
 *********************************************************** */
{
  double dc, scrh;

  if (dp*dm < 0.0) return(0.0);

  dc   = 0.5*(dp + dm);
  scrh = 2.0*(fabs(dp) < fabs(dm) ? dp:dm);
  return (fabs(dc) < fabs(scrh) ? dc:scrh);
}


