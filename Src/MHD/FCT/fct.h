/*
*      ____                         __ __
*     / __ \ ____ __ ___   __  __  / // /
*    / /_/ // __// // _ \ /  \/ / / // /
*    \____//_/  /_/ \___//_/\__/ /_//_/
*    
*    Based on Pluto 3.
*
*    Please refer to COPYING in Orion2's root directory.
*
*   Modification: Orional code based on PLUTO3.0
*   1) PS(06/05/09): Add emf retrieval for AMR.
*/

/* ###############################################################

         SET LABELS FOR Flux Constrained Transport

   ############################################################### */

#define STAGGERED_MHD

/* ---- set labels for CT_EMF_AVERAGE ---- */

#define ARITHMETIC     1
#define UCT0           2
#define UCT_CONTACT    3
#define UCT_HLL        4
#define RIEMANN_2D     5

/* ---- staggered component labels ---- */

#define BXs  0
#define BYs  1
#define BZs  2

#define FACE_EMF   11
#define EDGE_EMF   12

/*       Now define more convenient and user-friendly 
         pointer labels for geometry setting      */

#if GEOMETRY == CYLINDRICAL 
 #define iBRs    BXs
 #define iBZs    BYs
 #define iBPHIs  BZs
#endif

#if GEOMETRY == POLAR 
 #define iBRs    BXs
 #define iBPHIs  BYs
 #define iBZs    BZs
#endif

#if GEOMETRY == SPHERICAL 
 #define iBRs    BX
 #define iBTHs   BY
 #define iBPHIs  BZ
#endif

  
typedef struct ElectroMotiveForce{

/* -- fields at constant faces -- */

  real ***exj;
  real ***exk;
  real ***eyi;
  real ***eyk;
  real ***ezi;
  real ***ezj;

/* -- density flux signs -- */
  char ***svx, ***svy, ***svz;

/* -- domain of existence of ex,ey,ez -- */

  int  ibeg, jbeg, kbeg;
  int  iend, jend, kend;

/* -- signal velocities -- */

  real ***SxL;
  real ***SxR;
  real ***SyL;
  real ***SyR;
  real ***SzL;
  real ***SzR;

/* -- edge-centered fields -- */

  real ***ex;
  real ***ey;
  real ***ez;

/* -- slopes -- */

  double ***dbx_dy, ***dbx_dz;  
  double ***dby_dx, ***dby_dz;
  double ***dbz_dx, ***dbz_dy;

  double ***dvx_dx, ***dvy_dx, ***dvz_dx;
  double ***dvx_dy, ***dvy_dy, ***dvz_dy;
  double ***dvx_dz, ***dvy_dz, ***dvz_dz;

} EMF;

void AVERAGE_MAGNETIC_FIELD (real ***bf[], real ***UU[], Grid *);
void CHECK_DIVB (real ***b[], Grid *);
void EMF_BOUNDARY (EMF *, Grid *);
void EMF_PUT (const State_1D *, int, int, Grid *);
/*PS: add emf retrieval for AMR */
void EMF_RETRIEVE (real ***temfx, real ***temfy, real ***temfz);
void EMF_DIFF_PUT (const State_1D *state, int beg, int end, Grid *grid);

void EMF_PUT_RES (const State_1D *, int, int, Grid *);
EMF *EMF_GET (const Data *, int, Grid *);
void FCT_UPDATE(int, int, real ***, real ***, real ***, const Data *, Grid *);
void FILL_MAGNETIC_FIELD (Data_Arr, int, Grid *); 

void MULTI_D_RECONSTRUCT (real ***vv[], real ***bf[], real ***, 
                          real ***, real **, real **, Grid *);
/*
void EMF_BOUNDARY_STATES (Data_Arr, Data_Arr, 
                          Data_Arr, Data_Arr, 
                          Data_Arr, Data_Arr, Grid *);
*/
void EMF_USERDEF_BOUNDARY (EMF *, int, int, Grid *);

void STAGGERED_SLOPES (const Data_Arr, EMF *, Grid *);

void ARITHMETIC_AVERAGE   (const EMF *Z1, const double w);
void INTEGRATE_EMF_CORNER (const Data *d, const EMF *, Grid *);
void UCT_HLL_AVERAGE   (const Data *d, const EMF *, Grid *);
void STORE_VEL_SLOPES (EMF *, const State_1D *, int, int);
void CMUSCL_AVERAGE (const Data *, const EMF *, Grid *);


