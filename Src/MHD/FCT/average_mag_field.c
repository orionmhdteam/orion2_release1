/*
*      ____                         __ __
*     / __ \ ____ __ ___   __  __  / // /
*    / /_/ // __// // _ \ /  \/ / / // /
*    \____//_/  /_/ \___//_/\__/ /_//_/
*    
*    Based on Pluto 3.
*
*    Please refer to COPYING in Orion2's root directory.
*
*    Modification: Original code based on PLUTO3.0b2
*    1) PS-09/25/08: Add magnetic field averaging for cell center field
*                    as in PLUTO2.
*
*/
#include "orion2.h"

/* ********************************************************************** */
void AVERAGE_MAGNETIC_FIELD (real ***bf[], real ****UU, Grid *grid)
/*
 *
 *
 * PURPOSE
 * 
 *   given the area-averaged magnetic field bf, define the 
 *   volume averaged magnetic field.
 * 
 ************************************************************************ */
{
  int i, j, k;
  real b2_old, b2_new, bx_ave, by_ave, bz_ave;
  real rp, rm;
  real ***bx, ***by, ***bz;
  real *dx, *dy, *A1, *A2, *dV1, *dV2;
  real *r;
  
  D_EXPAND(bx = bf[BXs];  ,
           by = bf[BYs];  ,
           bz = bf[BZs]; )
  
  dx = grid[IDIR].dx; 
  dy = grid[JDIR].dx;
  A1  = grid[IDIR].A;
  A2  = grid[JDIR].A;
  dV1 = grid[IDIR].dV;
  dV2 = grid[JDIR].dV;
  r   = grid[IDIR].x;

  for (k = KBEG; k <= KEND; k++){
  for (j = JBEG; j <= JEND; j++){
  for (i = IBEG; i <= IEND; i++){

    #if GEOMETRY == CARTESIAN 

     D_EXPAND( bx_ave = 0.5*(bx[k][j][i] + bx[k][j][i - 1]);  ,
               by_ave = 0.5*(by[k][j][i] + by[k][j - 1][i]);  ,
               bz_ave = 0.5*(bz[k][j][i] + bz[k - 1][j][i]); )

    #elif GEOMETRY == CYLINDRICAL

     rp = grid[IDIR].xr[i];
     rm = grid[IDIR].xl[i];
/*
     bx_ave = (rp*bf[0][k][j][i] + rm*bf[0][k][j][i - 1])/(rp + rm);
     by_ave = 0.5*(bf[1][k][j][i] + bf[1][k][j - 1][i]);
*/

     bx_ave = 0.5*(bx[k][j][i] + bx[k][j][i - 1]);
     by_ave = 0.5*(by[k][j][i] + by[k][j - 1][i]);

    #elif GEOMETRY == POLAR

     rp = grid[IDIR].xr[i];
     rm = grid[IDIR].xl[i];

     D_EXPAND(bx_ave = (rp*bx[k][j][i] + rm*bx[k][j][i - 1])/(rp + rm); ,
              by_ave = 0.5*(by[k][j][i] + by[k][j - 1][i]);             ,
              bz_ave = 0.5*(bz[k][j][i] + bz[k - 1][j][i]);)

    #elif GEOMETRY == SPHERICAL

     D_EXPAND(bx_ave  = 0.5*(A1[i]*bx[k][j][i] + A1[i - 1]*bx[k][j][i - 1]);
              bx_ave *= dx[i]/dV1[i];                                        ,

              by_ave  = 0.5*(A2[j]*by[k][j][i] + A2[j - 1]*by[k][j - 1][i]);
              by_ave *= dy[j]/dV2[j];                                        ,

              bz_ave  = 0.5*(bz[k][j][i] + bz[k - 1][j][i]);)
     
/*
     D_EXPAND(bx_ave = 0.5*(bx[k][j][i] + bx[k][j][i - 1]);
              by_ave = 0.5*(by[k][j][i] + by[k][j - 1][i]);
              by_ave = 0.5*(bz[k][j][i] + bz[k][j - 1][i]);
    
*/
    #endif
   
   /* ---------------------------------------------
          apply energy correction if necessary 
       --------------------------------------------- */
 
    #if CT_EN_CORRECTION == YES && EOS != ISOTHERMAL
       /*
     b2_old = D_EXPAND(  UU[k][j][i][BX]*UU[k][j][i][BX], 
                       + UU[k][j][i][BY]*UU[k][j][i][BY],  
                       + UU[k][j][i][BZ]*UU[k][j][i][BZ]);
       */
     /*PS: Chombo */
     b2_old = D_EXPAND(  UU[BX][k][j][i]*UU[BX][k][j][i], 
                       + UU[BY][k][j][i]*UU[BY][k][j][i],  
                       + UU[BZ][k][j][i]*UU[BZ][k][j][i]);
    #endif   
     /*
    D_EXPAND( UU[k][j][i][BX] = bx_ave;  ,
              UU[k][j][i][BY] = by_ave;  ,
              UU[k][j][i][BZ] = bz_ave; )
     */
     /*PS: Chombo */
    D_EXPAND( UU[BX][k][j][i] = bx_ave;  ,
              UU[BY][k][j][i] = by_ave;  ,
              UU[BZ][k][j][i] = bz_ave; )

    #if CT_EN_CORRECTION == YES && EOS != ISOTHERMAL
     b2_new = D_EXPAND(bx_ave*bx_ave, + by_ave*by_ave, + bz_ave*bz_ave);
    /*     UU[k][j][i][EN] += 0.5*(b2_new - b2_old); */
     /*PS: Chobmo */
     UU[EN][k][j][i] += 0.5*(b2_new - b2_old);
    #endif
  }}}

}

