#include"orion2.h"

#define BX_MIN  1.e-9

/* ***************************************************************************** */
void HLLC_SOLVER (const State_1D *state, real *cmax, Grid *grid)
/* 
 *
 *
 * NAME
 *
 *   HLLC_SOLVER
 *
 *
 * PURPOSE
 *
 *  - Solve riemann problem for the MHD equations using the 
 *    two-state HLLC Riemann solver
 * 
 *     Reference:   "An HLLC RIemann Solver for MHD" 
 *                  S. Li, 2005, JCP 203, 344
 *
 * 
 *   - On input, it takes left and right primitive state
 *     vectors state->vL and state->vR at zone edge i+1/2;
 *     On output, return flux and pressure vectors at the
 *     same interface.
 *
 *   - Also, compute maximum wave propagation speed (cmax) 
 *     for  explicit time step computation
 *  
 *
 * LAST_MODIFIED
 *
 *   April 4th 2006, by Andrea Mignone  (mignone@to.astro.it)
 *
 *
 ******************************************************************************** */
{
  int   nv, i, beg, end;
  real  scrh;
  real  pl, pr;
  real  vBl, usl[NFLX];
  real  vBr, usr[NFLX];

  real  Bxs, Bys, Bzs, ps, vBs;
  real  vxl, vxr, vxs, vys, vzs;
  real  Fhll[NFLX], alpha_l, alpha_r;
  real  **bgf, *vl, *vr, *ul, *ur;
  static real **uR, **uL, **fL, **fR, **Uhll;
  static real *press_L, *press_R, *SR, *SL;

  beg = grid[DIR].lbeg - 1;
  end = grid[DIR].lend;

  if (fL == NULL){
    fL = array_2D(NMAX_POINT, NFLX);
    fR = array_2D(NMAX_POINT, NFLX);
    uL = array_2D(NMAX_POINT, NVAR);
    uR = array_2D(NMAX_POINT, NVAR);
    Uhll = array_2D(NMAX_POINT, NFLX);
    press_L = array_1D(NMAX_POINT);
    press_R = array_1D(NMAX_POINT);
    SL = array_1D(NMAX_POINT);
    SR = array_1D(NMAX_POINT);
  }
  
  #if INCLUDE_BACKGROUND_FIELD == YES
   print (" ! Background field splitting not allowed with HLLC solver\n");
   QUIT_ORION2(1);
  #endif
  
  FLUX  (uL, state->vL, bgf, fL, press_L, beg, end);
  FLUX  (uR, state->vR, bgf, fR, press_R, beg, end);

  /* ----------------------------------------
       get max and min signal velocities
     ---------------------------------------- */
             
  HLL_SPEED (state->vL, state->vR, bgf, SL, SR, beg, end);

  for (i = beg; i <= end; i++) {
    
  /* ----------------------------------------
      get max propagation speed for dt comp.
     ---------------------------------------- */	     

    scrh  = dmax(-SL[i], SR[i]);
    *cmax = dmax(*cmax, scrh);

    vl = state->vL[i]; ul = uL[i];
    vr = state->vR[i]; ur = uR[i];

/* --------------------------------------------
              compute fluxes 
   -------------------------------------------- */

    if (SL[i] >= 0.0){
    
      for (nv = 0; nv < NFLX; nv++) {
        state->flux[i][nv] = fL[i][nv];
      }
      state->press[i] = press_L[i];

    }else if (SR[i] <= 0.0){

      for (nv = 0; nv < NFLX; nv++) {
        state->flux[i][nv] = fR[i][nv];
      }
      state->press[i] = press_R[i];

    }else{

/* ----  define hll states  ----  */

      scrh = 1.0/(SR[i] - SL[i]);
      for (nv = 0; nv < NFLX; nv++){  
        Uhll[i][nv] =   SR[i]*ur[nv] - SL[i]*ul[nv] 
                      + fL[i][nv] - fR[i][nv];
        Uhll[i][nv] *= scrh;
  
        Fhll[nv]  = SL[i]*SR[i]*(ur[nv] - ul[nv])
                   + SR[i]*fL[i][nv] - SL[i]*fR[i][nv];
        Fhll[nv] *= scrh;
      }
      Uhll[i][M1] += (press_L[i] - press_R[i])*scrh;
      Fhll[M1] += (SR[i]*press_L[i] - SL[i]*press_R[i])*scrh;

   /* ---- define total pressure, vB in left and right states ---- */

      pl = EXPAND(vl[BX]*vl[BX], + vl[BY]*vl[BY], + vl[BZ]*vl[BZ]);
      pr = EXPAND(vr[BX]*vr[BX], + vr[BY]*vr[BY], + vr[BZ]*vr[BZ]);

      pl = vl[PR] + 0.5*pl;  
      pr = vr[PR] + 0.5*pr;

      vBl = EXPAND(vl[BX]*vl[VX], + vl[BY]*vl[VY], + vl[BZ]*vl[VZ]);
      vBr = EXPAND(vr[BX]*vr[VX], + vr[BY]*vr[VY], + vr[BZ]*vr[VZ]);

      vxl = vl[V1]; 
      vxr = vr[V1];

   /* ----  magnetic field ---- */

      EXPAND(Bxs = Uhll[i][B1];  ,
             Bys = Uhll[i][B2];  ,
             Bzs = Uhll[i][B3];)

   /* ---- normal velocity vx  ----  */

      vxs = Uhll[i][M1]/Uhll[i][DN];

      if (fabs(Bxs) < BX_MIN){

        ps  = Fhll[M1] - Fhll[DN]*vxs;              

        alpha_l = (SL[i] - vxl)/(SL[i] - vxs);
        alpha_r = (SR[i] - vxr)/(SR[i] - vxs);

        usl[DN] = ul[DN]*alpha_l;
        usr[DN] = ur[DN]*alpha_r;
      
        usl[EN] = (SL[i]*ul[EN] - fL[i][EN] + ps*vxs)/(SL[i] - vxs);
        usr[EN] = (SR[i]*ur[EN] - fR[i][EN] + ps*vxs)/(SR[i] - vxs);

        EXPAND(usl[M1] = usl[DN]*vxs; 
               usr[M1] = usr[DN]*vxs;        ,
               usl[M2] = ul[M2]*alpha_l; 
               usr[M2] = ur[M2]*alpha_r;  ,  
               usl[M3] = ul[M3]*alpha_l; 
               usr[M3] = ur[M3]*alpha_r; )

        EXPAND(usl[B1] = Bxs;
               usr[B1] = Bxs;                 ,
               usl[B2] = ul[B2]*alpha_l;
               usr[B2] = ur[B2]*alpha_r;   ,  
               usl[B3] = ul[B3]*alpha_l;
               usr[B3] = ur[B3]*alpha_r; )

      }else{

        ps  = Fhll[M1] + Bxs*Bxs - Fhll[DN]*vxs;

        EXPAND(                               ,
               vys = (Bys*vxs - Fhll[B2])/Bxs;    ,
               vzs = (Bzs*vxs - Fhll[B3])/Bxs;)
   

        vBs = EXPAND(Bxs*vxs, + Bys*vys, + Bzs*vzs);
/*
EXPAND(                               ,
       vys = (Bys*vxs - Fhll[B2]);    ,
       vzs = (Bzs*vxs - Fhll[B3]);)
scrh = EXPAND(Bxs*Bxs*vxs, + Bys*vys, + Bzs*vzs);
*/
        usl[DN] = ul[DN]*(SL[i] - vxl)/(SL[i] - vxs);
        usr[DN] = ur[DN]*(SR[i] - vxr)/(SR[i] - vxs);

        usl[EN] = (ul[EN]*(SL[i] - vxl) + 
                   ps*vxs - pl*vxl - Bxs*(vBs - vBl))/(SL[i] - vxs);
        usr[EN] = (ur[EN]*(SR[i] - vxr) + 
                   ps*vxs - pr*vxr - Bxs*(vBs - vBr))/(SR[i] - vxs);
/*
        usl[EN] = (ul[EN]*(SL[i] - vxl) + 
                   ps*vxs - pl*vxl - scrh + Bxs*vBl)/(SL[i] - vxs);
        usr[EN] = (ur[EN]*(SR[i] - vxr) + 
                   ps*vxs - pr*vxr - scrh + Bxs*vBr)/(SR[i] - vxs);
*/
        EXPAND(usl[M1] = usl[DN]*vxs;
               usr[M1] = usr[DN]*vxs;        ,

               usl[M2] = (ul[M2]*(SL[i] - vxl) - Bxs*(Bys - vl[B2]))/(SL[i] - vxs);
               usr[M2] = (ur[M2]*(SR[i] - vxr) - Bxs*(Bys - vr[B2]))/(SR[i] - vxs); ,

               usl[M3] = (ul[M3]*(SL[i] - vxl) - Bxs*(Bzs - vl[B3]))/(SL[i] - vxs);
               usr[M3] = (ur[M3]*(SR[i] - vxr) - Bxs*(Bzs - vr[B3]))/(SR[i] - vxs);)

        EXPAND(usl[B1] = usr[B1] = Bxs;   ,
               usl[B2] = usr[B2] = Bys;   ,
               usl[B3] = usr[B3] = Bzs;)

      }

      if (vxs >= 0.0){
        for (nv = 0; nv < NFLX; nv++) {
          state->flux[i][nv] = fL[i][nv] + SL[i]*(usl[nv] - ul[nv]);
        }
        state->press[i] = press_L[i];
      } else {
        for (nv = 0; nv < NFLX; nv++) {
          state->flux[i][nv] = fR[i][nv] + SR[i]*(usr[nv] - ur[nv]);
        }
        state->press[i] = press_R[i];
      }
    }
  }

/* -----------------------------------------------------
               initialize source term
   ----------------------------------------------------- */

  #if MHD_FORMULATION == EIGHT_WAVES
/*
   ROE_DIVB_SOURCE (state, beg, end, grid);
*/

   for (i = beg; i <= end; i++) {

     ur = uR[i];
     ul = uL[i];

     scrh = 1.0 / (SR[i] - SL[i]);
    
     for (nv = 0; nv < NFLX; nv++) {
       Uhll[i][nv] = SR[i]*ur[nv] - SL[i]*ul[nv] +
                     fL[i][nv] - fR[i][nv];
       Uhll[i][nv] *= scrh;
     }
     Uhll[i][M1] += (press_L[i] - press_R[i])*scrh;
   }
   HLL_DIVB_SOURCE (state, SL, SR, Uhll, grid);

  #endif

}
#undef BX_MIN
