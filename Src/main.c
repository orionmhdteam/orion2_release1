#include "orion2.h"
#include "global.h"

static real GET_NEXT_TIME_STEP (Time_Step *, struct INPUT *, real *);
static char *TOTAL_TIME (real dt);
static void INTEGRATE (Data *, Riemann_Solver *, Time_Step *, Grid *);
static void CHECK_FOR_OUTPUT (Data *, Input *, Grid *, int);
static void CHECK_FOR_ANALYSIS (Data *, Input *, Grid *);

int nop;  /* bocchi */

/* *************************************************************** */
int main (int argc, char *argv[])
/*!
 *
 *                     ORION2 MAIN PROGRAM
 *
 *  Main program for ORION2; it calls several initialization
 *  routines and handles the main integration loop.
 *
 *  \author  Andrea Mignone (mignone@to.astro.it)
 *  \date    Feb 21st 2008.  
 *
 ***************************************************************** */
{
  int nv, idim, just_restarted=0, quit_loop = 0;
  real scrh;
  real cmax[3] = {0.0, 0.0, 0.0};
  real dx_min[3];
  Data   data;
  time_t  tbeg, tend;
  Riemann_Solver *Solver;
  Grid      grd[3];
  Time_Step Dts;
  Cmd_Line cmd_line;
  Input  ini;
  Output *output;

  Dts.cmax[IDIR] = Dts.cmax[JDIR] = Dts.cmax[KDIR] = 0.0;
  Dts.eta_max   = 1.e-18;
  Dts.kappa_max = 1.e-18;
  Dts.nu_max    = 1.e-18;
  Dts.dt_cool   = 1.e38;

  #ifdef PARALLEL
   AL_Init (&argc, &argv);
   MPI_Comm_rank (MPI_COMM_WORLD, &prank);
  #endif

  INITIALIZE (argc, argv, &data, &ini, grd, dx_min, &cmd_line);
  Solver = SET_SOLVER (ini.solv_type);

  time (&tbeg);

  if (cmd_line.restart != NO) {
    RESTART (&ini, cmd_line.restart);
    just_restarted = 1;  /* -- avoid writing again as soon as integration restarts -- */
  }

  print1 ("> Starting computation... \n\n");

/* ====================================================================
          M A I N      L O O P      S T A R T S      H E R E
   ==================================================================== */

  while (!quit_loop){

  /* ------------------------------------------
         Adjust dt if tstop is exceeded 
     ------------------------------------------ */

    if ((glob_time + delta_t) >= ini.tstop*(1.0 - 1.e-8)) {
      delta_t   = (ini.tstop - glob_time);
      quit_loop = 1;
    }

    if (NSTEP%ini.log_freq == 0) 
      print1 ("step:%d ; t = %10.4e ; dt = %10.4e ; %d %% ; [%f, %d]\n",
               NSTEP, glob_time, delta_t, (int)(100.0*glob_time/ini.tstop), 
               MAX_MACH_NUMBER, MAX_RIEM);

  /* ----------------------------------------
        check for writing and analysis 
     ---------------------------------------- */

    if (!just_restarted && cmd_line.write) {
      if (!quit_loop){
        CHECK_FOR_OUTPUT (&data, &ini, grd, cmd_line.restart);
        CHECK_FOR_ANALYSIS (&data, &ini, grd);
      }
    }

  /* --------------------------------------------
         Advance system of conservation laws 
         by a time step delta_t
     -------------------------------------------- */

    if (cmd_line.jet) SET_JET_DOMAIN (&data, grd); 

    INTEGRATE (&data, Solver, &Dts, grd);

    if (cmd_line.jet) UNSET_JET_DOMAIN (&data, grd); 

    glob_time += delta_t;
    delta_t    = GET_NEXT_TIME_STEP(&Dts, &ini, dx_min);

    #ifdef PARALLEL
     MPI_Allreduce (&MAX_MACH_NUMBER, &scrh, 1, 
                    MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
     MAX_MACH_NUMBER = scrh;

     MPI_Allreduce (&MAX_RIEM, &nv, 1, MPI_INT, MPI_MAX, MPI_COMM_WORLD);
     MAX_RIEM = nv;
    #endif

    if ((++NSTEP) == (cmd_line.maxsteps + 1) && cmd_line.maxsteps > 0) break;
    just_restarted = 0;

  }

/* ====================================================================
          M A I N       L O O P      E N D S       H E R E 
   ==================================================================== */

  if (cmd_line.write){
    CHECK_FOR_OUTPUT (&data, &ini, grd, cmd_line.restart);
    CHECK_FOR_ANALYSIS (&data, &ini, grd);
  }

  #ifdef PARALLEL
   MPI_Barrier (MPI_COMM_WORLD);
   print  ("\n> Total allocated memory  %6.2f Mb (proc #%d)\n",(float)USED_MEMORY/1.e6,prank);
   MPI_Barrier (MPI_COMM_WORLD);
  #else
   print  ("\n> Total allocated memory  %6.2f Mb\n",(float)USED_MEMORY/1.e6);
  #endif

  time(&tend);
  delta_t = difftime(tend, tbeg);
  print1("> Elapsed time             %s\n", TOTAL_TIME (delta_t));
  print1("> Average time/step       %10.2e  (sec)  \n", 
          difftime(tend,tbeg)/(real)NSTEP);
  print1("> Local time                %s",asctime(localtime(&tend)));
  print1("> Done\n");

  free_array_4D (data.Vc);
  #ifdef PARALLEL
   MPI_Barrier (MPI_COMM_WORLD);
   AL_Finalize ();
  #endif

  return (0);
}

/* ******************************************************************** */
void INTEGRATE (Data *d, Riemann_Solver *Solver, Time_Step *Dts, Grid *grid)
/*
 *
 *
 *           Advance Solution Vector 
 *
 ********************************************************************** */
{
  int idim;

  #if INCLUDE_PARTICLES == YES
   real striction = 1.0;
   static CELL  *courant;
   static HEAD *list;
   static HEAD *temp_list;
  
   if (courant == NULL){
     list = (HEAD *) malloc (sizeof(HEAD));
     temp_list = (HEAD *) malloc (sizeof(HEAD));
  
     list->first = NULL;
     temp_list->first = NULL;
     courant=list->first;
    
     SET_PARTICLES (list,  temp_list, grid, d->Vc);
   }
  #endif

  MAX_MACH_NUMBER = 0.0;
  MAX_RIEM        = 0;

  #if INCLUDE_PARTICLES == YES      /* Perform the predictor step for particles */
   ADVANCE_PARTICLES_predictor (list,temp_list, d->Vc, grid, delta_t, striction );
  #endif

  /* ---------------------------------------------
        perform Strang Splitting on direction 
        (if necessary) and sources 
     --------------------------------------------- */

  FLAG_RESET (d);
  if ((NSTEP%2) == 0){
    #if DIMENSIONAL_SPLITTING == YES
     for (DIR = 0; DIR < DIMENSIONS; DIR++){
       SWEEP (d, Solver, Dts, grid);
     }
    #else
     UNSPLIT (d, Solver, Dts, grid);
    #endif
    SPLIT_SOURCE (d, delta_t, Dts, grid);
  }else{
    SPLIT_SOURCE (d, delta_t, Dts, grid);
    #if DIMENSIONAL_SPLITTING == YES
     for (DIR = DIMENSIONS - 1; DIR >= 0; DIR--){
       SWEEP (d, Solver, Dts, grid);
     }
    #else
     UNSPLIT (d, Solver, Dts, grid);
    #endif        
  }       

  #if INCLUDE_PARTICLES == YES
    
   /* update positions of Particles */   
   /*RENAME_PARTICLES( list );*/
   /* Corrector step of particles with the new fluid velocity */

    ADVANCE_PARTICLES_corrector (list,temp_list, d->Vc, grid, delta_t, striction );

    /*    if( nt%50 == 0){ 
    SAVE_PARTICLES ( list, nt, glob_time, striction, d->Vc );
    };*/

   SAVE_STEP( list, delta_t, nop);

    /* Nullify the temporary list of particles */

  #endif
}

/* ******************************************************************** */
char *TOTAL_TIME (real dt)
/*
 *
 *
 *
 ********************************************************************** */
{
  static char c[128];
  int days, hours, mins, secs;

  days  = (int) (dt/86400.0);
  hours = (int) ((dt - 86400.0*days)/3600.0);
  mins  = (int) ((dt - 86400.0*days - 3600.0*hours)/60.);
  secs  = (int) (dt - 86400.0*days - 3600.0*hours - 60.0*mins);

  sprintf (c, " %dd:%dh:%dm:%ds", days,hours, mins, secs);
  return (c);
}

/* ******************************************************************** */
real GET_NEXT_TIME_STEP (Time_Step *Dts, struct INPUT *ini, real *dx_min)
/*
 *
 *
 *
 ********************************************************************** */
{
  int idim;
  real dt_adv, dt_cool, dt_par;
  real dtnext, dtnext_glob;
  real dxmin2, max_par_coeff;
  
  #if    (DIMENSIONS == 1 && INCLUDE_SPLIT_SOURCE == NO) \
      || (DIMENSIONAL_SPLITTING == NO && INCLUDE_SPLIT_SOURCE == NO) 

  #else

  /*  ------------------------------------------------
       For Strang splitting to be 2nd order accurate,  
       change dt only every 2 time steps                 
      ------------------------------------------------ */
 
   if (NSTEP%2 == 0) return (delta_t);

  #endif

/* ----------------------------------
      Compute Advection time step
   ---------------------------------- */

  D_EXPAND( dt_adv = dx_min[IDIR]/Dts->cmax[IDIR];                  ,
            dt_adv = dmin(dt_adv, dx_min[JDIR]/Dts->cmax[JDIR]);    ,
            dt_adv = dmin(dt_adv, dx_min[KDIR]/Dts->cmax[KDIR]);  )

  dt_adv *= ini->cfl;

  dtnext = Dts->dt_adv = dt_adv;

/* ----------------------------------
      Compute Cooling time step
   ---------------------------------- */

  #if INCLUDE_COOLING != NO
   dtnext = dmin(dtnext, Dts->dt_cool);
  #endif

/* ----------------------------------
      Compute Parabolic Time step
   ---------------------------------- */

  #if INCLUDE_PARABOLIC_FLUX == YES
   dxmin2 = 1.e38;
   for (idim = 0; idim < DIMENSIONS; idim++){
     dxmin2 = dmin(dxmin2, dx_min[idim]*dx_min[idim]);
   }
   max_par_coeff = dmax(Dts->kappa_max, Dts->eta_max);
   max_par_coeff = dmax(max_par_coeff, Dts->nu_max);

   dt_par = 0.5*ini->cfl*dxmin2/(max_par_coeff + 1.e-18);
   dtnext = dmin(dtnext, dt_par);
  #endif
   
/* ---------------------------------------------------------
          Compute local processor time step 
   --------------------------------------------------------- */
  
  dtnext = dmin(dtnext, ini->cfl_max_var*delta_t);

/* -----------------------------------------------------
      Get min time step among ALL processors
   ----------------------------------------------------- */

  #ifdef PARALLEL
   MPI_Allreduce (&dtnext, &dtnext_glob, 1, MPI_ORION2_REAL, MPI_MIN, MPI_COMM_WORLD);
   dtnext = dtnext_glob;
  #endif

  #ifdef PSI_GLM
   dxmin2 = dmin(dx_min[IDIR], dx_min[JDIR]);
   #if DIMENSIONS == 3
    dxmin2 = dmin(dxmin2, dx_min[KDIR]);
   #endif

   MAXC = ini->cfl*dxmin2/dtnext;
   #if PHYSICS == RMHD 
    MAXC = 1.0;
   #endif
/*
  {
    double maxc_glob;
    D_EXPAND(MAXC = Dts->cmax[IDIR];             ,
             MAXC = dmax(MAXC, Dts->cmax[JDIR]); ,
             MAXC = dmax(MAXC, Dts->cmax[KDIR]); )

    #ifdef PARALLEL
     MPI_Allreduce (&MAXC, &maxc_glob, 1, MPI_ORION2_REAL, MPI_MAX, MPI_COMM_WORLD);
     MAXC = maxc_glob;
    #endif
  }
*/

  #endif


/* -----------------------------------------------------
          quit if dt gets too small 
   ----------------------------------------------------- */

  if (dtnext < ini->first_dt*1.e-9){
    print1 ("! dt is too small (%12.6e)!\n", dtnext);
    print1 ("! Cannot continue\n");
    QUIT_ORION2(1);
  }

/* -----------------------------------------------------
          Reset time step coefficients
   ----------------------------------------------------- */

  for (idim = 0; idim < DIMENSIONS; idim++) Dts->cmax[idim] = 0.0;
  Dts->eta_max = Dts->kappa_max = Dts->nu_max = 1.e-18;
  Dts->dt_cool = 1.e38;

/* ------------------------------------------------------
     Issue a warning if first_dt has been overestimaed
   ------------------------------------------------------ */

  if (NSTEP <= 1 && (dtnext < ini->first_dt)){
    print1 ("! initial dt exceeds stability limit\n");
  }

  return(dtnext);
}

/* ******************************************************************** */
void CHECK_FOR_OUTPUT (Data *d, Input *ini, Grid *grid, int nrestart)
/*
 *
 *
 *
 *
 *
 ********************************************************************** */
{
  int  n, check_dt, check_dn;
  int  dump_restart;
  real t, tnext;
  Output *output;

  dump_restart = -1;
  t     = glob_time;
  tnext = t + delta_t;

  for (n = 0; n < MAX_OUTPUT_TYPES; n++){
    output = ini->output + n;    

    check_dt = (int) (tnext/output->dt) - (int)(t/output->dt);
    check_dt = check_dt || NSTEP == 0 || fabs(t - ini->tstop) < 1.e-12;        
    check_dt = check_dt && (output->dt > 0.0);

    check_dn = (NSTEP%output->dn) == 0;
    check_dn = check_dn || NSTEP == 0;        
    check_dn = check_dn && (output->dn > 0);

    if (check_dt || check_dn) { 
      WRITE_DATA (d, output, grid);
      if (output->type == DBL_OUTPUT) dump_restart = output->nfile - 1;
    }
  }

/* -----------------------------------------------
               Dump restart data. 
   ----------------------------------------------- */

  if (dump_restart != -1) RESTART_DUMP (ini, dump_restart);    

}

/* ******************************************************************** */
void CHECK_FOR_ANALYSIS (Data *d, Input *ini, Grid *grid)
/*
 *
 *
 *
 *
 *
 ********************************************************************** */
{
  int check_dt, check_dn;
  double t, tnext;

  t     = glob_time;
  tnext = t + delta_t;
  check_dt = (int) (tnext/ini->anl_dt) - (int)(t/ini->anl_dt);
  check_dt = check_dt || NSTEP == 0 || fabs(t - ini->tstop) < 1.e-9; 
  check_dt = check_dt && (ini->anl_dt > 0.0);

  check_dn = (NSTEP%ini->anl_dn) == 0;
  check_dn = check_dn && (ini->anl_dn > 0);

  if (check_dt || check_dn) { 
    ANALYSIS (d, grid);
  }
}

/* ***************************************************************** */
#if SAVE_VEC_POT == YES
void VEC_POT (const Data *d, const void *vp, const State_1D *state,
              const Grid *grid)
/* 
 *
 * PURPOSE
 *
 *  Update the vector potential by properly averaging 
 *  the electric fields. 
 *
 *  staggered_mhd: we average the electric field values 
 *                 available at cell edges. In this
 *                 case the function does the job only 
 *                 if called from FCT;
 *
 *  Cell-cent. mhd: we average the face values. In this
 *                  case the function does the job only 
 *                  if called from time stepping algorithms.
 *
 *  Note that the vector potential is NOT used in the rest
 *  of the code and serves solely as a diagnostic tool.
 *
 * A NOTE ON CYLINDRICAL COORDINATES:
 * ---------------------------------
 *
 * ORION2 treats cylindrical (but not polar) coordinates as 
 * Cartesian so that (x1,x2) = (r,z).
 * Nevertheless, the correct right-handed cylindrical system is 
 * defined by (x1,x2,x3) = (r,phi,z). The fact that the phi 
 * coordinate is missing causes an ambiguity when computing the 
 * third component of the EMF in 2.5 D:
 *
 *    E3 = v2*B1 - v1*B2
 *
 * which is (wrongly) called "Ez" but is, indeed, "-E_\phi".
 * This is not a problem in the update of the magnetic field, since
 * the contribution coming from E_3 is also reversed:
 *
 * \delta B_1 = -dt * dE3/dx2    ===>   \delta B_r = dt*dE_\phi/dz
 *
 * as it should be.
 * However, it is a problem in the definition of the vector potential
 * which is consistently computed using its physical definition.
 * Therefore, to correctly update the phi component of the vector
 * potential we change sign to Ez (= -E_\phi).
 *
 ******************************************************************* */
{
  int    i,j,k;
  double scrh, dt, sEz = 0.25;
  double Ex, Ey, Ez;

/* ---------------------------------------------------------
    When this function is called from Time stepping 
    routines and CT is used, do nothing.
   --------------------------------------------------------- */

  #ifdef STAGGERED_MHD
   if (vp == NULL) return;
  #endif

  #if GEOMETRY == CYLINDRICAL 
   sEz = -0.25;
  #endif

/* ------------------------------------------------------
     Detetmine the dt according to the integration step
   ------------------------------------------------------ */

  dt = delta_t;
  #if TIME_STEPPING == RK2
   dt *= 0.5;
  #elif TIME_STEPPING == RK3
   dt /= 6.0;
   if (ISTEP == 3) dt *= 4.0;
  #endif

  #ifdef SINGLE_STEP  
   #if DIMENSIONAL_SPLITTING == NO
    if (ISTEP == 1) return;
   #endif
  #endif

  #ifdef STAGGERED_MHD
/* -----------------------------------------------------
     If staggered fields are used, we average the 
     EMF available at cell edges:

     A-----------B
     |           |
     |           |
     |     o     |    o = (A+B+C+D)/4
     |           |
     |           |
     D-----------C
   ----------------------------------------------------- */
  {
    EMF *emf;
    emf = (EMF *) vp;

    DOM_LOOP(k,j,i){
      Ez = sEz*(emf->ez[k][j][i]     + emf->ez[k][j-1][i] + 
                emf->ez[k][j-1][i-1] + emf->ez[k][j][i-1]);

      #if DIMENSIONS == 3
       Ex = 0.25*(emf->ex[k][j][i]     + emf->ex[k][j-1][i] + 
                  emf->ex[k-1][j-1][i] + emf->ex[k-1][j][i]);

       Ey = 0.25*(emf->ey[k][j][i]     + emf->ey[k][j][i-1] + 
                  emf->ey[k-1][j][i-1] + emf->ey[k-1][j][i]);
      #endif

      d->A3[k][j][i] -= dt*Ez; 
      #if DIMENSIONS == 3
       d->A1[k][j][i] -= dt*Ex; 
       d->A2[k][j][i] -= dt*Ey; 
      #endif

    }   
  }
  #else 
/* -----------------------------------------------------
     If cell-center is used, we average the 
     EMF available at cell faces:

     +-----B-----+
     |           |
     |           |
     A     o     C    o = (A+B+C+D)/4
     |           |
     |           |
     +-----D-----+
   ----------------------------------------------------- */
  {
    double **f;

/*
  EXPAND(vx = d->Vc[VX]; Bx = d->Vc[BX];  ,
         vy = d->Vc[VY]; By = d->Vc[BY];  ,
         vz = d->Vc[VZ]; Bz = d->Vc[BZ];)
*/
/*
  DOM_LOOP(k,j,i){
    D_EXPAND(                                                  ,
      Ez = vy[k][j][i]*Bx[k][j][i] - vx[k][j][i]*By[k][j][i];  ,
      Ex = vz[k][j][i]*By[k][j][i] - vy[k][j][i]*Bz[k][j][i];  
      Ey = vx[k][j][i]*Bz[k][j][i] - vz[k][j][i]*Bx[k][j][i];)

    D_EXPAND(                    ,
      d->A3[k][j][i] -= dt2*Ez;  ,
      d->A1[k][j][i] -= dt2*Ex; 
      d->A2[k][j][i] -= dt2*Ey;)

  }   
*/
    f = state->flux;
    if (DIR == IDIR){

      for (i = IBEG; i <= IEND; i++) 
        d->A3[*NZ_PT][*NY_PT][i] -= -sEz*dt*(f[i][BY] + f[i-1][BY]); 

    }else if (DIR == JDIR){

      for (j = JBEG; j <= JEND; j++) 
        d->A3[*NZ_PT][j][*NX_PT] -= sEz*dt*(f[j][BX] + f[j-1][BX]);  

    }
  }
  #endif /* STAGGERED_MHD */
}
#endif  /* SAVE_VEC_POT == YES */
