#ifdef CH_LANG_CC
/*
*      _______              __
*     / ___/ /  ___  __ _  / /  ___
*    / /__/ _ \/ _ \/  V \/ _ \/ _ \
*    \___/_//_/\___/_/_/_/_.__/\___/
*    Please refer to Copyright.txt, in Chombo's root directory.
*
*    Modification: Orional code based on PLUTO3.0b2
*    1) PS(09/25/08): Modified original code for CT scheme.
*    2) PS(12/04/08): Upgrade to PLUTO3 final release.
*    3) PS(02/20/09): Use FluxBox class for face-centered magnetic field.
*    4) PS(09/18/09): Upgrade to PLUTO 3.0.1
*    5) PS(08/06/10): Rename Pluto to Orion.
*/
#endif

#include <cstdio>
#include <string>
using std::string;

#include "PatchOrion.H"
#include "LoHiSide.H"


// Flag everything as not defined or set
PatchOrion::PatchOrion()
{
  m_isDefined = false;
  m_isBoundarySet = false;
  m_isRiemannSet = false;
  m_isExtFileSet = false;
  m_isCurrentTimeSet = false;
  m_isCurrentBoxSet = false;

}

PatchOrion::~PatchOrion()
{
}

// Define this object and the boundary condition object
void PatchOrion::define(ProblemDomain& a_domain,
                          const Real&    a_dx,
                          const int&     a_level,
                          const int&     a_numGhost)
{

  // Store the domain and grid spacing
  m_domain = a_domain;
  m_dx = a_dx;
  m_level = a_level;
  m_numGhost = a_numGhost;

  m_isDefined = true;
}

// Factory method - this object is its own factory.  It returns a pointer
// to new PatchOrion object with its boundary condtions defined.
 PatchOrion* PatchOrion::new_patchOrion() const
{
   // Make the new object
   PatchOrion* retval =
     static_cast<PatchOrion*>(new PatchOrion());

   // Set the boundary and Riemann solver values
   retval->setBoundary(left_bound_side,
                       right_bound_side);
   retval->setRiemann(rsolver);
   retval->setExtFile(ExtFile);

   // Return the new object
   return retval;
}

// Set the current physical time - used for time dependent boundary conditions
void PatchOrion::setCurrentTime(const Real& a_currentTime)
{
  m_currentTime = a_currentTime;
  m_isCurrentTimeSet = true;
}

// Set the current box for the UNSPLIT call
void PatchOrion::setCurrentBox(const Box& a_currentBox)
{
  m_currentBox = a_currentBox;
  m_isCurrentBoxSet = true;
}

// Set the values corresponding to boundary conditions
void PatchOrion::setBoundary(const int *leftbound,
                             const int *rightbound)
{

 for (int i = 0; i < 3; i++)
    {
      left_bound_side[i] =  leftbound[i];
     right_bound_side[i] = rightbound[i];
    }

 m_isBoundarySet = true;

}

// Set the riemann solver to be used
void PatchOrion::setRiemann(Riemann_Solver *solver)
{

   rsolver = solver; 	
   m_isRiemannSet = true;

}

// Set the flag to restart from an external file
void PatchOrion::setExtFile(const int exfile)
{

  ExtFile = exfile;
  m_isExtFileSet = true;
 
}

// Set the grid[3] structure to be passed to updateState
// (or UNSPLIT and SWEEP in the next future)
void PatchOrion::setGrid(const Box&    a_box, struct GRID  *grid)
{
  int    i, np_int, np_tot, np_int_glob, np_tot_glob;
  struct GRID *G;

  for (int dir = 0; dir < SpaceDim; ++dir){  
    grid[dir].np_tot      = a_box.size()[dir]+2*m_numGhost;
    grid[dir].np_int      = a_box.size()[dir];
    grid[dir].np_tot_glob = m_domain.domainBox().size()[dir]+2*m_numGhost;
    grid[dir].np_int_glob = m_domain.domainBox().size()[dir];
    grid[dir].nghost      = m_numGhost;
    grid[dir].beg         = a_box.loVect()[dir]+m_numGhost;
    grid[dir].end         = a_box.hiVect()[dir]+m_numGhost;
    grid[dir].gbeg        = m_domain.domainBox().loVect()[dir]+m_numGhost;
    grid[dir].gend        = m_domain.domainBox().hiVect()[dir]+m_numGhost;
    grid[dir].lbeg        = m_numGhost;
    grid[dir].lend        = grid[dir].np_int+m_numGhost-1;
   
    grid[dir].lbound = 0;
    grid[dir].rbound = 0;
   // Set flags to compute boundary conditions
   // is_gbeg (left boundary)
    Box tmp = a_box;
    tmp.shift(dir,-m_numGhost);
    tmp &= m_domain;
    //    tmp.shift(dir,1);
    tmp.shift(dir,m_numGhost);
    if (tmp != a_box) grid[dir].lbound = 1;
   // is_gend (right_boundary)
    tmp = a_box;
    tmp.shift(dir,m_numGhost);
    tmp &= m_domain;
    tmp.shift(dir,-m_numGhost);

    if (tmp != a_box) grid[dir].rbound = 1;
    //    cout << grid[dir].lbound << " " << grid[dir].rbound
    //	 << " dir = " << dir << endl;
  }

  for (int dir = 0; dir < 3; ++dir){
    G = grid + dir;
    if (dir >= SpaceDim) {
        G->np_int = G->np_tot = G->np_int_glob = G->np_tot_glob = 1;
        G->nghost = G->beg = G->end = G->gbeg = G->gend =
        G->lbeg   = G->lend = G->lbound = G->rbound = 0;
    }
    np_tot_glob = G->np_tot_glob;
    np_int_glob = G->np_int_glob;
    np_tot = G->np_tot;
    np_int = G->np_int;

    G->x  = Array_1D(np_tot, double);
    G->xr = Array_1D(np_tot, double);
    G->xl = Array_1D(np_tot, double);
    G->dx = Array_1D(np_tot, double);

    for (i = 0; i < np_tot; i++){
      G->xl[i]  = Real(i+grid[dir].beg-2*grid[dir].nghost)*m_dx;
      G->xl[i] += DOM_XBEG[dir];
      G->x[i]  = G->xl[i]+0.5*m_dx;
      G->xr[i] = G->xl[i]+m_dx;
      G->dx[i] = m_dx; 
    }
  }

  CH_assert(m_isBoundarySet);
  grid[IDIR].lbound *=   left_bound_side[IDIR];
  grid[IDIR].rbound *=  right_bound_side[IDIR];
  grid[JDIR].lbound *=   left_bound_side[JDIR];
  grid[JDIR].rbound *=  right_bound_side[JDIR];
  grid[KDIR].lbound *=   left_bound_side[KDIR];
  grid[KDIR].rbound *=  right_bound_side[KDIR];
  MAKE_GEOMETRY(grid);

}

// Loop over the patches of a level to assign initial conditions

#ifdef STAGGERED_MHD
#ifdef PUMP
void PatchOrion::initialize(LevelData<FArrayBox>& a_U,
                            LevelData<FArrayBox>& a_SDrive,
                            LevelData<FluxBox>& a_Bs)
{

 CH_assert(m_isDefined == true);

 DataIterator dit = a_U.boxLayout().dataIterator();

 // Iterator of all grids in this level
 for (dit.begin(); dit.ok(); ++dit)
 {
   // Storage for current grid
   FArrayBox& U = a_U[dit()];
   FArrayBox& SDrive = a_SDrive[dit()];
   FluxBox&    Bs = a_Bs[dit()];
   FArrayBox& Bxs = Bs[0];
   FArrayBox& Bys = Bs[1];
   FArrayBox& Bzs = Bs[2];
   // Set up initial condition in this patch
   STARTUP(U,SDrive,Bxs,Bys,Bzs);
 }
   
}

#else

void PatchOrion::initialize(LevelData<FArrayBox>& a_U,
                            LevelData<FluxBox>& a_Bs)
{

 CH_assert(m_isDefined == true);

 DataIterator dit = a_U.boxLayout().dataIterator();

 // Iterator of all grids in this level
 for (dit.begin(); dit.ok(); ++dit)
 {
   // Storage for current grid
   FArrayBox& U = a_U[dit()];
   FluxBox& Bs = a_Bs[dit()];
   FArrayBox& Bxs = Bs[0];
   FArrayBox& Bys = Bs[1];
   FArrayBox& Bzs = Bs[2];
   // Set up initial condition in this patch
   STARTUP(U,Bxs,Bys,Bzs);
 }
   
}

#endif /* PUMP */

#else /* STAGGERED_MHD */

#ifdef PUMP

void PatchOrion::initialize(LevelData<FArrayBox>& a_U,
                            LevelData<FArrayBox>& a_SDrive)
{

 CH_assert(m_isDefined == true);

 DataIterator dit = a_U.boxLayout().dataIterator();

 // Iterator of all grids in this level
 for (dit.begin(); dit.ok(); ++dit)
 {
   // Storage for current grid
   FArrayBox& U = a_U[dit()];
   FArrayBox& SDrive = a_SDrive[dit()];
   // Set up initial condition in this patch
   STARTUP(U,SDrive);
 }

}

#else /* PUMP */

void PatchOrion::initialize(LevelData<FArrayBox>& a_U)
{

 CH_assert(m_isDefined == true);

 DataIterator dit = a_U.boxLayout().dataIterator();

 // Iterator of all grids in this level
 for (dit.begin(); dit.ok(); ++dit)
 {
   // Storage for current grid
   FArrayBox& U = a_U[dit()];
   // Set up initial condition in this patch
   STARTUP(U);
 }
   
}
#endif /* PUMP */
#endif /* STAGGERED_MHD */

// Number of conserved variables
int PatchOrion::numConserved()
{
   return NVAR;
}

// Generate default names for the conserved variables, "variable#"
Vector<string> PatchOrion::stateNames()
{
  Vector<string> retval;
  int nv;
  char varNameChar[80];

  for (nv = 0; nv < NVAR; nv++){
    if (nv == DN) retval.push_back("density");
    if (nv == MX) retval.push_back("X-momentum");
    if (nv == MY) retval.push_back("Y-momentum");
    if (nv == MZ) retval.push_back("Z-momentum");
    #if PHYSICS == MHD || PHYSICS == RMHD
     if (nv == BX) retval.push_back("X-magnfield");
     if (nv == BY) retval.push_back("Y-magnfield");
     if (nv == BZ) retval.push_back("Z-magnfield");
    #endif
    #if EOS != ISOTHERMAL
     if (nv == EN) retval.push_back("energy-density");
    #endif

    #if NTRACER > 0   
     if (nv >= TR && nv < (TR + NTRACER)){
       sprintf(varNameChar,"tracer%d",nv+1-TR);
       retval.push_back(string(varNameChar));
     }
    #endif

    #ifdef PSI_GLM 
     if (nv == PSI_GLM) retval.push_back("psi_glm");
    #endif

    #if INCLUDE_COOLING == RAYMOND
     if (nv == FNEUT) retval.push_back("fneut");
    #endif
    #if INCLUDE_COOLING == NEQ
     if (nv == HI)    retval.push_back("fHI");
     if (nv == HeI)   retval.push_back("fHeI");
     if (nv == HeII)  retval.push_back("fHeII");
     if (nv == CI)    retval.push_back("fCI");
     if (nv == CII)   retval.push_back("fCII");
     if (nv == CIII)  retval.push_back("fCIII");
     if (nv == CIV)   retval.push_back("fCIV");
     if (nv == CV)    retval.push_back("fCV");
     if (nv == NI)    retval.push_back("fNI");
     if (nv == NII)   retval.push_back("fNII");
     if (nv == NIII)  retval.push_back("fNIII");
     if (nv == NIV)   retval.push_back("fNIV");
     if (nv == NV)    retval.push_back("fNV");
     if (nv == OI)    retval.push_back("fOI");
     if (nv == OII)   retval.push_back("fOII");
     if (nv == OIII)  retval.push_back("fOIII");
     if (nv == OIV)   retval.push_back("fOIV");
     if (nv == OV)    retval.push_back("fOV");
     if (nv == NeI)   retval.push_back("fNeI");
     if (nv == NeII)  retval.push_back("fNeII");
     if (nv == NeIII) retval.push_back("fNeIII");
     if (nv == NeIV)  retval.push_back("fNeIV");
     if (nv == NeV)   retval.push_back("fNeV");
     if (nv == SI)    retval.push_back("fSI");
     if (nv == SII)   retval.push_back("fSII");
     if (nv == SIII)  retval.push_back("fSIII");
     if (nv == SIV)   retval.push_back("fSIV");
     if (nv == SV)    retval.push_back("fSV");
    #endif
  }

#if NVAR > NFLX
 for (int ivar = 1; ivar <= NVAR-NFLX; ivar++)
 {
   char varNameChar[80];
   sprintf(varNameChar,"tracer%d",ivar);
   retval.push_back(string(varNameChar));
 }
#endif

  return retval; 
}

// Number of flux variables
int PatchOrion::numFluxes()
{
  // In some computations there may be more fluxes than conserved variables
  return NVAR;
}

// Number of primitive variables
int PatchOrion::numPrimitives()
{
  // This doesn't equal the number of conserved variables because
  // auxiliary/redundant variable may be computed and stored
  return NVAR;
}

// Component index within the primitive variables of the pressure
int PatchOrion::pressureIndex()
{
  #if EOS != ISOTHERMAL
   return PR;
  #else 
   return -1;
  #endif
}

// Return true if everything is defined and setup
bool PatchOrion::isDefined() const
{
  return m_isDefined        &&
         m_isBoundarySet    &&
         m_isRiemannSet     &&
	 m_isExtFileSet     && 
         m_isCurrentTimeSet &&
         m_isCurrentBoxSet;
}
