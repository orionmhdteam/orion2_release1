#ifdef CH_LANG_CC
/*
*      ____                         __ __
*     / __ \ ____ __ ___   __  __  / // /
*    / /_/ // __// // _ \ /  \/ / / // /
*    \____//_/  /_/ \___//_/\__/ /_//_/
*    
*    Based on Pluto 3 and Chombo.
*
*    Please refer to COPYING in Pluto's root directory and 
*    Copyright.txt, in Chombo's root directory.
*
*    Modification: Orional code based on PLUTO3.0b2
*    1) PS(09/25/08): Modified original code for CT scheme
*    2) PS(08/06/10): Rename Pluto to Orion.
*    3) PS(05/12/11): Implement gravity solver.
*
*/
#endif

#ifndef _AMR_LEVEL_GODUNOV_H_
#define _AMR_LEVEL_GODUNOV_H_

#include "AMRLevel.H"
#include "CoarseAverage.H"
//PS: AMR for face-centered and edge-centered boxes
#include "CoarseAverageFace.H"
#include "EdgeDataBox.H"

#include "LevelOrion.H"

// PS: self gravity
#ifdef GRAVITY
#include "Gradient.H"
#endif /* GRAVITY */

#ifdef SINKPARTICLE
#if defined(STARPARTICLE) 
#include <StarParticleList.H>
#include <StarParticleData.H>
#else
#include <SinkParticleList.H>
#include <SinkParticleData.H>
#endif //STARPARTICLE 
#endif //SINKPARTICLE






/// AMR Orion
/**
 */
class AMRLevelOrion : public AMRLevel
{
public:
   /// Constructor
  /**
   */
  AMRLevelOrion();

  /// Destructor
  /**
   */
  virtual ~AMRLevelOrion();

  /// This instance should never get called - historical
  /**
   */
  virtual void define(AMRLevel*  a_coarserLevelPtr,
                      const Box& a_problemDomain,
                      int        a_level,
                      int        a_refRatio);

  /// Define new AMR level
  /**
   */
  virtual void define(AMRLevel*            a_coarserLevelPtr,
                      const ProblemDomain& a_problemDomain,
                      int                  a_level,
                      int                  a_refRatio);


  /// Advance by one timestep
  /**
   */
  virtual Real advance();

  /// Things to do after a timestep
  /**
   */
  virtual void postTimeStep();

  /// Create tags for regridding
  /**
   */
  virtual void tagCells(IntVectSet& a_tags) ;

  /// Create tags at initialization
  /**
   */
  virtual void tagCellsInit(IntVectSet& a_tags) ;

  /// Set up data on this level after regridding
  /**
   */
  virtual void regrid(const Vector<Box>& a_newGrids);

  // PS: gravity
#ifdef GRAVITY
  /// Things to do after initialization (currently just self gravity)
  /**
   */

  virtual void postRegrid(int a_base_level);
#endif /* GRAVITY */

  /// Initialize grids
  /**
   */
  virtual void initialGrid(const Vector<Box>& a_newGrids);

  /// Initialize data
  /**
   */
  virtual void initialData();

  /// Things to do after initialization
  /**
   */
  virtual void postInitialize();

  /// Create a local PatchOrion factory using the argument as a factory
  /**
   */
  virtual void patchOrion(const PatchOrion* const a_patchOrion);

#ifdef CH_USE_HDF5
  /// Write checkpoint header
  /**
   */
  virtual void writeCheckpointHeader(HDF5Handle& a_handle) const;

  /// Write checkpoint data for this level
  /**
   */
  virtual void writeCheckpointLevel(HDF5Handle& a_handle) const;

  /// Read checkpoint header
  /**
   */
  virtual void readCheckpointHeader(HDF5Handle& a_handle);

  /// Read checkpoint data for this level
  /**
   */
  virtual void readCheckpointLevel(HDF5Handle& a_handle);

  /// Write plotfile header
  /**
   */
  virtual void writePlotHeader(HDF5Handle& a_handle) const;

  /// Write plotfile data for this level
  /**
   */
  virtual void writePlotLevel(HDF5Handle& a_handle) const;
#endif

  /// Returns the dt computed earlier for this level
  /**
   */
  virtual Real computeDt();

  /// Compute dt using initial data
  /**
   */
  virtual Real computeInitialDt();

  /// Set the CFL number
  /**
   */
  virtual void CFL(Real a_cfl);

  /// Set the physical dimension of the longest side of the domain
  /**
   */
  virtual void domainLength(Real a_domainLength);

  /// Set the refinement threshold
  /**
   */
  virtual void densityGradThreshold(vector<Real> *a_thresh);
  virtual void velocityGradThreshold(vector<Real> *a_thresh);
  virtual void diskTagThreshold(vector<Real> *a_thresh);
  virtual void BGradThreshold(vector<Real> *a_thresh);
  virtual void radiationGradThreshold(vector<Real> *a_thresh);
  virtual void shockDetectionThreshold(vector<Real> *a_thresh);
  virtual void JeansNoThreshold(vector<Real> *a_thresh);
  virtual void Zoom(vector<Real> *a_thresh);
  virtual void Nested(vector<Real> *a_thresh);
  virtual void NestedSphere(vector<Real> *a_thresh);
  virtual void DensityTagThreshold(vector<Real> *a_thresh);
  
  void mark_split(const DisjointBoxLayout& finerLevelDomain);

  /// Set the tag buffer size
  /**
   */
  void tagBufferSize(int a_tagBufferSize);

  // ATM - these functions are used in the radiation sync step to recursively 
  // interpolate the correction up to finer levels
 

  /// AJC - changed the return type of this to non-const
  // because sinkParticles need to be able to alter the state
  LevelData<FArrayBox>& getStateNew();
  LevelData<FArrayBox>& getForceNew();

  ///
  const LevelData<FArrayBox>& getStateOld() const;
  
  // AJC 
  const real getDx() {return m_dx;}

  // PS: self Gravity
#ifdef GRAVITY
  // ATM - need to enforce periodic boundary conditions
  Real getGlobalMeanDensity();

  // AJC - convenience functions for phiNew & phiOld
  LevelData<FArrayBox>& getPhiNew();
  const LevelData<FArrayBox>& getPhiOld() const;

  /// get the RHS to Poisson eq., interpolated to a_time
  virtual LevelData<FArrayBox>* getPoissonRhs(const Real& a_time);

  /// get the Phi, the gravitational potential
  virtual LevelData<FArrayBox>* getPhi(const Real& a_time);

  /// Solves Poisson eq. on the hierarchy of this and higher levels.
  virtual void gravity(const int  a_baseLevel, const bool a_srceCorr = true);

  // solves elliptic (Poisson's) equation
  virtual void ellipticSolver(const int  a_baseLevel,
                              const bool a_isLevelSolve);

  /// Overloaded version (see below) to be called by gravity()
  virtual void computeForce();

  /// compute 2nd order corrections for gravity source term
  virtual void secondOrderCorrection();

  /// set boolean stating whether or not this is the finest lev.
  virtual void isThisFinestLev(const bool a_isThisFinestLev);

  /// return bool stating whether or not this is the finest level.
  virtual bool isThisFinestLev() const;

  ///
  bool allDefined() const;
#endif /* GRAVITY */

#ifdef SINKPARTICLE
#if defined(STARPARTICLE) 
  void setSinkList(StarParticleList<StarParticleData>* a_sinks);
#else
  void setSinkList(SinkParticleList<SinkParticleData>* a_sinks);
#endif //STARPARTICLE 
#endif //SINKPARTICLE

protected:
  // Checks for negative energies and densities
  int checkState(int doGhosts, int loc);

  // enforces alfven speed cap
  static Real CeilVA_mass;
  // PS: add monitors
  static Real CeilVA_momentum, CeilVA_energy;
  Real * enforceCeilVA();

  // Create a load-balanced DisjointBoxLayout from a collection of Boxes
  DisjointBoxLayout loadBalance(const Vector<Box>& a_grids);

  // Setup menagerie of data structures
  void levelSetup();

  // Get the next coarser level
  AMRLevelOrion* getCoarserLevel() const;

  // Get the next finer level
  AMRLevelOrion* getFinerLevel() const;

  // PS : self gravity
#if defined(GRAVITY) || defined(SINKPARTICLE)
  // Compute force from a potential.
  void computeForce(LevelData<FArrayBox>&  a_force,
                    LevelData<FArrayBox>& a_phi,
                    const Real&           a_time);

  // Number of ghost cells where it's necessary to compute the force
  int m_numForceGhost;
#endif
#ifdef GRAVITY
  //
  void makePoissonRhs(LevelData<FArrayBox>&       a_rhs,
                      const LevelData<FArrayBox>& a_U,
                      const Real&                 a_time);

  // gradient operator
  Gradient* m_gradient;

  // Number of ghost cells (in each direction) for RHS to Poisson eq.
  int m_numRhsGhost;

  // Number of ghost for the potential; always 1 except for 4 pts stencil
  int m_numPhiGhost;

  StencilType a_stencil;

  // Gravitational potential Phi for projection at old and new time
  LevelData<FArrayBox> m_phiOld,m_phiNew,m_phiInt,m_deltaPhi;

  //
  LevelData<FArrayBox> m_forceOld, m_forceNew;

  // whether or not the deltaPhi correction should be applied
  bool m_useDeltaPhiCorr;
  bool a_useDeltaPhiCorr;

  // boolean stating whether or not this is the FINEst lev.
  bool m_isThisFinestLev;

  // grad(Phi)=Rhs = 1.5*rho_gas
  LevelData<FArrayBox> m_rhs;
  LevelData<FArrayBox> m_rhsCrseNew, m_rhsCrseOld;

  // Interpolation from coarse to fine level
  FineInterp m_fineInterpPhi;

  // Averaging from fine to coarse level
  CoarseAverage m_coarseAveragePhi;

  // 2nd order interpolant for filling in ghost cells from next coarser level
  QuadCFInterp m_quadCFInterp;

  // Interpolator for filling in force ghost cells from the next coarser level
  PiecewiseLinearFillPatch m_forcePatcher;

  //
  Real m_rhsOffset;

  // True if all the parameters for this object are defined
  bool m_paramsDefined;

#endif /* GRAVITY */

#ifdef SINKPARTICLE
  LevelData<FArrayBox> m_sinkAccel;
#if defined(STARPARTICLE) 
  static StarParticleList<StarParticleData>* m_sinks;
#else
  static SinkParticleList<SinkParticleData>* m_sinks;
#endif //STARPARTICLE 
#endif //SINKPARTICLE

  // Conserverd state, U, at old and new time
  LevelData<FArrayBox> m_UOld,m_UNew;

  // Face-centered B-field at old and new time
  LevelData<FluxBox> m_BsOld,m_BsNew;

  // Turbulence driving: driving arrays and driving amplitude
  LevelData<FArrayBox> m_DriveOld,m_DriveNew;

  /// do mini-projection on newly interpolated fine-grid cells
  /** do a small MAC-projection type operation to ensure that 
      newly interpolated B is divergence-free on new fine grid
      after regridding.  a_newFineCells contains the coarse-grid
      cells underlying the newly-refined fine grid cells. */
  void projectNewB(const LayoutData<IntVectSet>& a_newFineCells);


  // Union of FArrayBoxes marking the split/unsplit cells of this level
  LevelData<FArrayBox> m_split_tags;

  // Like m_split_tags but one cell smaller ...
  LevelData<FArrayBox> m_cover_tags;
  
  /// ensure face-centered magnetic field is divergence free after regrid
  static bool s_project_new_B;

  // CFL number
  Real m_cfl;

  // Grid spacing
  Real m_dx;

  // Interpolation from fine to coarse level
  FineInterpOrion m_fineInterp;
  FineInterpOrion m_fineInterpDrive;
  // ATM

  //PS: AMR for face-centered box
  FineInterpFaceOrion m_fineInterpFace;

  // Averaging from coarse to fine level
  CoarseAverage m_coarseAverage;
  //PS: AMR for face-centered box
  CoarseAverageFace m_coarseAverageFace;


  // New time step
  Real m_dtNew;

#ifdef GRAVITY
  Real m_dtOld;
  Real m_globalMeanDensity;
#endif

  // Number of converved states
  int m_numStates;

  // Names of conserved states
  Vector<string> m_stateNames;

  // Number of ghost cells (in each direction)
  int m_numGhost;

  // Physical dimension of the longest side of the domain
  Real m_domainLength;

  // Level integrator
  LevelOrion m_levelOrion;

  // Flux register
  LevelFluxRegister m_fluxRegister;

  // Flux register for EdgeDataBox
  LevelFluxRegisterEdge m_FC_fluxRegister;

// Chemistry Routines (ATL, 2014)



// Extract Disk Mass, AFW




  /*
  // Pointer to the class defining the turbulence driving of the problem
  DriveIBC* m_DriveIBC;
  */
  // Patch integrator (factory and object)
  PatchOrion* m_patchOrionFactory;
  PatchOrion* m_patchOrion;

  // Refinement threshold for gradient
  vector<Real> *m_densityGradThreshold, 
    *m_velocityGradThreshold, 
    *m_diskTagThreshold,
    *m_BGradThreshold, 
    *m_radiationGradThreshold, 
    *m_shockDetectionThreshold, 
    *m_JeansNoThreshold,
    *m_Zoom,
    *m_Nested, 
    *m_NestedSphere,
    *m_DensityTagThreshold;

  // Tag buffer size
  int m_tagBufferSize;

  // Flag coarser and finer levels
  bool m_hasCoarser;
  bool m_hasFiner;

  DisjointBoxLayout m_grids;

  Real TotalEnergy;

  Real alpha; //ATM used for updating flux registers when radiation is on

private:
  // Disallowed for all the usual reasons
  void operator=(const AMRLevelOrion& a_input)
  {
    MayDay::Error("invalid operator");
  }

  // Disallowed for all the usual reasons
  AMRLevelOrion(const AMRLevelOrion& a_input)
  {
    MayDay::Error("invalid operator");
  }
};
#endif
