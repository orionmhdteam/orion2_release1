#ifdef CH_LANG_CC
/*
*      ____                         __ __
*     / __ \ ____ __ ___   __  __  / // /
*    / /_/ // __// // _ \ /  \/ / / // /
*    \____//_/  /_/ \___//_/\__/ /_//_/
*    
*    Based on Pluto 3 and Chombo.
*
*    Please refer to COPYING in Pluto's root directory and 
*    Copyright.txt, in Chombo's root directory.
*
*    Modification: Orional code based on PLUTO3.0b2
*    1) PS(09/25/08): Modified original code for CT scheme
*    2) PS(12/04/08): Upgrade to PLUTO3 final release.
*    3) PS(02/20/09): Use FluxBox class for face-centered magnetic field.
*    4) PS(03/17/09): Create a new m_facepatcher to use 
*                     PiecewiseLinearFillPatchFace class for face-centered
*                     magnetic field.
*    5) PS(04/09/09): Add edge-centered flux register a_finerFCFluxRegister
*                     and a_coarserFCFluxRegister for the EdgeDataBox EMF.
*                     Three EMF components are transferred back from UNSPLIT.
*    6) PS(11/05/09): Change temfx, temfy, and temfz not to include ghostzones.
*    7) PS(08/06/10): Rename Pluto to Orion.
*    8) PS(09/29/10): Upgrade to PLTUO 3.1.
*    9) PS(01/20/11): Modifie timing calls.
*   10) PS(05/12/11): Add gravity source term in step and UNSPLIT calls.
*/
#endif

#include <cstdio>

#include "LevelOrion.H"
#include "BoxIterator.H"
#include "AMRIO.H"
#include "SPMD.H"
#include "LoHiSide.H"
#include "EdgeDataBox.H"
#include "CH_Timer.H"

#include "NamespaceHeader.H"
#include "amrorionfort_F.H"

// Constructor - set up some defaults
LevelOrion::LevelOrion()
{
  m_defined = false;
  m_dx = 0.0;
  m_refineCoarse = 0;
  m_patchOrion = NULL;
}

// Destructor - free up storage
LevelOrion::~LevelOrion()
{
  if (m_patchOrion != NULL)
    {
      delete m_patchOrion;
    }
}

// Define the object so that time stepping can begin
void LevelOrion::define(const DisjointBoxLayout&  a_thisDisjointBoxLayout,
			const DisjointBoxLayout&  a_coarserDisjointBoxLayout,
			const ProblemDomain&      a_domain,
			const int&                a_refineCoarse,
			const int&                a_level,
			const Real&               a_dx,
			const PatchOrion* const a_patchOrionFactory,
			const bool&               a_hasCoarser,
			const bool&               a_hasFiner)
{
  CH_TIME("LevelOrion::define");

  // Sanity checks
  CH_assert(a_refineCoarse > 0);
  CH_assert(a_dx > 0.0);

  // Make a copy of the current grids
  m_grids  = a_thisDisjointBoxLayout;

  // Cache data
  m_dx = a_dx;
  m_level = a_level;
  m_domain = a_domain;
  m_refineCoarse = a_refineCoarse;
  m_hasCoarser = a_hasCoarser;
  m_hasFiner = a_hasFiner;

  // Remove old patch integrator (if any), create a new one, and initialize
  if (m_patchOrion != NULL)
    {
      delete m_patchOrion;
    }

 // Determing the number of ghost cells necessary here
  m_numGhost = GET_NGHOST(NULL);
#if defined(GRAVITY) || defined(SINKPARTICLE)
  m_numForceGhost = 2;
#endif /* GRAVITY */

  m_patchOrion = a_patchOrionFactory->new_patchOrion();
  m_patchOrion->define(m_domain,m_dx,m_level,m_numGhost);
  
  // Set the grid for the entire level
  setGridLevel();

  // Get the number of conserved variable and face centered fluxes
  m_numCons   = m_patchOrion->numConserved();
  m_numFluxes = m_patchOrion->numFluxes();

  m_exchangeCopier.exchangeDefine(a_thisDisjointBoxLayout,
                                  m_numGhost*IntVect::Unit);

  // Setup an interval corresponding to the conserved variables
  Interval UInterval(0,m_numCons-1);

  // Create temporary storage with a layer of "m_numGhost" ghost cells
  {
    CH_TIME("setup::Udefine");
    m_U.define(m_grids,m_numCons,m_numGhost*IntVect::Unit);
#ifdef STAGGERED_MHD
    m_Bs.define(m_grids,1,m_numGhost*IntVect::Unit);
#endif
#if defined(GRAVITY) || defined(SINKPARTICLE)
    m_G.define(m_grids,SpaceDim,m_numForceGhost*IntVect::Unit);
#endif
  }

  // Set up the interpolator if there is a coarser level
  if (m_hasCoarser)
    {
      m_patcher.define(a_thisDisjointBoxLayout,
                       a_coarserDisjointBoxLayout,
                       m_numCons,
                       coarsen(a_domain,a_refineCoarse),
                       a_refineCoarse,
                       m_numGhost);
  // PS: Flux-ct scheme
#ifdef STAGGERED_MHD
      m_facepatcher.define(a_thisDisjointBoxLayout,
			   a_coarserDisjointBoxLayout,
			   1,
			   coarsen(a_domain,a_refineCoarse),
			   a_refineCoarse,
			   m_numGhost);
#endif
    }

  // Everything is defined
  m_defined = true;
}

// Advance the solution by "a_dt" by using an unsplit method.
// "a_finerFluxRegister" is the flux register with the next finer level.
// "a_coarseFluxRegister" is flux register with the next coarser level.
// If source terms do not exist, "a_S" should be null constructed and not
// defined (i.e. its define() should not be called).
Real LevelOrion::step(LevelData<FArrayBox>&       a_U,
/* PS: Flux-ct scheme */
#ifdef STAGGERED_MHD
                      LevelData<FluxBox>&         a_Bs,
                      const LevelData<FluxBox>&   a_BsCoarseOld,
                      const LevelData<FluxBox>&   a_BsCoarseNew,
#endif
                      LevelData<FArrayBox>        a_flux[CH_SPACEDIM],
                      LevelFluxRegister&          a_finerFluxRegister,
                      LevelFluxRegister&          a_coarserFluxRegister,
#ifdef STAGGERED_MHD
                      LevelFluxRegisterEdge&      a_finerFCFluxRegister,
                      LevelFluxRegisterEdge&      a_coarserFCFluxRegister,
#endif
#if defined(GRAVITY) || defined(SINKPARTICLE)
		      const LevelData<FArrayBox>& a_G,
#endif
#if defined(SINKPARTICLE)
#if defined(STARPARTICLE) 
		      StarParticleList<StarParticleData>& m_sinks,
#else
		      SinkParticleList<SinkParticleData>& m_sinks,
#endif
#endif //SINKPARTICLE
                      LevelData<FArrayBox>&       a_split_tags,
                      LevelData<FArrayBox>&       a_cover_tags,
                      const LevelData<FArrayBox>& a_UCoarseOld,
                      const Real&                 a_TCoarseOld,
                      const LevelData<FArrayBox>& a_UCoarseNew,
                      const Real&                 a_TCoarseNew,
                      const Real&                 a_time,
                      const Real&                 a_dt)
{
  CH_TIMERS("LevelOrion::step");

  CH_TIMER("LevelOrion::step::setup"   ,timeSetup);
  CH_TIMER("LevelOrion::step::update"  ,timeUpdate);
  CH_TIMER("LevelOrion::step::reflux"  ,timeReflux);
  CH_TIMER("LevelOrion::step::conclude",timeConclude);

  // Make sure everything is defined
  CH_assert(m_defined);

  CH_START(timeSetup);

  // Clear flux registers with next finer level
  //
  //pout() << "step chk 1" << endl; //BALG
  if (m_hasFiner)
    {
      a_finerFluxRegister.setToZero();
/* PS: Flux-ct scheme */
#ifdef STAGGERED_MHD
      a_finerFCFluxRegister.setToZero();
#endif
    }

  // Setup an interval corresponding to the conserved variables
  Interval UInterval(0,m_numCons-1);
/* PS: Flux-ct scheme */
#ifdef STAGGERED_MHD
  Interval BsInterval(0,0);
#endif

  // Create temporary storage with a layer of "m_numGhost" ghost cells
  IntVect ivGhost = m_numGhost*IntVect::Unit;

  {
    CH_TIME("setup::localU");
    for (DataIterator gdit = m_U.dataIterator(); gdit.ok(); ++gdit)
      {
	m_U[gdit()].copy(a_U[gdit()]);
#ifdef STAGGERED_MHD
	m_Bs[gdit()].copy(a_Bs[gdit()]);
#endif
#if defined(GRAVITY) || defined(SINKPARTICLE)
	m_G[gdit()].copy(a_G[gdit()]);
#endif
      }
    //pout() << "step chk 2" << endl;
    m_U.exchange(m_exchangeCopier);
#ifdef STAGGERED_MHD
    m_Bs.exchange(m_exchangeCopier);
#endif
  }

 // Fill U's ghost cells using fillInterp
  //pout() << "step chk 3" << endl; //BALG
  if (m_hasCoarser)
    {
      // Fraction "a_time" falls between the old and the new coarse times
      Real alpha = (a_time - a_TCoarseOld) / (a_TCoarseNew - a_TCoarseOld);

    // Truncate the fraction to the range [0,1] to remove floating-point
    // subtraction roundoff effects
      Real eps = 0.04 * a_dt / (a_TCoarseNew - a_TCoarseOld);

      if (Abs(alpha) < eps)
        {
          alpha = 0.0;
        }

      if (Abs(1.0-alpha) < eps)
        {
          alpha = 1.0;
        }

      // Current time before old coarse time
      if (alpha < 0.0)
        {
          MayDay::Error( "LevelOrion::step: alpha < 0.0");
        }

      // Current time after new coarse time
      if (alpha > 1.0)
        {
          MayDay::Error( "LevelOrion::step: alpha > 1.0");
        }

      // Interpolate ghost cells from next coarser level using both space
      // and time interpolation
      //pout() << "step chk 4" << endl;
      m_patcher.fillInterp(m_U,
                           a_UCoarseOld,
                           a_UCoarseNew,
                           alpha,
                           0,0,m_numCons);

      /* PS: Flux-ct scheme. */
#ifdef STAGGERED_MHD

      m_facepatcher.fillInterp(m_Bs,
			       a_BsCoarseOld,
			       a_BsCoarseNew,
			       alpha,
			       0,0,1);
      
#endif
    }

  //pout() << "step chk 5" << endl;
  // Potentially used in boundary conditions
  m_patchOrion->setCurrentTime(a_time);

  // Dummy source used if source term passed in is empty
  FArrayBox zeroSource;

  // Use to restrict maximum wave speed away from zero
  Real maxWaveSpeed = 1.0e-12;
  Real minDtCool = 1.e38;

  // The grid structure
  Grid *grid;
  static Time_Step Dts;
  
  static double cfl=-1.0;
  if (cfl < 0.0){
    PAR_OPEN("orion2.ini");
    cfl = atof(PAR_GET("CFL",1));
  }

  CH_STOP(timeSetup);

  // Beginning of loop through patches/grids.
  for (DataIterator dit = m_grids.dataIterator(); dit.ok(); ++dit)
    {
      CH_START(timeUpdate);

      // The current box
      Box curBox = m_grids.get(dit());
      Box curEMFBox = m_grids.get(dit());

    // The current grid of conserved variables
      FArrayBox& curU = m_U[dit()];

/* PS: Flux-ct scheme */
    // The current grid of face-centered B-field variables
#ifdef STAGGERED_MHD
      FluxBox& curBs = m_Bs[dit()];
      FArrayBox& curBxs = curBs[0];
      FArrayBox& curBys = curBs[1];
      FArrayBox& curBzs = curBs[2];

      EdgeDataBox curEMF(curEMFBox,1);
      curEMF.setVal(0.0);
      FArrayBox& curEMFx = curEMF[0];
      FArrayBox& curEMFy = curEMF[1];
      FArrayBox& curEMFz = curEMF[2];
#endif

#if defined(GRAVITY) || defined(SINKPARTICLE)
      FArrayBox& curG = m_G[dit()];
#endif

    // The current grid of split/unsplit tags
      FArrayBox& split_tags = a_split_tags[dit()];
      FArrayBox& cover_tags = a_cover_tags[dit()];

      // The fluxes computed for this grid - used for refluxing and returning
      // other face centered quantities
      FArrayBox flux[SpaceDim];
    // Set the current box for the patch integrator
      m_patchOrion->setCurrentBox(curBox);

      Real minDtCoolGrid;
    
      grid = m_structs_grid[dit()].getGrid();
 
      IBEG = grid[IDIR].lbeg;
      JBEG = grid[JDIR].lbeg;
      KBEG = grid[KDIR].lbeg;
      IEND = grid[IDIR].lend;
      JEND = grid[JDIR].lend;
      KEND = grid[KDIR].lend;

      NX = grid[IDIR].np_int;
      NY = grid[JDIR].np_int;
      NZ = grid[KDIR].np_int;

      NX_TOT = grid[IDIR].np_tot;
      NY_TOT = grid[JDIR].np_tot;
      NZ_TOT = grid[KDIR].np_tot;
 
      delta_t = a_dt;
      glob_time = a_time;
      MAX_RIEM = 0;

  // reset time step coefficients 
      Dts.cmax[0] = Dts.cmax[1] = Dts.cmax[2] = 1.e-12;
      Dts.dt_cool = 1.e14;
      WHERE(-1, grid); /* -- store grid for subsequent calls -- */

    // UNSPLIT!!!!! Finally...
      m_patchOrion->UNSPLIT(curU,
/* PS: Flux-ct scheme */
#ifdef STAGGERED_MHD
                            curBxs,
                            curBys,
                            curBzs,
                            curEMFx,
                            curEMFy,
                            curEMFz,
#endif
#if defined(GRAVITY) || defined(SINKPARTICLE)
			    curG,
#endif
#if defined(SINKPARTICLE)
                            m_sinks,
#endif //SINKPARTICLE
                            cover_tags,
                            split_tags,
                            flux,
                            &Dts,
                            curBox,
                            grid);

/*
      SourceTimer.start();
      m_patchOrion->SPLIT_SOURCE(curU,
                                 split_tags,
                                 grid,
                                 minDtCoolGrid);
      SourceTimer.stop();
*/
  
      // Clamp away from zero
      for (int idir = 0; idir < SpaceDim; idir++)
        maxWaveSpeed = Max(maxWaveSpeed, Dts.cmax[idir]);
      /*PS: Accroding to Yi Feng, add this line to enforce CEIL_VA */
      if (maxWaveSpeed > CEIL_VA) maxWaveSpeed = CEIL_VA;

      minDtCool = Min(minDtCool, Dts.dt_cool/cfl);

      CH_STOP(timeUpdate);

      CH_START(timeReflux);

      // Do flux register updates
      for (int idir = 0; idir < SpaceDim; idir++)
        {
          // Increment coarse flux register between this level and the next
          // finer level - this level is the next coarser level with respect
          // to the next finer level
          if (m_hasFiner)
            {
              a_finerFluxRegister.incrementCoarse(flux[idir],a_dt,dit(),
                                                  UInterval,
                                                  UInterval,idir);
            }

          // Increment fine flux registers between this level and the next
          // coarser level - this level is the next finer level with respect
          // to the next coarser level
          if (m_hasCoarser)
            {
              a_coarserFluxRegister.incrementFine(flux[idir],a_dt,dit(),
                                                  UInterval,
                                                  UInterval,idir,Side::Lo);
              a_coarserFluxRegister.incrementFine(flux[idir],a_dt,dit(),
                                                  UInterval,
                                                  UInterval,idir,Side::Hi);
            }
        }
/* PS: Flux-ct scheme */
#ifdef STAGGERED_MHD
      if (m_hasFiner)
	{
	  // Do edge data flux register updates
	  for (int edgeDir = 0; edgeDir < SpaceDim; edgeDir++)
	    {
	      // don't do this for the normal direction
	      a_finerFCFluxRegister.incrementCoarse(curEMF[edgeDir],
						    a_dt, dit(),
						    BsInterval,
						    BsInterval);
	    }
	}
      if (m_hasCoarser)
	{
	  // Do edge data flux register updates
	  for (int edgeDir = 0; edgeDir < SpaceDim; edgeDir++)
	    {
	      a_coarserFCFluxRegister.incrementFine(curEMF[edgeDir],
						    a_dt, dit(),
						    BsInterval,
						    BsInterval);
	    }
	}
#endif /* STAGGERED_MHD */

      CH_STOP(timeReflux);
    }

  CH_START(timeConclude);

  {
    CH_TIME("conclude::copyU");
    // Now that we have completed the updates of all the patches, we copy the
    // contents of temporary storage, U, into the permanent storage, a_U.
    // U.copyTo(UInterval,a_U,UInterval);

    //pout() << "step chk 11" << endl; //BALG
    for(DataIterator dit = m_U.dataIterator(); dit.ok(); ++dit)
      {
	a_U[dit()].copy(m_U[dit()]);
/* PS: Flux-ct scheme */
/*     Copy temporary storage, Bs, into the permanent storage, a_Bs */
#ifdef STAGGERED_MHD
	a_Bs[dit()].copy(m_Bs[dit()]);
#endif
      }
  }

  // Find the minimum of dt's over this level
  Real local_dtNew = m_dx / maxWaveSpeed;
  local_dtNew = Min(local_dtNew,minDtCool);
  Real dtNew;
  {
    CH_TIME("conclude::getDt");
#ifdef CH_MPI

    int result = MPI_Allreduce(&local_dtNew, &dtNew, 1, MPI_CH_REAL,
                                 MPI_MIN, Chombo_MPI::comm);
    if(result != MPI_SUCCESS){ //bark!!!
	MayDay::Error("sorry, but I had a communcation error on new dt");
    }
#else
  dtNew = local_dtNew;
#endif
  }

  CH_STOP(timeConclude);

  // Return the maximum stable time step
  return dtNew;
}

void LevelOrion::setGridLevel()
{

 CH_TIME("LevelOrion::setGrid");

 m_structs_grid.define(m_grids);

 for (DataIterator dit = m_grids.dataIterator(); dit.ok(); ++dit)
   {
      // The current box
      Box curBox = m_grids.get(dit());
      struct GRID* grid = m_structs_grid[dit()].getGrid();
 
      m_patchOrion->setGrid(curBox, grid);           
   
   }

}

#include "NamespaceFooter.H"
