#ifdef CH_LANG_CC
/*
*      ____                         __ __
*     / __ \ ____ __ ___   __  __  / // /
*    / /_/ // __// // _ \ /  \/ / / // /
*    \____//_/  /_/ \___//_/\__/ /_//_/
*    
*    Based on Pluto 3 and Chombo.
*
*    Please refer to COPYING in Pluto's root directory and 
*    Copyright.txt, in Chombo's root directory.
*
*/

#include "FArrayBox.H"
#include "REAL.H"
#include "LevelData.H"
#include "Box.H"
#include "FORT_PROTO.H"
#include "ParallelHelper.H"
#include "amrorionfort_F.H"
#include "PatchGrid.H"

#if DOEINTAVE == YES
// This method works by replacing the total energy with the internal
// one before invoking FineInterp::interpToFine, then reconstructing
// the total energy from the interpolated thermal energies and momenta
void FineInterpOrion::
interpToFineEInt(LevelData<FArrayBox>& a_fine_data,
		 const LevelData<FArrayBox>& a_coarse_data,
		 Real m_dx,
		 bool a_averageFromDest) {

  // Create a copy of the coarse data
  LevelData<FArrayBox> coarse_data_copy;
  coarse_data_copy.define(a_coarse_data);

#if EOS != ISOTHERMAL

  // Loop over the coarse data to remove the kinetic energy
  DataIterator dit = a_coarse_data.dataIterator();
  for (dit.reset(); dit.ok(); ++dit) {

    // Create a workspace to compute the kinetic energy
    FArrayBox wksp(a_coarse_data[dit()].box(), 2);
    wksp.setVal(0.0);

    // Compute the kinetic energy
    wksp.plus(a_coarse_data[dit()], MX, 0);
    wksp.mult(a_coarse_data[dit()], MX, 0);
#if CH_SPACEDIM > 1
    wksp.setVal(0.0, 1);
    wksp.plus(a_coarse_data[dit()], MY, 1);
    wksp.mult(a_coarse_data[dit()], MY, 1);
    wksp.plus(wksp, 1, 0);
#endif
#if CH_SPACEDIM > 2
    wksp.setVal(0.0, 1);
    wksp.plus(a_coarse_data[dit()], MZ, 1);
    wksp.mult(a_coarse_data[dit()], MZ, 1);
    wksp.plus(wksp, 1, 0);
#endif
    //wksp.mult(0.5, 0);
    wksp.divide(a_coarse_data[dit()], DN, 0);

    
#ifdef STAGGERED_MHD
    wksp.setVal(0.0,1);
    wksp.plus(a_coarse_data[dit()], BX, 1);
    wksp.mult(a_coarse_data[dit()], BX, 1);
    wksp.plus(wksp,1,0);
    wksp.setVal(0.0,1);
    wksp.plus(a_coarse_data[dit()], BY, 1);
    wksp.mult(a_coarse_data[dit()], BY, 1);
    wksp.plus(wksp,1,0);
    wksp.setVal(0.0,1);
    wksp.plus(a_coarse_data[dit()], BZ, 1);
    wksp.mult(a_coarse_data[dit()], BZ, 1);
    wksp.plus(wksp,1,0);
#endif
    
    wksp.mult(0.5, 0);


    // Replace the energy in the coarse data copy with the internal
    // energy, computed by subtracting off the kinetic energy
    coarse_data_copy[dit()].minus(wksp, 0, EN);
  }

#endif  // if EOS != ISOTHERMAL

  // Now invoke the usual interpolator, using the altered coarse data
#if PCINTERP != YES
  interpToFine(a_fine_data, coarse_data_copy, a_averageFromDest);
#else
  pwcinterpToFine(a_fine_data, coarse_data_copy, a_averageFromDest);
#endif


#if EOS != ISOTHERMAL
  // Last step: take the fine data, and turn the energy back into a
  // total energy by adding the kinetic component back in
  dit = a_fine_data.dataIterator();
  for (dit.reset(); dit.ok(); ++dit) {

    // Again, we need a temporary workspace to compute the kinetic
    // energy
    FArrayBox wksp(a_fine_data[dit()].box(), 2);
    wksp.setVal(0.0);

    // Compute the kinetic energy
    wksp.plus(a_fine_data[dit()], MX, 0);
    wksp.mult(a_fine_data[dit()], MX, 0);
#if CH_SPACEDIM > 1
    wksp.setVal(0.0, 1);
    wksp.plus(a_fine_data[dit()], MY, 1);
    wksp.mult(a_fine_data[dit()], MY, 1);
    wksp.plus(wksp, 1, 0);
#endif
#if CH_SPACEDIM > 2
    wksp.setVal(0.0, 1);
    wksp.plus(a_fine_data[dit()], MZ, 1);
    wksp.mult(a_fine_data[dit()], MZ, 1);
    wksp.plus(wksp, 1, 0);
#endif
   
    wksp.divide(a_fine_data[dit()], DN, 0);
#ifdef STAGGERED_MHD
    wksp.setVal(0.0,1);
    wksp.plus(a_fine_data[dit()], BX, 1);
    wksp.mult(a_fine_data[dit()], BX, 1);
    wksp.plus(wksp,1,0);
    wksp.setVal(0.0,1);
    wksp.plus(a_fine_data[dit()], BY, 1);
    wksp.mult(a_fine_data[dit()], BY, 1);
    wksp.plus(wksp,1,0);
    wksp.setVal(0.0,1);
    wksp.plus(a_fine_data[dit()], BZ, 1);
    wksp.mult(a_fine_data[dit()], BZ, 1);
    wksp.plus(wksp,1,0);
#endif
    wksp.mult(0.5, 0);

    // Now add the kinetic energy to the total energy in the fine data
    a_fine_data[dit()].plus(wksp, 0, EN);
  }
#endif


}
#endif

#endif
