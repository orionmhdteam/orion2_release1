/*
*      ____                         __ __
*     / __ \ ____ __ ___   __  __  / // /
*    / /_/ // __// // _ \ /  \/ / / // /
*    \____//_/  /_/ \___//_/\__/ /_//_/
*    
*    Based on Pluto 3 and Chombo.
*
*    Please refer to COPYING in Orion2's root directory and 
*    Copyright.txt, in Chombo's root directory.
*
*    Modification: Orional code based on PLUTO3.0
*    1) PS(03/17/09): Add PiecewiseLinearFillFacePluto class for face-centered
*                     magnetic field.
*    2) PS(08/06/10): Rename Pluto to Orion.
*/
#ifndef PATCH_GRID_H
#define PATCH_GRID_H

extern "C" {
#include "orion2.h"
}

#include "MayDay.H"
#include "PiecewiseLinearFillPatch.H"
#include "PiecewiseLinearFillPatchFace.H"
#include "FineInterp.H"
//PS: AMR for face-centered box
#include "FineInterpFace.H"

class PatchGrid
{
public:

 PatchGrid(){;}

 ~PatchGrid(){

  if (PGrid[0].x != NULL) { 
   for (int dir = 0; dir < 3; ++dir)
     {
      free_array_1D(PGrid[dir].x);
      free_array_1D(PGrid[dir].xl);
      free_array_1D(PGrid[dir].xr);
      free_array_1D(PGrid[dir].dx);
      free_array_1D(PGrid[dir].xvc);
      free_array_1D(PGrid[dir].dV);
      free_array_1D(PGrid[dir].A);
      free_array_1D(PGrid[dir].r_1);
      free_array_1D(PGrid[dir].ct);
     }
   } 

 }

 struct GRID* getGrid()
 {
   return PGrid;
 }

protected:

 struct GRID PGrid[3];

private:


};

class PiecewiseLinearFillOrion : public PiecewiseLinearFillPatch
{
public:

  PiecewiseLinearFillOrion(){m_is_defined = false;}

 ~PiecewiseLinearFillOrion(){;}

  int get_geometry ()
  {
   #if GEOMETRY == CARTESIAN
     return 1;
   #endif

   #if GEOMETRY == CYLINDRICAL
    #if DIMENSIONS == 2
     return 2;
    #else
     MayDay::Error("Cylindrical geometry supported in Orion/Chombo only in 2 dimensions");
    #endif
   #endif

   #if GEOMETRY > CYLINDRICAL
     MayDay::Error("Only cartesian and 2D cylindrical geometries are supported in Orion/Chombo");
   #endif
  }

};

class FineInterpOrion : public FineInterp
{
public:

  FineInterpOrion(){is_defined = false;}

 ~FineInterpOrion(){;}

#if DOEINTAVE == YES
  // If using internal energy averaging, we define a new interpolation
  // method here that interpolates the internal energy rather than the
  // total energy.
  void
  interpToFineEInt(LevelData<FArrayBox>& a_fine_data,
		   const LevelData<FArrayBox>& a_coarse_data,
		   Real m_dx,
		   bool a_averageFromDest=false);
#endif

  int get_geometry ()
  {
   #if GEOMETRY == CARTESIAN
     return 1;
   #endif

   #if GEOMETRY == CYLINDRICAL
    #if DIMENSIONS == 2
     return 2;
    #else
     MayDay::Error("Cylindrical geometry supported in Orion/Chombo only in 2 dimensions");
    #endif
   #endif

   #if GEOMETRY > CYLINDRICAL
     MayDay::Error("Only cartesian and 2D cylindrical geometries are supported in Orion/Chombo");
   #endif
  }

};

class PiecewiseLinearFillFaceOrion : public PiecewiseLinearFillPatchFace
{
public:

  PiecewiseLinearFillFaceOrion(){m_is_defined = false;}

 ~PiecewiseLinearFillFaceOrion(){;}

  int get_geometry ()
  {
   #if GEOMETRY == CARTESIAN
     return 1;
   #endif

   #if GEOMETRY == CYLINDRICAL
    #if DIMENSIONS == 2
     return 2;
    #else
     MayDay::Error("Cylindrical geometry supported in Orion/Chombo only in 2 dimensions");
    #endif
   #endif

   #if GEOMETRY > CYLINDRICAL
     MayDay::Error("Only cartesian and 2D cylindrical geometries are supported in Orion/Chombo");
   #endif
  }

};

class FineInterpFaceOrion : public FineInterpFace
{
public:

  FineInterpFaceOrion(){is_defined = false;}

 ~FineInterpFaceOrion(){;}

  int get_geometry ()
  {
   #if GEOMETRY == CARTESIAN
     return 1;
   #endif

   #if GEOMETRY == CYLINDRICAL
    #if DIMENSIONS == 2
     return 2;
    #else
     MayDay::Error("Cylindrical geometry supported in Orion/Chombo only in 2 dimensions");
    #endif
   #endif

   #if GEOMETRY > CYLINDRICAL
     MayDay::Error("Only cartesian and 2D cylindrical geometries are supported in Orion/Chombo");
   #endif
  }

};

#endif
