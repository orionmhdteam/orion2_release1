#include "orion2.h"

/* ****************************************************************** */
void STATES (const State_1D *state, int beg, int end, real dt, Grid *grid)
/*
 *
 * PURPOSE:
 *
 *  Make LR states to be used with a RK-type integrator
 *
 *
 ********************************************************************** */
{

  RECONSTRUCT (state, grid); 
  PRIMTOCON (state->vp, state->up, beg, end);
  PRIMTOCON (state->vm, state->um, beg, end);

}
