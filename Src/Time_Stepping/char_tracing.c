#include "orion2.h"

#define SMALL_VEL  1.e-10
static real VOL1 (real x1);
static real VOL2 (real x2);
static real VOL3 (real x3);

#if INTERPOLATION == mpPPM
/* *************************************************************************** */
void STATES (const State_1D *state, int ibeg, int iend, real dt, Grid *grid)
/*
 *
 * PURPOSE:
 *
 *   One-step characteristic tracing integrator for PPM;
 *   it must be combined with Parabolic reconstruction.
 *
 *
 * REFERENCE:    "The Piecewise Parabolic Method for \\
 *                Multidimensional Relativistic Fluid Dynamics"
 *               Mignone et al. (2006), ApJS, 160, 199
 *
 *
restart;
P0 := 3*w/2 - (wp + wm)/4;
P1 := wp - wm;
P2 := 3*(wp - 2*w + wm);

wL := P0 + P1*( 1/2 - x/2) + P2*(1/4 - x/2 + x*x/3);
wR := P0 + P1*(-1/2 - x/2) + P2*(1/4 + x/2 + x*x/3);

aL := wp - x/2*(wp - wm - (1 - 2/3*x)*6*(w - (wp + wm)/2));
aR := wm + x/2*(wp - wm + (1 - 2/3*x)*6*(w - (wp + wm)/2));

simplify(wL-aL);
simplify(wR-aR);

 *************************************************************************** */
{
  int    i, j, k, nv, S=2;
  int    beg, end;
  double *vp, *vm;
  double P0, P1, P2;
  double betap, betam, dtdx;
  double wp[NVAR], wm[NVAR], wc[NVAR];
  double wp0[NVAR], wm0[NVAR];
  double dw[NVAR], w6[NVAR], nu[NVAR], nuM[NVAR], num[NVAR];
  double lambda[NVAR];
  static double **L, **R;
  static double *wsp, *wsm, *a2, *h;

double w[NVAR];
static double **wsp2, **wsm2;


  if (wsp == NULL){
    wsp = Array_1D(NMAX_POINT, double);
    wsm = Array_1D(NMAX_POINT, double);
 wsp2 = Array_2D(NVAR, NMAX_POINT, double);
 wsm2 = Array_2D(NVAR, NMAX_POINT, double);
    a2  = Array_1D(NMAX_POINT, double);
    h   = Array_1D(NMAX_POINT, double);
    L   = Array_2D(NFLX, NFLX, double);
    R   = Array_2D(NFLX, NFLX, double);
  } 

 /* ---- Reset eigenvectors ---- */
                                                                                                                                        
  for (nv = NFLX; nv--; ){
  for (k  = NFLX; k--;  ){
    L[nv][k] = R[nv][k] = 0.0;
  }}
 
/* ----------------------------------------------------
   ---------------------------------------------------- */

  dtdx = delta_t/grid[DIR].dx[ibeg];
  beg  = grid[DIR].lbeg - grid[DIR].nghost + S;
  end  = grid[DIR].lend + grid[DIR].nghost - S;

  SOUND_SPEED2 (state->v, a2, h, beg, end);

  for (i = beg; i <= end; i++){    
    vp = state->vp[i];
    vm = state->vm[i];

    EIGENV (state->v[i], a2[i], h[i], lambda, L, R);

    for (j = i - S; j <= i + S; j++){
      LpPROJECT(L, state->v[j], w);
      for (k = NVAR; k--;   ){  
        wsp2[k][j]     = w[k];
        wsm2[k][2*i-j] = w[k];
      }
    }
    for (k = 0; k < NVAR; k++){  
/*
      wp[k] = MP5_RECONSTRUCT (wsp2[k], i);
      wm[k] = MP5_RECONSTRUCT (wsm2[k], i);
*/
PPM_RECONSTRUCT (wsp2[k], wm + k, wp + k, i);

      wc[k] = wsp2[k][i];

      nu[k] = dtdx*lambda[k];
      P0 = 1.5*wc[k] - 0.25*(wp[k] + wm[k]);
      P1 = wp[k] - wm[k];
      P2 = 3.0*(wp[k] - 2.0*wc[k] + wm[k]);
    
      betap = (nu[k] >= 0.0 ? 1.0:0.0);
      betam = 1.0 - betap;
      wp[k] = wc[k] + betap*(P0 + 0.5*P1*(1.0 - nu[k]) +
                             P2*(0.25 - 0.5*nu[k] + nu[k]*nu[k]/3.0) - wc[k]);
      wm[k] = wc[k] + betam*(P0 - 0.5*P1*(1.0 + nu[k]) +
                             P2*(0.25 + 0.5*nu[k] + nu[k]*nu[k]/3.0) - wc[k]);

/*
wp[k] = wc[k] + 0.5*P1*(1.0 - nu[k]);
wm[k] = wc[k] - 0.5*P1*(1.0 + nu[k]);

wp0[k] = wc[k] + 0.5*P1;
wm0[k] = wc[k] - 0.5*P1;
*/

wp0[k] = wc[k] + betap*(P0 + 0.5*(P1 + 0.5*P2) - wc[k]);
wm0[k] = wc[k] + betam*(P0 - 0.5*(P1 - 0.5*P2) - wc[k]);


    }

    for (nv = NFLX; nv--;   ){
      vp[nv] = vm[nv] = 0.0;
state->pnt_flx[i][nv] = 0.0;
      for (k = NFLX; k--;   ){
        vp[nv] += wp[k]*R[nv][k];
        vm[nv] += wm[k]*R[nv][k];
state->pnt_flx[i][nv] += (wp0[k] - wm0[k])*R[nv][k];
      }


state->pnt_flx[i][nv] = MinMod(state->v[i+1][nv]-state->v[i][nv],
                               state->v[i][nv]-state->v[i-1][nv]);
    }

/*
{
  double dbx, dpsi, *v, src[NVAR];
  v = state->v[i];
  dbx = vp[B1] - vm[B1];
  dbx = MinMod(state->v[i+1][B1] - state->v[i][B1],
               state->v[i][B1] - state->v[i-1][B1]);
  dpsi = MinMod(state->v[i+1][PSI_GLM] - state->v[i][PSI_GLM],
                state->v[i][PSI_GLM] - state->v[i-1][PSI_GLM]);

  src[DN] = 0.0;
  EXPAND(src[V1] = dbx*v[B1]/v[DN];  ,
         src[V2] = dbx*v[B2]/v[DN];  ,  
         src[V3] = dbx*v[B3]/v[DN];)
  EXPAND(src[B1] = 0*dpsi;       ,
         src[B2] = dbx*v[V2];  ,  
         src[B3] = dbx*v[V3];)
  src[PR]  = EXPAND(v[V1]*v[B1], + v[V2]*v[B2], + v[V3]*v[B3]);
  src[PR] *= -dbx*(gmm - 1.0);
  src[PR] += (gmm - 1.0)*v[B1]*dpsi;


  for (nv = NVAR; nv--;  ){
    vp[nv] += 0.5*dtdx*src[nv];
    vm[nv] += 0.5*dtdx*src[nv];
  }
}
*/
  /* -- check unphysical values -- */
/*
    if (vp[DN] < 0.0 || vm[DN] < 0.0) {
      PRIM_LIMIT (state, i, DN);
    }
    if (vp[PR] < 0.0 || vm[PR] < 0.0) {
      PRIM_LIMIT (state, i, PR);
    }
*/
  }

  CHECK_PRIM_STATES (state->vm, state->vp, state->v, beg, end);

/*  -------------------------------------------
      Assign face-centered magnetic field
    -------------------------------------------  */

  #ifdef STAGGERED_MHD
   for (i = beg-1; i <= end; i++) {
     state->vR[i][B1] = state->vL[i][B1] = state->bn[i];
   }
  #endif

/* -----------------------------------
      evolve center value by dt/2 
   ----------------------------------- */

  for (i = beg; i <= end; i++) {
    vp = state->vp[i];
    vm = state->vm[i];
    for (nv = NVAR; nv--; ) {
      state->vh[i][nv] = 0.5*(vp[nv] + vm[nv]);
    }
  }

  PRIMTOCON (state->vh, state->uh, beg, end);
  PRIMTOCON (state->vp, state->up, beg, end);
  PRIMTOCON (state->vm, state->um, beg, end);

}

#elif INTERPOLATION == PARABOLIC 
/* *************************************************************************** */
void STATES (const State_1D *state, int beg, int end, real dt, Grid *grid)
/*
 *
 * PURPOSE:
 *
 *   One-step characteristic tracing integrator for PPM;
 *   it must be combined with Parabolic reconstruction.
 *
 *
 * REFERENCE:    "The Piecewise Parabolic Method for \\
 *                Multidimensional Relativistic Fluid Dynamics"
 *               Mignone et al. (2006), ApJS, 160, 199
 *
 *
 *************************************************************************** */
{
  int  i, j, k, nv;
  real scrh_p, scrh_m;
  real dv[NVAR], v6[NVAR];
  real vp_av[NFLX][NFLX], vm_av[NFLX][NFLX];
  real eta;
  real lambda[NFLX];
  real dt_dh, dt_2, dx_p, dx_m, xp, xm;
  real g[3];
  real (*VOL)(real);
  real *vp_ref, *vm_ref, *vp, *vm, *vc;
  static real **ll, **rr, **src;
  static real *a2, *h;

  real ua, bnp, bnm;

  if (DIR == IDIR)      VOL = VOL1;
  else if (DIR == JDIR) VOL = VOL2;
  else if (DIR == KDIR) VOL = VOL3;

  if (ll == NULL){
    ll  = Array_2D(NFLX, NFLX, double);
    rr  = Array_2D(NFLX, NFLX, double);
    src = Array_2D(NMAX_POINT, NVAR, double);
    a2  = Array_1D(NMAX_POINT, double);
    h   = Array_1D(NMAX_POINT, double);
  }

  RECONSTRUCT  (state, grid); 
  SOUND_SPEED2 (state->v, a2, h, beg, end);
  PRIM_SOURCE  (state, beg, end, a2, h, src, grid);

/* -----------------------------------------------------
           Reset right and left eigenvectors
   ----------------------------------------------------- */

  for (k = NFLX; k--;   ){
  for (j = NFLX; j--;   ){
    rr[k][j] = ll[k][j] = 0.0;
  }}

  dt_2 = 0.5*dt;

    /*   ---   Set reference states  --- */

  vp_ref = vp_av[1];    /*  + characteristic  */
  vm_ref = vm_av[0];    /*  - characteristic  */

  for (i = beg; i <= end; i++) {

    vc = state->v[i];
    vp = state->vp[i];
    vm = state->vm[i];

    xm    = grid[DIR].xl[i];
    xp    = grid[DIR].xr[i];
    dt_dh = dt/1.0;
    for (nv = NVAR; nv--;  ) {
      dv[nv] = vp[nv] - vm[nv];
      v6[nv] = 2.0*vc[nv] - (vp[nv] + vm[nv]);
    }

    EIGENV (vc, a2[i], h[i], lambda, ll, rr);

    for (k = NFLX; k--;  ) {        /* Loop on characteristics  */

      if (lambda[k] >= 0.0){
        dx_p = lambda[k]*dt_dh;
        dx_m = 0.0;
      }else{
        dx_p = 0.0;
        dx_m = -lambda[k]*dt_dh;
      }

      #if GEOMETRY == CARTESIAN
       scrh_p = dx_p/grid[DIR].dx[i];
       scrh_m = dx_m/grid[DIR].dx[i]; 
      #else
       scrh_p =  (VOL(xp) - VOL(xp - dx_p))/grid[DIR].dV[i];
       scrh_m = -(VOL(xm) - VOL(xm + dx_m))/grid[DIR].dV[i];
      #endif

  /*  --------------------------------------------
        Compute characteristic averages
      -------------------------------------------- */	

      for (nv = NFLX; nv--;  ) {
        vp_av[k][nv] = vp[nv] - 0.5*scrh_p*
                       (dv[nv] - v6[nv]*(3.0 - 2.0*scrh_p));
        vm_av[k][nv] = vm[nv] + 0.5*scrh_m*
                       (dv[nv] + v6[nv]*(3.0 - 2.0*scrh_m));
      }
    }

  /* ----------------------------------------------
      Initialize vp and vm to the reference state
     ---------------------------------------------- */
     
    #ifdef STAGGERED_MHD
     bnp = vp[B1];
     bnm = vm[B1];
    #endif

    for (nv = NFLX; nv--;  ) {
      vp[nv] = vp_av[1][nv];
      vm[nv] = vm_av[0][nv];
    }

    #ifdef STAGGERED_MHD
     vp[B1] = bnp;
     vm[B1] = bnm;
    #endif
          
 /* ----------------------------------------
     use upwind limiting to select only 
     those characteristics which contribute 
     to the effective left and right states
    ---------------------------------------- */

    for (k = NFLX; k--;   ) {
    
      eta = 0.0;
      if (lambda[k] > SMALL_VEL) {
        for (nv = NFLX; nv--;  ) {
          eta += ll[k][nv]*(vp_ref[nv] - vp_av[k][nv]);
        }	
        for (nv = NFLX; nv--;  ) {
          vp[nv] -= eta*rr[nv][k];
        }
      }
      
      if (lambda[k] < -SMALL_VEL) {
        for (nv = NFLX; nv--;  ) {
          eta += ll[k][nv]*(vm_ref[nv] - vm_av[k][nv]);
        }
        for (nv = NFLX; nv--;  ) {
          vm[nv] -= eta*rr[nv][k];
        }
	
      }
    }

    for (nv = NFLX; nv--;  ) {
      vp[nv] += dt_2*src[i][nv];
      vm[nv] += dt_2*src[i][nv];
    }

/* ----------------------------------------------------------------- 
      Advection equations are treated separately, since
      states computation is greatly simplified by the fact that 
      the eigenvectors are l = r = (0,..., 1, 0, ...) and the 
      eigenvalue is the same (u) for all of them.
   ----------------------------------------------------------------- */

    #if NVAR != NFLX
     ua = vc[V1];       
     dx_p = dx_m = 0.0;
     if (ua >= 0.0) dx_p =  ua*dt_dh;
     else           dx_m = -ua*dt_dh;

     #if GEOMETRY == CARTESIAN
      scrh_p = dx_p/grid[DIR].dx[i];
      scrh_m = dx_m/grid[DIR].dx[i]; 
     #else
      scrh_p =  (VOL(xp) - VOL(xp - dx_p))/grid[DIR].dV[i];
      scrh_m = -(VOL(xm) - VOL(xm + dx_m))/grid[DIR].dV[i];
    #endif

     if (ua >= 0.0){   /* -- upwind selection -- */
       for (nv = NFLX; nv < NVAR; nv++) {
         vp[nv] -= 0.5*scrh_p*(dv[nv] - v6[nv]*(3.0 - 2.0*scrh_p));
       }
     }else{
       for (nv = NFLX; nv < NVAR; nv++) {
         vm[nv] += 0.5*scrh_m*(dv[nv] + v6[nv]*(3.0 - 2.0*scrh_m));
       }
     }
    #endif     
  }

  CHECK_PRIM_STATES (state->vm, state->vp, state->v, beg, end);
  
/* -----------------------------------
     evolve center value by dt/2 
   ----------------------------------- */

  for (i = beg; i <= end; i++) {
    vc = state->vh[i];
    vp = state->vp[i];
    vm = state->vm[i];
    for (nv = NVAR; nv--; ) {
      vc[nv]  = 0.5*(vp[nv] + vm[nv]);
    }
  }

  PRIMTOCON (state->vh, state->uh, beg, end);
  PRIMTOCON (state->vp, state->up, beg, end);
  PRIMTOCON (state->vm, state->um, beg, end);

}

#else

/* *************************************************************************** */
void STATES (const State_1D *state, int beg, int end, real dt, Grid *grid)

/*
 * PURPOSE:
 *
 *   One-step characteristic tracing integrator for PLM;
 *   it must be combined with Linear reconstruction.
 *
 *
 * REFERENCE:    "Total Variation Diminishing Scheme for Adiabatic 
 *                and Isothermal Magnetohydrodynamics"
 *                Balsara, D.S., ApJ 1998, 116, 133
 * 
 *
 *************************************************************************** */
{
  int    i, j, k, nv;
  real scrh_p, scrh_m;
  real dV[NVAR];
  real lambda[NFLX];
  real eta, bnp, bnm;
  real dx, dt_2, dtdx_2;
  real (*VOL)(real);
  real *vp, *vm, *vc;
  static real **ll, **rr, **src;
  static real *a2, *h, *dl;

  if (ll == NULL){
    ll   = Array_2D(NFLX,NFLX, double);
    rr   = Array_2D(NFLX,NFLX, double);
    src  = Array_2D(NMAX_POINT, NVAR, double);
    a2   = Array_1D(NMAX_POINT, double);
    h    = Array_1D(NMAX_POINT, double);
    dl   = Array_1D(NMAX_POINT, double);
  }

  #if GEOMETRY == SPHERICAL && DIMENSIONS > 1
   print1 ("! Characteristic tracing not implemented in this geometry\n");
   QUIT_ORION2(1);
  #endif

  #if GEOMETRY == POLAR
   if (DIR == IDIR || DIR == KDIR){
     for (i = beg; i <= end; i++) {
       dl[i] = grid[IDIR].dx[i];
     }
   } else if (DIR == JDIR) {
     for (i = beg; i <= end; i++) {
       dl[i] = grid[JDIR].dx[i]*grid[IDIR].x[*NX_PT];
     }
   }
  #elif GEOMETRY == SPHERICAL
   if (DIR == IDIR){
     for (i = beg; i <= end; i++) {
       dl[i] = grid[IDIR].dx[i];
     }
   }
  #endif
 
  RECONSTRUCT  (state, grid); 
  SOUND_SPEED2 (state->v, a2, h, beg, end);
  PRIM_SOURCE  (state, beg, end, a2, h, src, grid);

/* -----------------------------------------------------
           Reset right and left eigenvectors
   ----------------------------------------------------- */

  for (k = NFLX; k--;  ){
  for (j = NFLX; j--;  ){
    rr[k][j] = ll[k][j] = 0.0;
  }}

  dt_2 = 0.5*dt;

  for (i = beg; i <= end; i++) {

    vc = state->v[i];
    vp = state->vp[i];
    vm = state->vm[i];

    #if GEOMETRY == CARTESIAN || GEOMETRY == CYLINDRICAL
     dx     = grid[DIR].dx[i];
    #else
     dx     = dl[i];
    #endif

    dtdx_2 = dt_2/dx;

    for (nv = NVAR; nv--;  ) dV[nv] = vp[nv] - vm[nv];

    EIGENV (vc, a2[i], h[i], lambda, ll, rr);

    scrh_p = dtdx_2*dmax(lambda[1], 0.0);
    scrh_m = dtdx_2*dmin(lambda[0], 0.0);

 /* -----------------------------
      Initialize vp and vm to 
      the reference state
    ----------------------------- */

    #ifdef STAGGERED_MHD
     bnp = vp[B1];
     bnm = vm[B1];
    #endif

    for (nv = NFLX; nv--;  ) {
      vp[nv] -= scrh_p*dV[nv];
      vm[nv] -= scrh_m*dV[nv];      
    }

    #ifdef STAGGERED_MHD
     vp[B1] = bnp;
     vm[B1] = bnm;
    #endif
  
 /* ----------------------------------------
     use upwind limiting to select only 
     those characteristics which contribute 
     to the effective left and right states
    ---------------------------------------- */

    for (k = NFLX; k--;   ) {
    
      eta = 0.0;
      if (lambda[k] > SMALL_VEL){
        for (nv = NFLX; nv--;  ) {
          eta += ll[k][nv]*dV[nv];
        }
        eta *= dtdx_2*(lambda[1] - lambda[k]);
        for (nv = NFLX; nv--;  ) {
          vp[nv] += eta*rr[nv][k];
        }
      }
      
      if (lambda[k] < -SMALL_VEL){
        for (nv = NFLX; nv--;  ) {
          eta += ll[k][nv]*dV[nv];
        }
        eta *= dtdx_2*(lambda[0] - lambda[k]); 
        for (nv = NFLX; nv--;  ) {
          vm[nv] += eta*rr[nv][k];
        }
      }
    }

  /* ----------------------------
       Add source terms 
     ---------------------------- */  
     
    for (nv = NFLX; nv--;  ) {
      vp[nv] += dt_2*src[i][nv];
      vm[nv] += dt_2*src[i][nv];
    }

/* ----------------------------------------------------------------- 
      Advection equations are treated separately, since
      states computation is greatly simplified by the fact that 
      the eigenvectors are l = r = (0,..., 1, 0, ...) and the 
      eigenvalue is the same (u) for all of them.
   ----------------------------------------------------------------- */

    #if NVAR != NFLX

     eta = dtdx_2*vc[V1];
     if (vc[V1] >= 0.0){   /* -- upwind selection -- */
       for (nv = NFLX; nv < NVAR; nv++) {
         vp[nv] -= eta*dV[nv];
       }
     }else{
       for (nv = NFLX; nv < NVAR; nv++) {
         vm[nv] -= eta*dV[nv];
       }
     }

    #endif     

  }

/* ---- Check axial symmetry ---- */
/*
if (DIR == IDIR){
 double dv, s=0.0;
 i = IBEG - 1;
 for (nv = 0; nv < NVAR; nv++){
   if (nv == DN || nv == MY || nv == BY || nv == PR) s = -1.0;
   if (nv == VX || nv == BZ || nv == VZ) s = 1.0;
   if (s != 0.0) {
     dv = state->vL[i][nv] + s*state->vR[i][nv];
     if (fabs(dv) > 1.e-8){
       printf ("States not symmetric\n");
       SHOW(state->v,IBEG-1);
       SHOW(state->v,IBEG);
       exit(1);
     }
   }
 }
}
*/
/* ------------------------------ */


  CHECK_PRIM_STATES (state->vm, state->vp, state->v, beg, end);
  
/* -----------------------------------
     evolve center value by dt/2 
   ----------------------------------- */

  for (i = beg; i <= end; i++) {
    vc = state->vh[i];
    vp = state->vp[i];
    vm = state->vm[i];
    for (nv = NVAR; nv--; ) {
      vc[nv]  = 0.5*(vp[nv] + vm[nv]);
    }
  }

  PRIMTOCON (state->vh, state->uh, beg, end);
  PRIMTOCON (state->vp, state->up, beg, end);
  PRIMTOCON (state->vm, state->um, beg, end);

}
#endif
/* ########################################################## */
real VOL1(real x1)
/*
 #
 #  Define volume coordinate for the 1-st sweep
 #
 ############################################################ */
{

#if GEOMETRY == CARTESIAN

  return(x1);

#elif GEOMETRY == CYLINDRICAL || GEOMETRY == POLAR

  return(0.5*x1*fabs(x1));

#elif GEOMETRY == SPHERICAL

  return(x1*x1*x1/3.0);

#endif

}
/* ########################################################## */
real VOL2(real x2)
/*
 #
 #  Define volume coordinate for the 2-nd sweep
 #
 ############################################################ */
{

#if GEOMETRY == CARTESIAN

  return(x2);

#elif GEOMETRY == CYLINDRICAL  || GEOMETRY == POLAR

  return(x2);

#elif GEOMETRY == SPHERICAL

  return(dsign(x2)*(1.0 - cos(x2)));

#endif



}
/* ########################################################## */
real VOL3(real x3)
/*
 #
 #  Define volume coordinate for the 3-rd sweep
 #
 ############################################################ */
{

  return (x3);

}
#undef SMALL_VEL
