#include "orion2.h"

/* ************************************************************ */
void MAKE_GEOMETRY (Grid *GXYZ)
/* 
 *
 *
 *
 *
 *
 ************************************************************** */
{
  int     i, j, k, idim, ngh, ileft;
  int     iright;
  int     iend, jend, kend;
  real  xiL, xiR, dxi, dvol;
  real  x, dx, xr, xl;
  real  y, dy, yr, yl;
  struct  GRID *GG;

/* --------------------------------------------------------------------
      at the upper and lower boundaries grid should be symmetrized
      with respect to the VOLUME coordinate only when reflective
      conditions apply.
   -------------------------------------------------------------------- */
/*
  for (idim = 0; idim < 3; idim++) {

    GG     = GXYZ + idim;
    ngh    = GG->nghost;
    ileft  = GG->gbeg;
    iright = GG->gend;

    if (GG->lbound == REFLECTIVE) {           

      for (i = ngh - 1; i >= 0; i--) {
     
        dxi           =   VOL_COORD(G->xr_glob[2*ngh - i - 1], idim) 
                        - VOL_COORD(G->xl_glob[2*ngh - i - 1], idim);
        xiR           = VOL_COORD(G->xl_glob[i + 1], idim);
        G->xl_glob[i] = INV_VOL_COORD(xiR - dxi, idim);
        G->xr_glob[i] = G->xl_glob[i + 1];
        G->dx_glob[i] = fabs(G->xr_glob[i] - G->xl_glob[i]);
      }
    }

    if (GG->rbound == REFLECTIVE) {           

      for (i = 0; i < ngh; i++) {
        dxi                         =   VOL_COORD(G->xr_glob[iright - i], idim) 
                                      - VOL_COORD(G->xl_glob[iright - i], idim);
        xiL                         = VOL_COORD(G->xr_glob[iright + i], idim);
        G->xr_glob[i + iright + 1]  = INV_VOL_COORD(xiL + dxi, idim);
        G->dx_glob[i + iright + 1]  = fabs(G->xl_glob[i + 1] - G->xl_glob[i]);
        G->xl_glob[i + iright]      = G->xr_glob[i + iright - 1];
      }
    }
  }
*/    

/* -----------------------------------------------------------------------
             DEFINE VOLUME ELEMENTS AND AREAS
  ------------------------------------------------------------------------ */

  iend = GXYZ[0].lend + GXYZ[0].nghost;
  jend = GXYZ[1].lend + GXYZ[1].nghost;
  kend = GXYZ[2].lend + GXYZ[2].nghost;

  for (idim = 0; idim < 3; idim++) {
    (GXYZ + idim)->A   = Array_1D (GXYZ[idim].np_tot, double);
    (GXYZ + idim)->xvc = Array_1D (GXYZ[idim].np_tot, double);
    (GXYZ + idim)->dV  = Array_1D (GXYZ[idim].np_tot, double);
    (GXYZ + idim)->r_1 = Array_1D (GXYZ[idim].np_tot, double);
    (GXYZ + idim)->ct  = Array_1D (GXYZ[idim].np_tot, double);
  }
/*
GXYZ[IDIR].h = Array_2D(ny,nx);
GXYZ[JDIR].h = Array_2D(nx,ny);
*/
/*  ---------------------------------------------
      dV is always positive 
      xvc can be negative if x < 0 
    --------------------------------------------- */  

  GG = GXYZ;
  for (i = 0; i <= iend; i++) {

    dx  = GG->dx[i];
    x   = GG->x[i];
    xr  = x + 0.5*dx;
    xl  = x - 0.5*dx;

    #if GEOMETRY == CARTESIAN
     GG->A[i]    = 1.0;
     GG->dV[i]   = dx;
     GG->xvc[i]  = x;
    #elif GEOMETRY == CYLINDRICAL || \
          GEOMETRY == POLAR
     GG->A[i]    = fabs(xr);
     GG->dV[i]   = fabs(x)*dx;
     GG->xvc[i]  = dsign(x)*sqrt(0.5*(xr*xr + xl*xl));
     GG->r_1[i]  = 1.0/x;
    #elif GEOMETRY == SPHERICAL
     GG->A[i]    = xr*xr;
     GG->dV[i]   = fabs(xr*xr*xr - xl*xl*xl)/3.0;
     GG->xvc[i]  = dsign(x)*pow(fabs(0.5*(xr*xr*xr + xl*xl*xl)), 1.0/3.0);
     GG->r_1[i]  = 1.5*(xr*xr - xl*xl)/(xr*xr*xr - xl*xl*xl);
GG->r_1[i] = 1.0/x;
    #endif
/*
    GG->dl = Length_1;
    GG->h  = h_scale_1;
*/
  }
  
  GG = GXYZ + 1;
  for (j = 0; j <= jend; j++) {

    dx  = GG->dx[j];
    x   = GG->x[j];
    xr  = x + 0.5*dx;
    xl  = x - 0.5*dx;

    #if GEOMETRY == CARTESIAN || \
        GEOMETRY == CYLINDRICAL || \
        GEOMETRY == POLAR
     GG->A[j]    = 1.0;
     GG->dV[j]   = dx;
     GG->xvc[j]  = x;
    #elif GEOMETRY == SPHERICAL
     GG->A[j]    = fabs(sin(xr));
     GG->dV[j]   = fabs(cos(xl) - cos(xr));
     GG->xvc[j]  = dsign(x)*acos(0.5*(cos(xr) + cos(xl)));
     GG->ct[j]   = 1.0/tan(x);  /* (sin(xr) - sin(xl))/(cos(xl) - cos(xr)); */
    #endif
/*
    GG->dl = Length_2;
    GG->h  = h_scale_2;
*/
  }
  
  GG = GXYZ + 2;
  for (k = 0; k <= kend; k++) {

    dx  = GG->dx[k];
    x   = GG->x[k];
    xr  = x + 0.5*dx;
    xl  = x - 0.5*dx;

    GG->A[k]    = 1.0;
    GG->dV[k]   = dx;
    GG->xvc[k]  = x;
/*
    GG->dl = Length_3;
    GG->h  = h_scale_3;
*/
  }
  
}


/* #################################################################### 
     
    Define geometry.
    On input requires:

        xl[n], xr[n]      : upper and lower cell edges in the xn direction

    On output returns:


        A        : area at the right interface
        dxv[n]   : is the volume in the xn coordinate  
        xvc[n]   : is the volumetric centroid of the cell with 
                   respect to the n-th coordinate
    
        ls       : is the lenght factor for the gradient operator:
                    
                     dp      p(1/2) - p(-1/2)
                    ----  =  ----------------         
                    h dx           ls

        dA_dV     : derivative of the area with respect to the 
                    volume; 

    Reference: Appendix of 
               "The Piecewise Parabolic Method for Multidimensional
                Relativistic Fluid Dynamics".
               Mignone, Plewa and Bodo, ApJs (2005).

  ##################################################################### */

/* ********************************************************************** */
real Length_1 (int i, int j, int k, Grid *grid)
/*
 *
 *
 *
 *
 *
 ************************************************************************ */
{
  return (grid[0].dx[i]);
}

/* ********************************************************************** */
real Length_2 (int i, int j, int k, Grid *grid)
/*
 *
 *
 *
 *
 *
 ************************************************************************ */
{
  #if GEOMETRY == CARTESIAN || GEOMETRY == CYLINDRICAL
   return (grid[1].dx[j]);
  #elif GEOMETRY == POLAR ||  GEOMETRY == SPHERICAL
   return (fabs(grid[0].xvc[i])*grid[1].dx[j]);
  #endif
}

/* ********************************************************************** */
real Length_3 (int i, int j, int k, Grid *grid)
/*
 *
 *
 *
 *
 *
 ************************************************************************ */
{
  #if GEOMETRY == CARTESIAN || GEOMETRY == POLAR
   return (grid[2].dx[k]);
  #elif GEOMETRY == CYLINDRICAL
   return (fabs(grid[0].xvc[i])*grid[2].dx[k]);
  #elif GEOMETRY == SPHERICAL
   return (fabs(grid[0].xvc[i]*sin(grid[1].xvc[j]))*grid[2].dx[k]);
  #endif
}

real AREA_1 (int i, int j, int k, struct GRID GXYZ[])
{
  real dx1, dx2, dx3;
  real x1p, x1m;
  real x2p, x2m;
  real x3p, x3m;
  
  dx1 = GXYZ[0].dx[i];
  dx2 = GXYZ[1].dx[j];
  dx3 = GXYZ[2].dx[k];

  x1p = GXYZ[0].xr[i];
  x2p = GXYZ[1].xr[j];
  x3p = GXYZ[2].xr[k];

  x1m = GXYZ[0].xl[i];
  x2m = GXYZ[1].xl[j];
  x3m = GXYZ[2].xl[k];
  
  #if GEOMETRY == CARTESIAN
   return fabs(dx2*dx3);
  #elif GEOMETRY == CYLINDRICAL || GEOMETRY == POLAR
   return fabs(x1p*dx2*dx3);
  #elif GEOMETRY == SPHERICAL
   return fabs(x1p*x1p*(cos(x2m) - cos(x2p))*dx3);
  #endif    

}
real AREA_2 (int i, int j, int k, struct GRID GXYZ[])
{
  real dx1, dx2, dx3;
  real x1p, x1m;
  real x2p, x2m;
  real x3p, x3m;
  
  dx1 = GXYZ[0].dx[i];
  dx2 = GXYZ[1].dx[j];
  dx3 = GXYZ[2].dx[k];

  x1p = GXYZ[0].xr[i];
  x2p = GXYZ[1].xr[j];
  x3p = GXYZ[2].xr[k];

  x1m = GXYZ[0].xl[i];
  x2m = GXYZ[1].xl[j];
  x3m = GXYZ[2].xl[k];
  
  #if GEOMETRY == CARTESIAN
   return (dx1*dx3);
  #elif GEOMETRY == POLAR
   return (dx1*dx2);
  #elif GEOMETRY == CYLINDRICAL
   return (0.5*fabs(x1p*x1p - x1m*x1m)*dx3);
  #elif GEOMETRY == SPHERICAL
   return (0.5*(x1p*x1p - x1m*x1m)*sin(x2p)*dx3);
  #endif    
}
real AREA_3 (int i, int j, int k, struct GRID GXYZ[])
{
  real dx1, dx2, dx3;
  real x1p, x1m;
  real x2p, x2m;
  real x3p, x3m;
  
  dx1 = GXYZ[0].dx[i];
  dx2 = GXYZ[1].dx[j];
  dx3 = GXYZ[2].dx[k];

  x1p = GXYZ[0].xr[i];
  x2p = GXYZ[1].xr[j];
  x3p = GXYZ[2].xr[k];

  x1m = GXYZ[0].xl[i];
  x2m = GXYZ[1].xl[j];
  x3m = GXYZ[2].xl[k];
  
  #if GEOMETRY == CARTESIAN
   return (dx1*dx2);
  #elif GEOMETRY == POLAR
   return (0.5*fabs(x1p*x1p - x1m*x1m)*dx2);
  #elif GEOMETRY == CYLINDRICAL
   return (dx1*dx3);
  #elif GEOMETRY == SPHERICAL
   return ( 0.5*(x1p*x1p - x1m*x1m)*sin(x2p)*dx3);
  #endif    
}

real h_scale_1 (int i, int j, int k, struct GRID GXYZ[])
{
  return (1.0);
}
real h_scale_2 (int i, int j, int k, struct GRID GXYZ[])
{
  #if GEOMETRY == CARTESIAN || GEOMETRY == CYLINDRICAL
   return (1.0);
  #elif GEOMETRY == POLAR ||  GEOMETRY == SPHERICAL
   return (fabs(GXYZ[0].xvc[i]));
  #endif
}
real h_scale_3 (int i, int j, int k, struct GRID GXYZ[])
{
  #if GEOMETRY == CARTESIAN || GEOMETRY == POLAR
   return (1.0);
  #elif GEOMETRY == CYLINDRICAL
   return (fabs(GXYZ[0].xvc[i]));
  #elif GEOMETRY == SPHERICAL
   return (fabs(GXYZ[0].xvc[i]*sin(GXYZ[1].xvc[j])));
  #endif
}



