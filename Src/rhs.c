#include "orion2.h"

#ifdef CH_SPACEDIM
 #define USE_PR_GRADIENT  YES   
#elif INTERPOLATION == WENO5 || INTERPOLATION == WENO7 || INTERPOLATION == MP5
 #define USE_PR_GRADIENT  NO     
#else

 #define USE_PR_GRADIENT  YES   /* -- default, do not change!! -- */
#endif

#if GEOMETRY == CYLINDRICAL

 #define ir   IDIR
 #define iphi KDIR
 #define iz   JDIR
 #if COMPONENTS == 3
  #ifndef CONSERVE_ANG_MOM 
   #define CONSERVE_ANG_MOM     YES
  #endif
  #if PHYSICS == MHD || PHYSICS == RMHD
   #ifndef CONSERVE_ANG_FIELD
    #define CONSERVE_ANG_FIELD  NO
   #endif
  #endif
 #endif

#elif GEOMETRY == POLAR

 #define ir   IDIR
 #define iphi JDIR
 #define iz   KDIR
 #if COMPONENTS >= 2
  #ifndef CONSERVE_ANG_MOM 
   #define CONSERVE_ANG_MOM     YES
  #endif
  #if PHYSICS == MHD || PHYSICS == RMHD
   #ifndef CONSERVE_ANG_FIELD
    #define CONSERVE_ANG_FIELD  YES
   #endif
  #endif
 #endif

#elif GEOMETRY == SPHERICAL
 
 #define ir   IDIR
 #define ith  JDIR
 #define iphi KDIR

 #ifndef CONSERVE_ANG_MOM 
  #define CONSERVE_ANG_MOM YES
 #endif

 #if PHYSICS == MHD || PHYSICS == RMHD
  #ifndef CONSERVE_ANG_FIELD
   #define CONSERVE_ANG_FIELD  YES
  #endif
 #endif

#endif

/* -- set to -1 when phi does not exist as a coordinate -- */

#ifndef CONSERVE_ANG_MOM
 #define CONSERVE_ANG_MOM  (-1)
#endif

#ifndef CONSERVE_ANG_FIELD
 #define CONSERVE_ANG_FIELD (-1)
#endif


/* *********************************************************** */
#if SELF_GRAVITY == YES || SINK_PARTICLES == YES
void GET_RHS (const State_1D *state, int beg, int end, real dt,
	      real *gf1d, Grid *grid)
#else
void GET_RHS (const State_1D *state, int beg, int end, 
              real dt, Grid *grid)
#endif
/*! 
 *
 * Compute right hand side of the PDE in different 
 * geometries, taking contributions from one direction 
 * at a time. Include geometrical source terms and forces.
 *
 * 
 *  rhs = dt/dV * (Ap*Fp - Am*Fm) + dt*S
 *
 * \param state pointer to a state_1D strucutre;
 * \param dt current time increment
 * \param grid grid structure
 * 
 * \author A. Mignone (e-mail: mignone@to.astro.it)
 * \date   Aug 5th, 2008
 *
 ************************************************************* */
{
  int  i, nv, s;
  real dtdx, dtdV, scrh;
  real **flux, **rhs, *p;

  #if GEOMETRY != CARTESIAN
   static real ***MTsrc, ***ITsrc;
   static real **fA;

   real r1, r2, r3, r_1;
   real s1, s2, s_1, ct;
   real *r, *th;

   if (fA == NULL) {
     fA   = Array_2D(NMAX_POINT, NVAR, double);
     MTsrc = Array_3D(3, 3, NMAX_POINT, double);
     ITsrc = Array_3D(3, 3, NMAX_POINT, double);
   }
  #endif

/* --------------------------------------------------
             Compute passive scalar fluxes
   -------------------------------------------------- */

  #if NSCL > 0
   ADVECT_FLUX (state, beg - 1, end, grid);
  #endif

  rhs  = state->rhs;
  flux = state->flux;
  p    = state->press;

/* ------------------------------------------------
     Add pressure to normal component of 
     momentum flux if necessary.
   ------------------------------------------------ */
 
  #if USE_PR_GRADIENT == NO
   #if INTERPOLATION == MP5
    s = 1;
   #else
    s = 0;
   #endif
   for (i = beg - 1 - s; i <= end + s; i++) flux[i][M1] += p[i];
  #endif

/* ------------------------------------------------
             Build Right Hand Side
   ------------------------------------------------- */
 
  #if GEOMETRY == CARTESIAN

   for (i = beg; i <= end; i++) {
     dtdx = dt/grid[DIR].dx[i];
     for (nv = NVAR; nv--;  ) {
       rhs[i][nv] = -dtdx*(flux[i][nv] - flux[i - 1][nv]);
     }
     #if USE_PR_GRADIENT == YES
      rhs[i][M1] -= dtdx*(p[i] - p[i - 1]);
     #endif
   }

  #elif GEOMETRY == CYLINDRICAL || GEOMETRY == POLAR

   r = grid[ir].x;
   if (DIR == ir) {         /* *********************************************
                               *          Cylindrical, r                   *
                               ********************************************* */

   /* ------------------------------------------------
              Compute FLUX X AREA
      ------------------------------------------------ */

     for (i = beg - 1; i <= end; i++) {
       r1 = grid[ir].A[i];
       for (nv = NVAR; nv-- ;) fA[i][nv] = flux[i][nv]*r1;
       #if CONSERVE_ANG_MOM == YES
        fA[i][iMPHI] *= r1;
       #endif
     }

   /* ------------------------------------------------
            Construct rhs operators, r - dir
      ------------------------------------------------ */

     for (i = beg; i <= end; i++) {
       dtdV = dt/grid[ir].dV[i];
       dtdx = dt/grid[ir].dx[i];
       r_1  = 1.0/r[i];

       for (nv = NVAR; nv--;   ){
         rhs[i][nv] = -dtdV*(fA[i][nv] - fA[i - 1][nv]);
       }
/*     IFF(USE_PR_GRADIENT, rhs[i][M1] -= dtdx*(p[i] - p[i-1]); */
       #if USE_PR_GRADIENT == YES
        rhs[i][iMR] -= dtdx*(p[i] - p[i - 1]);   
       #endif
       #ifdef PSI_GLM
        rhs[i][B1] = -dtdx*(flux[i][B1] - flux[i-1][B1]);
       #endif
/*     IFF(CONSERVE_ANG_MOM,         rhs[i][iMPHI] *= r_1;);  */
       #if CONSERVE_ANG_MOM == YES
        rhs[i][iMPHI] *= fabs(r_1);  /* -- same sign as Area -- */
       #endif
       #if  CONSERVE_ANG_FIELD == YES || RESISTIVE_MHD == EXPLICIT
      /* -------------------------------------------------------
          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          in resistive MHD, we use the formulation without the 
          source terms, just because we cannot compute them!
          The other formulation (more stable for toroidal jet 
          configurations) will be used for ideal MHD
          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
         ------------------------------------------------------- */

        rhs[i][iBPHI] = - dtdx*(flux[i][iBPHI] - flux[i - 1][iBPHI]);
       #endif
     }
     
   } else if (DIR == iphi) {   /* *********************************************
                                  *          Polar, phi                       *
                                  ********************************************* */

      for (i = beg; i <= end; i++) {
        dtdx = dt/(r[*NX_PT]*grid[iphi].dx[i]);
        for (nv = NVAR; nv--;  ) {
          rhs[i][nv] = -dtdx*(flux[i][nv] - flux[i - 1][nv]);
        }
        #if USE_PR_GRADIENT == YES
         rhs[i][iMPHI] -= dtdx*(p[i] - p[i - 1]);
        #endif
      }

   } else if (DIR == iz) {    /* *********************************************
                                 *         Cylindrical, z                    *
                                 ********************************************* */

      for (i = beg; i <= end; i++) {
       dtdx = dt/grid[iz].dx[i];
       for (nv = NVAR; nv--;  ) {
         rhs[i][nv] = -dtdx*(flux[i][nv] - flux[i - 1][nv]);
       }
       #if USE_PR_GRADIENT == YES
        rhs[i][iMZ] -= dtdx*(p[i] - p[i - 1]);
       #endif
     }

   }

  #elif GEOMETRY == SPHERICAL

   r  = grid[IDIR].x;
   th = grid[JDIR].x;

   if (DIR == IDIR) {    /* *********************************************
                            *          Spherical, r                     *
                            ********************************************* */

   /* ------------------------------------------------
              Compute FLUX X AREA
      ------------------------------------------------ */

     for (i = beg - 1; i <= end; i++) {

       r1 = grid[IDIR].xr[i];
       r2 = r1*r1;
       r3 = r2*r1;

    /* ------------------------------------------------
         fphi -> fphi*r*sin(theta) , 
         but the sin(theta) factor cancels out at the 
         end (so we do not include it here)  
      -------------------------------------------------- */

       fA[i][DN] = flux[i][DN]*r2;
       EXPAND(fA[i][MX] = flux[i][MX]*r2;  ,
              fA[i][MY] = flux[i][MY]*r2;  ,
              fA[i][MZ] = flux[i][MZ]*r3;)

       #if PHYSICS == MHD || PHYSICS == RMHD
        EXPAND(fA[i][BX] = flux[i][BX]*r2;  ,
               fA[i][BY] = flux[i][BY]*r1;  ,
               fA[i][BZ] = flux[i][BZ]*r1;)
       #endif

       #if EOS != ISOTHERMAL
        fA[i][EN] = flux[i][EN]*r2;
       #endif

       #ifdef PSI_GLM
        fA[i][PSI_GLM] = flux[i][PSI_GLM]*r2;
       #endif

       /* ---- scalars ---- */

       for (nv = NFLX; nv < NVAR; nv++) fA[i][nv] = flux[i][nv]*r2;
     } 

   /* ------------------------------------------------
         Compute momentum tensor source terms
      ------------------------------------------------ */
/*
     MOM_SOURCE (state, beg, end, ith, ith, MTsrc[ith][ith], grid);
     #if COMPONENTS == 3
      MOM_SOURCE (state, beg, end, iphi, iphi, MTsrc[iphi][iphi], grid);
     #endif
*/         
   /* ------------------------------------------------
            Construct rhs operators, r - dir
      ------------------------------------------------ */

     for (i = beg; i <= end; i++) {
       dtdV = dt/grid[IDIR].dV[i];
       dtdx = dt/grid[IDIR].dx[i];
       r_1  = grid[IDIR].r_1[i];

     /* ---------------------------------
          add geometrical source terms 
        --------------------------------- */

       rhs[i][DN] = -dtdV*(fA[i][DN] - fA[i - 1][DN]);
       #if EOS != ISOTHERMAL
        rhs[i][EN] = -dtdV*(fA[i][EN] - fA[i - 1][EN]);
       #endif
       EXPAND(
         rhs[i][MX] = - dtdV*(fA[i][MX] - fA[i - 1][MX]);      ,
         rhs[i][MY] = - dtdV*(fA[i][MY] - fA[i - 1][MY]);      ,
         rhs[i][MZ] = - dtdV*(fA[i][MZ] - fA[i - 1][MZ])*r_1;
       )
       #if USE_PR_GRADIENT == YES
        rhs[i][MX] -= dtdx*(p[i] - p[i - 1]);
       #endif
       
       #if PHYSICS == MHD || PHYSICS == RMHD
        EXPAND(                                                     
          rhs[i][BX] = -dtdV*(fA[i][BX] - fA[i - 1][BX]);      ,
          rhs[i][BY] = -dtdx*(fA[i][BY] - fA[i - 1][BY])*r_1;  ,
          rhs[i][BZ] = -dtdx*(fA[i][BZ] - fA[i - 1][BZ])*r_1;
        )
       #endif
       #ifdef PSI_GLM
        rhs[i][B1]      = -dtdx*(flux[i][B1] - flux[i-1][B1]);
        rhs[i][PSI_GLM] = -dtdV*(fA[i][PSI_GLM] - fA[i - 1][PSI_GLM]);
       #endif

       /* ---- scalars ---- */

       for (nv = NFLX; nv < NVAR; nv++) rhs[i][nv] = -dtdV*(fA[i][nv] - fA[i - 1][nv]);
     }

   } else if (DIR == JDIR) {   /* *********************************************
                                  *          Spherical, Theta                 *
                                  ********************************************* */

   /* ------------------------------------------------
              Compute FLUX X AREA
      ------------------------------------------------ */

     for (i = beg - 1; i <= end; i++) {

       s1 = grid[JDIR].A[i];
       s2 = s1*s1;

       fA[i][DN] = flux[i][DN]*s1;
       EXPAND(fA[i][MX] = flux[i][MX]*s1;  ,
              fA[i][MY] = flux[i][MY]*s1;  ,
              fA[i][MZ] = flux[i][MZ]*s2;)  /* --  fphi -> fphi*sin(theta+) -- */

       #if PHYSICS == MHD || PHYSICS == RMHD
        EXPAND(fA[i][BX] = flux[i][BX]*s1;  ,
               fA[i][BY] = flux[i][BY]*s1;  ,
               fA[i][BZ] = flux[i][BZ];)
       #endif

       #if EOS != ISOTHERMAL
        fA[i][EN] = flux[i][EN]*s1;
       #endif

       #ifdef PSI_GLM
        fA[i][PSI_GLM] = flux[i][PSI_GLM]*s1;
       #endif

       /* ---- scalars ---- */

       for (nv = NFLX; nv < NVAR; nv++) fA[i][nv] = flux[i][nv]*s1;
     }
    
   /* ------------------------------------------------
         Compute momentum tensor source terms
      ------------------------------------------------ */
/*
     MOM_SOURCE (state, beg, end, JDIR, IDIR, MTsrc[ith][ir], grid);
     #if COMPONENTS == 3
      MOM_SOURCE (state, beg, end, KDIR, KDIR, MTsrc[iphi][iphi], grid);
     #endif
*/
   /* ------------------------------------------------
          Construct rhs operators, theta - dir
      ------------------------------------------------ */

     r_1 = grid[IDIR].r_1[*NX_PT];
     for (i = beg; i <= end; i++) {

       dtdV = dt/grid[JDIR].dV[i]*r_1;
       dtdx = dt/grid[JDIR].dx[i]*r_1;      

       s_1 = 1.0/sin(th[i]);   
       ct  = grid[JDIR].ct[i];         /* = cot(theta)  */

//       scrh = EXPAND(0.0, - MTsrc[ith][ir][i], + ct*MTsrc[iphi][iphi][i]);  /* -- m[th] source -- */

       rhs[i][DN] = - dtdV*(fA[i][DN] - fA[i - 1][DN]);
       #if EOS != ISOTHERMAL
        rhs[i][EN] = - dtdV*(fA[i][EN] - fA[i - 1][EN]);
       #endif
       EXPAND(
         rhs[i][MX] = - dtdV*(fA[i][MX] - fA[i - 1][MX]);      , 
         rhs[i][MY] = - dtdV*(fA[i][MY] - fA[i - 1][MY]);      , 
         rhs[i][MZ] = - dtdV*(fA[i][MZ] - fA[i - 1][MZ])*s_1;
       )
       #if USE_PR_GRADIENT == YES
        rhs[i][MY] -= dtdx*(p[i] - p[i - 1]);
       #endif
       #if PHYSICS == MHD || PHYSICS == RMHD
        EXPAND(                                                     
          rhs[i][BX] = - dtdV*(fA[i][BX] - fA[i - 1][BX]);  ,
          rhs[i][BY] = - dtdV*(fA[i][BY] - fA[i - 1][BY]);  ,
          rhs[i][BZ] = - dtdx*(fA[i][BZ] - fA[i - 1][BZ]);
        )
       #endif
       #ifdef PSI_GLM
        rhs[i][B1]      = -dtdx*(flux[i][B1] - flux[i-1][B1]);
        rhs[i][PSI_GLM] = -dtdV*(fA[i][PSI_GLM] - fA[i - 1][PSI_GLM]);
       #endif

       /* ---- scalars ---- */

       for (nv = NFLX; nv < NVAR; nv++) rhs[i][nv] = -dtdV*(fA[i][nv] - fA[i - 1][nv]);
     }

   } else if (DIR == KDIR) {    /* *********************************************
                                   *          Spherical, Phi                   *
                                   ********************************************* */

     for (i = beg; i <= end; i++) {
       dtdx = dt/(r[*NX_PT]*sin(th[*NY_PT])*grid[DIR].dx[i]);       
       for (nv = NVAR; nv--;  ) {
         rhs[i][nv] = -dtdx*(flux[i][nv] - flux[i - 1][nv]);
       }       
       #if USE_PR_GRADIENT == YES
        rhs[i][iMPHI] -= dtdx*(p[i] - p[i - 1]); 
       #endif
     }
   }

  #endif

/* ------------------------------------------------------------------
      Add geometrical sources
   ------------------------------------------------------------------ */

  #if GEOMETRY != CARTESIAN
   ADD_GEOM_SOURCE (state, beg, end, dt, grid);
  #endif

/* ----------------------------------------------------------------
                Powell's source terms
   ---------------------------------------------------------------- */

  #if PHYSICS == MHD || PHYSICS == RMHD 
   #if MHD_FORMULATION == EIGHT_WAVES

    for (i = beg; i <= end; i++) {
      EXPAND(rhs[i][MX] += dt*state->src[i][MX];  ,
             rhs[i][MY] += dt*state->src[i][MY];  ,
             rhs[i][MZ] += dt*state->src[i][MZ];) 

      EXPAND(rhs[i][BX] += dt*state->src[i][BX];  ,
             rhs[i][BY] += dt*state->src[i][BY];  ,
             rhs[i][BZ] += dt*state->src[i][BZ];) 
      #if EOS != ISOTHERMAL
       rhs[i][EN] += dt*state->src[i][EN];
      #endif
    }

   #endif
  #endif

/* -------------------------------------------------
            Extended GLM source terms 
   ------------------------------------------------- */

  #ifdef PSI_GLM 
  #if   EGLM == YES
   GLM_RHS_SOURCE (state, dt, beg, end, grid); 
  #endif
  #endif

  #if RESISTIVE_MHD == EXPLICIT 
   #if (ENTROPY_SWITCH == YES) && (EOS != ISOTHERMAL)
    for (i = beg; i <= end; i++) {
      rhs[i][ENTR] += dt*state->src[i][ENTR];
    }
   #endif
  #endif
    
/* ------------------------------------------------------------------
     Source terms coming from tensor discretazion of parabolic terms
     in curvilinear coordinates
   ------------------------------------------------------------------ */

  #if GEOMETRY != CARTESIAN
   #if VISCOSITY == EXPLICIT
    for (i = beg; i <= end; i++) {
    for (nv = NVAR; nv--;  ){
      rhs[i][nv] += dt*state->par_src[i][nv];
    }}
   #endif
  #endif

/* ------------------------------------------------------------------
                  Include body forces
   ------------------------------------------------------------------ */

  #if INCLUDE_BODY_FORCE == EXPLICIT
   ADD_BODY_FORCE (state, dt, beg, end, grid);
  #endif

   /*PS: gravity force from Chombo. */
  #if SELF_GRAVITY == YES || SINK_PARTICLES == YES
   GRAVITY_FORCE (state, gf1d, dt, beg, end, grid);
  #endif
}


/* *********************************************************************  */
void ADD_GEOM_SOURCE (const State_1D *state, int beg, int end, 
                      double dt, Grid *grid)
/* 
 *
 * PURPOSE
 *
 *   Add geometrical source terms arising in curvilinear 
 *   coordinates to the RHS of state_1D.
 *
 * ARGUMENTS
 *
 *   state   :  a pointer to a state structure
 *   beg, end:  initial and final points of the grid
 *   dt      : the current time step
 * 
 *
 * LAST MODIFIED
 *
 *   Sept 5th, 2008 by A. Mignone.  e-mail: mignone@ph.unito.it
 *    
 *
 *********************************************************************** */
#if PHYSICS == HD || PHYSICS == RHD

 #define T(a,b) (u[MX+a]*v[VX+b])

#elif PHYSICS == MHD

 #if INCLUDE_BACKGROUND_FIELD == NO

  #define T(a,b) (u[MX+a]*v[VX+b] - v[BX+a]*v[BX+b])
  #define O(a,b) (v[VX+a]*u[BX+b] - u[BX+a]*v[VX+b])

 #else

  #define T(a,b) (u[MX+a]*v[VX+b] - v[BX+a]*(v[BX+b] + B0[BX+b]) - B0[BX+a]*v[BX+b])
  #define O(a,b) (v[VX+a]*(v[BX+b]+B0[BX+b]) - (v[BX+a]+B0[BX+a])*v[VX+b])

 #endif

#elif PHYSICS == RMHD

  #define LOR_2  (1.0 - (EXPAND(v[VX]*v[VX], + v[VY]*v[VY], + v[VZ]*v[VZ])))
  #define vdotB  (EXPAND(v[VX]*v[BX], + v[VY]*v[BY], + v[VZ]*v[BZ]))

  #define T(a,b) (u[MX+a]*v[VX+b] - (v[BX+a]*LOR_2 + vdotB*v[VX+a])*v[BX+b])
  #define O(a,b) (v[VX+a]*u[BX+b] - u[BX+a]*v[VX+b])

#endif

{
  int  i;
  double dtr, scrh, r_1;
  real *u, *v, *p, *B0, *r, *ct;
  real **bgf;

  #if INCLUDE_BACKGROUND_FIELD == YES
   bgf = GET_BACKGROUND_FIELD (beg, end, CELL_CENTER, grid);
  #endif
  
  r = grid[IDIR].x;
  if (DIR == IDIR){
    for (i = beg; i <= end; i++){

      v = state->vh[i]; u = state->uh[i];
      #if INCLUDE_BACKGROUND_FIELD == YES
       B0 = bgf[i];
      #endif
      p  = state->press;
      r_1 = 1.0/r[i];
      dtr = dt/r[i];

  /* --------------------------------------------------------------
      Include pressure source if pressure gradients is not used.
      This is should be enabled ONLY in CYLINDRICAL geometry 
      in AMR.
     -------------------------------------------------------------- */

      #if USE_PR_GRADIENT == NO && (GEOMETRY != CARTESIAN)
       state->rhs[i][iMR] += 0.5*dt*(p[i] + p[i-1])*r_1;
       #if GEOMETRY == SPHERICAL
        !!!!!
       #endif
      #endif

  /* -----------------------------------------------------------------
       add Tensor components in the different systems of coordinates 
     ----------------------------------------------------------------- */
 
      #if  GEOMETRY == CYLINDRICAL && COMPONENTS == 3
       scrh = T(iphi, iphi);
       state->rhs[i][iMR] += dt*scrh*r_1;
       #if CONSERVE_ANG_MOM == NO
        scrh = T(iphi, ir);
        state->rhs[i][iMPHI] -= dt*scrh*r_1;
       #endif
       #if CONSERVE_ANG_FIELD == NO && RESISTIVE_MHD != EXPLICIT
        scrh = O(iphi, ir);
        state->rhs[i][iBPHI] -= dt*scrh*r_1;
       #endif
      #endif

      #if GEOMETRY == POLAR && COMPONENTS >= 2
       scrh = T(iphi, iphi);
       state->rhs[i][iMR] += dt*scrh*r_1;
      #endif

      #if GEOMETRY == SPHERICAL 
       scrh = EXPAND(0.0, + T(ith, ith), + T(iphi, iphi));
       state->rhs[i][iMR] += dt*scrh*r_1;
      #endif
    }
  }

  if (DIR == JDIR){

    r_1 = 1.0/r[*NX_PT];
 
    #if GEOMETRY == POLAR && COMPONENTS >= 2
     for (i = beg; i <= end; i++){
       #if CONSERVE_ANG_MOM == NO
        scrh = T(iphi, ir);
        state->rhs[i][iMPHI] -= dt*scrh*r_1;
       #endif
       #if CONSERVE_ANG_FIELD == NO && RESISTIVE_MHD != EXPLICIT
        scrh = O(iphi, ir);
        state->rhs[i][iBPHI] -= dt*scrh*r_1;
       #endif
     }
    #endif

    #if GEOMETRY == SPHERICAL
     ct  = grid[JDIR].ct;
     dtr = dt/r[*NX_PT];
     for (i = beg; i <= end; i++){
       v = state->vh[i]; u = state->uh[i];
       #if INCLUDE_BACKGROUND_FIELD == YES
        B0 = bgf[i];
       #endif
       scrh = EXPAND(0.0, - T(ith, ir), + ct[i]*T(iphi, iphi));
       state->rhs[i][iMTH] += dt*scrh*r_1;
     }
    #endif
  }
}
#undef T
#ifdef O
 #undef O
#endif
#if PHYSICS == RMHD
 #undef LOR_2
 #undef vdotB
#endif
