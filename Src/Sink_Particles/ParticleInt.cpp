#include <math.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#define NRANSI
#include "ParticleInt.H"
#define MAXSTP 10000
#define TINY 1.0e-30

int kmax=0,kount;
Real *xp,**yp,dxsav=0.0;
Real **d,*x;

void ParticleInt(Real ystart[], int nvar, Real x1, Real x2, Real eps, Real h1,
	    Real hmin, int *nok, int *nbad, Real *mass, Real G, Real soften,
	    void (*derivs)(Real, Real [], Real [], Real [], Real, Real, int),
	    void (*bsstep)(Real [], Real [], int, Real *, Real, Real, Real [],
			   Real *, Real *, Real *, Real, Real,
			   void (*)(Real, Real [], Real [], 
				    Real [], Real, Real, int)))
{
	int nstp,i,j,k,npart;
	Real xsav,x,hnext,hdid,h;
	Real *yscal,*y,*dydx;
	Real dist, distmin, vel, velmin;

	yscal=nr_vector(1,nvar);
	y=nr_vector(1,nvar);
	dydx=nr_vector(1,nvar);
	x=x1;
	h=SIGN(h1,x2-x1);
	*nok = (*nbad) = kount = 0;
	for (i=1;i<=nvar;i++) y[i]=ystart[i];
	if (kmax > 0) xsav=x-dxsav*2.0;
	for (nstp=1;nstp<=MAXSTP;nstp++) {
		(*derivs)(x,y,dydx,mass,G,soften,nvar);
		/*
		for (i=1;i<=nvar;i++)
			yscal[i]=fabs(y[i])+fabs(dydx[i]*h)+TINY;
		*/

		/* Use a different scaling here that doesn't break for
		   positions/velocities near zero */
		npart=nvar/(2*CH_SPACEDIM);
		distmin=velmin=1.0e50;
		for (i=0; i<npart; i++) {
		  for (j=i+1; j<npart; j++) {
		    dist=vel=0.0;
		    for (k=1; k<=CH_SPACEDIM; k++) {
		      dist += SQR(y[CH_SPACEDIM*i+k]-y[CH_SPACEDIM*j+k]);
		      vel += SQR(y[npart*CH_SPACEDIM+CH_SPACEDIM*i+k] - 
				 y[npart*CH_SPACEDIM+CH_SPACEDIM*j+k]);
		    }
		    dist=sqrt(dist);
		    vel=sqrt(vel);
		    if (distmin > dist) distmin=dist;
		    if (velmin > vel) velmin=vel;
		  }
		}
		if (velmin < sqrt(eps)*dist/(x2-x1)) velmin = sqrt(eps)*dist/(x2-x1);
		for (i=1; i<=nvar/2; i++) yscal[i] = distmin + TINY;
		for (i=nvar/2+1; i<=nvar; i++) yscal[i] = velmin + TINY;

		if (kmax > 0 && kount < kmax-1 && fabs(x-xsav) > fabs(dxsav)) {
			xp[++kount]=x;
			for (i=1;i<=nvar;i++) yp[i][kount]=y[i];
			xsav=x;
		}
		if ((x+h-x2)*(x+h-x1) > 0.0) h=x2-x;
		(*bsstep)(y,dydx,nvar,&x,h,eps,yscal,&hdid,&hnext,
			  mass,G,soften,derivs);
		if (hdid == h) ++(*nok); else ++(*nbad);
		if ((x-x2)*(x2-x1) >= 0.0) {
			for (i=1;i<=nvar;i++) ystart[i]=y[i];
			if (kmax) {
				xp[++kount]=x;
				for (i=1;i<=nvar;i++) yp[i][kount]=y[i];
			}
			free_nr_vector(dydx,1,nvar);
			free_nr_vector(y,1,nvar);
			free_nr_vector(yscal,1,nvar);
			return;
		}
		if (fabs(hnext) <= hmin) nrerror("Step size too small in odeint");
		h=hnext;
	}
	nrerror("Too many steps in routine odeint");
}
#undef MAXSTP
#undef TINY
#undef NRANSI

#define NRANSI
#define KMAXX 12
#define IMAXX (KMAXX+1)
#define SAFE1 0.25
#define SAFE2 0.7
#define REDMAX 1.0e-5
#define REDMIN 0.7
#define TINY 1.0e-30
#define SCALMX 0.1

void bsstep(Real y[], Real dydx[], int nv, Real *xx, Real htry, Real eps,
	    Real yscal[], Real *hdid, Real *hnext, Real mass [], Real G,
	    Real soften,
	    void (*derivs)(Real, Real [], Real [], Real [], Real, Real, int))
{
	int i,iq,k,kk,km;
	static int first=1,kmax,kopt;
	static Real epsold = -1.0,xnew;
	Real eps1,errmax,fact,h,red,scale,work,wrkmin,xest;
	Real *err,*yerr,*ysav,*yseq;
	static Real a[IMAXX+1];
	static Real alf[KMAXX+1][KMAXX+1];
	static int nseq[IMAXX+1]={0,1,2,3,4,5,6,7,8,9,10,11,12};
	int reduct,exitflag=0;

	d=nr_matrix(1,nv,1,KMAXX);
	err=nr_vector(1,KMAXX);
	x=nr_vector(1,KMAXX);
	yerr=nr_vector(1,nv);
	ysav=nr_vector(1,nv);
	yseq=nr_vector(1,nv);
	if (eps != epsold) {
		*hnext = xnew = -1.0e29;
		eps1=SAFE1*eps;
		a[1]=nseq[1]+1;
		for (k=1;k<=KMAXX;k++) a[k+1]=a[k]+nseq[k+1];
		for (iq=2;iq<=KMAXX;iq++) {
			for (k=1;k<iq;k++)
				alf[k][iq]=pow(eps1,(a[k+1]-a[iq+1])/
					((a[iq+1]-a[1]+1.0)*(2*k+1)));
		}
		epsold=eps;
		for (kopt=2;kopt<KMAXX;kopt++)
			if (a[kopt+1] > a[kopt]*alf[kopt-1][kopt]) break;
		kmax=kopt;
	}
	h=htry;
	for (i=1;i<=nv;i++) ysav[i]=y[i];
	if (*xx != xnew || h != (*hnext)) {
		first=1;
		kopt=kmax;
	}
	reduct=0;
	for (;;) {
		for (k=1;k<=kmax;k++) {
			xnew=(*xx)+h;
			if (xnew == (*xx)) nrerror("step size underflow in bsstep");
			stoerm(ysav,dydx,nv,*xx,h,nseq[k],yseq,
			       mass,G,soften,derivs);
			xest=SQR(h/nseq[k]);
			pzextr(k,xest,yseq,y,yerr,nv);
			if (k != 1) {
				errmax=TINY;
				for (i=1;i<=nv;i++) errmax=FMAX(errmax,fabs(yerr[i]/yscal[i]));
				errmax /= eps;
				km=k-1;
				err[km]=pow(errmax/SAFE1,1.0/(2*km+1));
			}
			if (k != 1 && (k >= kopt-1 || first)) {
				if (errmax < 1.0) {
					exitflag=1;
					break;
				}
				if (k == kmax || k == kopt+1) {
					red=SAFE2/err[km];
					break;
				}
				else if (k == kopt && alf[kopt-1][kopt] < err[km]) {
						red=1.0/err[km];
						break;
					}
				else if (kopt == kmax && alf[km][kmax-1] < err[km]) {
						red=alf[km][kmax-1]*SAFE2/err[km];
						break;
					}
				else if (alf[km][kopt] < err[km]) {
					red=alf[km][kopt-1]/err[km];
					break;
				}
			}
		}
		if (exitflag) break;
		red=FMIN(red,REDMIN);
		red=FMAX(red,REDMAX);
		h *= red;
		reduct=1;
	}
	*xx=xnew;
	*hdid=h;
	first=0;
	wrkmin=1.0e35;
	for (kk=1;kk<=km;kk++) {
		fact=FMAX(err[kk],SCALMX);
		work=fact*a[kk+1];
		if (work < wrkmin) {
			scale=fact;
			wrkmin=work;
			kopt=kk+1;
		}
	}
	*hnext=h/scale;
	if (kopt >= k && kopt != kmax && !reduct) {
		fact=FMAX(scale/alf[kopt-1][kopt],SCALMX);
		if (a[kopt+1]*fact <= wrkmin) {
			*hnext=h/fact;
			kopt++;
		}
	}
	free_nr_vector(yseq,1,nv);
	free_nr_vector(ysav,1,nv);
	free_nr_vector(yerr,1,nv);
	free_nr_vector(x,1,KMAXX);
	free_nr_vector(err,1,KMAXX);
	free_nr_matrix(d,1,nv,1,KMAXX);
}
#undef KMAXX
#undef IMAXX
#undef SAFE1
#undef SAFE2
#undef REDMAX
#undef REDMIN
#undef TINY
#undef SCALMX
#undef NRANSI

#define NRANSI
void pzextr(int iest, Real xest, Real yest[], Real yz[], Real dy[], int nv)
{
	int k1,j;
	Real q,f2,f1,delta,*c;

	c=nr_vector(1,nv);
	x[iest]=xest;
	for (j=1;j<=nv;j++) dy[j]=yz[j]=yest[j];
	if (iest == 1) {
		for (j=1;j<=nv;j++) d[j][1]=yest[j];
	} else {
		for (j=1;j<=nv;j++) c[j]=yest[j];
		for (k1=1;k1<iest;k1++) {
			delta=1.0/(x[iest-k1]-xest);
			f1=xest*delta;
			f2=x[iest-k1]*delta;
			for (j=1;j<=nv;j++) {
				q=d[j][k1];
				d[j][k1]=dy[j];
				delta=c[j]-q;
				dy[j]=f1*delta;
				c[j]=f2*delta;
				yz[j] += dy[j];
			}
		}
		for (j=1;j<=nv;j++) d[j][iest]=dy[j];
	}
	free_nr_vector(c,1,nv);
}
#undef NRANSI

#define NRANSI
void stoerm(Real y[], Real d2y[], int nv, Real xs, Real htot, int nstep,
	    Real yout[], Real mass[], Real G, Real soften,
	    void (*derivs)(Real, Real [], Real [], Real [], Real, Real, int))
{
	int i,n,neqns,nn;
	Real h,h2,halfh,x,*ytemp;

	ytemp=nr_vector(1,nv);
	h=htot/nstep;
	halfh=0.5*h;
	neqns=nv/2;
	for (i=1;i<=neqns;i++) {
		n=neqns+i;
		ytemp[i]=y[i]+(ytemp[n]=h*(y[n]+halfh*d2y[i]));
	}
	x=xs+h;
	(*derivs)(x,ytemp,yout,mass,G,soften,nv);
	h2=h*h;
	for (nn=2;nn<=nstep;nn++) {
		for (i=1;i<=neqns;i++)
			ytemp[i] += (ytemp[(n=neqns+i)] += h2*yout[i]);
		x += h;
		(*derivs)(x,ytemp,yout,mass,G,soften,nv);
	}
	for (i=1;i<=neqns;i++) {
		n=neqns+i;
		yout[n]=ytemp[n]/h+halfh*yout[i];
		yout[i]=ytemp[i];
	}
	free_nr_vector(ytemp,1,nv);
}
#undef NRANSI

void ParticleDerivs(Real time, Real x[], Real d2xdt2[], Real mass[], Real G,
	    Real soften, int nvar)
{
  Real r, r2;
  int i, j, k, nparticle=nvar/(2*CH_SPACEDIM);

#if 0
  // Disable softening
  soften = 1.0e-30;
#endif

  for (i=1; i<=nvar; i++) d2xdt2[i] = 0.0;
  for (i=0; i<nparticle; i++) {
    for (j=i+1; j<nparticle; j++) {
      r2 = 0.0;
      for (k=1; k<=CH_SPACEDIM; k++)
	r2 += SQR(x[CH_SPACEDIM*i+k] - x[CH_SPACEDIM*j+k]);
      r = sqrt(r2);
      r2 += SQR(soften);
      if (r==0.0) r = 1.0e-30;
      for (k=1; k<=CH_SPACEDIM; k++) {
	d2xdt2[3*i+k] += -G*mass[j+1]/r2 *
	  (x[CH_SPACEDIM*i+k]-x[CH_SPACEDIM*j+k])/r;
	d2xdt2[3*j+k] += G*mass[i+1]/r2 *
	  (x[CH_SPACEDIM*i+k]-x[CH_SPACEDIM*j+k])/r;
      }
    }
  }
}

/* CAUTION: This is the ANSI C (only) version of the Numerical Recipes
   utility file nrutil.c.  Do not confuse this file with the same-named
   file nrutil.c that is supplied in the 'misc' subdirectory.
   *That* file is the one from the book, and contains both ANSI and
   traditional K&R versions, along with #ifdef macros to select the
   correct version.  *This* file contains only ANSI C.               */

#define NR_END 1
#define FREE_ARG char*

void nrerror(char error_text[])
/* Numerical Recipes standard error handler */
{
	fprintf(stderr,"Numerical Recipes run-time error...\n");
	fprintf(stderr,"%s\n",error_text);
	fprintf(stderr,"...now exiting to system...\n");
	exit(1);
}

Real *nr_vector(long nl, long nh)
/* allocate a Real vector with subscript range v[nl..nh] */
{
	Real *v;

	v=(Real *)malloc((size_t) ((nh-nl+1+NR_END)*sizeof(Real)));
	if (!v) nrerror("allocation failure in vector()");
	return v-nl+NR_END;
}

int *nr_ivector(long nl, long nh)
/* allocate an int vector with subscript range v[nl..nh] */
{
	int *v;

	v=(int *)malloc((size_t) ((nh-nl+1+NR_END)*sizeof(int)));
	if (!v) nrerror("allocation failure in ivector()");
	return v-nl+NR_END;
}

unsigned char *nr_cvector(long nl, long nh)
/* allocate an unsigned char vector with subscript range v[nl..nh] */
{
	unsigned char *v;

	v=(unsigned char *)malloc((size_t) ((nh-nl+1+NR_END)*sizeof(unsigned char)));
	if (!v) nrerror("allocation failure in cvector()");
	return v-nl+NR_END;
}

unsigned long *nr_lvector(long nl, long nh)
/* allocate an unsigned long vector with subscript range v[nl..nh] */
{
	unsigned long *v;

	v=(unsigned long *)malloc((size_t) ((nh-nl+1+NR_END)*sizeof(long)));
	if (!v) nrerror("allocation failure in lvector()");
	return v-nl+NR_END;
}

double *nr_dvector(long nl, long nh)
/* allocate a double vector with subscript range v[nl..nh] */
{
	double *v;

	v=(double *)malloc((size_t) ((nh-nl+1+NR_END)*sizeof(double)));
	if (!v) nrerror("allocation failure in dvector()");
	return v-nl+NR_END;
}

Real **nr_matrix(long nrl, long nrh, long ncl, long nch)
/* allocate a Real matrix with subscript range m[nrl..nrh][ncl..nch] */
{
	long i, nrow=nrh-nrl+1,ncol=nch-ncl+1;
	Real **m;

	/* allocate pointers to rows */
	m=(Real **) malloc((size_t)((nrow+NR_END)*sizeof(Real*)));
	if (!m) nrerror("allocation failure 1 in matrix()");
	m += NR_END;
	m -= nrl;

	/* allocate rows and set pointers to them */
	m[nrl]=(Real *) malloc((size_t)((nrow*ncol+NR_END)*sizeof(Real)));
	if (!m[nrl]) nrerror("allocation failure 2 in matrix()");
	m[nrl] += NR_END;
	m[nrl] -= ncl;

	for(i=nrl+1;i<=nrh;i++) m[i]=m[i-1]+ncol;

	/* return pointer to array of pointers to rows */
	return m;
}

double **nr_dmatrix(long nrl, long nrh, long ncl, long nch)
/* allocate a double matrix with subscript range m[nrl..nrh][ncl..nch] */
{
	long i, nrow=nrh-nrl+1,ncol=nch-ncl+1;
	double **m;

	/* allocate pointers to rows */
	m=(double **) malloc((size_t)((nrow+NR_END)*sizeof(double*)));
	if (!m) nrerror("allocation failure 1 in matrix()");
	m += NR_END;
	m -= nrl;

	/* allocate rows and set pointers to them */
	m[nrl]=(double *) malloc((size_t)((nrow*ncol+NR_END)*sizeof(double)));
	if (!m[nrl]) nrerror("allocation failure 2 in matrix()");
	m[nrl] += NR_END;
	m[nrl] -= ncl;

	for(i=nrl+1;i<=nrh;i++) m[i]=m[i-1]+ncol;

	/* return pointer to array of pointers to rows */
	return m;
}

int **nr_imatrix(long nrl, long nrh, long ncl, long nch)
/* allocate a int matrix with subscript range m[nrl..nrh][ncl..nch] */
{
	long i, nrow=nrh-nrl+1,ncol=nch-ncl+1;
	int **m;

	/* allocate pointers to rows */
	m=(int **) malloc((size_t)((nrow+NR_END)*sizeof(int*)));
	if (!m) nrerror("allocation failure 1 in matrix()");
	m += NR_END;
	m -= nrl;


	/* allocate rows and set pointers to them */
	m[nrl]=(int *) malloc((size_t)((nrow*ncol+NR_END)*sizeof(int)));
	if (!m[nrl]) nrerror("allocation failure 2 in matrix()");
	m[nrl] += NR_END;
	m[nrl] -= ncl;

	for(i=nrl+1;i<=nrh;i++) m[i]=m[i-1]+ncol;

	/* return pointer to array of pointers to rows */
	return m;
}

Real **nr_submatrix(Real **a, long oldrl, long oldrh, long oldcl, long oldch,
	long newrl, long newcl)
/* point a submatrix [newrl..][newcl..] to a[oldrl..oldrh][oldcl..oldch] */
{
	long i,j,nrow=oldrh-oldrl+1,ncol=oldcl-newcl;
	Real **m;

	/* allocate array of pointers to rows */
	m=(Real **) malloc((size_t) ((nrow+NR_END)*sizeof(Real*)));
	if (!m) nrerror("allocation failure in submatrix()");
	m += NR_END;
	m -= newrl;

	/* set pointers to rows */
	for(i=oldrl,j=newrl;i<=oldrh;i++,j++) m[j]=a[i]+ncol;

	/* return pointer to array of pointers to rows */
	return m;
}

Real **nr_convert_matrix(Real *a, long nrl, long nrh, long ncl, long nch)
/* allocate a Real matrix m[nrl..nrh][ncl..nch] that points to the matrix
declared in the standard C manner as a[nrow][ncol], where nrow=nrh-nrl+1
and ncol=nch-ncl+1. The routine should be called with the address
&a[0][0] as the first argument. */
{
	long i,j,nrow=nrh-nrl+1,ncol=nch-ncl+1;
	Real **m;

	/* allocate pointers to rows */
	m=(Real **) malloc((size_t) ((nrow+NR_END)*sizeof(Real*)));
	if (!m) nrerror("allocation failure in convert_matrix()");
	m += NR_END;
	m -= nrl;

	/* set pointers to rows */
	m[nrl]=a-ncl;
	for(i=1,j=nrl+1;i<nrow;i++,j++) m[j]=m[j-1]+ncol;
	/* return pointer to array of pointers to rows */
	return m;
}

Real ***nr_f3tensor(long nrl, long nrh, long ncl, long nch, long ndl, long ndh)
/* allocate a Real 3tensor with range t[nrl..nrh][ncl..nch][ndl..ndh] */
{
	long i,j,nrow=nrh-nrl+1,ncol=nch-ncl+1,ndep=ndh-ndl+1;
	Real ***t;

	/* allocate pointers to pointers to rows */
	t=(Real ***) malloc((size_t)((nrow+NR_END)*sizeof(Real**)));
	if (!t) nrerror("allocation failure 1 in f3tensor()");
	t += NR_END;
	t -= nrl;

	/* allocate pointers to rows and set pointers to them */
	t[nrl]=(Real **) malloc((size_t)((nrow*ncol+NR_END)*sizeof(Real*)));
	if (!t[nrl]) nrerror("allocation failure 2 in f3tensor()");
	t[nrl] += NR_END;
	t[nrl] -= ncl;

	/* allocate rows and set pointers to them */
	t[nrl][ncl]=(Real *) malloc((size_t)((nrow*ncol*ndep+NR_END)*sizeof(Real)));
	if (!t[nrl][ncl]) nrerror("allocation failure 3 in f3tensor()");
	t[nrl][ncl] += NR_END;
	t[nrl][ncl] -= ndl;

	for(j=ncl+1;j<=nch;j++) t[nrl][j]=t[nrl][j-1]+ndep;
	for(i=nrl+1;i<=nrh;i++) {
		t[i]=t[i-1]+ncol;
		t[i][ncl]=t[i-1][ncl]+ncol*ndep;
		for(j=ncl+1;j<=nch;j++) t[i][j]=t[i][j-1]+ndep;
	}

	/* return pointer to array of pointers to rows */
	return t;
}

void free_nr_vector(Real *v, long nl, long nh)
/* free a Real vector allocated with vector() */
{
	free((FREE_ARG) (v+nl-NR_END));
}

void free_nr_ivector(int *v, long nl, long nh)
/* free an int vector allocated with ivector() */
{
	free((FREE_ARG) (v+nl-NR_END));
}

void free_nr_cvector(unsigned char *v, long nl, long nh)
/* free an unsigned char vector allocated with cvector() */
{
	free((FREE_ARG) (v+nl-NR_END));
}

void free_nr_lvector(unsigned long *v, long nl, long nh)
/* free an unsigned long vector allocated with lvector() */
{
	free((FREE_ARG) (v+nl-NR_END));
}

void free_nr_dvector(double *v, long nl, long nh)
/* free a double vector allocated with dvector() */
{
	free((FREE_ARG) (v+nl-NR_END));
}

void free_nr_matrix(Real **m, long nrl, long nrh, long ncl, long nch)
/* free a Real matrix allocated by matrix() */
{
	free((FREE_ARG) (m[nrl]+ncl-NR_END));
	free((FREE_ARG) (m+nrl-NR_END));
}

void free_nr_dmatrix(double **m, long nrl, long nrh, long ncl, long nch)
/* free a double matrix allocated by dmatrix() */
{
	free((FREE_ARG) (m[nrl]+ncl-NR_END));
	free((FREE_ARG) (m+nrl-NR_END));
}

void free_nr_imatrix(int **m, long nrl, long nrh, long ncl, long nch)
/* free an int matrix allocated by imatrix() */
{
	free((FREE_ARG) (m[nrl]+ncl-NR_END));
	free((FREE_ARG) (m+nrl-NR_END));
}

void free_nr_submatrix(Real **b, long nrl, long nrh, long ncl, long nch)
/* free a submatrix allocated by submatrix() */
{
	free((FREE_ARG) (b+nrl-NR_END));
}

void free_nr_convert_matrix(Real **b, long nrl, long nrh, long ncl, long nch)
/* free a matrix allocated by convert_matrix() */
{
	free((FREE_ARG) (b+nrl-NR_END));
}

void free_nr_f3tensor(Real ***t, long nrl, long nrh, long ncl, long nch,
	long ndl, long ndh)
/* free a Real f3tensor allocated by f3tensor() */
{
	free((FREE_ARG) (t[nrl][ncl]+ndl-NR_END));
	free((FREE_ARG) (t[nrl]+ncl-NR_END));
	free((FREE_ARG) (t+nrl-NR_END));
}
