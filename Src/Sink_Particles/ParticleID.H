#ifndef ParticleID_H_
#define ParticleID_H_

#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <fstream>

/* This is a helper class for ParticleList. It provides particle
   unique identifiers. */
class ParticleID {

public:

  /* The constructors */
  ParticleID(int proc, long n);
  ParticleID();
  ParticleID(ParticleID& source);
  ParticleID(const ParticleID& source);

  /* The destructor */
  ~ParticleID();

  /* Return particle ID */
  long partNum();
  
  /* Return processor responsible for creating particle */
  int partProc();

  /* Set particle ID */
  void setPartNum(long n);


  /* Assignment operator */
  ParticleID &operator=(const ParticleID& rhs);

  /* Comparison operators */
  bool operator==(const ParticleID& rhs);
  bool operator>=(const ParticleID& rhs);
  bool operator<=(const ParticleID& rhs);
  bool operator>(const ParticleID& rhs);
  bool operator<(const ParticleID& rhs);

private: /* The data */
  int createProc;
  long num;
};
#endif
