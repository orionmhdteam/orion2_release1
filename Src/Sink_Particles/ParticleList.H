#ifndef ParticleList_H_
#define ParticleList_H_

#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <fstream>
#include <vector>

#include <ParticleID.H>

/* This is a bare-bones holder for particles. It provides methods for
   adding and removing particles, for IO, for synchronizing particles
   between processors, and for accessing the particles. It is a template
   class; the template argument is the class that defines a single
   particle. The template argument P must provide the following methods:

   friend ostream &operator<<(ostream &os, const P& partdata);
   friend istream &operator>>(istream &is, P& partdata);
   P &operator=(const P& rhs)
   Real *position()
*/

// Need declaration here to define Amr in the scope of this file. We fully
// include Amr.H in ParticleList.cpp.
class AMR; 

template <class P>
class ParticleList {

public:

  /* The constructor: it just creates an empty Particle object with
     no particles. */
  ParticleList(AMR* Parent);
  ParticleList();
  
  /* The destructor: it deletes all particles. */
  virtual ~ParticleList();

  /* Method to add a particle */
  void addParticle(P &newpart);

  /* Method to delete a particle. */
  void deleteParticle(int num);

  /* Method to return the number of particles */
  int numParticles();

  /* Method to return a particle */
  P &operator[](int n);

  /* Method to write particles as part of a plot or check file. */
  void write(const std::string& dir, const std::string& filename);

  /* Method to read particles from a check file as part of a restart. */
  void read(const std::string& dir, const std::string& filename);

  /* Method to synchronize the particle lists between processors -- this
     method makes sure all processors know about all particles */
  void syncParticleList();

#ifdef BACKUP
  /* Backup methods */
  void saveBackup();
  void restoreBackup();
#endif

protected:

  // The data
  AMR* parent;          /* Parent AMR object */
  vector<ParticleID> id; /* Particle identifier list */
  vector<P> particles;   /* Particle list */
  long ncreated;         /* Number of particles created on this processor */
  long ncreated_old;     /* Number of particles created before last sync, 
			    for use with SINKID */
#ifdef BACKUP
  /* Backup copies of data */
  vector<ParticleID> id_backup;
  vector<P> particles_backup;
  long ncreated_backup;
#endif

  /* Method to add a particle with a specified id */
  void addParticle(P &newpart, ParticleID &newid);


private:
  
  /* Method to sort lists of ids, particles, and priorities */
  void quickSort(int n, vector<ParticleID> &id,
		 vector<P> &part, int priority[]);
};
#endif
