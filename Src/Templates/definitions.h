#define    PHYSICS   MHD
#define    DIMENSIONS	2
#define    EXTRA_DIM   NO
#define    GEOMETRY   CARTESIAN
#define    INCLUDE_GRAVITY   NO
#define    PRESSURE_FIX   NO
#define    DENSITY_FIX	 NO
#define    TEMPERATURE_FIX   NO
#define    N_TRACER   0
#define    USER_DEF_PARAMETERS	 1
#define    NGHOST 4
#define    NMAX   1500
#define    EXTRA_VAR	9
#define    DIV_B   NONE
#define    INCLUDE_BACKGROUND_FIELD   NO

 /* Define pointers to user-def parameters */ 

#define  MACH	0
