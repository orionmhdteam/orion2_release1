#include "StarParticleList.H"

#include <AMR.H>
#include <DataIterator.H>
#include <MayDay.H>
#include "ParmParse.H"
#include "SINKPARTICLE_F.H"
#include "STARPARTICLE_F.H"

#include <AMRLevelOrion.H>
//      AMRLevelOrion.H 
//      includes LevelOrion.H 
//      which includes PatchOrion.H
//      which includes PatchGrid.H
//      which includes orion2.h
//      which defines DOM_XBEG
//      which is needed to determine spatial positions of boxes

// Initialize static members
template <class S> int StarParticleList<S>::use_wind = 0;
template <class S> int StarParticleList<S>::use_isowind = 0;
template <class S> int StarParticleList<S>::tracer = -1;
template <class S> int StarParticleList<S>::wind_tracer = -1;
template <class S> int StarParticleList<S>::isowind_tracer = -1;
template <class S> Real StarParticleList<S>::vw_fkep = 0.3;
template <class S> Real StarParticleList<S>::vw_max = 4.e8;
// Wind model opening angle; set to old THETA0 hardwired default
//0.01 from Matzner & McKee 1999
template <class S> Real StarParticleList<S>::wind_theta = 0.01;


template <class S>
StarParticleList<S>::StarParticleList(AMR* parent) :
  SinkParticleList<S>(parent)
{
  if(UNIT_DENSITY!=1.0 && UNIT_VELOCITY!=1.0 && UNIT_LENGTH!=1.0) {
    cout << "Error:  The stellar evolution model only works in cgs units.  Have a nice day." << endl;
    MayDay::Abort();
  }
  // Read input parameters
  {
    ParmParse pp("radiation");
    pp.query("tracer", tracer); 
  }
  {
    ParmParse pp("star");
    pp.query("wind_tracer", wind_tracer);
    pp.query("use_wind", use_wind); //Note for collimated outflows
    pp.query("isowind_tracer", isowind_tracer); //For isotropic winds
    pp.query("use_isowind", use_isowind);
    pp.query("vw_fkep", vw_fkep);
    pp.query("vw_max", vw_max);
    pp.query("wind_theta", wind_theta);
  }
  {
  }
  if((wind_tracer > 0 && wind_tracer == tracer) || (isowind_tracer > 0 && isowind_tracer == tracer))
    MayDay::Abort("Error: cannot use the same tracer slot for winds and dust.");
  //this->windrad = 8; // for now hardcode this parameter
  this->windrad = 16; //For isowinds
}

template <class S>
StarParticleList<S>::~StarParticleList() {}

// overload estTimeStep to account for velocity of wind ejected
template <class S>
Real
StarParticleList<S>::estTimeStep(int ilevel)
{
  Real dt = this->SinkParticleList<S>::estTimeStep(ilevel);

  Vector<AMRLevel*> amrLevels = this->parent->getAMRLevels();
  AMRLevelOrion& level = *static_cast<AMRLevelOrion*>(amrLevels[ilevel]);
  Vector<Real> dx(CH_SPACEDIM, level.getDx());

  Real dxmin=dx[0];
  for (int i=1; i<CH_SPACEDIM; i++) dxmin=std::min(dxmin, dx[i]);
  
  for (int i=0; i<this->numParticles(); i++) {
    double vWind = this->particles[i].vWind(vw_fkep, vw_max, 6.*dx[0]);
    if(use_wind > 0) dt = std::min(dt,dxmin/std::max(vWind,1.e-300));
    double vIsoWind = this->particles[i].iso_vWind(vw_max);
    if (use_isowind > 0) dt = std::min(dt, dxmin/std::max(vIsoWind, 1e-300));
  }
  return dt;
}

template <class S>
void
StarParticleList<S>::starRadiation(LevelData<FArrayBox>& starrad, int lev) {

  // Set up useful shorthands
  int npart = this->numParticles();
  if (npart==0) return;
  
  // AJC - print the luminosity of the two most luminous sources
  Real lum1 = -1., lum2 = -1.;
  for (int j=0; j<npart; j++) {
    /*PS: Add netlum for luminosity computation */
    Real tnetlum = this->particles[j].netlum;
    Real lum;
    lum = this->particles[j].luminosity(tnetlum);
    if (lum < 0.0) lum = 0.0;

    if(lum >= lum1) {
      lum2 = lum1;
      lum1 = lum;
    }
    else if (lum >= lum2) {
      lum2 = lum;
    }
  }

  if(lum1 > 0.) pout() << "  1st Brightest Star, L =" << lum1/3.839e33 << endl;
  if(lum2 > 0.) pout() << "  2nd Brightest Star, L =" << lum2/3.839e33 << endl;

  // Initialize the array
  DataIterator dit = starrad.dataIterator();
  for (dit.reset(); dit.ok(); ++dit) starrad[dit()].setVal(0.);

  // Grab the data on this level
  Vector<AMRLevel*> amrLevels = this->parent->getAMRLevels();
  int maxlevel = amrLevels.size()-1;
  AMRLevelOrion& level = *static_cast<AMRLevelOrion*>(amrLevels[lev]);
  Vector<Real> dx(CH_SPACEDIM, level.getDx());
  const DisjointBoxLayout& grids = starrad.getBoxes();

  // AJC get the coarsening ratio from the finest level
  int ref = 1;
  for (int level=lev; level<maxlevel; level++) {
    AMRLevelOrion& lev_ref = *static_cast<AMRLevelOrion*>(amrLevels[lev]);
    ref *= lev_ref.refRatio();
  }
  int coarse_accrad = (int)ceil((Real)this->accrad/(Real)ref);
  //int coarse_accrad = this->accrad;

  // Loop through grids of the multifab
  dit = starrad.dataIterator();
  for (dit.reset(); dit.ok(); ++dit) {
    DataIndex i = dit();

    const int* rlo = starrad[i].loVect();
    const int* rhi = starrad[i].hiVect();

    // Loop over particles
    for (int j=0; j<npart; j++) {
      // Get the particle's location in the grid, and define a box
      // grown by accrad cells around it
      IntVect partidx;
      for (int k=0; k<CH_SPACEDIM; k++)
	partidx[k] = (this->particles[j].pos[k]-DOM_XBEG[k])/dx[k];
      Box partBox(partidx, partidx);
      for (int k=0; k<CH_SPACEDIM; k++) {
	partBox.setBig(k,partidx[k]+2*(coarse_accrad));
	partBox.setSmall(k,partidx[k]-2*(coarse_accrad));
      }

      IntVect sE, bE;
      sE = max(starrad[i].smallEnd(), partBox.smallEnd());
      bE = min(starrad[i].bigEnd(),   partBox.bigEnd());
      int nullOverlap = 0;
      for(int n=0; n<CH_SPACEDIM; n++) nullOverlap += sE[n] > bE[n];

      // Call fortran radiation routine if the particle box intersects
      // the grid box
      if (!nullOverlap) {
	/*PS: Include ionization energy balance*/
        Real tnetlum = this->particles[j].netlum;
	Real lum = this->particles[j].luminosity(tnetlum); // Get luminosity

	if (lum < 0.0) {
	  this->particles[j].netlum = lum;
	  lum = 0.0;
	}
	else {
	  this->particles[j].netlum = 0.0;
	}
	//if ((lum > 0.0) && (starrad[i].box().contains(partidx)))
		
	// Grab problem domain
	const Box domBox=level.problemDomain().domainBox();
	const int *domidxlo=domBox.loVect();
	int domidxhi[CH_SPACEDIM];
	for (int dir=0; dir<CH_SPACEDIM; dir++) domidxhi[dir] = domBox.hiVect()[dir]+1;

	//eliminates luminosity near the boundaries for stability.
	//CEH particles that are within radboundarybuffer cells of the domain
	//boundary do not radiate.  radboundarybuffer=0 prevents the effect
	int radboundarybuffer = 0;
	for (int k=0; k<CH_SPACEDIM; k++) {
	  if (this->particles[j].pos[k]/dx[k]-domidxlo[k] < radboundarybuffer) { 
	    lum = 0.0; 
	    pout() << "star particle " << j << " too close to lower " 
		   << k << " boundary" << std::endl;
	  }
	  if (domidxhi[k]-this->particles[j].pos[k]/dx[k] < radboundarybuffer) {
	    lum = 0.0;
	    pout() << "star particle " << j << " too close to upper " 
		      << k << " boundary" << std::endl;
	    pout() << "---" << domidxhi[k] << " " << this->particles[j].pos[k]/dx[k] << std::endl;
	  }
	}
	  FORT_STARRAD_EXTENDED(ARLIM(rlo), ARLIM(rhi),
			      starrad[i].dataPtr(), DOM_XBEG, &dx[0],
			      this->particles[j].pos, &lum, 
			      &coarse_accrad);
      }	
    }
  }
}

#ifdef MGRD
template <class S>
void
StarParticleList<S>::starRadiationMG(MultiFab& starrad, int lev, 
				     int ngroups, Array <Real> xnu,
				     Real dt ) {

  // Initialize the array
  starrad.setVal(0.0);

  // Set up useful shorthands
  int npart = numParticles();
  if (npart==0) return;

  //SSRO distribute energy over more cells than accretion radius
  int lumrad = 2*(this->accrad);

  // Grab the data on this level
  Array<RealBox> grid_loc = parent->getLevel(lev).gridLocations();
  const Real *dx = parent->getLevel(lev).Geom().CellSize();
  const Real *domlo = parent->Geom(lev).ProbLo();

  // Loop through grids of the multifab
  for (MFIter mfi(starrad); mfi.isValid(); ++mfi) {
    int i = mfi.index();
    //SSRO Store total energy in rad0
    const int* rlo = starrad[i].loVect();
    const int* rhi = starrad[i].hiVect();
      
      // Loop over particles
    for (int j=0; j<npart; j++) {
      
      
      // Get the particle's location in the grid, and define a box
      // grown by accrad cells around it
      IntVect partidx;
      for (int k=0; k<CH_SPACEDIM; k++)
	partidx[k] = (particles[j].pos[k]-domlo[k])/dx[k];
      Box partBox(partidx, partidx);
      for (int k=0; k<CH_SPACEDIM; k++) {
	partBox.setBig(k,partidx[k]+lumrad);
	partBox.setSmall(k,partidx[k]-lumrad);
      }
	
      // Call fortran radiation routine if the particle box intersects
      // the grid box
      if (partBox.intersects(starrad[i].box())) {
        /*PS: Include ionization energy balance.  If luminosity is negative
              it is stored in the sink file for restart.  If positive, it 
              is set to 0 in the sink file.*/
        Real tnetlum = this->particles[j].netlum();
	Real lum = particles[j].luminosity(tnetlum); // Get luminosity
	if (lum < 0.0) {
	  this->particles[j].netlum = lum;
	  lum = 0.0;
	}
	else {
	  this->particles[j].netlum = 0.0;
	}

	Real radius = particles[j].r;
	Real lstar = particles[j].lStar();
	Real ldisk = particles[j].lDisk();
	  	  
	if ((lum > 0.0) ){
	  lstar = lstar*(particles[j].l_hist/lum);
	  ldisk = ldisk*(particles[j].l_hist/lum);
	  lum = particles[j].l_hist;
     
	  pout() << "Star radiating L = " << lum/3.9e33 << std::endl;
	  //	  pout() << "lstar/ldisk = " << lstar/3.9e33 << " "<<ldisk/3.9e33;

	  Array<Real> ergynu;
	  ergynu.resize(ngroups, 0.0);
	  
	  Real energy = lum * dt/(dx[0]*dx[1]*dx[2]);
	  Real estar = lstar * dt/(dx[0]*dx[1]*dx[2]);
	  Real edisk = ldisk * dt/(dx[0]*dx[1]*dx[2]);
	  
	  // For lum, group structure is constant, so get T, E/vol=aT^4.
	  // routine to determine xnu_frac(T)=E_nu(T)/E_tot(T)
	  // This only requires, xnu, energy. 
	  // outputs: ergynu, the energy per group
	  {
	  FORT_MG_LUM_STAR(&energy, &estar, &lstar, &radius, &ngroups, 
			   xnu.dataPtr(), ergynu.dataPtr());
	  
	  FORT_MG_LUM_DISK(&energy, &edisk, &ngroups, xnu.dataPtr(), 
			   ergynu.dataPtr());
	  
	  // Modified STARRAD_EXTENDED to accept multigroup rad structure.
	  FORT_STARRAD_EXTENDED_MG(ARLIM(rlo), ARLIM(rhi),
				   starrad[i].dataPtr(), grid_loc[i].lo(), &dx[0],
				   this->particles[j].pos, partidx.getVect(), 
				   &energy, 
				   &lumrad, &ngroups, ergynu.dataPtr());
	  }
	}
      }	
    }
  }
}
#endif

template <class S>
void
StarParticleList<S>::starUpdate(Real dt) {
  for (int i=0; i<this->numParticles(); i++) this->particles[i].updateState(dt);
}

#define YEAR  3.15e7
template <class S>
void
StarParticleList<S>::starWind(Real dt) {

  if (!use_wind && !use_isowind)
    return;

  // Set up useful shorthands
  int npart = this->numParticles();
  if (npart==0) return;
  int nbox = this->windrad;

  // Only do this on the finest level
  Vector<AMRLevel*> amrLevels = this->parent->getAMRLevels();
  int maxlevel = amrLevels.size()-1;
  AMRLevelOrion& level = *static_cast<AMRLevelOrion*>(amrLevels[maxlevel]);
  Vector<Real> dx(CH_SPACEDIM, level.getDx());
  LevelData<FArrayBox>& state = level.getStateNew();
  const DisjointBoxLayout& grids = state.getBoxes();
  Real vWind = 0.0;
  Real dm = 0.0;
  Real vWindIso = 0.0;
  Real dmIso = 0.0;
  Real Twind = 0.0;
  Real dm_min = 0.0;
  // Loop over particles
  for (int j=0; j<npart; j++) {
    // Grab the wind speed. Skip uninitialized particles, 
    // which have vWind = 0.
#define FWIND 0.21
    if (use_wind){
      vWind = this->particles[j].vWind(vw_fkep, vw_max, 6.*dx[0]);
      // Since winds are called after updatestate, mdot_hist[0] is the accretion rate
      // from THIS time step.  Recall that this->particles[j].mdot is smoothed over 
      // NHIST time steps.  We've decided that we want winds to not be smoothed,
      // but we do want this for radiative feedback.
      dm = (this->particles[j].m-this->particles[j].mlast) * FWIND;
      //Wind temperature = Teff (star temperature) if Teff<1e4 K (i.e, assume
      //wind is not fully ionized unless stellar radiation is.
      //The wind is likely launched from the disk, but it's not necessarily 
      //fully ionized (this is seen with winds from low-mass stars at least)
      //ALR 12/17, Twind collimated for outflows
      Twind = std::pow(10, this->particles[j].get_logTeff());
      if (Twind > 1e4)
	Twind = 1e4;
    }//endif use_wind
    if (use_isowind){
      vWindIso =  this->particles[j].iso_vWind(vw_max);
      dmIso = this->particles[j].iso_mloss() * dt;
    }

    if (vWind==0.0 && vWindIso==0.0) continue;

    // keep particle speed constant
    Real part_vel[3];
    for (int i=0; i<3; i++) part_vel[i]=this->particles[j].mom[i]/this->particles[j].m;

    // Get mass ejected into wind, and modify star's mass appropriately
    //ALR - only adjust masses and mdots if self gravity (and therefore the protostellar model are TURNED on
    //If not we will keep the stellar properties constant
    //#ifdef GRAVITY
    if (use_wind) {
      this->particles[j].m -= dm;
      this->particles[j].mlast -= dm;
      this->particles[j].mdeut -= dm;
      this->particles[j].mdot *= (1.0 - FWIND);
    }
    if (use_isowind) {
      this->particles[j].m -= dmIso;
      this->particles[j].mlast -= dmIso;
      this->particles[j].mdeut -= dmIso;
      this->particles[j].mdot -= dmIso/dt;
    }
    //#endif
    // keep particle speed constant
    for (int i=0; i<3; i++) this->particles[j].mom[i]=part_vel[i]*this->particles[j].m;
    if (1) {
      if (use_wind) {
	pout() << "Injecting wind, mdot = " << this->particles[j].mdot << ", dm = "
	       << dm << ", dt = " << dt << ", m_new = " << this->particles[j].m 
	       << ", vwind = " << vWind << ", Twind = " << Twind << std::endl;
      }
      if (use_isowind) {
	pout() << "Injecting isotropic wind, mdot = " << this->particles[j].mdot << ", dm = "
	       << dmIso << ", dt = " << dt << ", m_new = " << this->particles[j].m
	       << ", vwind = " << vWindIso << std::endl;
      }
    }
    // Loop through grids of the multifab
    DataIterator dit = state.dataIterator();
    for (dit.reset(); dit.ok(); ++dit) {
      DataIndex i = dit();

      const int* slo = state[i].loVect();
      const int* shi = state[i].hiVect();

      // Get the particle's location in the grid, and define a box
      // grown by ngrow cells around it: this box is the feeding region
      IntVect partidx;
      for (int k=0; k<CH_SPACEDIM; k++)
	partidx[k] = (this->particles[j].pos[k]-DOM_XBEG[k])/dx[k];
      Box partBox(partidx, partidx);
      for (int k=0; k<CH_SPACEDIM; k++) {
	partBox.setBig(k,partidx[k]+nbox);
	partBox.setSmall(k,partidx[k]-nbox);
      }

      IntVect sE, bE;
      sE = max(grids[i].smallEnd(), partBox.smallEnd());
      bE = min(grids[i].bigEnd(),   partBox.bigEnd());
      int nullOverlap = 0;
      for(int n=0; n<CH_SPACEDIM; n++) nullOverlap += sE[n] > bE[n];

      // If the accretion box intersects this grid, modify the state
      // data within it
      if (use_wind) {
	//if (!use_isowind){
	if (!nullOverlap) {
	  int ncomp = state.nComp();
	  FORT_STAR_WIND_INJECT(ARLIM(slo), ARLIM(shi), &ncomp,
				state[i].dataPtr(), &nbox, &(this->accrad), DOM_XBEG,
				&dx[0], this->particles[j].pos, part_vel,
				this->particles[j].angmom, &vWind, &dm, &tracer, &wind_tracer, 
				&isowind_tracer, &wind_theta, &Twind); //ALR added Twind 12/17
	}
      }//endif use_wind
      if (use_isowind) {
	if (!nullOverlap) {
	  int ncomp = state.nComp();
	  FORT_STAR_ISOWIND_INJECT(ARLIM(slo), ARLIM(shi), &ncomp,
				state[i].dataPtr(), &nbox, &(this->accrad), DOM_XBEG,
				&dx[0], this->particles[j].pos, part_vel,
				this->particles[j].angmom, &vWindIso, &dmIso, &tracer, 
				&wind_tracer, &isowind_tracer, &wind_theta);
	  
	}
      }//endif use_isowind
    }
  }
}

//////////////////////////////////////////////
// Template explicit instantiations
//////////////////////////////////////////////

#ifdef CH_AIX
// On IBM system, use the compiler's pragma directive because the
// compiler doesn't support the ISO standard
#  ifdef STARPARTICLE
#    include "StarParticleData.H"
#      pragma define(StarParticleList<StarParticleData>);
#  endif
#else
// On linux systems, use ISO standard explicit instantiation, which is
// supported by compiler version 8.0 (and possibly earlier)
#  ifdef STARPARTICLE
#    include <StarParticleData.H>
     template class StarParticleList<StarParticleData>;
#  endif
#endif
