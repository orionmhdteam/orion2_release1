      INTEGER DN, MX, MY, MZ, VX, VY, VZ, BX, BY, BZ, PR, EN, 
     +     TR, NTRACER
      COMMON /STATE_MAP/ 
     +     DN, MX, MY, MZ, VX, VY, VZ, BX, BY, BZ, PR, EN, 
     +     TR, NTRACER

      REAL*8 CONST_PI, CONST_amu, CONST_mp, CONST_mn, CONST_me, 
     +     CONST_mH, CONST_kB, CONST_sigma, CONST_sigmaT, CONST_NA, 
     +     CONST_c, CONST_Msun, CONST_Rsun, CONST_Mearth, CONST_Rearth, 
     +     CONST_G, CONST_h, CONST_pc, CONST_ly, CONST_au, CONST_eV,
     +     gmm, c_iso
      COMMON /PHYSICAL_CONSTANTS/
     +     CONST_PI, CONST_amu, CONST_mp, CONST_mn, CONST_me, 
     +     CONST_mH, CONST_kB, CONST_sigma, CONST_sigmaT, CONST_NA, 
     +     CONST_c, CONST_Msun, CONST_Rsun, CONST_Mearth, CONST_Rearth, 
     +     CONST_G, CONST_h, CONST_pc, CONST_ly, CONST_au, CONST_eV,
     +     gmm, c_iso

      REAL*8 UNIT_DENSITY, UNIT_VELOCITY, UNIT_LENGTH
      COMMON /UNITS/
     +     UNIT_DENSITY, UNIT_VELOCITY, UNIT_LENGTH

      REAL*8 SMALL_DN, SMALL_PR, T_CUT_COOL, FLOOR_TRAD, 
     +     FLOOR_TGAS, CEIL_VA, CEIL_TGAS
      COMMON /FLOORS/
     +     SMALL_DN, SMALL_PR, T_CUT_COOL, FLOOR_TRAD, 
     +     FLOOR_TGAS, CEIL_VA, CEIL_TGAS
