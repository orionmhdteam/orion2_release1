#include "orion2.h"

#define NOPT      32              /*  # of possible options in a menu */
#define NLEN      128              /*  # default string length         */

#define COMPARE(s1,s2,ii) \
        for ( (ii) = 1 ; (ii) < NOPT && !(strcmp ( (s1), (s2)) == 0); (ii)++);

/* ****************************************************************** */
int SETUP (Input *input, char *ini_file)
/* 
 *
 *
 *    Set numerical parameters for the rest of integration :
 *
 *   - Rectangular domain defition
 *   - Time stepping parameters 
 *   - output frequency
 *   - Boundary
 *   - Output data
 *
 ********************************************************************* */
{
  int    idim, ip, ipos, itype, nlines;
  char   *bound_opt[NOPT], str_var[128], *str;
  char *glabel[]     = {"X1-grid", "X2-grid","X3-grid"};
  char *bbeg_label[] = {"X1-beg", "X2-beg","X3-beg"};
  char *bend_label[] = {"X1-end", "X2-end","X3-end"};
  double dbl_var;
  Output *output;
  FILE *fp;

  print1 ("> Reading %s (SETUP) ...\n\n", ini_file);

  for (itype = 0; itype < NOPT; itype++) {
    bound_opt[itype] = "0000";
  }

/*  ---------------------------------------------------
      available options are given as two set of names;
      This facilitates when updating the code and
      people are too lazy to read the manual !
    --------------------------------------------------- */

  bound_opt[OUTFLOW]      = "outflow";
  bound_opt[REFLECTIVE]   = "reflective";
  bound_opt[AXISYMMETRIC] = "axisymmetric";
  bound_opt[EQTSYMMETRIC] = "eqtsymmetric";
  bound_opt[PERIODIC]     = "periodic";
  bound_opt[USERDEF]      = "userdef";

  input->log_freq = 1; /* -- default -- */
 
  nlines = PAR_OPEN (ini_file);

/* ------------------------------------------------------------
                        [Grid] Section 
   ------------------------------------------------------------ */

  for (idim = 0; idim < 3; idim++){
    input->npatch[idim] = atoi(PAR_GET(glabel[idim], 1));
    input->npoint[idim] = 0;

    ipos = 1;
    for (ip = 1; ip <= input->npatch[idim]; ip++) {

      input->patch_left_node[idim][ip] = atof(PAR_GET(glabel[idim], ++ipos));
      input->patch_npoint[idim][ip]    = atoi(PAR_GET(glabel[idim], ++ipos));
      input->npoint[idim]             += input->patch_npoint[idim][ip];
      input->grid_is_uniform[idim]     = 0;

      strcpy (str_var, PAR_GET(glabel[idim], ++ipos)); 
/*
printf ("%f  %d %s\n",input->patch_left_node[idim][ip],input->patch_npoint[idim][ip],str_var);
*/
      if (strcmp(str_var,"u") == 0 || strcmp(str_var,"uniform") == 0) {
        input->patch_type[idim][ip] = UNIFORM_GRID;
        if (input->npatch[idim] == 1) input->grid_is_uniform[idim] = 1;        
      }else if (strcmp(str_var,"s") == 0 || strcmp(str_var,"strecthed") == 0) { 
        input->patch_type[idim][ip] = STRETCHED_GRID;
      }else if (strcmp(str_var,"l+") == 0){
        input->patch_type[idim][ip] = LOGARITHMIC_INC_GRID;
      }else if (strcmp(str_var,"l-") == 0){
        input->patch_type[idim][ip] = LOGARITHMIC_DEC_GRID;
      }else{ 
        print("\nYou must specify either 'u', 's', 'l+' or 'l-' as grid-type in %s\n", ini_file);
        QUIT_ORION2(1);
      }
    }
    
    input->patch_left_node[idim][ip] = atof(PAR_GET(glabel[idim], ++ipos));

    if ( (ipos+1) != (input->npatch[idim]*3 + 3)) {
      print (" ! Domain #%d setup is not properly defined \n", idim);
      QUIT_ORION2(1);
    }
    if (idim >= DIMENSIONS && input->npoint[idim] != 1) {
      print ("   ! %d point(s) on dim. %d is NOT valid, resetting to 1\n",
              input->npoint[idim],idim+1);
      input->npoint[idim]          = 1;
      input->npatch[idim]          = 1;
      input->patch_npoint[idim][1] = 1;
    }

  }

/* ------------------------------------------------------------
                     [Time] Section 
   ------------------------------------------------------------ */

  input->cfl         = atof(PAR_GET("CFL", 1));
  input->cfl_max_var = atof(PAR_GET("CFL_max_var", 1));
  input->tstop       = atof(PAR_GET("tstop", 1));
  input->first_dt    = atof(PAR_GET("first_dt", 1));

/* ------------------------------------------------------------
                     [Solver] Section 
   ------------------------------------------------------------ */

  sprintf (input->solv_type,"%s",PAR_GET("Solver",1));

/* ------------------------------------------------------------
                     [Boundary] Section 
   ------------------------------------------------------------ */

  for (idim = 0; idim < 3; idim++){

    str = PAR_GET(bbeg_label[idim], 1);
    COMPARE (str, bound_opt[itype], itype);
    if (itype == NOPT) {
      print (" ! Don't know how to put left boundary '%s'  \n", str);
      QUIT_ORION2(1);
    }
    input->lft_bound_side[idim] = itype;
  }

  for (idim = 0; idim < 3; idim++){

    str = PAR_GET(bend_label[idim], 1);
    COMPARE (str, bound_opt[itype], itype);
    if (itype == NOPT) {
      print (" ! Don't know how to put left boundary '%s'  \n", str);
      QUIT_ORION2(1);
    }
    input->rgt_bound_side[idim] = itype;
  }

/* ------------------------------------------------------------
                     [Output] Section 
   ------------------------------------------------------------ */

  input->user_var = atoi(PAR_GET("uservar", 1));
  for (ip = 0; ip < input->user_var; ip++){

    if ( (str = PAR_GET("uservar", 2 + ip)) != NULL){
      sprintf (input->user_var_name[ip], "%s", str);
    }else{
      print (" ! Missing name after user var name '%s'\n", input->user_var_name[ip-1]);
      QUIT_ORION2(1);
    } 
  }

 /* -- dbl output -- */

  ipos = 0;
  output = input->output + (ipos++);
  output->nfile = 0;
  output->type  = DBL_OUTPUT;
  output->dt = atof(PAR_GET("dbl", 1));
  output->dn = atoi(PAR_GET("dbl", 2));
  sprintf (output->mode,"%s",PAR_GET("dbl",3));
  if (   strcmp(output->mode,"single_file")
      && strcmp(output->mode,"multiple_files")){
     print1 (" ! Expecting 'single_file' or 'multiple_files' in dbl output\n");
     QUIT_ORION2(1);
  }     

 /* -- flt output -- */

  output = input->output + (ipos++);
  output->nfile = 0;
  output->type  = FLT_OUTPUT;
  output->dt = atof(PAR_GET("flt", 1));
  output->dn = atoi(PAR_GET("flt", 2));
  sprintf (output->mode,"%s",PAR_GET("flt",3));
  if (    strcmp(output->mode,"single_file") 
       && strcmp(output->mode,"multiple_files")){
     print1 (" ! Expecting 'single_file' or 'multiple_files' in flt output\n");
     QUIT_ORION2(1);
  }     

 /* -- vtk output -- */

  if (PAR_QUERY ("vtk")){
    output = input->output + (ipos++);
    output->nfile = 0;
    output->type  = VTK_OUTPUT;
    output->dt = atof(PAR_GET("vtk", 1));
    output->dn = atoi(PAR_GET("vtk", 2));
    if (PAR_GET("vtk",3) == NULL){
      print1 (" ! Extra field missing in vtk\n");
      QUIT_ORION2(1);
    }
    sprintf (output->mode,"%s",PAR_GET("vtk",3));
    if (   strcmp(output->mode,"single_file")
        && strcmp(output->mode,"multiple_files")){
       print1 (" ! Expecting 'single_file' or 'multiple_files' in vtk output\n");
       QUIT_ORION2(1);
    }

  }

 /* -- tab output -- */

  if (PAR_QUERY ("tab")){
    output = input->output + (ipos++);
    output->nfile = 0;
    output->type  = TAB_OUTPUT;
    output->dt = atof(PAR_GET("tab", 1));
    output->dn = atoi(PAR_GET("tab", 2));
  }

 /* -- ppm output -- */

  if (PAR_QUERY ("ppm")){
    output = input->output + (ipos++);
    output->nfile = 0;
    output->type  = PPM_OUTPUT;
    output->dt = atof(PAR_GET("ppm", 1));
    output->dn = atoi(PAR_GET("ppm", 2));
  }

 /* -- png output -- */

  if (PAR_QUERY ("png")){
    output = input->output + (ipos++);
    output->nfile = 0;
    output->type  = PNG_OUTPUT;
    output->dt = atof(PAR_GET("png", 1));
    output->dn = atoi(PAR_GET("png", 2));
  }

 /* -- log frequency -- */

  input->log_freq = atoi(PAR_GET("log", 1));
  input->log_freq = dmax(input->log_freq, 1);
  
  while (ipos < MAX_OUTPUT_TYPES){
    output = input->output + ipos;
    output->nfile = 0;
    output->type  = -1;
    output->dt    = -1.0;
    output->dn    = -1.0;
    ipos++;
  }

 /* -- analysis -- */

  if (PAR_QUERY ("analysis")){
    input->anl_dt = atof(PAR_GET("analysis", 1));
    input->anl_dn = atoi(PAR_GET("analysis", 2));
  }else{
    input->anl_dt = -1.0;   /* -- defaults -- */
    input->anl_dn = -1;
  }

/* ------------------------------------------------------------
                   [Parameters] Section 
   ------------------------------------------------------------ */

  fp = fopen(ini_file,"r");
  
/* -- find position at "[Parameters" -- */

  for (ipos = 0; ipos <= nlines; ipos++){ 
    fgets(str_var, 512, fp);
    
    if (strlen(str_var) > 0) {
      str = strtok (str_var,"]");
      if (strcmp(str,"[Parameters") == 0) break;
    }
  }

  fgets(str_var, 512, fp); 
  
  for (ip = 0; ip < USER_DEF_PARAMETERS; ip++){
    fscanf (fp,"%s %lf\n", str_var, &dbl_var);
    input->aux[ip] = dbl_var;
  }
  fclose(fp);

/* --------------------------------------------
            print some output
   -------------------------------------------- */
   
  print1 ("  CFL   : %4.2f\n",input->cfl);
  print1 ("  Solver: %s\n",input->solv_type);
  for (ip = 0; ip < USER_DEF_PARAMETERS; ip++){
    print1 ("  User def par #%d = %10.4e\n",ip,input->aux[ip]);
  }

  return(0);
}

#undef COMPARE
#undef NOPT
#undef NLEN

