#ifdef CH_LANG_CC
/*
*      _______              __
*     / ___/ /  ___  __ _  / /  ___
*    / /__/ _ \/ _ \/  V \/ _ \/ _ \
*    \___/_//_/\___/_/_/_/_.__/\___/
*    Please refer to Copyright.txt, in Chombo's root directory.
*/
#endif

// PSLi, Tue, May 12, 2009

#include <cassert>

#include "DataIterator.H"
#include "MhdF_F.H"
#include "Mhd.H"
#include "LevelMGF_F.H"
#include "AverageF_F.H"

//PS: modified Dan's code
void Mhd::smallEllipticSolve(FArrayBox& phiFab, const FArrayBox& rhsFab, const Real m_dx)
{

  // multigrid parameters
  int numSmoothDown = 2;
  int numSmoothUp = 2;
  int mgRefRatio = 2;
  // figure this is a good number of max iterations
  int max_iterations = 2*phiFab.box().size(0);
  max_iterations = 20;
  Box refBox(IntVect::Zero, (mgRefRatio-1)*IntVect::Unit);

  Real sumRHS = rhsFab.sum(0,1);

  // first define all the temp storage we'll need
  // assume that rhs has no ghost cells
  Box thisBox = rhsFab.box();
  int numCoarserLevels = 0;
  while (thisBox == refine(coarsen(thisBox,mgRefRatio),mgRefRatio)) {
    numCoarserLevels++;
    thisBox.coarsen(mgRefRatio);
  }
  
  // for debugging, turn MG off
  //numCoarserLevels = 0;

  Vector<FArrayBox*> residVect(numCoarserLevels+1, NULL);
  Vector<FArrayBox*> corrVect(numCoarserLevels+1, NULL);
  Vector<FArrayBox*> residCrseVect(numCoarserLevels+1, NULL);
  Vector<Real> dxLev(numCoarserLevels+1);

  thisBox = rhsFab.box();
  Real thisDx = m_dx;
  
  // as we proceed from fine to coarse in MG, go from low->high in index
  for (int lev=0; lev<residVect.size(); lev++) {
    Box thisGrownBox(thisBox);
    thisGrownBox.grow(1);
    residVect[lev] = new FArrayBox(thisBox,1);
    corrVect[lev] = new FArrayBox(thisGrownBox, 1);
    residCrseVect[lev] = new FArrayBox(thisBox,1);
    corrVect[lev]->setVal(0.0);
    dxLev[lev] = thisDx;

    thisBox.coarsen(mgRefRatio);
    thisDx = thisDx * mgRefRatio;
  }

  Real maxResid, initialMaxResid;

  // set Neumann BC's
  FORT_NEUMANNBC(CHF_FRA(phiFab),
		 CHF_BOX(residVect[0]->box()));

  // compute initial residual
  FORT_MHD_RESID(CHF_FRA1((*residVect[0]),0),
		 CHF_FRA1(phiFab,0),
		 CHF_CONST_FRA1(rhsFab,0),
		 CHF_REAL(initialMaxResid),
		 CHF_CONST_REAL(m_dx));

  maxResid = initialMaxResid;
  int num_iter = 0;
  //PS: temporary set MHD_SOLVER_TOL
  Real MHD_SOLVER_TOL = 1.0e-12;

  while ((maxResid > MHD_SOLVER_TOL*initialMaxResid) && (maxResid > 0)
	 && (num_iter < max_iterations)) {
    num_iter++;
    // loop over grids from fine->coarse and smooth
    for (int lev=0; lev<residVect.size(); lev++) {
      // relax on this level
      for (int n=0; n<numSmoothDown; n++) {
	for (int whichpass=0; whichpass<2; whichpass++) {
	  // set Neumann BC's
	  FORT_NEUMANNBC(CHF_FRA((*corrVect[lev])),
			 CHF_BOX(residVect[lev]->box()));

	  FORT_OLDGSRBLEVELLAP(CHF_FRA((*corrVect[lev])),
			       CHF_FRA((*residVect[lev])),
			       CHF_BOX(residVect[lev]->box()),
			       CHF_REAL(dxLev[lev]),
			       CHF_CONST_INT(whichpass));
	}
      }
      
      


      // now average to coarser level
      if (lev<numCoarserLevels) {
	FORT_NEUMANNBC(CHF_FRA((*corrVect[lev])),
		       CHF_BOX(residVect[lev]->box()));

	FORT_MHD_RESID(CHF_FRA1((*residCrseVect[lev]),0),
		       CHF_FRA1((*corrVect[lev]),0),
		       CHF_FRA1((*residVect[lev]),0),
		       CHF_REAL(maxResid),
		       CHF_CONST_REAL(dxLev[lev]));
	

	FORT_AVERAGE(CHF_FRA((*residVect[lev+1])),
		     CHF_CONST_FRA((*residCrseVect[lev])),
		     CHF_BOX(residVect[lev+1]->box()),
		     CHF_CONST_INT(mgRefRatio),
		     CHF_BOX(refBox));

      }
    } // end loop over levels

    // now come back up
    for (int lev=residVect.size()-1; lev>=0; lev--) {
      for (int n=0; n<numSmoothUp; n++) {
	for (int whichpass=0; whichpass<2; whichpass++) {
	  FORT_NEUMANNBC(CHF_FRA((*corrVect[lev])),
			 CHF_BOX(residVect[lev]->box()));

	  FORT_OLDGSRBLEVELLAP(CHF_FRA((*corrVect[lev])),
			       CHF_FRA((*residVect[lev])),
			       CHF_BOX(residVect[lev]->box()),
			       CHF_REAL(dxLev[lev]),
			       CHF_CONST_INT(whichpass));
	}
      }
      
      // now interpolate correction back up
      if (lev>0) {
	FORT_INTERPMG(CHF_FRA((*corrVect[lev-1])),
		      CHF_FRA((*corrVect[lev])),
		      CHF_BOX(residVect[lev]->box()),
		      CHF_CONST_INT(mgRefRatio),
		      CHF_BOX(refBox));
      }

    } // end loop over levels back up
    

    FORT_NEUMANNBC(CHF_FRA((*corrVect[0])),
		   CHF_BOX(residVect[0]->box()));

    // compute new residual on finest grid
    FORT_MHD_RESID(CHF_FRA1((*residCrseVect[0]),0),
		   CHF_FRA1((*corrVect[0]),0),
		   CHF_FRA1((*residVect[0]),0),
		   CHF_REAL(maxResid),
		   CHF_CONST_REAL(dxLev[0]));
    
  } // end MG outer iteration
  
  // debugging check
  //  cout << "Max Resid = " << maxResid << endl;
  //  if (maxResid > 1.0e-10) cout << "Max Resid = " << maxResid << endl;

  // now increment phi with correction
  phiFab += (*corrVect[0]);

  // that should be it!
  
  // deallocate storage
  for (int lev=0; lev<residVect.size(); lev++) {
    delete residVect[lev];
    delete corrVect[lev];
    delete residCrseVect[lev];
  }
  

}
