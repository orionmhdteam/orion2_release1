#ifdef CH_LANG_CC
/*
*      _______              __
*     / ___/ /  ___  __ _  / /  ___
*    / /__/ _ \/ _ \/  V \/ _ \/ _ \
*    \___/_//_/\___/_/_/_/_.__/\___/
*    Please refer to Copyright.txt, in Chombo's root directory.
*/
#endif

// PSLi, Tue, May 12, 2009

#ifndef _MHD_H_
#define _MHD_H_

#include "LevelData.H"
#include "FArrayBox.H"
#include "FluxBox.H"

#include "UsingNamespace.H"

class Mhd
{

public:

/// actually does the small elliptic solve for the mini-projection
static void smallEllipticSolve(FArrayBox& phiFab, const FArrayBox& rhsFab,
                const Real m_dx);

};

#endif
